-- insert default values to revisions if table is empty
INSERT INTO revisions SELECT * FROM (SELECT 0) AS dummy WHERE NOT EXISTS (SELECT * FROM revisions);

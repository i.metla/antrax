/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.storage.dao.impl;

import static com.flamesgroup.storage.jooq.Tables.GSM_CHANNEL;
import static com.flamesgroup.storage.jooq.Tables.LINK_WITH_SIM_GROUP;
import static com.flamesgroup.storage.jooq.Tables.ROUTE_CONFIG;
import static com.flamesgroup.storage.jooq.Tables.SCRIPTS_COMMONS;
import static com.flamesgroup.storage.jooq.Tables.SCRIPT_DEFINITION;
import static com.flamesgroup.storage.jooq.Tables.SCRIPT_FILE;
import static com.flamesgroup.storage.jooq.Tables.SCRIPT_INSTANCE;
import static com.flamesgroup.storage.jooq.Tables.SCRIPT_PARAM;
import static com.flamesgroup.storage.jooq.Tables.SCRIPT_PARAM_DEF;
import static com.flamesgroup.storage.jooq.Tables.SIM_GROUP;
import static com.flamesgroup.storage.jooq.tables.GsmGroup.GSM_GROUP;
import static com.flamesgroup.storage.jooq.tables.Server.SERVER;

import com.flamesgroup.antrax.automation.meta.ScriptDefinition;
import com.flamesgroup.antrax.automation.meta.ScriptField;
import com.flamesgroup.antrax.automation.meta.ScriptInstance;
import com.flamesgroup.antrax.automation.meta.ScriptInstanceImpl;
import com.flamesgroup.antrax.automation.meta.ScriptParameter;
import com.flamesgroup.antrax.storage.commons.GSMChannel;
import com.flamesgroup.antrax.storage.commons.GSMGroup;
import com.flamesgroup.antrax.storage.commons.IServerData;
import com.flamesgroup.antrax.storage.commons.LinkWithSIMGroup;
import com.flamesgroup.antrax.storage.commons.SIMGroup;
import com.flamesgroup.antrax.storage.commons.impl.GSMGroupImpl;
import com.flamesgroup.antrax.storage.commons.impl.LinkWithSIMGroupImpl;
import com.flamesgroup.antrax.storage.commons.impl.RouteConfig;
import com.flamesgroup.antrax.storage.commons.impl.ScriptCommons;
import com.flamesgroup.antrax.storage.commons.impl.SimData;
import com.flamesgroup.antrax.storage.commons.impl.SimNoGroup;
import com.flamesgroup.antrax.storage.connection.IDbConnection;
import com.flamesgroup.antrax.storage.connection.IDbConnectionPool;
import com.flamesgroup.antrax.storage.dao.IConfigViewDAO;
import com.flamesgroup.antrax.storage.exceptions.DataSelectionException;
import com.flamesgroup.commons.ChannelUID;
import com.flamesgroup.storage.jooq.Tables;
import com.flamesgroup.storage.jooq.tables.GsmChannel;
import com.flamesgroup.storage.jooq.tables.records.GsmChannelRecord;
import com.flamesgroup.storage.jooq.tables.records.GsmGroupRecord;
import com.flamesgroup.storage.jooq.tables.records.LinkWithSimGroupRecord;
import com.flamesgroup.storage.jooq.tables.records.RouteConfigRecord;
import com.flamesgroup.storage.jooq.tables.records.ScriptDefinitionRecord;
import com.flamesgroup.storage.jooq.tables.records.ScriptFileRecord;
import com.flamesgroup.storage.jooq.tables.records.ScriptInstanceRecord;
import com.flamesgroup.storage.jooq.tables.records.ScriptParamDefRecord;
import com.flamesgroup.storage.jooq.tables.records.ScriptParamRecord;
import com.flamesgroup.storage.jooq.tables.records.ScriptsCommonsRecord;
import com.flamesgroup.storage.jooq.tables.records.ServerRecord;
import com.flamesgroup.storage.jooq.tables.records.SimGroupRecord;
import com.flamesgroup.storage.jooq.tables.records.SimRecord;
import com.flamesgroup.storage.map.GsmChannelMapper;
import com.flamesgroup.storage.map.GsmGroupMapper;
import com.flamesgroup.storage.map.RouteConfigMapper;
import com.flamesgroup.storage.map.ScriptDefinitionMapper;
import com.flamesgroup.storage.map.ServerDataMapper;
import com.flamesgroup.storage.map.SimGroupMapper;
import com.flamesgroup.storage.map.SimMapper;
import com.flamesgroup.unit.ICCID;
import com.flamesgroup.utils.codebase.ScriptFile;
import com.flamesgroup.utils.codebase.ScriptFileInfo;
import org.jooq.DSLContext;
import org.jooq.JoinType;
import org.jooq.SelectQuery;
import org.jooq.exception.DataAccessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ConfigViewDAO implements IConfigViewDAO {

  private final Logger logger = LoggerFactory.getLogger(ConfigViewDAO.class);

  private final IDbConnectionPool dbConnectionPool;

  public ConfigViewDAO(final IDbConnectionPool dbConnectionPool) {
    this.dbConnectionPool = dbConnectionPool;
  }

  @Override
  public List<IServerData> listServers() {
    logger.trace("[{}] - trying to select all servers", this);
    IDbConnection connection = dbConnectionPool.getConnection();

    try {
      DSLContext context = connection.getDSLContext();
      return context.transactionResult(t -> {
        SelectQuery<ServerRecord> selectQuery = context.selectQuery(SERVER);
        selectQuery.addOrderBy(SERVER.ID);

        List<IServerData> listServer = selectQuery.fetch().into(ServerRecord.class).stream().map(ServerDataMapper::mapServerRecordToServerData).collect(Collectors.toList());
        if (listServer.size() > 0) {
          logger.trace("[{}] - servers selected: [{}]", this, listServer);
        } else {
          logger.trace("[{}] - servers aren't found", this);
        }
        return listServer;
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - while selecting all servers", this, e);
    } finally {
      connection.close();
    }
    return new ArrayList<>();
  }

  @Override
  public IServerData getServerByName(final String name) {
    logger.trace("[{}] - trying to select server by name: [{}]", this, name);
    if (name == null || name.isEmpty()) {
      logger.warn("[{}] - can't select server by name: [{}]", this, name);
      return null;
    }
    IDbConnection connection = dbConnectionPool.getConnection();

    try {
      DSLContext context = connection.getDSLContext();
      return context.transactionResult(t -> {
        final SelectQuery<ServerRecord> selectQuery = context.selectQuery(SERVER);
        selectQuery.addConditions(SERVER.NAME.eq(name));
        return ServerDataMapper.mapServerRecordToServerData(selectQuery.fetchAny());
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - while selecting server: [{}]", this, name, e);
      return null;
    } finally {
      connection.close();
    }
  }

  @Override
  public LinkWithSIMGroup[] listSIMGSMLinks(final GSMGroup gsmGroup) throws DataSelectionException {
    logger.trace("[{}] - trying to select linkWithSIMGroups by gsmGroup: [{}]", this, gsmGroup);
    if (!(gsmGroup instanceof GSMGroupImpl) || gsmGroup.getID() < 1) {
      logger.warn("[{}] - can't select linkWithSIMGroups (GSM group is not valid: [{}])", this, gsmGroup);
      return new LinkWithSIMGroup[0];
    }

    IDbConnection connection = dbConnectionPool.getConnection();

    try {
      DSLContext context = connection.getDSLContext();
      return context.transactionResult(t -> {
        List<LinkWithSIMGroup> linkWithSIMGroups = new LinkedList<>();
        SelectQuery<LinkWithSimGroupRecord> selectQuery = context.selectQuery(LINK_WITH_SIM_GROUP);
        selectQuery.addConditions(LINK_WITH_SIM_GROUP.GSM_GROUP_ID.eq(gsmGroup.getID()));
        selectQuery.addOrderBy(LINK_WITH_SIM_GROUP.ID);

        List<LinkWithSimGroupRecord> linkWithSimGroupRecords = selectQuery.fetch().into(LinkWithSimGroupRecord.class);
        for (final LinkWithSimGroupRecord linkWithSimGroupRecord : linkWithSimGroupRecords) {
          SIMGroup simGroup = getSIMGroupByID(context, null, linkWithSimGroupRecord.getSimGroupId());
          if (simGroup == null) {
            logger.warn("[{}] - not valid linkWithSIMGroups found (simGroup is null)", this);
            continue;
          }
          linkWithSIMGroups.add(new LinkWithSIMGroupImpl(linkWithSimGroupRecord.getId(), simGroup, gsmGroup));
        }
        if (linkWithSIMGroups.size() > 0) {
          logger.trace("[{}] - linkWithSIMGroups found: [{}] by gsmGroup: [{}]", this, linkWithSIMGroups, gsmGroup);
        } else {
          logger.trace("[{}] - linkWithSIMGroups aren't found by gsmGroup: [{}]", this, gsmGroup);
        }
        return linkWithSIMGroups.toArray(new LinkWithSIMGroup[linkWithSIMGroups.size()]);
      });
    } catch (DataAccessException e) {
      throw new DataSelectionException(String.format("[%s] - while selecting linkWithSIMGroups by gsmGroup: [%s]", this, gsmGroup), e);
    } finally {
      connection.close();
    }
  }

  @Override
  public LinkWithSIMGroup[] listSIMGSMLinks() throws DataSelectionException {
    logger.trace("[{}] - trying to select all linkWithSIMGroups", this);
    List<LinkWithSIMGroup> linkWithSIMGroups = new LinkedList<>();
    Map<Long, GSMGroup> gsmGroupMap = new HashMap<>();
    Map<Long, SIMGroup> simGroupMap = new HashMap<>();
    IDbConnection connection = dbConnectionPool.getConnection();

    try {
      DSLContext context = connection.getDSLContext();
      SelectQuery<LinkWithSimGroupRecord> selectQuery = context.selectQuery(LINK_WITH_SIM_GROUP);
      selectQuery.addOrderBy(LINK_WITH_SIM_GROUP.ID);

      List<LinkWithSimGroupRecord> linkWithSimGroupRecords = selectQuery.fetch().into(LinkWithSimGroupRecord.class);
      for (final LinkWithSimGroupRecord linkWithSimGroupRecord : linkWithSimGroupRecords) {
        GSMGroup gsmGroup = getGSMGroupByID(context, gsmGroupMap, linkWithSimGroupRecord.getGsmGroupId());
        if (gsmGroup == null) {
          logger.warn("[{}] - not valid linkWithSIMGroups found (gsmGroup is null)", this);
          continue;
        }
        SIMGroup simGroup = getSIMGroupByID(context, simGroupMap, linkWithSimGroupRecord.getSimGroupId());
        if (simGroup == null) {
          logger.warn("[{}] - not valid linkWithSIMGroups found (simGroup is null)", this);
          continue;
        }
        linkWithSIMGroups.add(new LinkWithSIMGroupImpl(linkWithSimGroupRecord.getId(), simGroup, gsmGroup));
      }
    } catch (DataAccessException e) {
      throw new DataSelectionException(String.format("[%s] - while selecting all linkWithSIMGroups", this), e);
    } finally {
      connection.close();
    }
    if (linkWithSIMGroups.size() > 0) {
      logger.trace("[{}] - linkWithSIMGroups found: [{}]", this, linkWithSIMGroups);
    } else {
      logger.trace("[{}] - linkWithSIMGroups aren't found", this);
    }
    return linkWithSIMGroups.toArray(new LinkWithSIMGroup[linkWithSIMGroups.size()]);
  }

  @Override
  public SimData getSimData(final ICCID iccid) throws DataSelectionException {
    logger.trace("[{}] - trying to select simData by ICCID [{}]", this, iccid);
    if (iccid == null) {
      throw new DataSelectionException(String.format("[%s] - can't find simData by ICCID (ICCID is null)", this));
    }
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      return context.transactionResult(configuration -> {
        SelectQuery<SimRecord> selectQuery = context.selectQuery(Tables.SIM);
        selectQuery.addConditions(Tables.SIM.ICCID.eq(iccid));

        SimRecord simRecord = selectQuery.fetchOneInto(Tables.SIM);
        if (simRecord == null) {
          throw new DataSelectionException(String.format("[%s] - simData is not found by ICCID [%s]", this, iccid));
        }
        SIMGroup simGroup = simRecord.getSimGroupId() == null ? null : getSIMGroupByID(context, null, simRecord.getSimGroupId());

        SimData simData = SimMapper.mapSimRecordToSimData(simRecord, simGroup);
        logger.trace("[{}] - simData selected: [{}]", this, simData);
        return simData;
      });
    } catch (DataAccessException e) {
      throw new DataSelectionException(String.format("[%s] - while selecting simData by ICCID: [%s]", this, iccid), e);
    } finally {
      connection.close();
    }
  }

  @Override
  public SIMGroup[] listSIMGroups() throws DataSelectionException {
    logger.trace("[{}] - trying to select all SIM groups", this);
    IDbConnection connection = dbConnectionPool.getConnection();

    try {
      DSLContext context = connection.getDSLContext();
      return context.transactionResult(t -> {
        List<SIMGroup> simGroups = new ArrayList<>();
        SelectQuery<SimGroupRecord> selectQuery = context.selectQuery(SIM_GROUP);
        selectQuery.addOrderBy(SIM_GROUP.NAME);

        List<SimGroupRecord> simGroupRecords = selectQuery.fetch().into(SimGroupRecord.class);

        for (final SimGroupRecord simGroupRecord : simGroupRecords) {
          simGroups.add(SimGroupMapper.mapSimGroupRecordToSimGroup(simGroupRecord, listScriptCommons(context, simGroupRecord.getId()), listScriptInstances(context, simGroupRecord.getId()
          )));
        }
        if (simGroups.size() > 0) {
          logger.trace("[{}] - SIM groups selected: [{}]", this, simGroups);
        } else {
          logger.trace("[{}] - SIM groups aren't found", this);
        }
        return simGroups.toArray(new SIMGroup[simGroups.size()]);
      });
    } catch (DataAccessException e) {
      throw new DataSelectionException(String.format("[%s] - while selecting all SIM groups", this), e);
    } finally {
      connection.close();
    }
  }

  @Override
  public SIMGroup getSimGroup(final long simGroupId) throws DataSelectionException {
    logger.trace("[{}] - trying to select sim group by id [{}]", this, simGroupId);
    if (simGroupId <= 0) {
      logger.trace("[{}] return no group by id: [{}]", this, simGroupId);
      return new SimNoGroup();
    }

    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      SelectQuery<SimGroupRecord> selectQuery = context.selectQuery(SIM_GROUP);
      selectQuery.addConditions(SIM_GROUP.ID.eq(simGroupId));

      SIMGroup simGroup;
      try {
        simGroup = SimGroupMapper.mapSimGroupRecordToSimGroup(selectQuery.fetchOneInto(SIM_GROUP), listScriptCommons(context, simGroupId), listScriptInstances(context, simGroupId));
        logger.trace("[{}] - simGroup: [{}] found by id: [{}]", this, simGroup, simGroupId);
        return simGroup;
      } catch (SQLException | DataAccessException e) {
        throw new DataSelectionException(String.format("[%s] - while selecting simGroup by id: [%d]", this, simGroupId), e);
      }
    } finally {
      connection.close();
    }
  }

  @Override
  public GSMGroup[] listGSMGroups() {
    logger.trace("[{}] - trying to select all gsmGroups", this);
    IDbConnection connection = dbConnectionPool.getConnection();

    try {
      DSLContext context = connection.getDSLContext();
      return context.transactionResult(t -> {
        SelectQuery<GsmGroupRecord> selectQuery = context.selectQuery(GSM_GROUP);
        selectQuery.addOrderBy(GSM_GROUP.NAME);

        List<GSMGroup> gsmGroups = selectQuery.fetch().into(GsmGroupRecord.class).stream().map(GsmGroupMapper::mapGsmGroupRecordToGsmGroup).collect(Collectors.toList());
        if (gsmGroups.size() > 0) {
          logger.trace("[{}] - gsmGroups selected: [{}]", this, gsmGroups);
        } else {
          logger.trace("[{}] - gsmGroups aren't found", this);
        }
        return gsmGroups.toArray(new GSMGroup[gsmGroups.size()]);
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - while selecting all gsmGroups", this, e);
      return new GSMGroup[0];
    } finally {
      connection.close();
    }
  }

  @Override
  public GSMChannel getGSMChannel(final ChannelUID channelUID) {
    logger.trace("[{}] - trying to select GSM channel by channelUID: [{}] ", this, channelUID);
    IDbConnection connection = dbConnectionPool.getConnection();

    try {
      DSLContext context = connection.getDSLContext();
      return context.transactionResult(t -> {
        SelectQuery<GsmChannelRecord> selectQuery = context.selectQuery(GsmChannel.GSM_CHANNEL);
        selectQuery.addJoin(GSM_GROUP, JoinType.LEFT_OUTER_JOIN, GsmChannel.GSM_CHANNEL.GSM_GROUP_ID.eq(GSM_GROUP.ID));
        selectQuery.addConditions(GSM_CHANNEL.DEVICE_UID.eq(channelUID.getDeviceUID().getUID()));
        selectQuery.addConditions(GSM_CHANNEL.CHANNEL_NUM.eq((short) channelUID.getChannelNumber()));

        GSMChannel gsmChannel = selectQuery.fetchGroups(GsmChannel.GSM_CHANNEL.fields(), GsmGroupRecord.class).entrySet().stream()
            .map(entry -> GsmChannelMapper.mapGsmChannelRecordToGsmChannel(entry.getKey().into(GsmChannelRecord.class), entry.getValue().get(0)))
            .findAny().orElse(null);
        if (gsmChannel == null) {
          logger.trace("[{}] - can't find GSM channel by channelUID: [{}]", this, channelUID);
        } else {
          logger.trace("[{}] - GSM channel: [{}] found by channelUID: [{}]", this, gsmChannel, channelUID);
        }
        return gsmChannel;
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - while selecting GSM channel by channelUID: [{}]", this, channelUID, e);
      return null;
    } finally {
      connection.close();
    }
  }

  @Override
  public boolean isGSMChannelStored(final ChannelUID gsmChannel) {
    logger.trace("[{}] - trying to check: is gsmChannel: [{}] stored in db", this, gsmChannel);
    if (gsmChannel == null) {
      logger.warn("[{}] - can't check : is gsmChannel stored in db, gsmChannel is null", this);
      return false;
    }
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      return context.transactionResult(t -> {
        SelectQuery<GsmChannelRecord> selectQuery = context.selectQuery(GSM_CHANNEL);
        selectQuery.addConditions(GSM_CHANNEL.CHANNEL_NUM.eq((short) gsmChannel.getChannelNumber()));
        selectQuery.addConditions(GSM_CHANNEL.DEVICE_UID.eq(gsmChannel.getDeviceUID().getUID()));
        return selectQuery.fetch().size() > 0;
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - while check: is gsmChannel: [{}] stored in db", this, gsmChannel, e);
      return false;
    } finally {
      connection.close();
    }
  }

  @Override
  public String[] listSimGroupNames() {
    logger.trace("[{}] - trying to select all simGroup names", this);
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      return context.transactionResult(t -> {
        SelectQuery<SimGroupRecord> selectQuery = context.selectQuery(SIM_GROUP);
        selectQuery.addSelect(SIM_GROUP.NAME);
        selectQuery.addOrderBy(SIM_GROUP.NAME);
        List<String> simGroupNames = selectQuery.fetch().into(String.class);
        if (simGroupNames.size() > 0) {
          logger.trace("[{}] - [{}] SIM group names selected", this, simGroupNames.size());
        } else {
          logger.trace("[{}] - SIM group names aren't found", this);
        }
        return simGroupNames.toArray(new String[simGroupNames.size()]);
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - while selecting all simGroup names", this, e);
      return new String[0];
    } finally {
      connection.close();
    }
  }

  @Override
  public String[] listGsmGroupNames() {
    logger.trace("[{}] - trying to select all gsmGroup names", this);
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      return context.transactionResult(t -> {
        SelectQuery<GsmGroupRecord> selectQuery = context.selectQuery(GSM_GROUP);
        selectQuery.addSelect(GSM_GROUP.NAME);
        selectQuery.addOrderBy(GSM_GROUP.NAME);
        List<String> gsmGroupNames = selectQuery.fetch().into(String.class);
        if (gsmGroupNames.size() > 0) {
          logger.trace("[{}] - [{}] GSM group names selected", this, gsmGroupNames.size());
        } else {
          logger.trace("[{}] - GSM group names aren't found", this);
        }
        return gsmGroupNames.toArray(new String[gsmGroupNames.size()]);
      });

    } catch (DataAccessException e) {
      logger.warn("[{}] - while selecting all gsmGroup names", this, e);
      return new String[0];
    } finally {
      connection.close();
    }
  }

  @Override
  public ScriptDefinition[] listScriptDefinitions() {
    logger.trace("[{}] - trying to select all script definitions", this);
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      return context.transactionResult(t -> {
        SelectQuery<ScriptDefinitionRecord> selectQuery = context.selectQuery(SCRIPT_DEFINITION);
        selectQuery.addOrderBy(SCRIPT_DEFINITION.ID);

        selectQuery.addJoin(SCRIPT_PARAM_DEF, JoinType.LEFT_OUTER_JOIN,
            SCRIPT_PARAM_DEF.SCRIPT_DEF_ID.eq(SCRIPT_DEFINITION.as(SCRIPT_DEFINITION.getName()).ID));

        List<ScriptDefinition> scriptDefinitions = selectQuery.fetchGroups(SCRIPT_DEFINITION.fields(), ScriptParamDefRecord.class).entrySet().stream()
            .map(entry -> ScriptDefinitionMapper
                .mapScriptDefinitionRecordToScriptDefinition(entry.getKey().into(ScriptDefinitionRecord.class),
                    entry.getValue())).collect(Collectors.toList());
        if (scriptDefinitions.size() > 0) {
          logger.trace("[{}] - [{}] - script definitions found", this, scriptDefinitions.size());
        } else {
          logger.trace("[{}] - script definitions aren't found", this);
        }
        return scriptDefinitions.toArray(new ScriptDefinition[scriptDefinitions.size()]);
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - while selecting script definitions", this, e);
      return new ScriptDefinition[0];
    } finally {
      connection.close();
    }
  }

  @Override
  public ScriptFile getLastScriptFile() {
    logger.trace("[{}] - trying to select last script file", this);
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      return context.transactionResult(t -> {
        SelectQuery<ScriptFileRecord> selectQuery = context.selectQuery(SCRIPT_FILE);

        selectQuery.addOrderBy(SCRIPT_FILE.ID.desc());
        selectQuery.addLimit(1);

        ScriptFileRecord scriptFileRecord = selectQuery.fetchOneInto(ScriptFileRecord.class);

        ScriptFile scriptFile = null;
        if (scriptFileRecord != null) {
          ScriptFileInfo scriptFileInfo = new ScriptFileInfo(scriptFileRecord.getVersion(), scriptFileRecord.getCheckSum(),
              scriptFileRecord.getCompilationTime(), scriptFileRecord.getUploadTime(),
              scriptFileRecord.getFileCount(), scriptFileRecord.getClassCount());

          scriptFile = new ScriptFile(scriptFileRecord.getName(), scriptFileRecord.getData(), scriptFileInfo);
        }
        return scriptFile;
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - while selecting script definitions", this, e);
      return null;
    } finally {
      connection.close();
    }
  }

  public static class ScriptInstanceData {

    public ScriptInstance scriptInstance;
    public ScriptField scriptField;

    @Override
    public String toString() {
      return String.format("%s %s", scriptField, scriptInstance);
    }

  }

  private ScriptDefinition getScriptDefinition(final DSLContext context, final long scriptDefinitionId) {
    logger.trace("[{}] - trying to select scriptDefinitions by id", this);
    ScriptDefinition scriptDefinition;
    try {
      SelectQuery<ScriptDefinitionRecord> selectQuery = context.selectQuery(SCRIPT_DEFINITION);
      selectQuery.addOrderBy(SCRIPT_DEFINITION.ID);
      selectQuery.addConditions(SCRIPT_DEFINITION.ID.eq(scriptDefinitionId));

      selectQuery.addJoin(SCRIPT_PARAM_DEF, JoinType.LEFT_OUTER_JOIN, SCRIPT_PARAM_DEF.SCRIPT_DEF_ID.eq(SCRIPT_DEFINITION.as(SCRIPT_DEFINITION.getName()).ID));

      scriptDefinition = selectQuery.fetchGroups(SCRIPT_DEFINITION.fields(), ScriptParamDefRecord.class).entrySet().stream()
          .map(entry -> ScriptDefinitionMapper.mapScriptDefinitionRecordToScriptDefinition(entry.getKey().into(ScriptDefinitionRecord.class), entry.getValue()))
          .findFirst().orElse(null);
    } catch (DataAccessException e) {
      logger.warn("[{}] - while selecting scriptDefinition", this, e);
      return null;
    }
    if (scriptDefinition != null) {
      logger.debug("[{}] - [{}] scriptDefinition found", this, scriptDefinition);
    } else {
      logger.warn("[{}] - scriptDefinitions aren't found", this);
    }
    return scriptDefinition;
  }

  private ScriptParameter[] listScriptParameters(final DSLContext context, final ScriptInstanceImpl scriptInstance) {
    try {
      SelectQuery<ScriptParamRecord> selectQuery = context.selectQuery(SCRIPT_PARAM);
      selectQuery.addOrderBy(SCRIPT_PARAM.SCRIPT_PARAM_DEF_ID);
      selectQuery.addOrderBy(SCRIPT_PARAM.ID);
      selectQuery.addConditions(SCRIPT_PARAM.SCRIPT_INSTANCE_ID.eq(scriptInstance.getID()));

      selectQuery.addJoin(SCRIPT_PARAM_DEF, JoinType.LEFT_OUTER_JOIN, SCRIPT_PARAM.SCRIPT_PARAM_DEF_ID.eq(SCRIPT_PARAM_DEF.ID));

      List<ScriptParameter> scriptParameters = selectQuery.fetchGroups(SCRIPT_PARAM.fields(), ScriptParamDefRecord.class).entrySet().stream()
          .map(entry -> ScriptDefinitionMapper.mapScriptParameterRecordAndScriptParameterDefinitionRecordToScriptParameter(
              entry.getKey().into(ScriptParamRecord.class), entry.getValue(), scriptInstance)).collect(Collectors.toList());
      if (scriptParameters.size() > 0) {
        logger.trace("[{}] - [{}] script parameters found by scriptInstanceId: [{}]", this, scriptParameters.size(), scriptInstance.getID());
      } else {
        logger.trace("[{}] - script parameters aren't found by scriptInstanceId: [{}]", this, scriptInstance.getID());
      }

      return scriptParameters.toArray(new ScriptParameter[scriptParameters.size()]);
    } catch (DataAccessException e) {
      logger.warn("[{}] - while selecting script parameters", this, e);
      return new ScriptParameter[0];
    }
  }

  private ScriptInstanceData[] listScriptInstances(final DSLContext context, final Long simGroupId) throws DataSelectionException {
    logger.trace("[{}] - trying to select script instances by simGroupId: [{}]", this, simGroupId);
    List<ScriptInstanceData> scriptInstanceDatas = new ArrayList<>();
    try {
      SelectQuery<ScriptInstanceRecord> selectQuery = context.selectQuery(SCRIPT_INSTANCE);
      selectQuery.addConditions(SCRIPT_INSTANCE.SIM_GROUP_ID.eq(simGroupId));
      selectQuery.addOrderBy(SCRIPT_INSTANCE.ID);
      selectQuery.addOrderBy(SCRIPT_INSTANCE.FIELD_INDEX);

      List<ScriptInstanceRecord> scriptInstanceRecords = selectQuery.fetch().into(ScriptInstanceRecord.class);
      for (final ScriptInstanceRecord scriptInstanceRecord : scriptInstanceRecords) {
        ScriptDefinition scriptDef = getScriptDefinition(context, scriptInstanceRecord.getScriptDefId());
        if (scriptDef == null) {
          continue;
        }
        ScriptInstanceImpl scriptInstance = new ScriptInstanceImpl(scriptDef);
        scriptInstance.setID(scriptInstanceRecord.getId());
        scriptInstance.setParameters(listScriptParameters(context, scriptInstance));
        ScriptInstanceData data = new ScriptInstanceData();
        data.scriptInstance = scriptInstance;
        data.scriptField = scriptInstanceRecord.getField();
        logger.debug("[{}] - selected {}", this, data);
        scriptInstanceDatas.add(data);
      }
    } catch (DataAccessException e) {
      throw new DataSelectionException(String.format("[%s] - while selecting script instances by simGroupId: [%d]", this, simGroupId), e);
    }
    if (scriptInstanceDatas.size() > 0) {
      logger.trace("[{}] - [{}] script instances found by simGroupId: [{}]", this, scriptInstanceDatas.size(), simGroupId);
    } else {
      logger.trace("[{}] - script instances aren't found by simGroupId: [{}]", this, simGroupId);
    }
    return scriptInstanceDatas.toArray(new ScriptInstanceData[scriptInstanceDatas.size()]);
  }

  private ScriptCommons[] listScriptCommons(final DSLContext context, final long simGroupId) {
    logger.trace("[{}] - trying to select list scriptCommons by simGroupId: [{}]", this, simGroupId);
    List<ScriptCommons> scriptCommonses;
    try {
      SelectQuery<ScriptsCommonsRecord> scriptsCommonsSelectQuery = context.selectQuery(SCRIPTS_COMMONS);
      scriptsCommonsSelectQuery.addConditions(SCRIPTS_COMMONS.SIM_GROUP_ID.eq(simGroupId));

      scriptCommonses = scriptsCommonsSelectQuery.fetch().into(ScriptsCommonsRecord.class).stream()
          .map(scriptsCommonsRecord -> {
            ScriptCommons scriptCommons = new ScriptCommons();
            scriptCommons.setName(scriptsCommonsRecord.getName());
            scriptCommons.setSerializedValue(scriptsCommonsRecord.getValue());
            scriptCommons.setID(scriptsCommonsRecord.getId());
            return scriptCommons;
          })
          .collect(Collectors.toList());
    } catch (DataAccessException e) {
      logger.warn("[{}] - while selecting list scriptCommons by simGroupId: [{}]", this, simGroupId, e);
      return new ScriptCommons[0];
    }
    if (scriptCommonses.size() > 0) {
      logger.trace("[{}] - [{}] scriptCommons found by simGroupId: [{}]", this, scriptCommonses.size(), simGroupId);
    } else {
      logger.trace("[{}] - scriptCommons aren't found by simGroupId: [{}]", this, simGroupId);
    }
    return scriptCommonses.toArray(new ScriptCommons[scriptCommonses.size()]);
  }

  private SIMGroup getSIMGroupByID(final DSLContext context, final Map<Long, SIMGroup> groupMap, final long id) throws DataSelectionException {
    logger.trace("[{}] - trying to select simGroup by id", this);
    if (id < 1) {
      throw new DataSelectionException(String.format("[%s] - can't find simGroup by id: [%d])", this, id));
    }
    SIMGroup simGroup = null;
    if (groupMap != null) {
      simGroup = groupMap.get(id);
    }
    if (simGroup != null) {
      logger.trace("[{}] - simGroup: [{}] found by id: [{}] from cache", this, simGroup, id);
      return simGroup;
    }
    try {
      SelectQuery<SimGroupRecord> selectQuery = context.selectQuery(SIM_GROUP);
      selectQuery.addConditions(SIM_GROUP.ID.eq(id));

      simGroup = SimGroupMapper.mapSimGroupRecordToSimGroup(selectQuery.fetchOneInto(SIM_GROUP), listScriptCommons(context, id), listScriptInstances(context, id));
      if (groupMap != null) {
        groupMap.put(id, simGroup);
      }
      logger.trace("[{}] - simGroup: [{}] found by id: [{}]", this, simGroup, id);
      return simGroup;
    } catch (SQLException | DataAccessException e) {
      throw new DataSelectionException(String.format("[%s] - while selecting simGroup by id: [%d]", this, id), e);
    }
  }

  private GSMGroup getGSMGroupByID(final DSLContext context, final Map<Long, GSMGroup> groupMap, final long id) {
    logger.trace("[{}] - trying to select gsmGroup by id", this);
    if (id < 1) {
      logger.trace("[{}] - can't find gsmGroup by id: [{}])", this, id);
      return null;
    }
    GSMGroup gsmGroup = null;
    if (groupMap != null) {
      gsmGroup = groupMap.get(id);
    }
    if (gsmGroup != null) {
      logger.trace("[{}] - gsmGroup: [{}] found by id: [{}] from cache", this, gsmGroup, id);
      return gsmGroup;
    }
    try {
      SelectQuery<GsmGroupRecord> selectQuery = context.selectQuery(GSM_GROUP);
      selectQuery.addConditions(GSM_GROUP.ID.eq(id));

      gsmGroup = GsmGroupMapper.mapGsmGroupRecordToGsmGroup(selectQuery.fetchOneInto(GSM_GROUP));
      if (groupMap != null) {
        groupMap.put(id, gsmGroup);
      }
      logger.trace("[{}] - gsmGroup: [{}] found by id: [{}]", this, gsmGroup, id);
    } catch (DataAccessException e) {
      logger.warn("[{}] - while selecting gsmGroup by id: [{}]", this, id, e);
    }
    return gsmGroup;
  }

  @Override
  public RouteConfig getRouteConfig() {
    logger.trace("[{}] - trying to select route config", this);
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      return context.transactionResult(t -> {
        SelectQuery<RouteConfigRecord> routeConfigQuery = context.selectQuery(ROUTE_CONFIG);
        return RouteConfigMapper.mapRouteConfigRecordToRouteConfig(routeConfigQuery.fetchAny());
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - while selecting route config", this, e);
      return null;
    } finally {
      connection.close();
    }
  }

}

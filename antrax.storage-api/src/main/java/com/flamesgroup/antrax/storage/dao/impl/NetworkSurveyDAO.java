/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.storage.dao.impl;

import static com.flamesgroup.storage.jooq.Tables.NETWORK_SURVEY;

import com.flamesgroup.antrax.storage.commons.impl.NetworkSurvey;
import com.flamesgroup.antrax.storage.connection.IDbConnection;
import com.flamesgroup.antrax.storage.connection.IDbConnectionPool;
import com.flamesgroup.antrax.storage.dao.INetworkSurveyDAO;
import com.flamesgroup.commons.ChannelUID;
import com.flamesgroup.storage.jooq.tables.records.NetworkSurveyRecord;
import com.flamesgroup.storage.map.NetworkSurveyMapper;
import org.jooq.DSLContext;
import org.jooq.InsertQuery;
import org.jooq.SelectQuery;
import org.jooq.exception.DataAccessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class NetworkSurveyDAO implements INetworkSurveyDAO {

  private static final Logger logger = LoggerFactory.getLogger(NetworkSurveyDAO.class);

  private final IDbConnectionPool dbConnectionPool;

  public NetworkSurveyDAO(final IDbConnectionPool dbConnectionPool) {
    this.dbConnectionPool = dbConnectionPool;
  }

  @Override
  public void saveNetworkSurveys(final List<NetworkSurvey> networkSurveys) {
    logger.debug("[{}] - trying to save networkSurveys [{}]", this, networkSurveys.hashCode());
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      context.transaction(configuration -> {
        InsertQuery<NetworkSurveyRecord> insertQuery = context.insertQuery(NETWORK_SURVEY);
        for (NetworkSurvey networkSurvey : networkSurveys) {
          NetworkSurveyRecord networkSurveyRecord = context.newRecord(NETWORK_SURVEY);
          NetworkSurveyMapper.mapNetworkSurveyToNetworkSurveyRecord(networkSurvey, networkSurveyRecord);
          insertQuery.addRecord(networkSurveyRecord);
        }

        if (insertQuery.execute() > 0) {
          logger.debug("[{}] - saved networkSurveys [{}]", this, networkSurveys.hashCode());
        } else {
          logger.debug("[{}] - failed save networkSurveys [{}]", this, networkSurveys.hashCode());
        }
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - while saving networkSurveys [{}]", this, networkSurveys.hashCode(), e);
    } finally {
      connection.close();
    }
  }

  @Override
  public List<NetworkSurvey> getNetworkSurveyForChannelUID(final ChannelUID channelUID, final Date addTime) {
    logger.debug("[{}] - trying to select networkSurvey for channelUID [{}], addTime [{}]", this, channelUID, addTime);
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      return context.transactionResult(configuration -> {
        SelectQuery<NetworkSurveyRecord> selectQuery = context.selectQuery(NETWORK_SURVEY);
        selectQuery.addConditions(NETWORK_SURVEY.GSM_CHANNEL.eq(channelUID));
        selectQuery.addConditions(NETWORK_SURVEY.ADD_TIME.eq(addTime));

        return selectQuery.fetchInto(NetworkSurveyRecord.class).stream().map(NetworkSurveyMapper::mapNetworkSurveyRecordToNetworkSurvey).collect(Collectors.toList());
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - while selecting list of networkSurvey for channelUID [{}], addTime [{}]", this, channelUID, addTime, e);
      return new ArrayList<>();
    } finally {
      connection.close();
    }
  }

  @Override
  public List<NetworkSurvey> getNetworkSurveyForChannelUID(final ChannelUID channelUID, final Date startTime, final Date finishTime) {
    logger.debug("[{}] - trying to select networkSurvey for channelUID [{}], startTime [{}], finishTime [{}]", this, channelUID, startTime, finishTime);
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      return context.transactionResult(configuration -> {
        SelectQuery<NetworkSurveyRecord> selectQuery = context.selectQuery(NETWORK_SURVEY);
        selectQuery.addConditions(NETWORK_SURVEY.GSM_CHANNEL.eq(channelUID));
        selectQuery.addConditions(NETWORK_SURVEY.ADD_TIME.greaterOrEqual(startTime));
        selectQuery.addConditions(NETWORK_SURVEY.ADD_TIME.lessOrEqual(finishTime));
        selectQuery.addOrderBy(NETWORK_SURVEY.ADD_TIME);

        return selectQuery.fetchInto(NetworkSurveyRecord.class).stream().map(NetworkSurveyMapper::mapNetworkSurveyRecordToNetworkSurvey).collect(Collectors.toList());
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - while selecting list of networkSurvey for channelUID [{}], startTime [{}], finishTime [{}]", this, channelUID, startTime, finishTime, e);
      return new ArrayList<>();
    } finally {
      connection.close();
    }
  }

  @Override
  public List<NetworkSurvey> getBCCHNetworkSurveyByServer(final Long serverId, final Date startTime, final Date finishTime) {
    logger.debug("[{}] - trying to select networkSurvey by serverId [{}], startTime [{}], finishTime [{}]", this, serverId, startTime, finishTime);
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      return context.transactionResult(configuration -> {
        SelectQuery<NetworkSurveyRecord> selectQuery = context.selectQuery(NETWORK_SURVEY);
        selectQuery.addConditions(NETWORK_SURVEY.SERVER_ID.eq(serverId));
        selectQuery.addConditions(NETWORK_SURVEY.ADD_TIME.greaterOrEqual(startTime));
        selectQuery.addConditions(NETWORK_SURVEY.ADD_TIME.lessOrEqual(finishTime));
        selectQuery.addConditions(NETWORK_SURVEY.BSIC.greaterOrEqual(0));
        selectQuery.addOrderBy(NETWORK_SURVEY.BSIC);

        return selectQuery.fetchInto(NetworkSurveyRecord.class).stream().map(NetworkSurveyMapper::mapNetworkSurveyRecordToNetworkSurvey).collect(Collectors.toList());
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - while selecting list of networkSurvey by serverId [{}], startTime [{}], finishTime [{}]", this, serverId, startTime, finishTime, e);
      return new ArrayList<>();
    } finally {
      connection.close();
    }
  }

  @Override
  public List<NetworkSurvey> getBCCHNetworkSurveyByServerSortedByTime(final Long serverId, final Date startTime, final Date finishTime) {
    logger.debug("[{}] - trying to select networkSurvey by serverId [{}], startTime [{}], finishTime [{}]", this, serverId, startTime, finishTime);
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      return context.transactionResult(configuration -> {
        SelectQuery<NetworkSurveyRecord> selectQuery = context.selectQuery(NETWORK_SURVEY);
        selectQuery.addConditions(NETWORK_SURVEY.SERVER_ID.eq(serverId));
        selectQuery.addConditions(NETWORK_SURVEY.ADD_TIME.greaterOrEqual(startTime));
        selectQuery.addConditions(NETWORK_SURVEY.ADD_TIME.lessOrEqual(finishTime));
        selectQuery.addConditions(NETWORK_SURVEY.BSIC.greaterOrEqual(0));
        selectQuery.addOrderBy(NETWORK_SURVEY.ADD_TIME);

        return selectQuery.fetchInto(NetworkSurveyRecord.class).stream().map(NetworkSurveyMapper::mapNetworkSurveyRecordToNetworkSurvey).collect(Collectors.toList());
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - while selecting list of networkSurvey by serverId [{}], startTime [{}], finishTime [{}]", this, serverId, startTime, finishTime, e);
      return new ArrayList<>();
    } finally {
      connection.close();
    }
  }

  @Override
  public Date getStartNetworkSurvey(final Long serverId) {
    logger.debug("[{}] - trying to get start survey time for serverId [{}]", this, serverId);
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      return context.transactionResult(configuration -> {
        SelectQuery<NetworkSurveyRecord> selectQuery = context.selectQuery(NETWORK_SURVEY);
        selectQuery.addSelect(NETWORK_SURVEY.ADD_TIME.min());
        selectQuery.addConditions(NETWORK_SURVEY.SERVER_ID.eq(serverId));
        selectQuery.addConditions(NETWORK_SURVEY.BSIC.greaterOrEqual(0));

        return selectQuery.fetchOneInto(Date.class);
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - while get start survey time for serverId [{}]", this, serverId, e);
      return null;
    } finally {
      connection.close();
    }
  }

  @Override
  public Date getLastNetworkSurvey(final Long serverId) {
    logger.debug("[{}] - trying to get last survey time for serverId [{}]", this, serverId);
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      return context.transactionResult(configuration -> {
        SelectQuery<NetworkSurveyRecord> selectQuery = context.selectQuery(NETWORK_SURVEY);
        selectQuery.addSelect(NETWORK_SURVEY.ADD_TIME.max());
        selectQuery.addConditions(NETWORK_SURVEY.SERVER_ID.eq(serverId));
        selectQuery.addConditions(NETWORK_SURVEY.BSIC.greaterOrEqual(0));

        return selectQuery.fetchOneInto(Date.class);
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - while get last survey time for serverId [{}]", this, serverId, e);
      return null;
    } finally {
      connection.close();
    }
  }

}

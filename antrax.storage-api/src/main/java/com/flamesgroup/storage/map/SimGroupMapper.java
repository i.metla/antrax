/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.storage.map;

import com.flamesgroup.antrax.automation.meta.ScriptInstance;
import com.flamesgroup.antrax.commons.impl.InvariableTimeout;
import com.flamesgroup.antrax.storage.commons.Revisions;
import com.flamesgroup.antrax.storage.commons.SIMGroup;
import com.flamesgroup.antrax.storage.commons.impl.SIMGroupImpl;
import com.flamesgroup.antrax.storage.commons.impl.ScriptCommons;
import com.flamesgroup.antrax.storage.dao.impl.ConfigViewDAO.ScriptInstanceData;
import com.flamesgroup.storage.jooq.tables.records.SimGroupRecord;

import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

public final class SimGroupMapper {

  private SimGroupMapper() {
  }

  public static void mapSimGroupToSimGroupRecord(final SIMGroup simGroup, final SimGroupRecord simGroupRecord) {
    if (simGroup.getID() > 0) {
      simGroupRecord.setId(simGroup.getID());
    }

    simGroupRecord.setName(simGroup.getName())
        .setIdleAfterRegistrationMediana(simGroup.getIdleAfterRegistration() == null ? 0 : simGroup.getIdleAfterRegistration().getMediana())
        .setIdleAfterRegistrationDelta(simGroup.getIdleAfterRegistration() == null ? 0 : simGroup.getIdleAfterRegistration().getDelta())
        .setIdleAfterSuccessfulCallDelta(simGroup.getIdleAfterSuccessfullCall() == null ? 0 : simGroup.getIdleAfterSuccessfullCall().getDelta())
        .setIdleAfterSuccessfulCallMediana(simGroup.getIdleAfterSuccessfullCall() == null ? 0 : simGroup.getIdleAfterSuccessfullCall().getMediana())
        .setIdleBeforeUnregistrationDelta(simGroup.getIdleBeforeUnregistration() == null ? 0 : simGroup.getIdleBeforeUnregistration().getDelta())
        .setIdleBeforeUnregistrationMediana(simGroup.getIdleBeforeUnregistration() == null ? 0 : simGroup.getIdleBeforeUnregistration().getMediana())
        .setSelfDialFactor(simGroup.getSelfDialFactor())
        .setSelfDialChance(simGroup.getSelfDialChance())
        .setIdleBeforeSelfCallDelta(simGroup.getIdleBeforeSelfCall() == null ? 0 : simGroup.getIdleBeforeSelfCall().getDelta())
        .setIdleBeforeSelfCallMediana(simGroup.getIdleBeforeSelfCall() == null ? 0 : simGroup.getIdleBeforeSelfCall().getMediana())
        .setIdleAfterSelfCallTimeoutDelta(simGroup.getIdleAfterSelfCallTimeout() == null ? 0 : simGroup.getIdleAfterSelfCallTimeout().getDelta())
        .setIdleAfterSelfCallTimeoutMediana(simGroup.getIdleAfterSelfCallTimeout() == null ? 0 : simGroup.getIdleAfterSelfCallTimeout().getMediana())
        .setDescription(simGroup.getDescription())
        .setCanBeAppointed(simGroup.canBeAppointed())
        .setSyntheticRinging(simGroup.isSyntheticRinging())
        .setFasDetection(simGroup.isFASDetection())
        .setSuppressIncomingCallSignal(simGroup.isSuppressIncomingCallSignal())
        .setSmsDeliveryReport(simGroup.isSmsDeliveryReport())
        .setOperatorSelection(simGroup.getOperatorSelection())
        .setIdleAfterZeroCallDelta(simGroup.getIdleAfterZeroCall() == null ? 0 : simGroup.getIdleAfterZeroCall().getDelta())
        .setIdleAfterZeroCallMediana(simGroup.getIdleAfterZeroCall() == null ? 0 : simGroup.getIdleAfterZeroCall().getMediana())
        .setRegistrationPeriod(simGroup.getRegistrationPeriod())
        .setRegistrationCount(simGroup.getRegistrationCount());
  }

  public static SIMGroup mapSimGroupRecordToSimGroup(final SimGroupRecord simGroupRecord, final ScriptCommons[] scriptCommonses,
      final ScriptInstanceData[] scriptInstances) throws SQLException {
    Objects.requireNonNull(simGroupRecord, "simGroupRecord mustn't be null");

    SIMGroupImpl simGroup = new SIMGroupImpl(simGroupRecord.getId());
    simGroup.setRevision(Revisions.getInstance().getSimGroupRevision(simGroup));
    simGroup.setName(simGroupRecord.getName())
        .setIdleAfterRegistration(new InvariableTimeout(simGroupRecord.getIdleAfterRegistrationMediana(), simGroupRecord.getIdleAfterRegistrationDelta()))
        .setIdleAfterSuccessfullCall(new InvariableTimeout(simGroupRecord.getIdleAfterSuccessfulCallMediana(), simGroupRecord.getIdleAfterSuccessfulCallDelta()))
        .setIdleAfterZeroCall(new InvariableTimeout(simGroupRecord.getIdleAfterZeroCallMediana(), simGroupRecord.getIdleAfterZeroCallDelta()))
        .setIdleBeforeUnregistration(new InvariableTimeout(simGroupRecord.getIdleBeforeUnregistrationMediana(), simGroupRecord.getIdleBeforeUnregistrationDelta()))
        .setSelfDialFactor(simGroupRecord.getSelfDialFactor())
        .setSelfDialChance(simGroupRecord.getSelfDialChance())
        .setIdleBeforeSelfCall(new InvariableTimeout(simGroupRecord.getIdleBeforeSelfCallMediana(), simGroupRecord.getIdleBeforeSelfCallDelta()))
        .setIdleAfterSelfCallTimeout(new InvariableTimeout(simGroupRecord.getIdleAfterSelfCallTimeoutMediana(), simGroupRecord.getIdleAfterSelfCallTimeoutDelta()))
        .setDescription(simGroupRecord.getDescription())
        .setCanBeAppointed(simGroupRecord.getCanBeAppointed())
        .setSyntheticRinging(simGroupRecord.getSyntheticRinging())
        .setFASDetection(simGroupRecord.getFasDetection())
        .setSuppressIncomingCallSignal(simGroupRecord.getSuppressIncomingCallSignal())
        .setSmsDeliveryReport(simGroupRecord.getSmsDeliveryReport())
        .setOperatorSelection(simGroupRecord.getOperatorSelection())
        .setRegistrationPeriod(simGroupRecord.getRegistrationPeriod())
        .setRegistrationCount(simGroupRecord.getRegistrationCount())
        .setScriptCommons(scriptCommonses);
    fillSimGroupScripts(simGroup, scriptInstances);

    return simGroup;
  }

  private static void fillSimGroupScripts(final SIMGroup simGroup, final ScriptInstanceData[] scriptInstances) throws SQLException {
    if (simGroup == null) {
      return;
    }
    List<ScriptInstance> businessActivityScripts = new LinkedList<>();
    List<ScriptInstance> actionProviderScripts = new LinkedList<>();
    List<ScriptInstance> callFilterScripts = new LinkedList<>();
    List<ScriptInstance> smsFilterScripts = new LinkedList<>();
    int activityPeriodScriptCnt = 0;
    int sessionCnt = 0;
    int factorScriptCnt = 0;
    int vsfactorScriptCnt = 0;
    int gwSelectorScriptCnt = 0;
    int incomingCallManagementScriptCnt = 0;
    int imeiGeneratorScriptCnt = 0;

    for (ScriptInstanceData scriptInstance : scriptInstances) {
      switch (scriptInstance.scriptField) {
        case SS_ACTIVITY:
          if (activityPeriodScriptCnt > 0) {
            throw new SQLException("trying to set more than one ACTIVITY_PERIOD scripts for simGroup:[" + simGroup + "]");
          }
          simGroup.setActivityPeriodScript(scriptInstance.scriptInstance);
          activityPeriodScriptCnt++;
          break;
        case SS_SESSION:
          if (sessionCnt > 0) {
            throw new SQLException("trying to set more than one SESSION scripts for simGroup:[" + simGroup + "]");
          }
          simGroup.setSessionScript(scriptInstance.scriptInstance);
          sessionCnt++;
          break;
        case SS_FACTOR:
          if (factorScriptCnt > 0) {
            throw new SQLException("trying to set more than one FACTOR scripts for simGroup:[" + simGroup + "]");
          }
          simGroup.setFactorScript(scriptInstance.scriptInstance);
          factorScriptCnt++;
          break;
        case VS_FACTOR:
          if (vsfactorScriptCnt > 0) {
            throw new SQLException("trying to set more than one VS_FACTOR scripts for simGroup:[" + simGroup + "]");
          }
          simGroup.setVSFactorScript(scriptInstance.scriptInstance);
          vsfactorScriptCnt++;
          break;
        case VS_CALL_FILTER:
          callFilterScripts.add(scriptInstance.scriptInstance);
          break;
        case VS_SMS_FILTER:
          smsFilterScripts.add(scriptInstance.scriptInstance);
          break;
        case VS_BUSINESS:
          businessActivityScripts.add(scriptInstance.scriptInstance);
          break;
        case VS_ACTION_PROVIDER:
          actionProviderScripts.add(scriptInstance.scriptInstance);
          break;
        case SS_GW_SELECTOR:
          if (gwSelectorScriptCnt > 0) {
            throw new SQLException("trying to set more than one GATEWAY_SELECTOR scripts for simGroup:[" + simGroup + "]");
          }
          simGroup.setGWSelectorScript(scriptInstance.scriptInstance);
          gwSelectorScriptCnt++;
          break;
        case VS_INCOMING_CALL_MANAGER:
          if (incomingCallManagementScriptCnt > 0) {
            throw new SQLException("trying to set more than one INCOMING_CALL_MANAGEMENT scripts for simGroup:[" + simGroup + "]");
          }
          simGroup.setIncomingCallManagementScript(scriptInstance.scriptInstance);
          incomingCallManagementScriptCnt++;
          break;
        case SS_IMEI_GENERATOR:
          if (imeiGeneratorScriptCnt > 0) {
            throw new SQLException("trying to set more than one IMEI_GENERATOR_SCRIPT scripts for simGroup:[" + simGroup + "]");
          }
          simGroup.setIMEIGeneratorScript(scriptInstance.scriptInstance);
          imeiGeneratorScriptCnt++;
          break;
      }
    }
    simGroup.setBusinessActivityScripts(businessActivityScripts.toArray(new ScriptInstance[businessActivityScripts.size()]));
    simGroup.setActionProviderScripts(actionProviderScripts.toArray(new ScriptInstance[actionProviderScripts.size()]));
    simGroup.setCallFilterScripts(callFilterScripts.toArray(new ScriptInstance[callFilterScripts.size()]));
    simGroup.setSmsFilterScripts(smsFilterScripts.toArray(new ScriptInstance[smsFilterScripts.size()]));
  }

}

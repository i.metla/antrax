/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.storage.map;

import com.flamesgroup.antrax.storage.commons.impl.CDR;
import com.flamesgroup.antrax.storage.commons.impl.CallPath;
import com.flamesgroup.commons.ChannelUID;
import com.flamesgroup.device.DeviceUID;
import com.flamesgroup.storage.jooq.tables.records.CallPathRecord;
import com.flamesgroup.storage.jooq.tables.records.CdrRecord;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

public final class CdrMapper {

  private CdrMapper() {
  }

  public static CDR mapCdrRecordToCdr(final CdrRecord cdrRecord, final List<CallPathRecord> callPathRecords) {
    Objects.requireNonNull(cdrRecord, "cdrRecord mustn't be null");
    Objects.requireNonNull(callPathRecords, "callPathRecords mustn't be null");

    LinkedList<CallPath> callPaths = new LinkedList<>();
    callPathRecords.stream().map(CdrMapper::mapCallPathRecordToCallPath).filter(Objects::nonNull).forEach(callPaths::add);
    return new CDR(cdrRecord.getId())
        .setSetupTime(cdrRecord.getSetupTime() == null ? 0 : cdrRecord.getSetupTime().getTime())
        .setDialTime(cdrRecord.getDialTime() == null ? 0 : cdrRecord.getDialTime().getTime())
        .setAlertTime(cdrRecord.getAlertTime() == null ? 0 : cdrRecord.getAlertTime().getTime())
        .setStartTime(cdrRecord.getStartTime() == null ? 0 : cdrRecord.getStartTime().getTime())
        .setStopTime(cdrRecord.getStopTime() == null ? 0 : cdrRecord.getStopTime().getTime())
        .setCallerPhoneNumber(cdrRecord.getCallerPhoneNumber())
        .setCalledPhoneNumber(cdrRecord.getCalledPhoneNumber())
        .setVoiceServerName(cdrRecord.getVoiceServerName())
        .setCallType(cdrRecord.getCallType())
        .setVoiceServerName(cdrRecord.getVoiceServerName())
        .setCallType(cdrRecord.getCallType())
        .setCallPath(callPaths);
  }

  public static void mapCdrToCdrRecord(final CDR cdr, final CdrRecord cdrRecord) {
    Objects.requireNonNull(cdr, "cdr mustn't be null");
    Objects.requireNonNull(cdrRecord, "cdrRecord mustn't be null");

    cdrRecord.setSetupTime(new Date(cdr.getSetupTime()))
        .setDialTime(cdr.getDialTime() <= 0 ? null : new Date(cdr.getDialTime()))
        .setAlertTime(cdr.getAlertTime() <= 0 ? null : new Date(cdr.getAlertTime()))
        .setStartTime(cdr.getStartTime() <= 0 ? null : new Date(cdr.getStartTime()))
        .setStopTime(cdr.getStopTime() <= 0 ? null : new Date(cdr.getStopTime()))
        .setCallerPhoneNumber(cdr.getCallerPhoneNumber())
        .setCalledPhoneNumber(cdr.getCalledPhoneNumber())
        .setVoiceServerName(cdr.getVoiceServerName())
        .setCallType(cdr.getCallType());
  }

  public static void mapCallPathToCallPathRecord(final long cdrId, final CallPath callPath, final CallPathRecord callPathRecord) {
    Objects.requireNonNull(callPath, "callPath mustn't be null");
    Objects.requireNonNull(callPathRecord, "callPathRecord mustn't be null");

    callPathRecord.setCdrId(cdrId)
        .setSimDeviceUid(callPath.getSimChannelUID() == null ? null : callPath.getSimChannelUID().getDeviceUID().getUID())
        .setSimChannelNum(callPath.getSimChannelUID() == null ? null : (short) callPath.getSimChannelUID().getChannelNumber())
        .setGsmDeviceUid(callPath.getGsmChannelUID() == null ? null : callPath.getGsmChannelUID().getDeviceUID().getUID())
        .setGsmChannelNum(callPath.getGsmChannelUID() == null ? null : (short) callPath.getGsmChannelUID().getChannelNumber())
        .setIccid(callPath.getSimUID() == null ? null : callPath.getSimUID())
        .setSimGroupName(callPath.getSimGroupName())
        .setGsmGroupName(callPath.getGsmGroupName())
        .setStartTime(callPath.getStartTime() <= 0 ? null : new Date(callPath.getStartTime()))
        .setCdrDropReason(callPath.getCdrDropReason())
        .setCdrDropCode(callPath.getCdrDropCode());
  }

  private static CallPath mapCallPathRecordToCallPath(final CallPathRecord callPathRecord) {
    Objects.requireNonNull(callPathRecord, "callPathRecord mustn't be null");

    if (callPathRecord.getId() == null) {
      return null;
    }

    return new CallPath(callPathRecord.getId())
        .setSimChannelUID(callPathRecord.getSimDeviceUid() == null ? null : new ChannelUID(new DeviceUID(callPathRecord.getSimDeviceUid()), callPathRecord.getSimChannelNum().byteValue()))
        .setGsmChannelUID(callPathRecord.getGsmDeviceUid() == null ? null : new ChannelUID(new DeviceUID(callPathRecord.getGsmDeviceUid()), callPathRecord.getGsmChannelNum().byteValue()))
        .setSimUID(callPathRecord.getIccid() == null ? null : callPathRecord.getIccid())
        .setSimGroupName(callPathRecord.getSimGroupName())
        .setGsmGroupName(callPathRecord.getGsmGroupName())
        .setStartTime(callPathRecord.getStartTime() == null ? 0 : callPathRecord.getStartTime().getTime())
        .setCdrDropReason(callPathRecord.getCdrDropReason())
        .setCdrDropCode(callPathRecord.getCdrDropCode());
  }

  public static String convertDurationToString(final long duration) {
    return String.format("%02d:%02d:%02d",
        TimeUnit.MILLISECONDS.toHours(duration),
        TimeUnit.MILLISECONDS.toMinutes(duration) - TimeUnit.MINUTES.toMinutes(TimeUnit.MILLISECONDS.toHours(duration)),
        TimeUnit.MILLISECONDS.toSeconds(duration) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(duration)));
  }

}

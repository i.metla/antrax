/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.simserver.properties;

import com.flamesgroup.properties.PropertiesLoader;
import com.flamesgroup.properties.ServerProperties;

import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

public class SimServerProperties implements ServerProperties {

  private static final String SET_IP_CONFIG_INTERVAL = "set.ip.config.interval";
  private static final String PING_CONTROL_SERVER_TIMEOUT = "ping.control.server.timeout";

  private static final String RMI_REGISTRY_PORT = "rmi.registry.port";

  private final AtomicLong setIpConfigInterval = new AtomicLong(TimeUnit.HOURS.toMillis(6));
  private final AtomicLong pingControlServerTimeout = new AtomicLong(TimeUnit.SECONDS.toMillis(10));

  private final AtomicInteger rmiRegistryPort = new AtomicInteger();

  @Override
  public void load(final Properties properties) {
    PropertiesLoader propertiesLoader = new PropertiesLoader(properties);
    setIpConfigInterval.set(propertiesLoader.getPeriod(SET_IP_CONFIG_INTERVAL));
    pingControlServerTimeout.set(propertiesLoader.getPeriod(PING_CONTROL_SERVER_TIMEOUT));

    rmiRegistryPort.set(propertiesLoader.getInt(RMI_REGISTRY_PORT));
  }

  public long getSetIpConfigInterval() {
    return setIpConfigInterval.get();
  }

  public long getPingControlServerTimeout() {
    return pingControlServerTimeout.get();
  }

  public int getRmiRegistryPort() {
    return rmiRegistryPort.get();
  }

}

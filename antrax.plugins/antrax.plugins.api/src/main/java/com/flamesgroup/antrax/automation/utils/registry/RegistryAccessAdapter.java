/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.automation.utils.registry;

import com.flamesgroup.antrax.automation.registry.RegistryEntry;
import com.flamesgroup.antrax.storage.exceptions.DataModificationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;

public class RegistryAccessAdapter implements RegistryAccess {

  private final static Logger logger = LoggerFactory.getLogger(RegistryAccessAdapter.class);

  private final RemoteRegistryAccess remoteRegistryAccess;

  public RegistryAccessAdapter(final RemoteRegistryAccess remoteRegistryAccess) {
    this.remoteRegistryAccess = remoteRegistryAccess;
  }

  @Override
  public RegistryEntry[] listEntries(final String path, final int maxSize) {
    try {
      return remoteRegistryAccess.listEntries(path, maxSize);
    } catch (RemoteException e) {
      logger.error("[{}] - can't execute remote method", this, e);
    }
    return new RegistryEntry[0];
  }

  @Override
  public RegistryEntry move(final RegistryEntry entry, final String path) throws UnsyncRegistryEntryException, DataModificationException {
    try {
      return remoteRegistryAccess.move(entry, path);
    } catch (RemoteException e) {
      logger.error("[{}] - can't execute remote method", this, e);
      return entry;
    }
  }

  @Override
  public void remove(final RegistryEntry entry) throws UnsyncRegistryEntryException, DataModificationException {
    try {
      remoteRegistryAccess.remove(entry);
    } catch (RemoteException e) {
      logger.error("[{}] - can't execute remote method", this, e);
    }
  }

  @Override
  public void add(final String path, final String value, final int maxSize) {
    try {
      remoteRegistryAccess.add(path, value, maxSize);
    } catch (RemoteException e) {
      logger.error("[{}] - can't execute remote method", this, e);
    }
  }

  @Override
  public String[] listAllPaths() {
    try {
      return remoteRegistryAccess.listAllPaths();
    } catch (RemoteException e) {
      logger.error("[{}] - can't execute remote method", this, e);
    }
    return new String[0];
  }

  @Override
  public void removePath(final String path) {
    try {
      remoteRegistryAccess.removePath(path);
    } catch (RemoteException e) {
      logger.error("[{}] - can't execute remote method", this, e);
    }
  }
}

/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.automation.editors;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.util.EventObject;

import javax.swing.*;

/**
 * Base editor for lists of items represented as strings
 *
 * @param <T>
 */
public abstract class BaseListBasedPropertyEditor<T> extends BasePropertyEditor<T> {

  abstract protected T[] listAvaliableValues();

  private final JComboBox editorComponent;

  public BaseListBasedPropertyEditor() {
    this(null);
    setValue(null);
  }

  public BaseListBasedPropertyEditor(final ListCellRenderer cbRenderer) {
    editorComponent = new JComboBox();
    if (cbRenderer != null) {
      editorComponent.setRenderer(cbRenderer);
    }
    editorComponent.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(final ActionEvent e) {
        fireEditingStopped();
      }
    });
  }

  @Override
  public boolean shouldSelectCell(final EventObject anEvent) {
    if (anEvent instanceof MouseEvent) {
      MouseEvent e = (MouseEvent) anEvent;
      return e.getID() != MouseEvent.MOUSE_DRAGGED;
    }
    return true;
  }

  @Override
  public boolean stopEditing() {
    if (editorComponent.isEditable()) {
      // Commit edited value.
      editorComponent.actionPerformed(new ActionEvent(this, 0, ""));
    }
    return super.stopEditing();
  }

  protected Object getSelected() {
    return editorComponent.getSelectedItem();
  }

  @Override
  public final void setValue(final T value) {
    editorComponent.removeAllItems();
    for (T item : listAvaliableValues()) {
      editorComponent.addItem(item);
    }
    if (value != null) {
      editorComponent.setSelectedItem(value);
    }

  }

  @Override
  public final T getValue() {
    return getType().cast(editorComponent.getSelectedItem());
  }

  @Override
  public Component getEditorComponent() {
    //setValue(getValue()); // force update list available items
    return editorComponent;
  }

  protected void fireValuesChanged() {
    setValue(getValue());
  }

}

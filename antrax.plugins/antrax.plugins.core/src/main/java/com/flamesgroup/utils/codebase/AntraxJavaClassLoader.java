/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.utils.codebase;

import com.flamesgroup.antrax.automation.meta.JavaFileName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.security.SecureClassLoader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.jar.JarEntry;
import java.util.jar.JarInputStream;

public class AntraxJavaClassLoader implements Serializable {

  private static final Logger logger = LoggerFactory.getLogger(AntraxJavaClassLoader.class);

  private static final long serialVersionUID = -1815445937130754560L;

  private final Set<JavaFileName> javaFileNames = new HashSet<>();
  private final Map<String, byte[]> javaCodeClasses = new HashMap<>();

  private transient volatile ClassLoader classLoader;

  public synchronized void analyze(final InputStream in) {
    try {
      try (JarInputStream jis = new JarInputStream(in)) {
        JarEntry entry;
        while ((entry = jis.getNextJarEntry()) != null) {
          if (!entry.isDirectory() && entry.getName().endsWith(".class")) {
            String className = entry.getName().replace('/', '.');
            className = className.substring(0, className.length() - ".class".length());
            String uniqueClassName = className;
            int index = uniqueClassName.indexOf('$');
            if (index > 0) {
              uniqueClassName = uniqueClassName.substring(0, index);
            }
            javaFileNames.add(new JavaFileName(uniqueClassName));

            byte[] byteArray = readClass(jis);
            javaCodeClasses.put(className, byteArray);
          }
        }

        try (ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream()) {
          ObjectOutputStream oos = new ObjectOutputStream(byteArrayOutputStream);
          oos.writeObject(javaCodeClasses);
          logger.info("[{}] - [{}] bytes - size of read scripts from inputStream [{}]", this, byteArrayOutputStream.size(), in);
        }
      }

      classLoader = null; // need to create new instance and clear previous loaded classes
    } catch (IOException e) {
      logger.error("[{}] - can't get read inputStream: [{}]", this, in, e);
    }
  }

  private byte[] readClass(final InputStream inputStream) throws IOException {
    try (ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream()) {
      int read;
      while ((read = inputStream.read()) >= 0) {
        byteArrayOutputStream.write(read);
      }
      return byteArrayOutputStream.toByteArray();
    }
  }

  public synchronized Set<JavaFileName> getJavaFileNames() {
    return javaFileNames;
  }

  public synchronized ClassLoader getClassLoader() {
    if (classLoader == null) {
      classLoader = new JavaClassLoader();
    }
    return classLoader;
  }

  private class JavaClassLoader extends SecureClassLoader {

    @Override
    protected Class<?> findClass(final String name) throws ClassNotFoundException {
      byte[] code = javaCodeClasses.get(name);
      if (code == null) {
        return Thread.currentThread().getContextClassLoader().loadClass(name);
      } else {
        return super.defineClass(name, code, 0, code.length);
      }
    }

  }

}

/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.voiceserver.manager.rtstat;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import com.flamesgroup.antrax.storage.commons.impl.GSMChannelImpl;
import com.flamesgroup.antrax.storage.state.CallChannelState;
import com.flamesgroup.antrax.storage.state.CallState;
import com.flamesgroup.commons.ChannelUID;
import com.flamesgroup.device.DeviceType;
import com.flamesgroup.device.DeviceUID;
import com.flamesgroup.unit.ICCID;
import com.flamesgroup.unit.PhoneNumber;
import org.junit.Assert;
import org.junit.Test;

public class ChannelsRuntimeTest {

  @Test
  public void testBEKKIChannels() {
    ChannelsRuntime channelsRuntime = new ChannelsRuntime();
    ChannelUID bekki11 = new ChannelUID(new DeviceUID(DeviceType.GSMB, 1), (byte) 1);
    ChannelUID bekki12 = new ChannelUID(new DeviceUID(DeviceType.GSMB, 1), (byte) 2);
    assertEquals(0, channelsRuntime.getChannelsInfo().size());

    channelsRuntime.logDeclaredGsmUnit(new GSMChannelImpl().setChannelUID(bekki11));
    assertEquals(1, channelsRuntime.getChannelsInfo().size());
    Assert.assertEquals(bekki11, channelsRuntime.getChannelsInfo().get(0).getMobileGatewayChannelInfo().getGSMChannel().getChannelUID());

    channelsRuntime.logDeclaredGsmUnit(new GSMChannelImpl().setChannelUID(bekki12));
    assertEquals(2, channelsRuntime.getChannelsInfo().size());
    Assert.assertEquals(bekki11, channelsRuntime.getChannelsInfo().get(0).getMobileGatewayChannelInfo().getGSMChannel().getChannelUID());
    Assert.assertEquals(bekki12, channelsRuntime.getChannelsInfo().get(1).getMobileGatewayChannelInfo().getGSMChannel().getChannelUID());
  }

  @Test
  public void testCEMChannels() throws Exception {
    ChannelsRuntime channelsRuntime = new ChannelsRuntime();
    ChannelUID cem11 = new ChannelUID(new DeviceUID(DeviceType.SIMB, 1), (byte) 1);
    assertEquals(0, channelsRuntime.getChannelsInfo().size());

    channelsRuntime.logSIMTakenFromSimServer(cem11, new ICCID("11"));
    assertFalse(channelsRuntime.findSimChannelInfo(cem11).isEnabled());
    channelsRuntime.logSIMChangedEnableStatus(cem11, true);
    assertTrue(channelsRuntime.findSimChannelInfo(cem11).isEnabled());
    channelsRuntime.logSIMChangedEnableStatus(cem11, false);
    assertFalse(channelsRuntime.findSimChannelInfo(cem11).isEnabled());

    assertNull(channelsRuntime.findSimChannelInfo(cem11).getLastUSSDResponse());
    Assert.assertEquals(0, channelsRuntime.findSimChannelInfo(cem11).getLastUSSDTimeout());
    channelsRuntime.logCallChannelSendUSSD(cem11, "NEW USSD REQUEST", "NEW USSD RESPONSE");
    Thread.sleep(200);
    Assert.assertEquals("NEW USSD RESPONSE", channelsRuntime.findSimChannelInfo(cem11).getLastUSSDResponse());
    assertTrue(channelsRuntime.findSimChannelInfo(cem11).getLastUSSDTimeout() > 0);
  }

  @Test
  public void testCallChannels() throws Exception {
    ChannelsRuntime channelsRuntime = new ChannelsRuntime();
    ChannelUID bekki11 = new ChannelUID(new DeviceUID(DeviceType.GSMB, 1), (byte) 1);
    ChannelUID bekki12 = new ChannelUID(new DeviceUID(DeviceType.GSMB, 1), (byte) 2);
    ChannelUID cem11 = new ChannelUID(new DeviceUID(DeviceType.SIMB, 1), (byte) 1);
    ChannelUID cem12 = new ChannelUID(new DeviceUID(DeviceType.SIMB, 1), (byte) 2);
    assertEquals(0, channelsRuntime.getChannelsInfo().size());

    channelsRuntime.logDeclaredGsmUnit(new GSMChannelImpl().setChannelUID(bekki11));
    channelsRuntime.logDeclaredGsmUnit(new GSMChannelImpl().setChannelUID(bekki12));
    channelsRuntime.logSIMTakenFromSimServer(cem11, new ICCID("11"));
    channelsRuntime.logSIMTakenFromSimServer(cem12, new ICCID("12"));

    channelsRuntime.logCallChannelCreated(bekki11, cem11);
    assertEquals(2, channelsRuntime.getChannelsInfo().size());
    Assert.assertEquals(bekki11, channelsRuntime.getChannelsInfo().get(0).getMobileGatewayChannelInfo().getGSMChannel().getChannelUID());
    Assert.assertEquals(cem11, channelsRuntime.getChannelsInfo().get(0).getSimChannelInfo().getSimChannelUID());
    Assert.assertEquals(new ICCID("11"), channelsRuntime.getChannelsInfo().get(0).getSimChannelInfo().getSimUID());
    Assert.assertEquals(CallState.State.IDLE, channelsRuntime.getChannelsInfo().get(0).getCallState().getState());

    channelsRuntime.logCallChannelChangedState(cem11, CallChannelState.State.READY_TO_CALL, "", 0);
    Assert.assertEquals(CallChannelState.State.READY_TO_CALL, channelsRuntime.getChannelsInfo().get(0).getChannelState().getState());

    channelsRuntime.logCallStarted(cem11, new PhoneNumber("111"));
    channelsRuntime.logCallChangeState(cem11, CallState.State.DIALING);
    channelsRuntime.logCallChangeState(cem11, CallState.State.ACTIVE);
    Assert.assertEquals(CallState.State.ACTIVE, channelsRuntime.getChannelsInfo().get(0).getCallState().getState());
    Thread.sleep(200);

    channelsRuntime.logCallEnded(cem11, 10);
    Assert.assertEquals(CallState.State.IDLE, channelsRuntime.getChannelsInfo().get(0).getCallState().getState());
    Assert.assertEquals(1, channelsRuntime.getChannelsInfo().get(0).getMobileGatewayChannelInfo().getTotalCallsCount());
    Assert.assertEquals(1, channelsRuntime.getChannelsInfo().get(0).getMobileGatewayChannelInfo().getSuccessfulCallsCount());
    assertTrue(channelsRuntime.getChannelsInfo().get(0).getMobileGatewayChannelInfo().getCallsDuration() > 0);

    channelsRuntime.logCallStarted(cem11, new PhoneNumber("111"));
    channelsRuntime.logCallEnded(cem11, 0);
    Assert.assertEquals(CallState.State.IDLE, channelsRuntime.getChannelsInfo().get(0).getCallState().getState());
    Assert.assertEquals(2, channelsRuntime.getChannelsInfo().get(0).getMobileGatewayChannelInfo().getTotalCallsCount());
    Assert.assertEquals(1, channelsRuntime.getChannelsInfo().get(0).getMobileGatewayChannelInfo().getSuccessfulCallsCount());

    channelsRuntime.logCallChannelReleased(cem11);
    assertEquals(2, channelsRuntime.getChannelsInfo().size());
    assertNull(channelsRuntime.getChannelsInfo().get(0).getSimChannelInfo());
    Assert.assertEquals(2, channelsRuntime.getChannelsInfo().get(0).getMobileGatewayChannelInfo().getTotalCallsCount());
    Assert.assertEquals(1, channelsRuntime.getChannelsInfo().get(0).getMobileGatewayChannelInfo().getSuccessfulCallsCount());

    channelsRuntime.logCallChannelCreated(bekki11, cem12);
    Assert.assertEquals(bekki11, channelsRuntime.getChannelsInfo().get(0).getMobileGatewayChannelInfo().getGSMChannel().getChannelUID());
    Assert.assertEquals(cem12, channelsRuntime.getChannelsInfo().get(0).getSimChannelInfo().getSimChannelUID());
    Assert.assertEquals(new ICCID("12"), channelsRuntime.getChannelsInfo().get(0).getSimChannelInfo().getSimUID());
    Assert.assertEquals(CallState.State.IDLE, channelsRuntime.getChannelsInfo().get(0).getCallState().getState());

    channelsRuntime.logCallStarted(cem12, new PhoneNumber("111"));
    channelsRuntime.logCallEnded(cem12, 0);
    Assert.assertEquals(CallState.State.IDLE, channelsRuntime.getChannelsInfo().get(0).getCallState().getState());
    Assert.assertEquals(3, channelsRuntime.getChannelsInfo().get(0).getMobileGatewayChannelInfo().getTotalCallsCount());
    Assert.assertEquals(1, channelsRuntime.getChannelsInfo().get(0).getMobileGatewayChannelInfo().getSuccessfulCallsCount());
  }

  @Test
  public void testUSSD() throws Exception {
    ChannelsRuntime channelsRuntime = new ChannelsRuntime();
    ChannelUID bekki11 = new ChannelUID(new DeviceUID(DeviceType.GSMB, 1), (byte) 1);
    ChannelUID bekki12 = new ChannelUID(new DeviceUID(DeviceType.GSMB, 1), (byte) 2);
    ChannelUID cem11 = new ChannelUID(new DeviceUID(DeviceType.SIMB, 1), (byte) 1);
    ChannelUID cem12 = new ChannelUID(new DeviceUID(DeviceType.SIMB, 1), (byte) 2);
    assertEquals(0, channelsRuntime.getChannelsInfo().size());

    channelsRuntime.logDeclaredGsmUnit(new GSMChannelImpl().setChannelUID(bekki11));
    channelsRuntime.logDeclaredGsmUnit(new GSMChannelImpl().setChannelUID(bekki12));
    channelsRuntime.logSIMTakenFromSimServer(cem11, new ICCID("11"));
    channelsRuntime.logSIMTakenFromSimServer(cem12, new ICCID("12"));

    channelsRuntime.logCallChannelCreated(bekki11, cem11);
    assertEquals(2, channelsRuntime.getChannelsInfo().size());
    assertNull(channelsRuntime.getChannelsInfo().get(0).getSimChannelInfo().getLastUSSDResponse());
    Assert.assertEquals(0, channelsRuntime.getChannelsInfo().get(0).getSimChannelInfo().getLastUSSDTimeout());
    channelsRuntime.logCallChannelSendUSSD(cem11, "NEW USSD REQUEST", "NEW USSD RESPONSE");
    Thread.sleep(200);
    Assert.assertEquals("NEW USSD RESPONSE", channelsRuntime.getChannelsInfo().get(0).getSimChannelInfo().getLastUSSDResponse());
    assertTrue(channelsRuntime.getChannelsInfo().get(0).getSimChannelInfo().getLastUSSDTimeout() > 0);

    channelsRuntime.logCallChannelReleased(cem11);
    channelsRuntime.logCallChannelCreated(bekki12, cem11);
    Assert.assertEquals("NEW USSD RESPONSE", channelsRuntime.getChannelsInfo().get(1).getSimChannelInfo().getLastUSSDResponse());
    assertTrue(channelsRuntime.getChannelsInfo().get(1).getSimChannelInfo().getLastUSSDTimeout() > 0);
    channelsRuntime.logCallChannelSendUSSD(cem11, "NEW USSD REQUEST", "YET ANOTHER NEW USSD RESPONSE");
    Assert.assertEquals("YET ANOTHER NEW USSD RESPONSE", channelsRuntime.getChannelsInfo().get(1).getSimChannelInfo().getLastUSSDResponse());
  }

}

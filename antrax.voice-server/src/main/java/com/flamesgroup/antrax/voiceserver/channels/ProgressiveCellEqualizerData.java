/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.voiceserver.channels;

public class ProgressiveCellEqualizerData {

  private int currentArfcn = -1;
  private int currentRxLev;
  private int prevArfcn = -1;
  private int prevCountRxLevDelta;
  private int destinationRxLev;
  private int minWaitingTime;
  private int maxWaitingTime;

  public int getCurrentArfcn() {
    return currentArfcn;
  }

  public ProgressiveCellEqualizerData setCurrentArfcn(final int currentArfcn) {
    this.currentArfcn = currentArfcn;
    return this;
  }

  public int getCurrentRxLev() {
    return currentRxLev;
  }

  public ProgressiveCellEqualizerData setCurrentRxLev(final int currentRxLev) {
    this.currentRxLev = currentRxLev;
    return this;
  }

  public int getPrevArfcn() {
    return prevArfcn;
  }

  public ProgressiveCellEqualizerData setPrevArfcn(final int prevArfcn) {
    this.prevArfcn = prevArfcn;
    return this;
  }

  public int getPrevCountRxLevDelta() {
    return prevCountRxLevDelta;
  }

  public ProgressiveCellEqualizerData setPrevCountRxLevDelta(final int prevCountRxLevDelta) {
    this.prevCountRxLevDelta = prevCountRxLevDelta;
    return this;
  }

  public int getDestinationRxLev() {
    return destinationRxLev;
  }

  public ProgressiveCellEqualizerData setDestinationRxLev(final int destinationRxLev) {
    this.destinationRxLev = destinationRxLev;
    return this;
  }

  public int getMinWaitingTime() {
    return minWaitingTime;
  }

  public ProgressiveCellEqualizerData setMinWaitingTime(final int minWaitingTime) {
    this.minWaitingTime = minWaitingTime;
    return this;
  }

  public int getMaxWaitingTime() {
    return maxWaitingTime;
  }

  public ProgressiveCellEqualizerData setMaxWaitingTime(final int maxWaitingTime) {
    this.maxWaitingTime = maxWaitingTime;
    return this;
  }

  @Override
  public String toString() {
    return getClass().getSimpleName() + '@' + Integer.toHexString(hashCode()) +
        "[currentArfcn:" + currentArfcn +
        " currentRxLev:" + currentRxLev +
        " prevArfcn:" + prevArfcn +
        " prevCountRxLevDelta:" + prevCountRxLevDelta +
        " destinationRxLev:" + destinationRxLev +
        " minWaitingTime:" + minWaitingTime +
        " maxWaitingTime:" + maxWaitingTime +
        ']';
  }

}

/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.voiceserver.properties;

import com.flamesgroup.properties.PropertiesLoader;
import com.flamesgroup.properties.ServerProperties;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;

public class VoiceServerProperties implements ServerProperties {

  private static final String SET_IP_CONFIG_INTERVAL = "set.ip.config.interval";
  private static final String PING_CONTROL_SERVER_TIMEOUT = "ping.control.server.timeout";

  private static final String RMI_REGISTRY_PORT = "rmi.registry.port";

  private static final String SIM_ACTIVITY_START_CACHE_LIFETIME = "sim.activity.start.cache.lifetime";
  private static final String SIM_ACTIVITY_STOP_CACHE_LIFETIME = "sim.activity.stop.cache.lifetime";
  private static final String SIM_PLUGED_CACHE_LIFETIME = "sim.pluged.cache.lifetime";
  private static final String SIM_SESSION_CACHE_LIFETIME = "sim.session.cache.lifetime";
  private static final String SIM_RECONFIGURATION_CACHE_LIFETIME = "sim.reconfiguration.cache.lifetime";

  private static final String CORE_RESORT_CALL_CHANNELS_TIMEOUT = "core.resort.call.channels.timeout";
  private static final String CORE_VOIP_CALLPROCESSINGDELAY = "core.voip.callProcessingDelay";
  private static final String CORE_RECONFIGURATION_TIMEOUT = "core.reconfiguration.timeout";
  private static final String CORE_FAS_ACTIVE_TIMEOUT = "core.fas.active.timeout";
  private static final String CORE_FAS_ATTEMPTS = "core.fas.attempts";
  private static final String CORE_REGISTRATION_TIMEOUT = "core.registration.timeout";
  private static final String CORE_REGISTRATION_PHASE2PLUS_DISABLE = "core.registration.phase2plus.disable";
  private static final String CORE_SAT_DISABLE = "core.sat.disable";

  private static final String VOICE_SERVER_AUDIO_CAPTURE_ENABLED = "voice-server.audio.capture.enabled";
  private static final String VOICE_SERVER_AUDIO_CAPTURE_PATH = "voice-server.audio.capture.path";

  private static final String VOICE_SERVER_TELIT_TRACE_ENABLED = "voice-server.telit.trace.enabled";
  private static final String VOICE_SERVER_TELIT_TRACE_MASK = "voice-server.telit.trace.mask";
  private static final String VOICE_SERVER_TELIT_TRACE_PATH = "voice-server.telit.trace.path";

  private static final String VOICE_SERVER_HTTP_PORT = "voice-server.http_port";

  private static final String NETWORK_SURVEY_ENABLED = "network.survey.enabled";
  private static final String NETWORK_SURVEY_DURATION = "network.survey.duration";
  private static final String NETWORK_SURVEY_PERIOD = "network.survey.period";

  private static final String CELL_EQUALIZER_ENABLED = "cell.equalizer.enabled";

  private static final String IMSI_CATCHER_ENABLED = "imsi.catcher.enabled";
  private static final String IMSI_CATCHER_CHECK_NEIGHBOURS_ENABLED = "imsi.catcher.check.neighbours.enabled";

  private static final String IVR_CORRELATION_ENABLE = "ivr.correlation.enable";
  private static final String IVR_CORRELATION_TIME = "ivr.correlation.time";
  private static final String IVR_TEMPLATES_PATH = "ivr.templates.path";
  private static final String IVR_TEMPLATES_MAX_LENGTH_TIME = "ivr.templates.max.length.time";

  private static final String INCOMING_SMS_VOLUME_DISABLED = "incoming.sms.volume.disabled";

  private final AtomicLong setIpConfigInterval = new AtomicLong();
  private final AtomicLong pingControlServerTimeout = new AtomicLong();

  private final AtomicInteger rmiRegistryPort = new AtomicInteger();

  private final AtomicInteger simActivityStartCacheLifetime = new AtomicInteger();
  private final AtomicInteger simActivityStopCacheLifetime = new AtomicInteger();
  private final AtomicInteger simPlugedCacheLifetime = new AtomicInteger();
  private final AtomicInteger simSessionCacheLifetime = new AtomicInteger();
  private final AtomicInteger simReconfigurationCacheLifetime = new AtomicInteger();

  private final AtomicLong coreResortCallChannelsTimeout = new AtomicLong();
  private final AtomicLong coreVoipCallProcessingDelay = new AtomicLong();
  private final AtomicLong coreReconfigurationTimeout = new AtomicLong();
  private final AtomicLong coreFasActiveTimeout = new AtomicLong();
  private final AtomicInteger coreFasAttempts = new AtomicInteger();
  private final AtomicLong coreRegistrationTimeout = new AtomicLong();
  private final AtomicBoolean coreRegistrationPhase2PlusDisable = new AtomicBoolean();
  private final AtomicBoolean coreSatDisable = new AtomicBoolean();

  private final AtomicBoolean voiceServerAudioCaptureEnabled = new AtomicBoolean();
  private final AtomicReference<Path> voiceServerAudioCapturePath = new AtomicReference<>();

  private final AtomicBoolean voiceServerTelitTraceEnabled = new AtomicBoolean();
  private final AtomicReference<String> voiceServerTelitTraceMask = new AtomicReference<>();
  private final AtomicReference<Path> voiceServerTelitTracePath = new AtomicReference<>();

  private final AtomicInteger voiceServerHttpPort = new AtomicInteger();

  private final AtomicBoolean networkSurveyEnabled = new AtomicBoolean();
  private final AtomicLong networkSurveyDuration = new AtomicLong();
  private final AtomicLong networkSurveyPeriod = new AtomicLong();

  private final AtomicBoolean cellEqualizerEnabled = new AtomicBoolean();

  private final AtomicBoolean imsiCatcherEnabled = new AtomicBoolean();
  private final AtomicBoolean imsiCatcherCheckNeighboursEnabled = new AtomicBoolean();

  private final AtomicBoolean ivrCorrelationEnable = new AtomicBoolean();
  private final AtomicLong ivrCorrelationTime = new AtomicLong();
  private final AtomicReference<String> ivrTemplatesPath = new AtomicReference<>();
  private final AtomicLong ivrTemplatesMaxLengthTime = new AtomicLong();

  private final AtomicBoolean incomingSmsVolumeDisabled = new AtomicBoolean();

  @Override
  public void load(final Properties properties) {
    PropertiesLoader propertiesLoader = new PropertiesLoader(properties);
    setIpConfigInterval.set(propertiesLoader.getPeriod(SET_IP_CONFIG_INTERVAL));
    pingControlServerTimeout.set(propertiesLoader.getPeriod(PING_CONTROL_SERVER_TIMEOUT));

    rmiRegistryPort.set(propertiesLoader.getInt(RMI_REGISTRY_PORT));

    simActivityStartCacheLifetime.set(propertiesLoader.getInt(SIM_ACTIVITY_START_CACHE_LIFETIME));
    simActivityStopCacheLifetime.set(propertiesLoader.getInt(SIM_ACTIVITY_STOP_CACHE_LIFETIME));
    simPlugedCacheLifetime.set(propertiesLoader.getInt(SIM_PLUGED_CACHE_LIFETIME));
    simSessionCacheLifetime.set(propertiesLoader.getInt(SIM_SESSION_CACHE_LIFETIME));
    simReconfigurationCacheLifetime.set(propertiesLoader.getInt(SIM_RECONFIGURATION_CACHE_LIFETIME));

    coreResortCallChannelsTimeout.set(propertiesLoader.getPeriod(CORE_RESORT_CALL_CHANNELS_TIMEOUT));
    coreVoipCallProcessingDelay.set(propertiesLoader.getPeriod(CORE_VOIP_CALLPROCESSINGDELAY));
    coreReconfigurationTimeout.set(propertiesLoader.getPeriod(CORE_RECONFIGURATION_TIMEOUT));
    coreFasActiveTimeout.set(propertiesLoader.getPeriod(CORE_FAS_ACTIVE_TIMEOUT));
    coreFasAttempts.set(propertiesLoader.getInt(CORE_FAS_ATTEMPTS));
    coreRegistrationTimeout.set(propertiesLoader.getPeriod(CORE_REGISTRATION_TIMEOUT));
    coreRegistrationPhase2PlusDisable.set(propertiesLoader.getBoolean(CORE_REGISTRATION_PHASE2PLUS_DISABLE));
    coreSatDisable.set(propertiesLoader.getBoolean(CORE_SAT_DISABLE));

    voiceServerAudioCaptureEnabled.set(propertiesLoader.getBoolean(VOICE_SERVER_AUDIO_CAPTURE_ENABLED));
    voiceServerAudioCapturePath.set(Paths.get(propertiesLoader.getString(VOICE_SERVER_AUDIO_CAPTURE_PATH)));

    voiceServerTelitTraceEnabled.set(propertiesLoader.getBoolean(VOICE_SERVER_TELIT_TRACE_ENABLED));
    voiceServerTelitTraceMask.set(propertiesLoader.getString(VOICE_SERVER_TELIT_TRACE_MASK));
    voiceServerTelitTracePath.set(Paths.get(propertiesLoader.getString(VOICE_SERVER_TELIT_TRACE_PATH)));

    voiceServerHttpPort.set(propertiesLoader.getInt(VOICE_SERVER_HTTP_PORT));

    networkSurveyEnabled.set(propertiesLoader.getBoolean(NETWORK_SURVEY_ENABLED));
    networkSurveyDuration.set(propertiesLoader.getPeriod(NETWORK_SURVEY_DURATION));
    networkSurveyPeriod.set(propertiesLoader.getPeriod(NETWORK_SURVEY_PERIOD));

    cellEqualizerEnabled.set(propertiesLoader.getBoolean(CELL_EQUALIZER_ENABLED));

    imsiCatcherEnabled.set(propertiesLoader.getBoolean(IMSI_CATCHER_ENABLED));
    imsiCatcherCheckNeighboursEnabled.set(propertiesLoader.getBoolean(IMSI_CATCHER_CHECK_NEIGHBOURS_ENABLED));

    ivrCorrelationEnable.set(propertiesLoader.getBoolean(IVR_CORRELATION_ENABLE));
    ivrCorrelationTime.set(propertiesLoader.getPeriod(IVR_CORRELATION_TIME));
    ivrTemplatesPath.set(propertiesLoader.getString(IVR_TEMPLATES_PATH));
    ivrTemplatesMaxLengthTime.set(propertiesLoader.getPeriod(IVR_TEMPLATES_MAX_LENGTH_TIME));

    incomingSmsVolumeDisabled.set(propertiesLoader.getBoolean(INCOMING_SMS_VOLUME_DISABLED));
  }

  public long getSetIpConfigInterval() {
    return setIpConfigInterval.get();
  }

  public long getPingControlServerTimeout() {
    return pingControlServerTimeout.get();
  }

  public int getRmiRegistryPort() {
    return rmiRegistryPort.get();
  }

  public int getSimActivityStartCacheLifetime() {
    return simActivityStartCacheLifetime.get();
  }

  public int getSimActivityStopCacheLifetime() {
    return simActivityStopCacheLifetime.get();
  }

  public int getSimPlugedCacheLifetime() {
    return simPlugedCacheLifetime.get();
  }

  public int getSimSessionCacheLifetime() {
    return simSessionCacheLifetime.get();
  }

  public int getSimReconfigurationCacheLifetime() {
    return simReconfigurationCacheLifetime.get();
  }

  public long getCoreResortCallChannelsTimeout() {
    return coreResortCallChannelsTimeout.get();
  }

  public long getCoreVoipCallProcessingDelay() {
    return coreVoipCallProcessingDelay.get();
  }

  public long getCoreReconfigurationTimeout() {
    return coreReconfigurationTimeout.get();
  }

  public long getCoreFasActiveTimeout() {
    return coreFasActiveTimeout.get();
  }

  public int getCoreFasAttempts() {
    return coreFasAttempts.get();
  }

  public long getCoreRegistrationTimeout() {
    return coreRegistrationTimeout.get();
  }

  public boolean getCoreRegistrationPhase2PlusDisable() {
    return coreRegistrationPhase2PlusDisable.get();
  }

  public boolean getCoreSatDisable() {
    return coreSatDisable.get();
  }

  public boolean getVoiceServerAudioCaptureEnabled() {
    return voiceServerAudioCaptureEnabled.get();
  }

  public Path getVoiceServerAudioCapturePath() {
    return voiceServerAudioCapturePath.get();
  }

  public boolean getVoiceServerTelitTraceEnabled() {
    return voiceServerTelitTraceEnabled.get();
  }

  public String getVoiceServerTelitTraceMask() {
    return voiceServerTelitTraceMask.get();
  }

  public Path getVoiceServerTelitTracePath() {
    return voiceServerTelitTracePath.get();
  }

  public int getVoiceServerHttpPort() {
    return voiceServerHttpPort.get();
  }

  public boolean geNetworkSurveyEnabled() {
    return networkSurveyEnabled.get();
  }

  public long getNetworkSurveyDuration() {
    return networkSurveyDuration.get();
  }

  public long getNetworkSurveyPeriod() {
    return networkSurveyPeriod.get();
  }

  public boolean getCellEqualizerEnabled() {
    return cellEqualizerEnabled.get();
  }

  public boolean getImsiCatcherEnabled() {
    return imsiCatcherEnabled.get();
  }

  public boolean getImsiCatcherCheckNeighboursEnabled() {
    return imsiCatcherCheckNeighboursEnabled.get();
  }

  public boolean getIvrCorrelationEnable() {
    return ivrCorrelationEnable.get();
  }

  public long getIvrCorrelationTime() {
    return ivrCorrelationTime.get();
  }

  public String getIvrTemplatesPath() {
    return ivrTemplatesPath.get();
  }

  public long getIvrTemplatesMaxLengthTime() {
    return ivrTemplatesMaxLengthTime.get();
  }

  public boolean getIncomingSmsVolumeDisabled() {
    return incomingSmsVolumeDisabled.get();
  }

}

/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.voiceserver.states;

import com.flamesgroup.antrax.timemachine.State;
import com.flamesgroup.antrax.voiceserver.CallChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class BaseWaitingState implements State {

  private final Logger logger = LoggerFactory.getLogger(BaseWaitingState.class);

  private final State delegate;
  private final State sessionEndNextState;
  private final State activityEndNextState;
  private final CallChannel callChannel;

  private boolean sessionEnded;
  private boolean activityEnded;

  public BaseWaitingState(final State delegate, final State sessionEndNextState, final State activityEndNextState, final CallChannel cc) {
    this.sessionEndNextState = sessionEndNextState;
    this.activityEndNextState = activityEndNextState;
    this.callChannel = cc;
    this.delegate = delegate;
  }

  @Override
  public void tickRoutine() {
    if (!activityEnded && !callChannel.isGroupApplies()) {
      logger.debug("[{}] - {} should stop activity because it contains unallowed gsm and sim units", this, callChannel);
      activityEnded = true;
    } else if (!activityEnded && callChannel.getSIMUnit().shouldStopActivity()) {
      logger.debug("[{}] - {} should stop activity", this, callChannel);
      activityEnded = true;
    } else if (!activityEnded && !sessionEnded && callChannel.getSIMUnit().shouldStopSession()) {
      logger.debug("[{}] - {} should stop session", this, callChannel);
      sessionEnded = true;
    } else {
      delegate.tickRoutine();
    }
  }

  @Override
  public State getNextState() {
    if (activityEnded) {
      logger.debug("[{}] - {} was asked to end activity, so next state will be {}", this, callChannel, activityEndNextState);
      return activityEndNextState;
    }
    if (sessionEnded) {
      logger.debug("[{}] - {} was asked to end session, so next state will be {}", this, callChannel, sessionEndNextState);
      return sessionEndNextState;
    }
    return delegate.getNextState();
  }

  @Override
  public void enterState() {
    sessionEnded = false;
    activityEnded = false;
    delegate.enterState();
  }

  protected boolean requiresStateFinish() {
    return sessionEnded || activityEnded;
  }

}

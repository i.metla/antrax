/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.voiceserver;

import com.flamesgroup.antrax.activity.IRemoteHistoryLogger;
import com.flamesgroup.antrax.storage.enums.CallType;
import com.flamesgroup.antrax.voiceserver.audio.codec.AudioCodecFactory;
import com.flamesgroup.antrax.voiceserver.properties.Jiax2Properties;
import com.flamesgroup.antrax.voiceserver.properties.VoiceServerPropUtils;
import com.flamesgroup.jiax2.AudioMediaFormatOption;
import com.flamesgroup.jiax2.AudioOptions;
import com.flamesgroup.jiax2.IIaxPeer;
import com.flamesgroup.jiax2.IOriginatingCall;
import com.flamesgroup.jiax2.IOriginatingCallHandler;
import com.flamesgroup.jiax2.IaxPeer;
import com.flamesgroup.jiax2.TrunkOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.net.UnknownHostException;
import java.util.List;
import java.util.Map;

public class VoIPPeer {

  private static final Logger logger = LoggerFactory.getLogger(VoIPPeer.class);

  private final IIaxPeer iaxPeer;

  private final CallChannelPool callChannelPool;
  private final IRemoteHistoryLogger historyLogger;
  private final VSStatus status;

  public VoIPPeer(final CallChannelPool callChannelPool, final IRemoteHistoryLogger historyLogger, final VSStatus status) {
    this.callChannelPool = callChannelPool;
    this.historyLogger = historyLogger;
    this.status = status;

    SocketAddress listenerAddress = VoiceServerPropUtils.getInstance().getJiax2Properties().getIaxPeerListenerAddress();
    List<AudioMediaFormatOption> audioMediaFormatOptions = VoiceServerPropUtils.getInstance().getJiax2Properties().getAudioOptionFormats();
    Map<SocketAddress, TrunkOptions> trunkOptionsMap = VoiceServerPropUtils.getInstance().getJiax2Properties().getTrunkOptionsMap();

    iaxPeer = new IaxPeer(listenerAddress, new AudioOptions(audioMediaFormatOptions), new AudioCodecFactory(), trunkOptionsMap);
  }

  public void start() throws IOException, InterruptedException {
    iaxPeer.startup(terminatingCall -> {
      if (status.terminating) {
        return null;
      }

      long callProcessDelay = VoiceServerPropUtils.getInstance().getVoiceServerProperties().getCoreVoipCallProcessingDelay();
      TerminatingCallHandlerAdapter adapter = new TerminatingCallHandlerAdapter(terminatingCall, CallType.VOIP_TO_GSM, callChannelPool, historyLogger, callProcessDelay);
      new Thread(adapter, "VoIPPeer").start();

      return adapter;
    });
  }

  public IOriginatingCall newCall(final String target, final String called, final String caller, final String callerName, final IOriginatingCallHandler originationCallHandler) {
    String[] targetParameters = target.split(":");
    InetAddress address;
    try {
      address = InetAddress.getByName(targetParameters[0]);
    } catch (UnknownHostException e) {
      logger.warn("[{}] - incorrect target address [{}]", this, target, e);
      return null;
    }
    int port = (targetParameters.length < 2) ? Jiax2Properties.DEFAULT_IAX_PORT : Integer.parseInt(targetParameters[1]);

    return iaxPeer.newCall(new InetSocketAddress(address, port), called, caller, callerName, originationCallHandler);
  }

  public void stop() throws IOException, InterruptedException {
    iaxPeer.shutdown();
  }

}

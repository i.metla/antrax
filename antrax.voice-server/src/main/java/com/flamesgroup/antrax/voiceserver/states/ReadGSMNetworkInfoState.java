/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.voiceserver.states;

import com.flamesgroup.antrax.timemachine.ConditionalStateImpl;
import com.flamesgroup.antrax.timemachine.State;
import com.flamesgroup.antrax.voiceserver.CallChannel;
import com.flamesgroup.commons.GSMNetworkInfo;
import com.flamesgroup.commons.OperatorSelectionMode;
import com.flamesgroup.commons.Pair;
import com.flamesgroup.unit.CellInfo;
import com.flamesgroup.unit.RegistrationStatus.NetworkRegistrationStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.atomic.AtomicBoolean;

// TODO: remove after architecture reconsideration
public class ReadGSMNetworkInfoState extends ConditionalStateImpl {

  private final AtomicBoolean finished = new AtomicBoolean();
  private static final Logger logger = LoggerFactory.getLogger(ReadGSMNetworkInfoState.class);

  private final CallChannel callChannel;
  private State idleBetweenChecks;

  public ReadGSMNetworkInfoState(final CallChannel callChannel) {
    assert callChannel != null;
    this.callChannel = callChannel;
  }

  @Override
  public boolean isFinished() {
    logger.trace("[{}] - isFinished asked. finished={}", this, finished);
    return finished.get();
  }

  @Override
  public void enterState() {
    finished.set(false);
    CellInfo[] cellInfo = callChannel.getCellMonitoringInfo();
    Pair<NetworkRegistrationStatus, String> registrationStatus = callChannel.getNetworkRegistrationStatus();
    if (cellInfo != null && cellInfo.length > 0 && registrationStatus != null) {
      OperatorSelectionMode operatorSelectionMode = callChannel.getSIMUnit().getOperatorSelectionMode();
      GSMNetworkInfo info = new GSMNetworkInfo(cellInfo, registrationStatus.second(), operatorSelectionMode, registrationStatus.first());
      callChannel.updateGSMNetworkInfo(info);
      logger.trace("[{}] - GSMNetworkInfo was read:[{}]", this, info);
    }
    finished.set(true);
  }

  @Override
  public State getNextState() {
    return idleBetweenChecks;
  }

  public void setNextState(final State nextState) {
    idleBetweenChecks = nextState;
  }

}

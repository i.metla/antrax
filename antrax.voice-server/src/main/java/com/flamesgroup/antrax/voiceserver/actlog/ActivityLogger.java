/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.voiceserver.actlog;

import com.flamesgroup.antrax.control.communication.ServerStatus;
import com.flamesgroup.antrax.distributor.ChannelConfig;
import com.flamesgroup.antrax.storage.commons.GSMChannel;
import com.flamesgroup.antrax.storage.commons.GSMGroup;
import com.flamesgroup.antrax.storage.state.CallChannelState;
import com.flamesgroup.antrax.storage.state.CallState;
import com.flamesgroup.commons.ChannelUID;
import com.flamesgroup.commons.GSMNetworkInfo;
import com.flamesgroup.unit.ICCID;
import com.flamesgroup.unit.PhoneNumber;

public interface ActivityLogger {

  void changeServerStatus(ServerStatus status);

  void logDeclaredGsmUnit(GSMChannel channel);

  void logActiveGsmUnit(ChannelUID gsmUnit, GSMChannel gsmChannel, ChannelConfig channelConfig);

  void updateChannelConfig(ChannelUID gsmUnit, GSMChannel gsmChannel, ChannelConfig channelConfig);

  void logGsmUnitChangedGroup(ChannelUID gsmUnit, GSMGroup group);

  void logGsmUnitLock(ChannelUID gsmUnit, boolean lock, String lockReason);

  void logGSMUnitReleased(ChannelUID gsmUnit);

  void logSIMTakenFromSimServer(ChannelUID simUnit, ICCID simUid);

  void logSIMChangedEnableStatus(ChannelUID simUnit, boolean enable);

  void logSIMChangedGroup(ChannelUID simUnit, String group);

  void logSIMReturnedToSimServer(ChannelUID simUnit);

  void logCallChannelCreated(ChannelUID gsmUnit, ChannelUID simUnit);

  void logCallChannelChangedState(ChannelUID simUnit, CallChannelState.State state, String advInfo, long periodPrediction);

  void logCallChannelChangedAdvInfo(ChannelUID simUnit, String advInfo);

  void logGsmUnitChangedState(ChannelUID gsmUnit, CallChannelState.State state, String advInfo, long periodPrediction);

  void logCallChannelStartActivity(ChannelUID simUnit);

  void logCallChannelReleased(ChannelUID simUnit);

  void logCallStarted(ChannelUID simUnit, PhoneNumber phoneNumber);

  void logCallChangeState(ChannelUID simUnit, CallState.State callState);

  void logCallEnded(ChannelUID simUnit, long duration);

  void logCallChannelSendUSSD(ChannelUID simUnit, String ussd, String response);

  void logCallChannelSuccessSentSMS(ChannelUID simUnit, String number, String text, int parts);

  void logCallChannelFailSentSMS(ChannelUID simUnit, String number, String text, int totalSmsParts, int successSendSmsParts);

  void logCallChannelReceivedSMS(ChannelUID simUnit, String number, String text, final int parts);

  void logSignalQualityChange(ChannelUID gsmUnit, int signalStrength, int bitErrorRate);

  void logGSMNetworkInfoChange(ChannelUID gsmUnit, GSMNetworkInfo info);

  void logPdd(ChannelUID simUnit, long pdd);

  void resetStatistic();

  void logGsmUnitLockToArfcn(ChannelUID channel, Integer arfcn);

  void logCallSetup(ChannelUID simUID, PhoneNumber phoneNumber);

  void logCallRelease(ChannelUID simUID);

}

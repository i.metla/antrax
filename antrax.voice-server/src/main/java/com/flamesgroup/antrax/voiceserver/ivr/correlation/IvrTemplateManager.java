/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.voiceserver.ivr.correlation;

import com.flamesgroup.antrax.voiceserver.properties.VoiceServerPropUtils;
import com.flamesgroup.properties.IReloadListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class IvrTemplateManager implements IReloadListener {

  private static final Logger logger = LoggerFactory.getLogger(IvrTemplateManager.class);

  private final Lock ivrTemplatesLock = new ReentrantLock();

  private Map<String, List<IvrTemplate>> ivrTemplatesMap = new HashMap<>();

  public IvrTemplateManager() {
    reload();
  }

  public List<IvrTemplate> getIvrTemplates(final String simGroupName) {
    List<IvrTemplate> ivrTemplates;
    ivrTemplatesLock.lock();
    try {
      ivrTemplates = ivrTemplatesMap.get(simGroupName);
    } finally {
      ivrTemplatesLock.unlock();
    }

    if (ivrTemplates == null) {
      return Collections.emptyList();
    } else {
      return ivrTemplates;
    }
  }

  @Override
  public String getName() {
    return "IvrTemplateManager";
  }

  @Override
  public boolean reload() {
    if (!VoiceServerPropUtils.getInstance().getVoiceServerProperties().getIvrCorrelationEnable()) {
      return true;
    }

    Path ivrTemplatesPath = Paths.get(VoiceServerPropUtils.getInstance().getVoiceServerProperties().getIvrTemplatesPath());
    try {
      logger.debug("[{}] - loading ivr templates by path {}", this, ivrTemplatesPath);
      long maxTemplateLengthOfTime = VoiceServerPropUtils.getInstance().getVoiceServerProperties().getIvrTemplatesMaxLengthTime();
      Map<String, List<IvrTemplate>> ivrTemplatesLocal = IvrTemplateLoader.loadIvrTemplates(ivrTemplatesPath, (int) TimeUnit.MILLISECONDS.toSeconds(maxTemplateLengthOfTime));

      ivrTemplatesLock.lock();
      try {
        ivrTemplatesMap = ivrTemplatesLocal;
      } finally {
        ivrTemplatesLock.unlock();
      }
    } catch (IOException e) {
      logger.error("[{}] - can't load ivr templates by path {}", this, ivrTemplatesPath, e);
      return false;
    }

    return true;
  }

}

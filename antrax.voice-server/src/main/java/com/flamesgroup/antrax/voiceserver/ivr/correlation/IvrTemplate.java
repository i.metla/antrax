/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.voiceserver.ivr.correlation;

import com.flamesgroup.jiax2.CallDropReason;

public final class IvrTemplate {

  public static final int BIT_RATE_IN_BYTES = 8000 * 16 * 1 / 8; // sample rate * bit depth * channels / byte length

  private final String name;
  private final CallDropReason callDropReason;
  private final ICorrelationData correlationData;

  public IvrTemplate(final String name, final CallDropReason callDropReason, final ICorrelationData correlationData) {
    this.name = name;
    this.callDropReason = callDropReason;
    this.correlationData = correlationData;
  }

  public String getName() {
    return name;
  }

  public CallDropReason getCallDropReason() {
    return callDropReason;
  }

  public ICorrelationData getCorrelationData() {
    return correlationData;
  }

}

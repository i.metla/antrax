/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.voiceserver.states;

import com.flamesgroup.antrax.activity.IRemoteHistoryLogger;
import com.flamesgroup.antrax.activity.SimEvent;
import com.flamesgroup.antrax.storage.enums.CallType;
import com.flamesgroup.antrax.storage.state.CallChannelState;
import com.flamesgroup.antrax.timemachine.ConditionalStateImpl;
import com.flamesgroup.antrax.timemachine.State;
import com.flamesgroup.antrax.voiceserver.CallChannel;
import com.flamesgroup.antrax.voiceserver.CallChannelPool;
import com.flamesgroup.antrax.voiceserver.SilentMediaStream;
import com.flamesgroup.antrax.voiceserver.TerminatingCallHandlerAdapter;
import com.flamesgroup.jiax2.CallDropReason;
import com.flamesgroup.jiax2.CallState;
import com.flamesgroup.jiax2.IMediaStream;
import com.flamesgroup.jiax2.ITerminatingCall;
import com.flamesgroup.media.codec.audio.IAudioCodec;
import com.flamesgroup.unit.PhoneNumber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SelfCallState extends ConditionalStateImpl {

  private final Logger logger = LoggerFactory.getLogger(SelfCallState.class);

  private volatile boolean finished = false;
  private volatile boolean answered = false;

  private final CallChannel cc;
  private final CallChannelPool callChannelPool;
  private final IRemoteHistoryLogger historyLogger;

  private State idleAfterSelfCallState;

  public SelfCallState(final CallChannel cc, final CallChannelPool callChannelPool, final IRemoteHistoryLogger historyLogger) {
    this.cc = cc;
    this.callChannelPool = callChannelPool;
    this.historyLogger = historyLogger;
  }

  @Override
  public boolean isFinished() {
    return finished || (answered && cc.isIncomingCallReleased());
  }

  @Override
  public void enterState() {
    cc.changeChannelState(CallChannelState.State.WAITING_SELF_CALL, -1);
    cc.getSIMUnit().handleEvent(SimEvent.selfCallExpectation());
    final PhoneNumber phoneNumber;
    phoneNumber = cc.getSIMUnit().getSimData().getPhoneNumber();
    if (phoneNumber == null) {
      logger.warn("[{}] - {}: Self phone number is absent. Self calling will not be provided", this, cc);
      finished = true;
      return;
    }

    TerminatingCallHandlerAdapter adapter = new TerminatingCallHandlerAdapter(new SelfTerminatingCall(phoneNumber.getValue()), CallType.VS_TO_VS, callChannelPool, historyLogger);
    new Thread(adapter, "SelfCall(" + cc.getGSMUnit().getChannelUID() + ":" + cc.getSIMUnit().getChannelUID() + ")").start();
  }

  @Override
  public State getNextState() {
    return idleAfterSelfCallState;
  }

  public void initialize(final StatesBuilder b) {
    idleAfterSelfCallState = b.getIdleAfterSelfCallState();
  }

  private class SelfTerminatingCall implements ITerminatingCall {

    private final String called;

    public SelfTerminatingCall(final String called) {
      this.called = called;
    }

    @Override
    public void ringing() {
      logger.debug("[{}] - selfCall to {} got RINGING", this, cc);
    }

    @Override
    public IMediaStream getAudioMediaStream() {
      return new SilentMediaStream();
    }

    @Override
    public long getMediaFormat() {
      throw new UnsupportedOperationException();
    }

    @Override
    public IAudioCodec getAudioCodec() {
      throw new UnsupportedOperationException();
    }

    @Override
    public CallState getState() {
      throw new UnsupportedOperationException();
    }

    @Override
    public void answer() {
      logger.debug("[{}] - selfCall to {} got ANSWER", this, cc);
      answered = true;
    }

    @Override
    public String toString() {
      return "SelfCall(" + cc + ")";
    }

    @Override
    public String getCalled() {
      return called;
    }

    @Override
    public String getCalledContext() {
      throw new UnsupportedOperationException();
    }

    @Override
    public String getCaller() {
      return called;
    }

    @Override
    public void hangup(final CallDropReason callDropReason) {
      logger.debug("[{}] - selfCall to {} got DROP", this, cc);
      finished = true;
    }

    @Override
    public void sendDtmf(final char dtmf) {
    }

  }

}

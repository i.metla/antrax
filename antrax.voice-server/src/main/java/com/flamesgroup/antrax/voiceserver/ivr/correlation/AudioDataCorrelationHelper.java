/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.voiceserver.ivr.correlation;

public final class AudioDataCorrelationHelper {

  private static final int SILENCE_LEVEL = 150;

  private AudioDataCorrelationHelper() {
  }

  public static double avgAsAbsShort(final byte[] data, int offset, final int length) {
    assert length % 2 == 0;

    int n = length / 2;

    double r = 0;
    for (int i = 0; i < n; i++) {
      byte b0 = data[offset++];
      byte b1 = data[offset++];
      short v = (short) ((b0 & 0xFF) | (b1 & 0xFF) << 8);
      r += Math.abs(v);
    }
    r /= n;
    return r;
  }

  public static boolean silenceDetect(final byte[] data, int offset, final int length) {
    for (int i = 0; i < length; i += 2) {
      byte b0 = data[offset++];
      byte b1 = data[offset++];
      short v = (short) ((b0 & 0xFF) | (b1 & 0xFF) << 8);
      int absV = Math.abs(v);
      if (absV > SILENCE_LEVEL) {
        return true;
      }
    }
    return false;
  }

}

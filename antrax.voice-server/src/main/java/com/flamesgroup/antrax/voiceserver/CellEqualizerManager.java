/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.voiceserver;

import com.flamesgroup.antrax.activity.IGsmViewManager;
import com.flamesgroup.antrax.control.communication.ControlServerPingException;
import com.flamesgroup.antrax.storage.commons.impl.CellEqualizerAlgorithm;
import com.flamesgroup.antrax.storage.commons.impl.GsmView;
import com.flamesgroup.antrax.voiceserver.channels.GSMUnit;
import com.flamesgroup.antrax.voiceserver.channels.error.GsmHardwareError;
import com.flamesgroup.antrax.voiceserver.manager.rtstat.ChannelsRuntime;
import com.flamesgroup.antrax.voiceserver.properties.VoiceServerPropUtils;
import com.flamesgroup.utils.AntraxProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

public class CellEqualizerManager {

  private static final Logger logger = LoggerFactory.getLogger(CellEqualizerManager.class);

  private final IGsmViewManager gsmViewManager;
  private final List<GSMUnit> gsmUnits;
  private final ChannelsRuntime channelsRuntime;
  private final Map<Integer, Integer> serverCellAdjustments = new ConcurrentHashMap<>();
  private final AtomicLong lastTimeUpdateCellAdjustments = new AtomicLong(0);

  private CellEqualizerAlgorithm cellEqualizerAlgorithm;

  public CellEqualizerManager(final IGsmViewManager gsmViewManager, final List<GSMUnit> gsmUnits, final ChannelsRuntime channelsRuntime) {
    this.gsmViewManager = gsmViewManager;
    this.gsmUnits = gsmUnits;
    this.channelsRuntime = channelsRuntime;
  }

  public void start() {
    new CellEqualizerService().start();
  }

  public Map<Integer, Integer> getServerCellAdjustments() {
    return serverCellAdjustments;
  }

  public ChannelsRuntime getChannelsRuntime() {
    return channelsRuntime;
  }

  private class CellEqualizerService extends Thread {

    private final Long WAITING_TIME = TimeUnit.SECONDS.toMillis(10);

    public CellEqualizerService() {
      setName(CellEqualizerService.class.getSimpleName());
      setDaemon(true);
    }

    @Override
    public void run() {
      while (true) {
        try {
          updateCellAdjustments();
        } catch (RemoteException e) {
          logger.error("[{}] - can't updateCellAdjustments", this, e);
          throw new ControlServerPingException(e);
        }

        try {
          Thread.sleep(WAITING_TIME);
        } catch (InterruptedException e) {
          logger.error("[{}] - can't waiting", this, e);
          return;
        }

        if (serverCellAdjustments.isEmpty()) {
          continue;
        }

        for (GSMUnit gsmUnit : gsmUnits) {
          if (gsmUnit.isConnect()) {
            try {
              gsmUnit.executeCellEqualizer(lastTimeUpdateCellAdjustments.get(), cellEqualizerAlgorithm);
            } catch (GsmHardwareError e) {
              logger.error("[{}] - can't executeCellEqualizer", this, e);
            }
          }
        }
      }
    }

    private void updateCellAdjustments() throws RemoteException {
      if (VoiceServerPropUtils.getInstance().getVoiceServerProperties().getCellEqualizerEnabled()
          && (serverCellAdjustments.isEmpty() || gsmViewManager.isUpdateCellAdjustments(AntraxProperties.SERVER_NAME))) {
        cellEqualizerAlgorithm = gsmViewManager.getCellEqualizerAlgorithm(AntraxProperties.SERVER_NAME);
        List<GsmView> gsmViews = gsmViewManager.listGsmView(AntraxProperties.SERVER_NAME);
        for (GsmView gsmView : gsmViews) {
          if (gsmView.isServingCell()) {
            serverCellAdjustments.remove(gsmView.getCellKey().getArfcn());
          } else {
            serverCellAdjustments.put(gsmView.getCellKey().getArfcn(), -60);
          }
        }
        lastTimeUpdateCellAdjustments.set(System.currentTimeMillis());
        gsmViewManager.setServerForUpdateCellAdjustments(AntraxProperties.SERVER_NAME, false);
      }
    }

  }

}

/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.voiceserver.channels;

import com.flamesgroup.antrax.storage.commons.impl.CellEqualizerAlgorithm;
import com.flamesgroup.antrax.storage.commons.impl.CellEqualizerRandomAlgorithm;
import com.flamesgroup.antrax.voiceserver.CellEqualizerManager;
import com.flamesgroup.antrax.voiceserver.channels.error.GsmHardwareError;
import com.flamesgroup.unit.CellInfo;

import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class CellEqualizerRandomAlgorithmExecutor extends CellEqualizerExecutor {

  private static final int DESTINATION_RX_LEV_DELTA = 10;

  private final ProgressiveCellEqualizerData progressiveCellEqualizerData = new ProgressiveCellEqualizerData();
  private long nextRegeneratedTime;

  private ProgressiveCellEqualizer progressiveCellEqualizer;

  public CellEqualizerRandomAlgorithmExecutor(final GSMUnit gsmUnit, final CellEqualizerManager cellEqualizerManager) {
    super(gsmUnit, cellEqualizerManager);
  }

  @Override
  public void execute(final CellEqualizerAlgorithm cellEqualizerAlgorithm) throws GsmHardwareError {
    if (lastCellInfos == null) {
      return;
    }

    if (System.currentTimeMillis() >= nextRegeneratedTime) {
      CellEqualizerRandomAlgorithm cellEqualizerRandomAlgorithm = (CellEqualizerRandomAlgorithm) cellEqualizerAlgorithm;

      Map<Integer, Integer> serverCellAdjustments = cellEqualizerManager.getServerCellAdjustments();
      List<CellInfo> cellInfos = Arrays.stream(lastCellInfos)
          .filter(cellInfo -> cellInfo.getCellId() > 0 && cellInfo.getLac() > 0 && !serverCellAdjustments.containsKey(cellInfo.getArfcn()))
          .collect(Collectors.toList());

      if (cellInfos.size() <= 1) {
        return;
      }

      logger.debug("[{}] - start executeCellEqualizer on [{}]", this, cellEqualizerRandomAlgorithm);

      terminate();

      Random random = new Random();
      int randomNextTime = random.nextInt(cellEqualizerRandomAlgorithm.getConfiguration().getMaxRegeneratedTime() - cellEqualizerRandomAlgorithm.getConfiguration().getMinRegeneratedTime() + 1)
          + cellEqualizerRandomAlgorithm.getConfiguration().getMinRegeneratedTime();
      nextRegeneratedTime = System.currentTimeMillis() + TimeUnit.MINUTES.toMillis(randomNextTime);

      LocalTime nowTime = LocalTime.now();
      LocalTime fromLeadTime = cellEqualizerRandomAlgorithm.getConfiguration().getFromLeadTime();
      LocalTime toLeadTime = cellEqualizerRandomAlgorithm.getConfiguration().getToLeadTime();

      if (fromLeadTime == null || toLeadTime == null) {
        logger.warn("[{}] - please set fromLeadTime and toLeadTime", this);
        return;
      }

      //check lead time
      if (!(fromLeadTime.isBefore(nowTime) && toLeadTime.isAfter(nowTime))) {
        logger.info("[{}] - lead time is different from now time, info: [{}]", this, getInfo());
        return;
      }

      if (progressiveCellEqualizerData.getCurrentArfcn() < 0) {
        progressiveCellEqualizerData.setDestinationRxLev(cellInfos.get(0).getRxLev() + DESTINATION_RX_LEV_DELTA);
      }

      int posAtCellInfo = random.nextInt(cellInfos.size());
      progressiveCellEqualizerData.setCurrentArfcn(cellInfos.get(posAtCellInfo).getArfcn());
      progressiveCellEqualizerData.setCurrentRxLev(cellInfos.get(posAtCellInfo).getRxLev());

      if (progressiveCellEqualizerData.getPrevArfcn() == progressiveCellEqualizerData.getCurrentArfcn()) {
        logger.debug("[{}] - stay on serving Cell", this);
        return;
      }
      progressiveCellEqualizerData.setMinWaitingTime(cellEqualizerRandomAlgorithm.getConfiguration().getMinWaitingTime());
      progressiveCellEqualizerData.setMaxWaitingTime(cellEqualizerRandomAlgorithm.getConfiguration().getMaxWaitingTime());

      logger.debug("[{}] - start progressive cellEqualizer with info: [{}]", this, getInfo());
      progressiveCellEqualizer = new ProgressiveCellEqualizer(gsmUnit, cellEqualizerManager, progressiveCellEqualizerData);
      progressiveCellEqualizer.start();
    }
  }

  @Override
  public void terminate() {
    if (progressiveCellEqualizer != null && progressiveCellEqualizer.isAlive()) {
      progressiveCellEqualizer.terminate();
      try {
        progressiveCellEqualizer.join();
      } catch (InterruptedException ignore) {
      }
      logger.debug("[{}] - terminate thread: [{}]", this, progressiveCellEqualizer);
    }
  }

  private String getInfo() {
    SimpleDateFormat dt = new SimpleDateFormat("YYYY-MM-dd'T'HH:mm:ss.SSSXXX");
    return "nextRegeneratedTime:" + dt.format(new Date(nextRegeneratedTime)) +
        " progressiveCellEqualizerData:" + progressiveCellEqualizerData +
        " lastCellInfos:" + Arrays.toString(lastCellInfos);
  }

}

/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import com.flamesgroup.antrax.automation.meta.JavaFileName;
import com.flamesgroup.antrax.automation.meta.ScriptDefinition;
import com.flamesgroup.antrax.automation.meta.ScriptDefinitionImpl;
import com.flamesgroup.antrax.automation.meta.ScriptInstance;
import com.flamesgroup.antrax.automation.meta.ScriptParameterDefinition;
import com.flamesgroup.antrax.automation.meta.ScriptParameterDefinitionImpl;
import com.flamesgroup.antrax.automation.meta.ScriptType;
import com.flamesgroup.antrax.commons.impl.InvariableTimeout;
import com.flamesgroup.antrax.commons.impl.VariableTimeout;
import com.flamesgroup.antrax.control.authorization.NotPermittedException;
import com.flamesgroup.antrax.control.communication.ConfigurationBean;
import com.flamesgroup.antrax.control.communication.StorageException;
import com.flamesgroup.antrax.control.communication.TransactionException;
import com.flamesgroup.antrax.control.guiclient.MainApp;
import com.flamesgroup.antrax.control.swingwidgets.table.ExportException;
import com.flamesgroup.antrax.storage.commons.SIMGroup;
import com.flamesgroup.antrax.storage.commons.impl.SIMGroupImpl;
import com.flamesgroup.antrax.storage.commons.impl.ScriptCommons;
import mockit.Expectations;
import mockit.Mocked;
import mockit.integration.junit4.JMockit;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Properties;

@RunWith(JMockit.class)
public class SimGroupExporterTest {

  @Mocked
  private ConfigurationBean silentBean;

  private final int transactionId = 1;

  private ScriptCommons createScriptCommon(final String name) {
    ScriptCommons retval = new ScriptCommons();
    retval.setName(name);
    retval.setValue("hello");
    return retval;
  }

  private ScriptDefinition createScriptDefintion(final String defName, final ScriptType scriptType) {
    ScriptParameterDefinition paramDef = new ScriptParameterDefinitionImpl("param", "setParam", "doc", "int", false, null);
    return new ScriptDefinitionImpl(new JavaFileName("test"), defName, "description", scriptType, paramDef);
  }

  private ScriptInstance createScript(final String defName, final ScriptType scriptType) {
    ScriptDefinition def = createScriptDefintion(defName, scriptType);
    ScriptInstance retval = def.createInstance();
    retval.setParameter("param", 10, SimGroupExporterTest.class.getClassLoader());
    return retval;
  }

  private ClassLoader getClassLoader() {
    return getClass().getClassLoader();
  }

  private void assertExport(final SIMGroupImpl simGroup, final String expected, final String propName) {
    Properties props = SimGroupExporter.export(simGroup);
    assertEquals(expected, props.getProperty(propName, "NO_PROP"));
  }

  @Test
  public void exportString() {
    SIMGroupImpl simGroup = new SIMGroupImpl();
    simGroup.setName("testName");
    assertExport(simGroup, "testName", "Name");
  }

  @Test
  public void exportInt() {
    SIMGroupImpl simGroup = new SIMGroupImpl();
    simGroup.setSelfDialChance(500);
    assertExport(simGroup, "500", "SelfDialChance");
  }

  @Test
  public void exportBoolean() {
    SIMGroupImpl simGroup = new SIMGroupImpl();
    simGroup.setCanBeAppointed(true);
    assertExport(simGroup, "true", "CanBeAppointed");
  }

  @Test
  public void exportTimeout() {
    SIMGroupImpl simGroup = new SIMGroupImpl();
    simGroup.setIdleAfterRegistration(new VariableTimeout(5, 3));
    assertExport(simGroup, "3:5", "IdleAfterRegistration");
  }

  @Test
  public void exportScript() {
    SIMGroupImpl simGroup = new SIMGroupImpl();
    simGroup.setSessionScript(createScript("SessionScript", ScriptType.ACTIVITY_PERIOD));
    Properties props = SimGroupExporter.export(simGroup);
    assertEquals("SessionScript", props.getProperty("SessionScript", "NO_PROP"));
    assertNotNull(props.getProperty("SessionScript.param"));
  }

  @Test
  public void exportScripts() {
    SIMGroupImpl simGroup = new SIMGroupImpl();
    ScriptInstance[] scripts = {
    /*  */createScript("BusinessOne", ScriptType.BUSINESS_ACTIVITY),
    /*  */createScript("BusinessTwo", ScriptType.BUSINESS_ACTIVITY)};
    simGroup.setBusinessActivityScripts(scripts);
    Properties props = SimGroupExporter.export(simGroup);
    assertEquals("BusinessOne", props.getProperty("BusinessActivityScripts.0", "NO_PROP"));
    assertNotNull(props.getProperty("BusinessActivityScripts.0.param"));
    assertEquals("BusinessTwo", props.getProperty("BusinessActivityScripts.1", "NO_PROP"));
    assertNotNull(props.getProperty("BusinessActivityScripts.1.param"));
  }

  @Test
  public void exportScriptCommons() {
    SIMGroupImpl simGroup = new SIMGroupImpl();
    simGroup.setScriptCommons(new ScriptCommons[] {createScriptCommon("first"), createScriptCommon("second")});
    Properties props = SimGroupExporter.export(simGroup);
    assertEquals("first:-0x54 -0x13 0x00 0x05 0x74 0x00 0x05 0x68 0x65 0x6c 0x6c 0x6f", props.getProperty("ScriptCommons.0", "NO_PROP"));
    assertNotNull(props.getProperty("ScriptCommons.0"));
    assertEquals("second:-0x54 -0x13 0x00 0x05 0x74 0x00 0x05 0x68 0x65 0x6c 0x6c 0x6f", props.getProperty("ScriptCommons.1", "NO_PROP"));
    assertNotNull(props.getProperty("ScriptCommons.1"));
  }

  @Test
  public void importString() throws ExportException {
    SIMGroupImpl simGroup = new SIMGroupImpl();
    simGroup.setName("testName");
    Properties props = SimGroupExporter.export(simGroup);
    SIMGroup imported = SimGroupExporter.importSimGroup(props, getClassLoader(), createBeansPool(silentBean), transactionId);
    assertEquals("testName", imported.getName());
  }

  @Test
  public void importInt() throws ExportException {
    SIMGroupImpl simGroup = new SIMGroupImpl();
    simGroup.setSelfDialChance(50);
    Properties props = SimGroupExporter.export(simGroup);
    SIMGroup imported = SimGroupExporter.importSimGroup(props, getClassLoader(), createBeansPool(silentBean), transactionId);
    assertEquals(50, imported.getSelfDialChance());
  }

  @Test
  public void importTimeout() throws ExportException {
    SIMGroupImpl simGroup = new SIMGroupImpl();
    simGroup.setIdleBeforeUnregistration(new InvariableTimeout(10, 5));
    Properties props = SimGroupExporter.export(simGroup);
    SIMGroup imported = SimGroupExporter.importSimGroup(props, getClassLoader(), createBeansPool(silentBean), transactionId);
    assertEquals(new InvariableTimeout(10, 5), imported.getIdleBeforeUnregistration());
  }

  @Test
  public void importBoolean() throws ExportException {
    SIMGroupImpl simGroup = new SIMGroupImpl();
    simGroup.setSuppressIncomingCallSignal(false);
    Properties props = SimGroupExporter.export(simGroup);
    SIMGroup imported = SimGroupExporter.importSimGroup(props, getClassLoader(), createBeansPool(silentBean), transactionId);
    assertEquals(false, imported.isSuppressIncomingCallSignal());
  }

  @Test
  public void importScriptInstance(@Mocked final ConfigurationBean cfgBean) throws ExportException, StorageException, NotPermittedException {
    new Expectations() {{
      cfgBean.getScriptDefinition(MainApp.clientUID, "SessionScript");
      result = createScriptDefintion("SessionScript", ScriptType.ACTIVITY_PERIOD);
    }};

    SIMGroupImpl simGroup = new SIMGroupImpl();
    simGroup.setSessionScript(createScript("SessionScript", ScriptType.ACTIVITY_PERIOD));
    Properties props = SimGroupExporter.export(simGroup);
    SIMGroup imported = SimGroupExporter.importSimGroup(props, getClassLoader(), createBeansPool(cfgBean), transactionId);
    Assert.assertEquals("SessionScript", imported.getSessionScript().getDefinition().getName());
    assertEquals(10, imported.getSessionScript().getParameter("param").getValue(getClassLoader()));
  }

  @Test
  public void importScriptInstances(@Mocked final ConfigurationBean cfgBean) throws ExportException, StorageException, NotPermittedException {
    new Expectations() {{
      cfgBean.getScriptDefinition(MainApp.clientUID, "BusinessScript1");
      result = createScriptDefintion("BusinessScript1", ScriptType.BUSINESS_ACTIVITY);
      cfgBean.getScriptDefinition(MainApp.clientUID, "BusinessScript2");
      result = createScriptDefintion("BusinessScript2", ScriptType.BUSINESS_ACTIVITY);
    }};

    SIMGroupImpl simGroup = new SIMGroupImpl();
    simGroup.setBusinessActivityScripts(new ScriptInstance[] {createScript("BusinessScript1", ScriptType.BUSINESS_ACTIVITY), createScript("BusinessScript2", ScriptType.BUSINESS_ACTIVITY)});
    Properties props = SimGroupExporter.export(simGroup);
    SIMGroup imported = SimGroupExporter.importSimGroup(props, getClassLoader(), createBeansPool(cfgBean), transactionId);
    assertEquals(2, imported.getBusinessActivityScripts().length);
    Assert.assertEquals("BusinessScript1", imported.getBusinessActivityScripts()[0].getDefinition().getName());
    Assert.assertEquals("BusinessScript2", imported.getBusinessActivityScripts()[1].getDefinition().getName());
  }

  @Test
  public void importScriptCommons() throws ExportException {
    SIMGroupImpl simGroup = new SIMGroupImpl();
    simGroup.setScriptCommons(new ScriptCommons[] {createScriptCommon("name1"), createScriptCommon("name2")});
    Properties props = SimGroupExporter.export(simGroup);
    SIMGroup imported = SimGroupExporter.importSimGroup(props, getClassLoader(), createBeansPool(silentBean), transactionId);
    assertEquals(2, imported.listScriptCommons().length);
    assertEquals("name1", imported.listScriptCommons()[0].getName());
    assertEquals("name2", imported.listScriptCommons()[1].getName());
  }

  private BeansPool createBeansPool(final ConfigurationBean cfgBeanMock) {
    new Expectations() {{
      try {
        cfgBeanMock.createSIMGroup(MainApp.clientUID, transactionId);
      } catch (NotPermittedException | TransactionException | StorageException e) {
      }
      result = new SIMGroupImpl();
    }};

    return new BeansPool(cfgBeanMock, null, null, null, null, null, null);
  }

}

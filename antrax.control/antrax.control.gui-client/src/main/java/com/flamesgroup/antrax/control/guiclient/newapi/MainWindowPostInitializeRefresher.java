/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.newapi;

import com.flamesgroup.antrax.control.guiclient.TransactionBar;
import com.flamesgroup.antrax.control.guiclient.utils.BeansPool;
import com.flamesgroup.antrax.control.guiclient.utils.Refresher;
import com.flamesgroup.antrax.control.guiclient.utils.RefresherThread;
import com.flamesgroup.antrax.control.guiclient.utils.TransactionManager;

import java.awt.*;

import javax.swing.*;
import javax.swing.border.LineBorder;

public class MainWindowPostInitializeRefresher extends Refresher {

  private final JPanel progressPanel;
  private final JPanel rightPanel;
  private final JProgressBar progressBar;

  private final AppMenuTree appMenuTree;
  private final RefresherThread refresherThread;
  private final JPanel leftPanel;
  private final Frame rootFrame;
  private final TransactionManager transactionManager;

  public MainWindowPostInitializeRefresher(final RefresherThread refresherThread, final TransactionManager transactionManager, final AppMenuTree appMenuTree, final JPanel rightPanel,
      final CardLayout cardLayout, final JPanel
      leftPanel, final Frame rootFrame) {
    super("MainWindowPostInitialize");
    this.rightPanel = rightPanel;
    this.appMenuTree = appMenuTree;
    this.refresherThread = refresherThread;
    this.transactionManager = transactionManager;
    this.leftPanel = leftPanel;
    this.rootFrame = rootFrame;
    progressBar = new JProgressBar();
    progressPanel = createProgressPanel();
    rightPanel.add(progressPanel, "progress");
    cardLayout.show(rightPanel, "progress");
  }

  private JPanel createProgressPanel() {
    JPanel progressPanel = new JPanel();
    progressPanel.add(progressBar);

    JPanel retval = new JPanel();
    retval.add(new JLabel("Initializing: "));
    retval.add(progressPanel);
    return retval;
  }

  @Override
  public void handleDisabled(final BeansPool beansPool) {

  }

  @Override
  public void handleEnabled(final BeansPool beansPool) {
  }

  @Override
  protected void refreshInformation(final BeansPool beansPool) throws Exception {
    new AppMenuBuilder().buildMenu(appMenuTree.getMenuNode(), beansPool);
    progressBar.setMaximum(appMenuTree.getMenuNode().getRecursiveChildCount());

    requestPanelCreation(appMenuTree.getMenuNode());
  }

  private void requestPanelCreation(final AppMenuNode menuNode) {
    menuNode.getPanel();
    progressBar.setValue(progressBar.getValue() + 1);
    for (int i = 0; i < menuNode.getChildCount(); ++i) {
      requestPanelCreation(menuNode.getChild(i));
    }
  }

  @Override
  public void updateGuiElements() {
    refresherThread.removeRefresher(this);
    refresherThread.addRefresher(new AppMenuTreeRefresher(appMenuTree));
    JScrollPane scrollBar = new JScrollPane(appMenuTree.getComponent());
    TransactionBar transactionBar = new TransactionBar(transactionManager, rootFrame);
    scrollBar.setBorder(new LineBorder(transactionBar.getBackground().darker()));
    transactionBar.setBackground(appMenuTree.getComponent().getBackground());
    leftPanel.add(scrollBar, BorderLayout.CENTER);
    leftPanel.add(transactionBar, BorderLayout.NORTH);
    rightPanel.remove(progressPanel);
    rightPanel.revalidate();

    this.interrupt();
    refresherThread.forceRefresh();
  }
}

/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.panels;

import com.flamesgroup.antrax.control.guiclient.utils.RefresherThread;

import java.awt.*;

public class BaseEditorPanelPreprocessor<T> {

  private final RefresherThread refresherThread;
  private final EditingPreprocessor<T> editingPreprocessor;

  public BaseEditorPanelPreprocessor(final RefresherThread refresherThread, final EditingPreprocessor<T> editingPreprocessor) {
    this.refresherThread = refresherThread;
    this.editingPreprocessor = editingPreprocessor;
  }

  public T beforeAdd(final T elem, final int transactionId, final Component invoker) throws OperationCanceledException {
    return editingPreprocessor.beforeAdd(elem, refresherThread, transactionId, invoker);
  }

  public void beforeRemove(final T elem, final int transactionId, final Component invoker) throws OperationCanceledException {
    editingPreprocessor.beforeRemove(elem, refresherThread, transactionId, invoker);
  }

  public void beforeUpdate(final T elem, final int transactionId, final Component invoker) throws OperationCanceledException {
    editingPreprocessor.beforeUpdate(elem, refresherThread, transactionId, invoker);
  }

}

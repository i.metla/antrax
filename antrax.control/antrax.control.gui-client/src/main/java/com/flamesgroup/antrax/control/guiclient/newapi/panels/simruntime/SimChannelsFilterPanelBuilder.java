/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.newapi.panels.simruntime;

import com.flamesgroup.antrax.control.communication.SimViewData;
import com.flamesgroup.antrax.control.guiclient.panels.SimChannelsTable;
import com.flamesgroup.antrax.control.swingwidgets.table.TableFilterPanelBuilder;
import com.flamesgroup.antrax.control.swingwidgets.table.UpdatableTableFilter;

import java.util.Arrays;
import java.util.Set;
import java.util.TreeSet;

import javax.swing.*;

public class SimChannelsFilterPanelBuilder {
  private final TableFilterPanelBuilder<SimViewData> builder;

  public SimChannelsFilterPanelBuilder(final SimChannelsTable table) {
    builder = new TableFilterPanelBuilder<>(table);
    builder.addBooleanFilter("requires pin", createNoPinFilter());
    builder.addBooleanFilter("no phone number", createNoPhoneNumberFilter());
    builder.addBooleanFilter("no group", createNoGroupFilter());
    builder.addBooleanFilter("locked", createLockedFilter());
    builder.addBooleanFilter("unlocked", createUnlockedFilter());
    builder.addBooleanFilter("empty", createEmptyFilter());
    builder.addBooleanFilter("unavailable", createUnLiveFilter());
  }

  private UpdatableTableFilter<SimViewData> createEmptyFilter() {
    return elem -> elem.isEmpty() && elem.isLive();
  }

  private UpdatableTableFilter<SimViewData> createUnLiveFilter() {
    return elem -> !elem.isLive();
  }

  private UpdatableTableFilter<SimViewData> createUnlockedFilter() {
    return elem -> !elem.isLocked();
  }

  private UpdatableTableFilter<SimViewData> createLockedFilter() {
    return SimViewData::isLocked;
  }

  private UpdatableTableFilter<SimViewData> createNoGroupFilter() {
    return elem -> elem.getSimGroupName() == null;
  }

  private UpdatableTableFilter<SimViewData> createNoPhoneNumberFilter() {
    return elem -> elem.getPhoneNumber() == null || elem.getPhoneNumber().isPrivate();
  }

  private UpdatableTableFilter<SimViewData> createNoPinFilter() {
    final Set<String> noPinStatuses = new TreeSet<>(Arrays.asList("REQUIRES PIN", "removing PIN", "WRONG PIN"));
    return elem -> elem.getStatus() != null && noPinStatuses.contains(elem.getStatus());
  }

  public JPanel getFiltersPanel() {
    return builder.getFilterPanel();
  }

}

/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.utils;

public class CharUtils {
  public static boolean isDigit(String str) {
    str = str.trim();
    try {
      Long.parseLong(str);
    } catch (RuntimeException ignored) {
      return false;
    } catch (Exception ignored) {
      return false;
    }
    for (int i = 0; i < str.length(); i++) {
      if (!Character.isDigit(str.charAt(i)))
        return false;
    }
    return true;
  }

  public static boolean isHasLetter(String str) {
    str = str.trim();
    for (int i = 0; i < str.length(); i++) {
      if (Character.isLetter(str.charAt(i)))
        return true;
    }
    return false;
  }
}

/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.newapi;

import javax.swing.*;
import javax.swing.tree.TreePath;

public class AppMenuTreeTransferHandler extends TransferHandler {
  private static final long serialVersionUID = -3888094650043334452L;

  @Override
  public boolean importData(final TransferSupport support) {
    JComponent panel = getPanelForImport(support);
    if (panel == null || panel.getTransferHandler() == null) {
      return false;
    }
    return panel.getTransferHandler().importData(support);
  }

  @Override
  public boolean canImport(final TransferSupport support) {
    JComponent panel = getPanelForImport(support);
    return panel != null && panel.getTransferHandler() != null && panel.getTransferHandler().canImport(support);
  }

  private JComponent getPanelForImport(final TransferSupport support) {
    AppMenuNode node = null;
    TreePath path;
    if (support.isDrop()) {
      path = ((JTree.DropLocation) support.getDropLocation()).getPath();
    } else {
      path = ((JTree) support.getComponent()).getSelectionPath();
    }

    if (path != null) {
      node = (AppMenuNode) path.getLastPathComponent();
    }

    if (node == null || node.getPanel() == null) {
      return null;
    }
    return node.getPanel().getComponent();
  }
}

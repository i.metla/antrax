/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.widgets.renderers;

import static com.flamesgroup.antrax.control.swingwidgets.IconPool.getShared;

import java.awt.*;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;

public class ChannelLiveRenderer extends DefaultTableCellRenderer {

  private static final long serialVersionUID = 5470721762072126364L;

  private static final Icon liveIcon = getShared("/img/channels/live.png");
  private static final Icon unLiveIcon = getShared("/img/channels/unlive.png");

  public ChannelLiveRenderer() {
    this.setHorizontalAlignment(SwingConstants.CENTER);
  }

  @Override
  public Component getTableCellRendererComponent(final JTable table, final Object value, final boolean isSelected, final boolean hasFocus, final int row, final int column) {
    Component cmp = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
    setIcon(null);
    if (value instanceof Boolean) {
      setText(null);
      Boolean bool = (Boolean) value;
      if (bool) {
        setIcon(liveIcon);
      } else {
        setIcon(unLiveIcon);
      }
    } else {
      setText(value.toString());
    }
    return cmp;
  }

}

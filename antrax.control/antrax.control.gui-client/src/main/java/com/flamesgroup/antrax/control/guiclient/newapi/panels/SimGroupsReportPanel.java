/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.newapi.panels;

import com.flamesgroup.antrax.control.GuiProperties;
import com.flamesgroup.antrax.control.communication.SimViewData;
import com.flamesgroup.antrax.control.communication.SimpleSimGroup;
import com.flamesgroup.antrax.control.guiclient.MainApp;
import com.flamesgroup.antrax.control.guiclient.newapi.AppPanel;
import com.flamesgroup.antrax.control.guiclient.utils.BeansPool;
import com.flamesgroup.antrax.control.guiclient.utils.Refresher;
import com.flamesgroup.antrax.control.guiclient.utils.RefresherThread;
import com.flamesgroup.antrax.control.guiclient.utils.TransactionManager;
import com.flamesgroup.antrax.control.guiclient.widgets.renderers.TimeoutTCRenderer;
import com.flamesgroup.antrax.control.swingwidgets.contextsearch.JContextSearch;
import com.flamesgroup.antrax.control.swingwidgets.editor.JReflectiveBar;
import com.flamesgroup.antrax.control.swingwidgets.table.ColumnWriter;
import com.flamesgroup.antrax.control.swingwidgets.table.JUpdatableTable;
import com.flamesgroup.antrax.control.swingwidgets.table.TableBuilder;
import com.flamesgroup.antrax.control.swingwidgets.table.TableFilterPanelBuilder;
import com.flamesgroup.antrax.control.swingwidgets.table.UpdatableTableFilter;
import com.flamesgroup.antrax.control.swingwidgets.table.UpdateTableColumnModel;
import com.flamesgroup.antrax.storage.commons.IServerData;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;

public class SimGroupsReportPanel extends JPanel implements AppPanel {

  private static final long serialVersionUID = 5203052095972550998L;
  private RefresherThread refresherThread;

  private final Refresher refresher = new Refresher("SimGroupsReportRefresher") {

    private volatile SimGroupsReportData[] reportsData;

    @Override
    public void updateGuiElements() {
      table.setData(reportsData);
      reportsData = null;
    }

    @Override
    protected void refreshInformation(final BeansPool beansPool) throws Exception {
      List<IServerData> simServers = beansPool.getControlBean().listSimServers();
      List<SimViewData> simViewData = new ArrayList<>();
      for (IServerData simServer : simServers) {
        simViewData.addAll(beansPool.getControlBean().getSimRuntimeData(simServer.getName()));
      }

      SimpleSimGroup[] groups = beansPool.getConfigBean().listSimpleSIMGroups(MainApp.clientUID);
      reportsData = SimGroupsReportData.collect(simViewData, groups);
    }

    @Override
    public void handleEnabled(final BeansPool beansPool) {
    }

    @Override
    public void handleDisabled(final BeansPool beansPool) {
    }
  };

  private final JUpdatableTable<SimGroupsReportData, String> table = new JUpdatableTable<>(createTableBuilder());
  private final TableFilterPanelBuilder<SimGroupsReportData> filterPanelBuilder = new TableFilterPanelBuilder<>(table);
  private final GuiProperties guiProperties;

  public SimGroupsReportPanel() {
    super(new BorderLayout());
    add(new JScrollPane(table), BorderLayout.CENTER);
    add(createToolbar(), BorderLayout.NORTH);
    table.setName(getClass().getSimpleName());
    table.getUpdatableTableProperties().restoreProperties();
    guiProperties = new GuiProperties(getClass().getSimpleName()) {

      private static final String FILTERS = "filters";

      @Override
      protected void restoreOutProperties() {
        String filtersProperty = properties.getProperty(FILTERS);
        if (filtersProperty != null) {
          Boolean[] filters = gson.fromJson(filtersProperty, Boolean[].class);
          int i = 0;
          for (Component component : filterPanelBuilder.getFilterPanel().getComponents()) {
            if (component instanceof JCheckBox) {
              ((JCheckBox) component).setSelected(filters[i]);
              i++;
            }
          }
        }
      }

      @Override
      protected void saveOutProperties() {
        List<Boolean> filters = new ArrayList<>();
        for (Component component : filterPanelBuilder.getFilterPanel().getComponents()) {
          if (component instanceof JCheckBox) {
            filters.add(((JCheckBox) component).isSelected());
          }
        }
        properties.setProperty(FILTERS, gson.toJson(filters));
      }
    };
    guiProperties.restoreProperties();
  }

  private JReflectiveBar createToolbar() {
    JReflectiveBar retval = new JReflectiveBar();
    JContextSearch contextSearch = new JContextSearch();
    contextSearch.registerSearchItem(table);
    retval.addToRight(contextSearch);
    filterPanelBuilder.addBooleanFilter("non zero groups", true, new UpdatableTableFilter<SimGroupsReportData>() {

      @Override
      public boolean isAccepted(final SimGroupsReportData elem) {
        return elem.total != 0;
      }
    });
    retval.addToLeft(filterPanelBuilder.getFilterPanel());
    return retval;
  }

  @Override
  public JComponent getComponent() {
    return this;
  }

  @Override
  public void postInitialize(final RefresherThread refresher, final TransactionManager transactionManager) {
    this.refresherThread = refresher;
  }

  @Override
  public void release() {
    refresher.interrupt();
    table.getUpdatableTableProperties().saveProperties();
    guiProperties.saveProperties();
  }

  @Override
  public void setActive(final boolean active) {
    if (active) {
      refresherThread.addRefresher(refresher);
    } else {
      refresherThread.removeRefresher(refresher);
    }

  }

  @Override
  public void setEditable(final boolean editable) {
  }

  private TableBuilder<SimGroupsReportData, String> createTableBuilder() {
    return new TableBuilder<SimGroupsReportData, String>() {

      @Override
      public String getUniqueKey(final SimGroupsReportData src) {
        if (src.groupName == null) {
          return "NO GROUP";
        }
        return src.groupName;
      }

      @Override
      public void buildRow(final SimGroupsReportData src, final ColumnWriter<SimGroupsReportData> dest) {
        dest.writeColumn(src.groupName, src.groupName);
        dest.writeColumn(src.total);
        dest.writeColumn(src.locked);
        dest.writeColumn(src.unlocked);
        dest.writeColumn(src.totalCallsCount);
        dest.writeColumn(src.successfulCallsCount);
        dest.writeColumn(src.callsDuration);
        dest.writeColumn(src.getAverageCallDuration());
        dest.writeColumn(src.totalIncomingCallsCount);
        dest.writeColumn(src.successfulIncomingCallsCount);
        dest.writeColumn(src.successOutgoingSmsCount);
        dest.writeColumn(src.totalOutgoingSmsCount);
        dest.writeColumn(src.totalIncomingSmsCount);
        dest.writeColumn(src.totalSmsStatuses);
      }

      @Override
      public void buildColumns(final UpdateTableColumnModel columns) {
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(SwingConstants.CENTER);
        TimeoutTCRenderer timeoutRenderer = new TimeoutTCRenderer();

        columns.addColumn("Group", String.class);
        columns.addColumn("Total", Integer.class).setRenderer(centerRenderer);
        columns.addColumn("Locked", Integer.class).setRenderer(centerRenderer);
        columns.addColumn("Unlocked", Integer.class).setRenderer(centerRenderer);
        columns.addColumn("Total Calls", Integer.class).setRenderer(centerRenderer);
        columns.addColumn("Successful Calls", Integer.class).setRenderer(centerRenderer);
        columns.addColumn("Calls Duration", Long.class).setRenderer(timeoutRenderer);
        columns.addColumn("ACD", Long.class).setRenderer(timeoutRenderer);
        columns.addColumn("Inc. Total Calls", Integer.class).setRenderer(centerRenderer);
        columns.addColumn("Inc. Successful Calls", Integer.class).setRenderer(centerRenderer);
        columns.addColumn("Success Sms", Integer.class).setRenderer(centerRenderer);
        columns.addColumn("Total Sms", Integer.class).setRenderer(centerRenderer);
        columns.addColumn("Inc. Total SMS", Integer.class).setRenderer(centerRenderer);
        columns.addColumn("Total SMS Statuses", Integer.class).setRenderer(centerRenderer);
      }
    };
  }

}

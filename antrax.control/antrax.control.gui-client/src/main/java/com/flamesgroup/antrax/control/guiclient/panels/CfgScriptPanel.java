/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.panels;

import com.flamesgroup.antrax.control.guiclient.MainApp;
import com.flamesgroup.antrax.control.guiclient.newapi.AppPanel;
import com.flamesgroup.antrax.control.guiclient.utils.BeansPool;
import com.flamesgroup.antrax.control.guiclient.utils.Refresher;
import com.flamesgroup.antrax.control.guiclient.utils.RefresherThread;
import com.flamesgroup.antrax.control.guiclient.utils.TransactionCallbackHandler;
import com.flamesgroup.antrax.control.guiclient.utils.TransactionManager;
import com.flamesgroup.antrax.control.guiclient.utils.TransactionState;
import com.flamesgroup.antrax.control.guiclient.utils.UserProperty;
import com.flamesgroup.antrax.control.swingwidgets.IconPool;
import com.flamesgroup.antrax.control.swingwidgets.MessageUtils;
import com.flamesgroup.antrax.control.swingwidgets.editor.JReflectiveBar;
import com.flamesgroup.antrax.control.swingwidgets.editor.JReflectiveButton;
import com.flamesgroup.antrax.plugins.core.ScriptsHelper;
import com.flamesgroup.utils.codebase.ScriptFile;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.StringTokenizer;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

public class CfgScriptPanel extends JPanel implements AppPanel {

  private static final long serialVersionUID = 5641498542237246169L;

  private final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-YYY HH:mm:ss");
  private Refresher refresher;

  private RefresherThread refresherThread;

  private TransactionManager transactionManager;

  private JButton btnImport;

  private JLabel scriptName;
  private JLabel scriptSize;
  private JLabel version;
  private JLabel checkSum;
  private JLabel compilationTime;
  private JLabel uploadTime;
  private JLabel fileCount;
  private JLabel classCount;

  private ScriptFile scriptFile;
  private volatile ImportJavaFilesHelper importFilesHelper;
  private final UserProperty sourceDirectory = new UserProperty(System.getProperty("user.home"), "scripts.source-dir");

  public CfgScriptPanel() {
    setLayout(new BorderLayout());
    add(createToolbar(), BorderLayout.NORTH);
    add(createInformation(), BorderLayout.WEST);
  }

  @Override
  public void setActive(final boolean active) {
    if (active) {
      refresherThread.addRefresher(refresher);
    } else {
      refresherThread.removeRefresher(refresher);
    }
  }

  private Refresher createRefresher() {
    return new Refresher("ScriptPanelRefresher") {

      @Override
      protected void refreshInformation(final BeansPool beansPool) throws Exception {
        sourceDirectory.syncWithRegistry(beansPool);
        try {
          if (importFilesHelper != null) {
            String versionOfNewScript = ScriptsHelper.getScriptsVersion(importFilesHelper.getJarFile());
            String versionOfAntrax = beansPool.getConfigBean().getVersionOfAntrax(MainApp.clientUID);
            if (versionOfNewScript != null && versionOfAntrax != null) {
              StringTokenizer stVersionOfNewScript = new StringTokenizer(versionOfNewScript, ".");
              StringTokenizer stVersionOfAntrax = new StringTokenizer(versionOfAntrax, ".");
              if (Integer.parseInt(stVersionOfNewScript.nextToken()) != Integer.parseInt(stVersionOfAntrax.nextToken())) {
                String message = "New scripts [" + versionOfNewScript + "] isn't for this version of antrax [" + versionOfAntrax + "]";
                MessageUtils.showError(CfgScriptPanel.this, "Error", message);
              } else if (Integer.valueOf(stVersionOfNewScript.nextToken()) > Integer.valueOf(stVersionOfAntrax.nextToken())) {
                String message = "Version of new scripts [" + versionOfNewScript + "] higher than current antrax version [" + versionOfAntrax + "]";
                MessageUtils.showError(CfgScriptPanel.this, "Error", message);
              } else {
                importFilesHelper.execute(beansPool, transactionManager);
                final ScriptConflictResolver conflictsResolver = importFilesHelper.getConflictsResolver();

                final AtomicBoolean resolved = new AtomicBoolean(false);

                SwingUtilities.invokeAndWait(() -> resolved.set(conflictsResolver.resolveConflicts()));

                if (resolved.get()) {
                  conflictsResolver.commit(beansPool, transactionManager);
                }
              }
            } else {
              MessageUtils.showInfo(CfgScriptPanel.this, "Can't get version of antrax", "Can't get version of antrax");
            }
          }
        } catch (Exception e) {
          MessageUtils.showError(CfgScriptPanel.this, e.getMessage(), e);
          if (transactionManager.isEditTransactionStarted()) {
            transactionManager.rollbackEditTransaction(new TransactionCallbackHandler() {
              @Override
              public void onTransactionActionSuccess(final TransactionState state) {
                MessageUtils.showInfo(CfgScriptPanel.this, "Transaction rollback", "Transaction rollback after Exception");
              }

              @Override
              public void onTransactionActionFailure(final TransactionState state, final java.util.List<Throwable> caught) {
                MessageUtils.showError(CfgScriptPanel.this, "Transaction error", caught.get(0));
              }

            });
          }
        } finally {
          importFilesHelper = null;
        }
        scriptFile = beansPool.getConfigBean().getScriptFile(MainApp.clientUID);
      }

      @Override
      public void updateGuiElements() {
        if (scriptFile != null) {
          scriptName.setText(scriptFile.getName());
          scriptSize.setText(objectToText(scriptFile.getData().length));
          version.setText(objectToText(scriptFile.getScriptFileInfo().getVersion()));
          checkSum.setText(objectToText(scriptFile.getScriptFileInfo().getCheckSum()));
          compilationTime.setText(objectToText(scriptFile.getScriptFileInfo().getCompilationTime()));
          uploadTime.setText(objectToText(scriptFile.getScriptFileInfo().getUploadTime()));
          fileCount.setText(objectToText(scriptFile.getScriptFileInfo().getFileCount()));
          classCount.setText(objectToText(scriptFile.getScriptFileInfo().getClassCount()));
        }
        importFilesHelper = null;
      }

      @Override
      public void handleEnabled(final BeansPool beansPool) {
      }

      @Override
      public void handleDisabled(final BeansPool beansPool) {
      }
    };
  }

  private String objectToText(final Object object) {
    return object == null ? "-" : object instanceof Date ? simpleDateFormat.format(object) : String.valueOf(object);
  }

  private JReflectiveBar createToolbar() {
    JReflectiveBar jReflectiveBar = new JReflectiveBar();
    jReflectiveBar.addToLeft(btnImport = createImportButton());
    return jReflectiveBar;
  }

  private JPanel createInformation() {
    JPanel backgroundPanel = new JPanel();
    JPanel columnPanel = new JPanel();
    backgroundPanel.add(columnPanel);
    BoxLayout backgroundBoxLayout = new BoxLayout(columnPanel, BoxLayout.X_AXIS);
    columnPanel.setLayout(backgroundBoxLayout);
    JPanel titlePanel = new JPanel();
    BoxLayout titleBoxLayout = new BoxLayout(titlePanel, BoxLayout.Y_AXIS);
    titlePanel.setLayout(titleBoxLayout);
    JPanel valuesPanel = new JPanel();
    BoxLayout valuesBoxLayout = new BoxLayout(valuesPanel, BoxLayout.Y_AXIS);
    valuesPanel.setLayout(valuesBoxLayout);

    columnPanel.add(titlePanel);
    columnPanel.add(Box.createRigidArea(new Dimension(20, 0)));
    columnPanel.add(valuesPanel);
    Dimension horizontalDimension = new Dimension(0, 5);
    titlePanel.add(new JLabel("File name:"));
    titlePanel.add(Box.createRigidArea(horizontalDimension));
    valuesPanel.add(scriptName = new JLabel(""));
    valuesPanel.add(Box.createRigidArea(horizontalDimension));
    titlePanel.add(new JLabel("Size:"));
    titlePanel.add(Box.createRigidArea(horizontalDimension));
    valuesPanel.add(scriptSize = new JLabel(""));
    valuesPanel.add(Box.createRigidArea(horizontalDimension));
    titlePanel.add(new JLabel("Version:"));
    titlePanel.add(Box.createRigidArea(horizontalDimension));
    valuesPanel.add(version = new JLabel(""));
    valuesPanel.add(Box.createRigidArea(horizontalDimension));
    titlePanel.add(new JLabel("Check sum"));
    titlePanel.add(Box.createRigidArea(horizontalDimension));
    valuesPanel.add(checkSum = new JLabel(""));
    valuesPanel.add(Box.createRigidArea(horizontalDimension));
    titlePanel.add(new JLabel("Compilation time:"));
    titlePanel.add(Box.createRigidArea(horizontalDimension));
    valuesPanel.add(compilationTime = new JLabel(""));
    valuesPanel.add(Box.createRigidArea(horizontalDimension));
    titlePanel.add(new JLabel("Upload time:"));
    titlePanel.add(Box.createRigidArea(horizontalDimension));
    valuesPanel.add(uploadTime = new JLabel(""));
    valuesPanel.add(Box.createRigidArea(horizontalDimension));
    titlePanel.add(new JLabel("File count:"));
    titlePanel.add(Box.createRigidArea(horizontalDimension));
    valuesPanel.add(fileCount = new JLabel(""));
    valuesPanel.add(Box.createRigidArea(horizontalDimension));
    titlePanel.add(new JLabel("Class count:"));
    titlePanel.add(Box.createRigidArea(horizontalDimension));
    valuesPanel.add(classCount = new JLabel(""));
    valuesPanel.add(Box.createRigidArea(horizontalDimension));
    return backgroundPanel;
  }

  private JButton createImportButton() {
    final JReflectiveButton retval = new JReflectiveButton.JReflectiveButtonBuilder().setIcon(IconPool.getShared("/img/buttons/import.png")).setToolTipText("Import").build();
    retval.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(final ActionEvent e) {
        File jarFile = selectJarFile();
        if (jarFile != null) {
          importFilesHelper = new ImportJavaFilesHelper(jarFile, retval);
        }
        refresherThread.forceRefresh();
      }

      private File selectJarFile() {
        File current = new File(sourceDirectory.getValue());
        JFileChooser chooser = new JFileChooser();
        FileFilter jarFilter = new FileNameExtensionFilter("Java files (*.jar)", "jar");
        chooser.setFileFilter(jarFilter);
        chooser.setDialogTitle("Select jar file");
        chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        chooser.setAcceptAllFileFilterUsed(false);
        chooser.setSelectedFile(current);

        if (chooser.showOpenDialog(retval) == JFileChooser.APPROVE_OPTION) {
          sourceDirectory.setValue(chooser.getCurrentDirectory().getAbsolutePath());
          return chooser.getSelectedFile();
        } else {
          return null;
        }
      }
    });
    return retval;
  }

  @Override
  public JComponent getComponent() {
    return this;
  }

  @Override
  public void postInitialize(final RefresherThread refresher, final TransactionManager transactionManager) {
    this.refresherThread = refresher;
    this.refresher = createRefresher();
    this.transactionManager = transactionManager;
  }

  @Override
  public void release() {
    if (refresher != null) {
      this.refresher.interrupt();
    }
  }

  @Override
  public void setEditable(final boolean editable) {
    this.btnImport.setEnabled(editable);
  }
}

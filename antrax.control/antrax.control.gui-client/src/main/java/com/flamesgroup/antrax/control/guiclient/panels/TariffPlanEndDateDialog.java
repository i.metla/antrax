/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.panels;

import com.flamesgroup.antrax.control.swingwidgets.calendar.JXDatePicker;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.KeyEvent;
import java.time.LocalDate;
import java.time.ZoneId;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

public class TariffPlanEndDateDialog extends JDialog {
  private static final long serialVersionUID = -8953770278632613871L;

  private final JButton btnOk = new JButton("Set");
  private final JButton btnCancel = new JButton("Cancel");

  private boolean applied = false;

  private JXDatePicker datePicker;

  public TariffPlanEndDateDialog(final Window window) {
    super(window);
    setTitle("Set the end date of the tariff plan");
    setLayout(new BoxLayout(getContentPane(), 3));
    setContentPane(createPanel());
    initializeEvents();
    pack();
  }

  private JPanel createPanel() {
    datePicker = new JXDatePicker(LocalDate.now().atStartOfDay().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli());
    datePicker.setFormats("EEE dd-MM-yyyy HH:mm:ss");
    datePicker.setMaximumSize(new Dimension(150, 22));
    datePicker.setPreferredSize(new Dimension(150, 22));

    JPanel retval = new JPanel();
    SpringLayout layout = new SpringLayout();
    retval.setLayout(layout);

    JLabel lblEvent = new JLabel("Tariff plan end date:", JLabel.TRAILING);
    lblEvent.setLabelFor(datePicker);

    lblEvent.setPreferredSize(new Dimension(lblEvent.getPreferredSize().width, datePicker.getPreferredSize().height));
    resizeToWider(btnOk, btnCancel, btnOk.getPreferredSize().height);

    layout.putConstraint(SpringLayout.NORTH, lblEvent, 5, SpringLayout.NORTH, retval);
    layout.putConstraint(SpringLayout.NORTH, datePicker, 5, SpringLayout.NORTH, retval);

    layout.putConstraint(SpringLayout.WEST, lblEvent, 5, SpringLayout.WEST, retval);
    layout.putConstraint(SpringLayout.WEST, datePicker, 5, SpringLayout.EAST, lblEvent);

    layout.putConstraint(SpringLayout.NORTH, btnOk, 5, SpringLayout.SOUTH, datePicker);
    layout.putConstraint(SpringLayout.EAST, btnOk, 5, SpringLayout.HORIZONTAL_CENTER, retval);
    layout.putConstraint(SpringLayout.NORTH, btnCancel, 5, SpringLayout.SOUTH, datePicker);
    layout.putConstraint(SpringLayout.WEST, btnCancel, 5, SpringLayout.HORIZONTAL_CENTER, retval);

    layout.putConstraint(SpringLayout.EAST, retval, 5, SpringLayout.EAST, datePicker);
    layout.putConstraint(SpringLayout.SOUTH, retval, 5, SpringLayout.SOUTH, btnOk);

    retval.add(lblEvent);
    retval.add(datePicker);
    retval.add(btnOk);
    retval.add(btnCancel);

    return retval;
  }

  private void resizeToWider(final Component a, final Component b, final int height) {
    int maxWidth = Math.max(a.getPreferredSize().width, b.getPreferredSize().width);
    Dimension size = new Dimension(maxWidth, height);
    a.setPreferredSize(size);
    b.setPreferredSize(size);
  }

  private void initializeEvents() {
    addComponentListener(new ComponentAdapter() {
      @Override
      public void componentShown(final ComponentEvent e) {
        applied = false;
        datePicker.requestFocus();
      }
    });

    ActionListener listener = e -> {
      applied = e.getSource() == btnOk;
      if (getDefaultCloseOperation() == JDialog.HIDE_ON_CLOSE) {
        setVisible(false);
      } else if (getDefaultCloseOperation() == JDialog.DISPOSE_ON_CLOSE) {
        dispose();
      } else if (getDefaultCloseOperation() == JDialog.DO_NOTHING_ON_CLOSE) {
      }
    };

    btnCancel.addActionListener(listener);
    btnOk.addActionListener(listener);
  }

  public long getEndDate() {
    return datePicker.getDateInMillis();
  }

  public boolean showDialog() {
    setVisible(true);
    return applied;
  }

}

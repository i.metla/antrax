/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.scripts.insteditor;

import com.flamesgroup.antrax.automation.editors.PropertyEditor;
import com.flamesgroup.antrax.control.swingwidgets.DialogUtil;

import java.applet.Applet;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

/**
 * Property table value editor popup dialog
 */
public class CellEditorDialog {

  private JDialog dlg;
  private boolean accepted;
  private PropertyEditor<Object> propertyEditor;

  public CellEditorDialog() {
  }

  public boolean showEditor(final Component invoker, final String title, final PropertyEditor<Object> propertyEditor) {
    this.accepted = false;
    this.propertyEditor = propertyEditor;

    dlg = new JDialog(DialogUtil.findParentWindow(invoker), title);
    JPanel contentPanel = new JPanel(new BorderLayout());
    contentPanel.add(createEditorPanel(propertyEditor.getEditorComponent()), BorderLayout.CENTER);
    contentPanel.add(createButtonsPanel(), BorderLayout.SOUTH);

    dlg.getContentPane().add(contentPanel, BorderLayout.CENTER);
    dlg.setModal(true);
    dlg.setResizable(true);
    dlg.pack();

    setRightRelativePosition(dlg, invoker);
    dlg.setVisible(true);

    dlg = null;

    return accepted;

    //    PopupDialog dialog = new PopupDialog(null, title, "", "Apply", "Cancel");
    //    dialog.add(createEditorPanel(editorComponent), BorderLayout.CENTER);
    //    return dialog.showModal(invoker);
  }

  private void setRightRelativePosition(final JDialog cmp, final Component invoker) {
    Container root = null;

    if (invoker != null) {
      if (invoker instanceof Window || invoker instanceof Applet) {
        root = cmp;
      } else {
        Container parent;
        for (parent = cmp.getParent(); parent != null; parent = parent.getParent()) {
          if (parent instanceof Window || parent instanceof Applet) {
            root = parent;
            break;
          }
        }
      }
    }

    if (root != null) {
      Dimension invokerSize = invoker.getSize();
      Point invokerScreenLocation = invoker.getLocationOnScreen();

      Rectangle windowBounds = cmp.getBounds();
      int dx = invokerScreenLocation.x - (windowBounds.width - invokerSize.width);
      int dy = invokerScreenLocation.y + invokerSize.height;
      Rectangle screen = root.getGraphicsConfiguration().getBounds();

      // Adjust for bottom edge being offscreen
      if (dy + windowBounds.height > screen.y + screen.height) {
        dy = screen.y + screen.height - windowBounds.height;
      }

      // Avoid being placed off the edge of the screen
      if (dx + windowBounds.width > screen.x + screen.width) {
        dx = screen.x + screen.width - windowBounds.width;
      }

      if (dx < screen.x) {
        dx = screen.x;
      }

      if (dy < screen.y) {
        dy = screen.y;
      }

      cmp.setLocation(dx, dy);
    } else {
      cmp.setLocationRelativeTo(invoker);
    }
  }

  private JComponent createEditorPanel(final Component editorComponent) {
    JPanel formPanel = new JPanel();

    GroupLayout layout = new GroupLayout(formPanel);
    formPanel.setLayout(layout);
    layout.setHorizontalGroup(
    /**/layout.createParallelGroup()
    /*    */.addGroup(layout.createSequentialGroup()
    /*        */.addContainerGap()
    /*        */.addComponent(editorComponent, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
    /*        */.addContainerGap()));

    layout.setVerticalGroup(
    /**/layout.createParallelGroup()
    /*    */.addGroup(layout.createSequentialGroup()
    /*        */.addContainerGap()
    /*        */.addComponent(editorComponent, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
    /*        */.addContainerGap()));

    return formPanel;
  }

  private JComponent createButtonsPanel() {
    JButton applyButton = new JButton("Apply");
    applyButton.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent e) {
        onApply();
      }
    });

    JButton cancelButton = new JButton("Cancel");
    cancelButton.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent e) {
        onCancel();
      }
    });

    JPanel buttonsPanel = new JPanel();

    GroupLayout layout = new GroupLayout(buttonsPanel);
    buttonsPanel.setLayout(layout);
    layout.setHorizontalGroup(
    /**/layout.createParallelGroup()
    /*    */.addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
    /*        */.addContainerGap(5, Short.MAX_VALUE)
    /*        */.addComponent(cancelButton)
    /*        */.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
    /*        */.addComponent(applyButton)
    /*        */.addContainerGap()));
    layout.linkSize(SwingConstants.HORIZONTAL, new Component[] {applyButton, cancelButton});

    layout.setVerticalGroup(
    /**/layout.createParallelGroup()
    /*    */.addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
    /*        */.addContainerGap(5, Short.MAX_VALUE)
    /*        */.addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
    /*            */.addComponent(applyButton)
    /*            */.addComponent(cancelButton))
    /*        */.addContainerGap()));
    layout.linkSize(SwingConstants.VERTICAL, new Component[] {applyButton, cancelButton});

    return buttonsPanel;
  }

  private void onApply() {
    this.accepted = true;
    this.dlg.setVisible(!propertyEditor.isValid());

  }

  private void onCancel() {
    this.accepted = false;
    this.dlg.setVisible(false);
  }

  public boolean isVisible() {
    if (dlg != null) {
      return dlg.isVisible();
    }
    return false;
  }

  public void cancelEditing() {
    onCancel();
  }

  public void stopEditing() {
    onApply();
  }

}

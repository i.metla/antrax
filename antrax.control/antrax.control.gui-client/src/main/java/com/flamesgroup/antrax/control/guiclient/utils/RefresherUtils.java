/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.utils;

import java.util.List;


public final class RefresherUtils {

  private static <T> String getRefresherTypeName(final CallableRefresher<T> callable) {
    String name = callable.toString();
    int startPos = name.lastIndexOf(".") + 1;
    if (startPos >= 0 && startPos < name.length()) {
      name = name.substring(startPos, name.length());
    }
    return String.format("WaitableRefresher-[%s]", name);
  }

  public static <T> WaitingReference<T> executeWaitableAction(final RefresherThread refresherThread, final CallableRefresher<T> callable) {

    final WaitingReference<T> retval = new WaitingReference<>(null);

    ActionCallbackHandler<T> handler = new ActionCallbackHandler<T>() {

      @Override
      public void onActionRefresherFailure(final Refresher refresher, final List<Throwable> caught) {
        retval.release(caught.get(0));
      }

      @Override
      public void onActionRefresherSuccess(final Refresher refresher, final T result) {
        retval.setAndRelease(result);
      }

      @Override
      public void onRefreshUI(final Refresher refresher) {
      }

    };


    ActionRefresher<T> refresher = new ActionRefresher<T>(getRefresherTypeName(callable), refresherThread, handler) {

      @Override
      protected T performAction(final BeansPool pool) throws Exception {
        return callable.performAction(pool);
      }

    };

    refresher.execute();
    retval.waitValue();

    return retval;
  }

}

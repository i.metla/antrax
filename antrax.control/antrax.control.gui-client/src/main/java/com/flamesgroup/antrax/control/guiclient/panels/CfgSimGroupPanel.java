/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.panels;

import com.flamesgroup.antrax.automation.meta.ScriptDefinition;
import com.flamesgroup.antrax.automation.meta.ScriptInstance;
import com.flamesgroup.antrax.automation.meta.ScriptType;
import com.flamesgroup.antrax.automation.utils.registry.RegistryAccess;
import com.flamesgroup.antrax.control.authorization.NotPermittedException;
import com.flamesgroup.antrax.control.communication.StorageException;
import com.flamesgroup.antrax.control.communication.TransactionException;
import com.flamesgroup.antrax.control.guiclient.MainApp;
import com.flamesgroup.antrax.control.guiclient.panels.tablebuilders.SimGroupTableBuilder;
import com.flamesgroup.antrax.control.guiclient.scripts.insteditor.PropertyEditorsSource;
import com.flamesgroup.antrax.control.guiclient.utils.BeansPool;
import com.flamesgroup.antrax.control.guiclient.utils.ExportHelper;
import com.flamesgroup.antrax.control.guiclient.utils.RefresherThread;
import com.flamesgroup.antrax.control.guiclient.utils.TransactionListener;
import com.flamesgroup.antrax.control.guiclient.utils.TransactionManager;
import com.flamesgroup.antrax.control.guiclient.utils.TransactionState;
import com.flamesgroup.antrax.control.swingwidgets.IconPool;
import com.flamesgroup.antrax.control.swingwidgets.MessageUtils;
import com.flamesgroup.antrax.control.swingwidgets.editor.JReflectiveButton;
import com.flamesgroup.antrax.control.swingwidgets.table.TableBuilder;
import com.flamesgroup.antrax.plugins.core.AntraxPluginsStore;
import com.flamesgroup.antrax.plugins.core.data.SimGroupReferenceImpl;
import com.flamesgroup.antrax.storage.commons.SIMGroup;
import com.flamesgroup.antrax.storage.commons.impl.ScriptCommons;
import com.flamesgroup.utils.ExceptionlessProxy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collections;
import java.util.List;

import javax.swing.*;

public class CfgSimGroupPanel extends BaseEditorPanel<SimGroupWrapper, Long> {
  private class BeanCommunicationImplementation implements BeanCommunication<SimGroupWrapper> {
    private final Logger logger = LoggerFactory.getLogger(BeanCommunicationImplementation.class);
    private volatile long pluginsStoreRevision = -1;
    private volatile long groupsRevision = -1;
    private volatile AntraxPluginsStore pluginsStore;
    private volatile ScriptDefinition[] definitions;
    private volatile PropertyEditorsSource propEditors;
    private SimGroupReferenceImpl[] groupReferences;

    @Override
    public SimGroupWrapper updateElem(final SimGroupWrapper elem, final BeansPool pool, final int transactionId) throws NotPermittedException, TransactionException, StorageException {
      SIMGroup updatedSIMGroup = pool.getConfigBean().updateSIMGroup(MainApp.clientUID, transactionId, elem.getSimGroup());

      if (groupReferences == null) {
        groupReferences = new SimGroupReferenceImpl[0];
      }
      for (int i = 0; i < groupReferences.length; ++i) {
        if (groupReferences[i].getId() == elem.getSimGroup().getID()) {
          groupReferences[i] = new SimGroupReferenceImpl(updatedSIMGroup);
        }
      }
      propEditors.handleGroupReferencesChanged(groupReferences);

      return elem.wrap(updatedSIMGroup);
    }

    @Override
    public void removeElem(final SimGroupWrapper elem, final BeansPool pool, final int transactionId) throws NotPermittedException, TransactionException, StorageException {
      pool.getConfigBean().deleteSIMGroup(MainApp.clientUID, transactionId, elem.getSimGroup());
    }

    @Override
    public SimGroupWrapper[] listElems(final BeansPool pool, final int transactionId) throws Exception {
      readRelativeData(pool, transactionId);
      logger.debug("[{}] - reading sim groups", this);
      groupsRevision = pool.getConfigBean().getSIMGroupsRevision(MainApp.clientUID, transactionId);
      SIMGroup[] groups = pool.getConfigBean().listSIMGroups(MainApp.clientUID, transactionId);
      logger.debug("[{}] - setting classLoader for sim groups", this);

      groupReferences = new SimGroupReferenceImpl[groups.length];
      for (int i = 0; i < groupReferences.length; ++i) {
        groupReferences[i] = new SimGroupReferenceImpl(groups[i]);
      }
      propEditors.handleGroupReferencesChanged(groupReferences);

      return SimGroupWrapper.wrap(pluginsStore, propEditors, definitions, groups);
    }

    private void readRelativeData(final BeansPool pool, final int transactionId) throws Exception {
      logger.debug("[{}] - reading plugins store", this);
      pluginsStore = pool.getConfigBean().getPluginsStore(MainApp.clientUID, transactionId);
      pluginsStoreRevision = pluginsStore.getRevision();

      logger.debug("[{}] - reading script definitions", this);
      definitions = pool.getConfigBean().listScriptDefinitions(MainApp.clientUID, ScriptType.values());
      logger.debug("[{}] - wrapping registry", this);
      RegistryAccess registry = ExceptionlessProxy.proxy(pool.getRegistryAccess(), RegistryAccess.class);
      propEditors = new PropertyEditorsSource(pluginsStore, registry);
    }

    @Override
    public boolean refreshForced() {
      return false;
    }

    @Override
    public SimGroupWrapper createElem(final BeansPool beansPool, final int transactionId) throws Exception {
      if (pluginsStore == null) {
        readRelativeData(beansPool, transactionId);
      }

      if (groupReferences == null) // this should never happen, but anyway, we should be careful
      {
        groupReferences = new SimGroupReferenceImpl[0];
      }

      SIMGroup simGroup = beansPool.getConfigBean().createSIMGroup(MainApp.clientUID, transactionId);
      simGroup.setName("New group");
      simGroup.setCanBeAppointed(true);

      return SimGroupWrapper.wrap(pluginsStore, propEditors, definitions, simGroup);
    }

    @Override
    public SimGroupWrapper copyElement(final SimGroupWrapper src, final SimGroupWrapper dst) {
      SIMGroup srcElem = src.getSimGroup();
      SIMGroup dstElem = dst.getSimGroup();
      dstElem.setName(srcElem.getName() + "-copy");

      dstElem.setOperatorSelection(srcElem.getOperatorSelection());

      dstElem.setIdleAfterRegistration(srcElem.getIdleAfterRegistration());
      dstElem.setIdleAfterSuccessfullCall(srcElem.getIdleAfterSuccessfullCall());
      dstElem.setIdleAfterZeroCall(srcElem.getIdleAfterZeroCall());
      dstElem.setIdleBeforeUnregistration(srcElem.getIdleBeforeUnregistration());
      dstElem.setIdleBeforeSelfCall(srcElem.getIdleBeforeSelfCall());
      dstElem.setIdleAfterSelfCallTimeout(srcElem.getIdleAfterSelfCallTimeout());

      dstElem.setSelfDialFactor(srcElem.getSelfDialFactor());
      dstElem.setSelfDialChance(srcElem.getSelfDialChance());

      dstElem.setRegistrationPeriod(srcElem.getRegistrationPeriod());
      dstElem.setRegistrationCount(srcElem.getRegistrationCount());

      dstElem.setBusinessActivityScripts(copyInstances(srcElem.getBusinessActivityScripts()));
      dstElem.setActionProviderScripts(copyInstances(srcElem.getActionProviderScripts()));
      dstElem.setFactorScript(copyInstance(srcElem.getFactorScript()));
      dstElem.setGWSelectorScript(copyInstance(srcElem.getGWSelectorScript()));
      dstElem.setSessionScript(copyInstance(srcElem.getSessionScript()));
      dstElem.setActivityPeriodScript(copyInstance(srcElem.getActivityPeriodScript()));
      dstElem.setIncomingCallManagementScript(copyInstance(srcElem.getIncomingCallManagementScript()));
      dstElem.setVSFactorScript(copyInstance(srcElem.getVSFactorScript()));
      dstElem.setCallFilterScripts(copyInstances(srcElem.getCallFilterScripts()));
      dstElem.setSmsFilterScripts(copyInstances(srcElem.getSmsFilterScripts()));
      dstElem.setIMEIGeneratorScript(copyInstance(srcElem.getIMEIGeneratorScript()));

      dstElem.setDescription(srcElem.getDescription());
      dstElem.setCanBeAppointed(srcElem.canBeAppointed());
      dstElem.setSyntheticRinging(srcElem.isSyntheticRinging());
      dstElem.setScriptCommons(copyCommons(srcElem.listScriptCommons()));
      dstElem.setFASDetection(srcElem.isFASDetection());
      dstElem.setSmsDeliveryReport(srcElem.isSmsDeliveryReport());
      return dst;
    }

    private ScriptCommons[] copyCommons(final ScriptCommons[] commons) {
      ScriptCommons[] retval = new ScriptCommons[commons.length];
      for (int i = 0; i < retval.length; ++i) {
        retval[i] = new ScriptCommons();
        retval[i].setName(commons[i].getName());
        retval[i].setSerializedValue(commons[i].getSerializedValue());
      }
      return retval;
    }

    @Override
    public boolean checkReadRequired(final BeansPool pool, final int transactionId) throws NotPermittedException, TransactionException, StorageException {
      long psRev = pool.getConfigBean().getPluginsStoreRevision(MainApp.clientUID, transactionId);
      long grRev = pool.getConfigBean().getSIMGroupsRevision(MainApp.clientUID, transactionId);
      return pluginsStoreRevision < psRev || groupsRevision < grRev;
    }

    public void reset() {
      groupsRevision = -1;
    }
  }

  private static final long serialVersionUID = -8553188248751797865L;

  protected RefresherThread refresherThread;
  protected TransactionManager transactionManager;

  protected BeanCommunicationImplementation beanCommunication;

  private JButton exportSimGroupButton;
  private JButton importSimGroupButton;

  @Override
  protected BaseEditorBox<SimGroupWrapper> createEditorBox(final RefresherThread refresherThread, final TransactionManager transactionManager) {
    return new SimGroupEditorBox();
  }

  @Override
  protected Component[] getToolbarComponents() {
    return new Component[] {getExportSimGroupButton(), getImportSimGroupButton()};
  }

  @Override
  protected boolean isDisableAllowed(final Component comp) {
    return getExportSimGroupButton() != comp;
  }

  @Override
  public void postInitialize(final RefresherThread refresher, final TransactionManager transactionManager) {
    super.postInitialize(refresher, transactionManager);
    this.refresherThread = refresher;
    this.transactionManager = transactionManager;
    transactionManager.addListener(new TransactionListener() {

      @Override
      public void onTransactionStateChanged(final TransactionState newState) {
        if (newState != TransactionState.START) {
          beanCommunication.reset();
        }
      }

      @Override
      public List<Throwable> handleTransactionGoingToBeCommitted(final int transactionId, final BeansPool beansPool) {
        return Collections.emptyList();
      }
    });
  }

  public CfgSimGroupPanel() {
    enableCopy(true);
  }

  @Override
  public void setActive(final boolean active) {
    super.setActive(active);
  }

  @Override
  protected TableBuilder<SimGroupWrapper, Long> createTableBuilder() {
    return new SimGroupTableBuilder();
  }

  @Override
  protected BeanCommunication<SimGroupWrapper> createBeanCommunication() {
    return beanCommunication = new BeanCommunicationImplementation();
  }

  @Override
  protected boolean isEditorBoxStretched() {
    return true;
  }

  protected static ScriptInstance copyInstance(final ScriptInstance src) {
    if (src == null) {
      return null;
    }
    ScriptInstance dst = src.getDefinition().createInstance();
    dst.setParameters(src);
    return dst;
  }

  protected static ScriptInstance[] copyInstances(final ScriptInstance... src) {
    ScriptInstance[] dst = new ScriptInstance[src.length];
    for (int i = 0; i < src.length; i++) {
      dst[i] = copyInstance(src[i]);
    }
    return dst;
  }

  private JButton getExportSimGroupButton() {
    if (exportSimGroupButton == null) {
      exportSimGroupButton = (new JReflectiveButton.JReflectiveButtonBuilder()).setIcon(IconPool.getShared("/img/buttons/export.png")).build();
      exportSimGroupButton.setToolTipText("Export");
      exportSimGroupButton.addActionListener(new ActionListener() {

        @Override
        public void actionPerformed(final ActionEvent e) {
          SimGroupWrapper simGroup = getTable().getSelectedElem();
          if (simGroup != null) {
            new ExportHelper().exportSimGroup(simGroup.getSimGroup(), CfgSimGroupPanel.this);
          } else {
            MessageUtils.showError(exportSimGroupButton, "Nothing to export", "Select sim group before exporting it");
          }
        }

      });
    }
    return exportSimGroupButton;
  }

  private JButton getImportSimGroupButton() {
    if (importSimGroupButton == null) {
      importSimGroupButton = (new JReflectiveButton.JReflectiveButtonBuilder()).setIcon(IconPool.getShared("/img/buttons/import.png")).build();
      importSimGroupButton.setEnabled(false);
      importSimGroupButton.setToolTipText("Import");
      importSimGroupButton.addActionListener(new ActionListener() {

        @Override
        public void actionPerformed(final ActionEvent e) {
          int tid = transactionManager.getGuaranteedTransactionId();
          new ExportHelper().importSimGroup(CfgSimGroupPanel.this, refresherThread, tid, true);
          refresherThread.forceRefresh();
        }

      });
    }
    return importSimGroupButton;
  }

}

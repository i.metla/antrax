/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient;

import com.flamesgroup.antrax.control.guiclient.domain.SimEventRecRow;
import com.flamesgroup.antrax.control.guiclient.panels.SimHistoryPanel;
import com.flamesgroup.antrax.control.guiclient.utils.TimeUtils;
import com.flamesgroup.antrax.storage.commons.impl.CDR;
import com.flamesgroup.antrax.storage.commons.impl.SIMEventRec;
import com.flamesgroup.antrax.storage.enums.SIMEvent;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Properties;

public class SimHistoryEventFormatter {

  private static final String EVENT_PROPERTIES = "simevents.properties";
  private static Properties simEventMsgs;

  private static String getProperty(final String key) {
    Properties prop = getSimEventMsgs();
    if (prop == null) {
      return null;
    }

    return prop.getProperty(key);
  }

  private static Properties getSimEventMsgs() {
    if (simEventMsgs == null) {
      URL url = SimHistoryPanel.class.getResource(EVENT_PROPERTIES);
      if (url == null) {
        url = SimHistoryPanel.class.getResource("/" + EVENT_PROPERTIES);
      }

      if (url != null) {
        try {
          InputStream in = url.openStream();
          Properties props = new Properties();
          props.load(in);
          simEventMsgs = props;
        } catch (IOException ignored) {
        }
      }
    }

    return simEventMsgs;
  }

  public static String getEventDescription(final SIMEventRec record) {
    Properties prop = getSimEventMsgs();
    if (prop == null) {
      return null;
    }

    SIMEvent event = record.getEvent();
    String pattern = prop.getProperty(event.toString());
    if (pattern == null) {
      return null;
    }

    return MessageFormat.format(pattern, record.getArgs());
  }

  public static String convertSimEventToText(final SimEventRecRow recRow/*
                                   * , boolean
                                   * addTime
                                   */) {
    SIMEventRec record = recRow.getRecord();
    if (record == null) {
      return "";
    }

    String description = getEventDescription(record);

    if (description != null) {
      RowValueBuilder builder = new RowValueBuilder();
      builder.text(description);

      switch (record.getEvent()) {
        case SIM_LOCKED:
          break;
        case INCOMING_CALL_ENDED:
          CDR cdr = recRow.getCDR();
          if (cdr != null) {
            builder.sep().date(new Date(cdr.getStopTime())).text(",").sep();
            builder.duration(cdr.getDuration()).text(",").sep();
            builder.duration(cdr.getPDD()).text(",").sep();
            builder.bold(cdr.getCallerPhoneNumber() == null ? "" : cdr.getCallerPhoneNumber().getValue()).text(",").sep();
            builder.bold(cdr.getCallType()).text(",").sep();
            builder.bold(cdr.getDropReason());
          }
          break;
        case OUTGOING_CALL_ENDED:
          cdr = recRow.getCDR();
          if (cdr != null) {
            builder.sep().date(new Date(cdr.getStopTime())).text(",").sep();
            builder.duration(cdr.getDuration()).text(",").sep();
            builder.duration(cdr.getPDD()).text(",").sep();
            builder.bold(cdr.getCalledPhoneNumber() == null ? "" : cdr.getCalledPhoneNumber().getValue()).text(",").sep();
            builder.bold(cdr.getCallType()).text(",").sep();
            builder.bold(cdr.getDropReason());
          }
          break;
        case GENERIC_EVENT:
          if (record.getArgs().length > 0) {
            String prop = getProperty(record.getEvent().toString() + "_" + record.getArgs().length);
            if (prop == null) {
              return defaultValueBuilder(record);
            }
            String details = MessageFormat.format(prop, record.getArgs());
            builder.sep().text(details);
          }
          break;
      }

      // colorize parent lockGsmChannel event
      hasSimLockEvent(recRow);

      description = builder.build();
    }

    return (description == null) ? defaultValueBuilder(record) : description;
  }

  public static boolean hasSimLockEvent(final SimEventRecRow recRow) {
    boolean hasSimLockEvent = false;
    SIMEventRec eventRec = recRow.getRecord();
    if (eventRec != null && eventRec.getEvent() == SIMEvent.SIM_LOCKED) {
      hasSimLockEvent = true;
    }
    return hasSimLockEvent;
  }

  private static String defaultValueBuilder(final SIMEventRec record) {
    RowValueBuilder builder = new RowValueBuilder();
    String color = "#333333";
    builder.text("time: ", color).date(record.getStartTime()).sep();
    builder.text("event: ", color).bold(record.getEvent()).sep();
    builder.text("args: ", color).bold(Arrays.toString(record.getArgs())).sep();
    return builder.build();
  }

  /**
   * SIM history row text value builder
   */
  public static class RowValueBuilder {

    private final StringBuilder str = new StringBuilder();
    private final DateFormat dateFormat = new SimpleDateFormat(TimeUtils.DEFAULT_DATE_FORMAT);

    public RowValueBuilder() {
    }

    public String build() {

      return str.toString();
    }

    public RowValueBuilder bold(final Object msg) {
      text(msg);
      return this;
    }

    public RowValueBuilder text(final Object msg) {
      if (msg != null) {
        str.append(String.valueOf(msg));
      }
      return this;
    }

    public RowValueBuilder text(final Object msg, final String color) {
      if (msg != null) {
        str.append(String.valueOf(msg));
      }
      return this;
    }

    public RowValueBuilder sep() {
      return text(" ");
    }

    public RowValueBuilder date(final Date date) {
      return bold(dateFormat.format(date));
    }

    public RowValueBuilder duration(final long duration) {
      str.append(TimeUtils.writeTime(duration));
      return this;
    }

  }

}

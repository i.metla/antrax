/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.panels.registry;

import com.flamesgroup.antrax.automation.registry.RegistryEntry;
import com.flamesgroup.antrax.control.guiclient.MainApp;
import com.flamesgroup.antrax.control.guiclient.utils.BeansPool;
import com.flamesgroup.antrax.control.guiclient.utils.Refresher;
import com.flamesgroup.rmi.exception.RemoteAccessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RegistryRefresher extends Refresher {
  private final Logger logger = LoggerFactory.getLogger(RegistryRefresher.class);
  private String[] paths;
  private RegistryEntry[] elems;
  private final RegistryModel model;
  private volatile boolean updating;
  private final RegistryPathTable pathsList;
  private final RegistryValuesTable valuesList;

  public RegistryRefresher(final RegistryModel model, final RegistryPathTable pathsList, final RegistryValuesTable valuesList) {
    super("RegistryRefresher");
    this.model = model;
    this.pathsList = pathsList;
    this.valuesList = valuesList;
  }

  @Override
  public void updateGuiElements() {
    logger.debug("[{}] - updating GUI elements started", this);
    this.updating = true;
    if (paths != null) {
      model.setPaths(paths);
    }
    paths = null;
    if (elems != null) {
      model.setEntries(elems);
    }
    elems = null;
    valuesList.setEnabled(true);
    this.updating = false;
    logger.debug("[{}] - updating GUI elements done", this);
  }

  @Override
  protected void refreshInformation(final BeansPool beansPool) throws RemoteAccessException {
    logger.debug("[{}] - refreshing information started", this);
    try {
      model.applyOutgoingOperations(beansPool.getRegistryAccessBean());
      this.paths = beansPool.getRegistryAccessBean().listAllPaths(MainApp.clientUID);
      if (pathsList.getSelectedRow() >= 0) {
        String path = pathsList.getSelectedElem();
        this.elems = beansPool.getRegistryAccessBean().listEntries(MainApp.clientUID, path, Integer.MAX_VALUE);
      } else {
        this.elems = null;
      }
    } catch (Exception e) {
      logger.warn("[{}] - failed to refresh information", this, e);
      throw new RemoteAccessException("fail", e);
    }
    logger.debug("[{}] - refreshing information done", this);
  }

  public boolean isUpdating() {
    return updating;
  }

  @Override
  public void handleEnabled(final BeansPool beansPool) {
  }

  @Override
  public void handleDisabled(final BeansPool beansPool) {
  }
}

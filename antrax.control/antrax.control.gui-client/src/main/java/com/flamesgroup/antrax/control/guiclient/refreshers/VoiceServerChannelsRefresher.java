/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.refreshers;

import com.flamesgroup.antrax.control.authorization.NotPermittedException;
import com.flamesgroup.antrax.control.communication.IVoiceServerChannelsInfo;
import com.flamesgroup.antrax.control.communication.SimpleSimGroup;
import com.flamesgroup.antrax.control.communication.StorageException;
import com.flamesgroup.antrax.control.guiclient.MainApp;
import com.flamesgroup.antrax.control.guiclient.commadapter.VoiceServerChannelAdapter;
import com.flamesgroup.antrax.control.guiclient.indicators.VoiceServerChannelIndicators;
import com.flamesgroup.antrax.control.guiclient.newapi.panels.ExecutiveRefresher;
import com.flamesgroup.antrax.control.guiclient.newapi.panels.RefresherExecution;
import com.flamesgroup.antrax.control.guiclient.panels.VoiceServerChannelsPanel;
import com.flamesgroup.antrax.control.guiclient.panels.VoiceServerChannelsTable;
import com.flamesgroup.antrax.control.guiclient.utils.BeansPool;
import com.flamesgroup.antrax.control.guiclient.utils.CallBackRefresher;
import com.flamesgroup.antrax.control.guiclient.utils.RefresherThread;
import com.flamesgroup.antrax.control.guiclient.utils.TransactionManager;
import com.flamesgroup.antrax.control.swingwidgets.MessageUtils;
import com.flamesgroup.antrax.control.swingwidgets.editor.ExtendedComboBox;
import com.flamesgroup.antrax.distributor.ChannelConfig;
import com.flamesgroup.antrax.storage.commons.GSMChannel;
import com.flamesgroup.antrax.storage.commons.GSMGroup;
import com.flamesgroup.antrax.storage.commons.IServerData;
import com.flamesgroup.commons.ChannelUID;
import com.flamesgroup.commons.LockArfcnException;
import com.flamesgroup.commons.LockException;
import com.flamesgroup.commons.NetworkSurveyException;
import com.flamesgroup.unit.ICCID;

import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

import javax.swing.*;

public class VoiceServerChannelsRefresher extends ExecutiveRefresher {
  private static final VoiceServerChannelAdapter[] EMPTY_VS_CHANNELS_LIST = new VoiceServerChannelAdapter[0];

  private CallBackRefresher<VoiceServerChannelAdapter[]> refresher;

  private VoiceServerChannelAdapter[] voiceServerChannelAdaptersData = EMPTY_VS_CHANNELS_LIST;
  private GSMGroup[] gsmGroupData;
  private SimpleSimGroup[] simpleSimGroupsData;

  private volatile boolean showUnliveChannels;
  private final AtomicBoolean ussdDialogOpen = new AtomicBoolean();

  public VoiceServerChannelsRefresher(final RefresherThread refresherThread, final TransactionManager transactionManager, final Component invoker) {
    super("VoiceServerChannelsRefresher", refresherThread, transactionManager, invoker);
  }

  public void release() {
    if (refresher != null) {
      refresher.interrupt();
    }
  }

  public void updateGSMGroup(final Component parentComponent, final List<VoiceServerChannelAdapter> selectedElems, final GSMGroup group) {
    addExecution(new RefresherExecution() {

      @Override
      public void updateGUIComponent() {
      }

      @Override
      public void invokeExecution(final BeansPool beansPool, final TransactionManager transactionManager) throws Exception {
        List<GSMChannel> markedForUpdate = new LinkedList<>();
        List<ChannelUID> badChannels = new ArrayList<>();
        for (VoiceServerChannelAdapter a : selectedElems) {
          GSMChannel gsmChannel = a.getGSMChannel();
          gsmChannel.setGsmGroup(group);
          if (a.isLive() && a.getChannelConfig().getChannelState() == ChannelConfig.ChannelState.ACTIVE) {
            markedForUpdate.add(gsmChannel);
          } else {
            badChannels.add(gsmChannel.getChannelUID());
          }
        }
        GSMChannel[] updating = markedForUpdate.toArray(new GSMChannel[markedForUpdate.size()]);
        beansPool.getActivationBean().updateGSMChannels(MainApp.clientUID, updating);
        if (!badChannels.isEmpty()) {
          MessageUtils.showInfo(parentComponent, "Update Gsm group", "Can't update Gsm group for channels " + badChannels + ", because they aren't verified or live or valid licence.");
        }
      }

      @Override
      public String describeExecution() {
        return "set gsm group";
      }
    });
  }

  public void updateSimGroupGroup(final List<VoiceServerChannelAdapter> selectedElems, final SimpleSimGroup group) {
    addExecution(new RefresherExecution() {

      @Override
      public void updateGUIComponent() {
      }

      @Override
      public void invokeExecution(final BeansPool beansPool, final TransactionManager transactionManager) throws Exception {
        List<ICCID> simForUpdate = selectedElems.stream().map(VoiceServerChannelAdapter::getSimUID).filter(e -> e != null).collect(Collectors.toList());
        if (simForUpdate.isEmpty()) {
          return;
        }
        beansPool.getConfigBean().updateSimCardGroup(MainApp.clientUID, -1, simForUpdate.toArray(new ICCID[simForUpdate.size()]), group);
      }

      @Override
      public String describeExecution() {
        return "set sim group";
      }
    });
  }

  public void initializeGsmGroupsPermanentRefresher(final ExtendedComboBox gsmGroupComboBox, final ExtendedComboBox simGroupComboBox) {
    addPermanentExecution(new RefresherExecution() {

      @Override
      public void updateGUIComponent() {
        setGSMGroupData(gsmGroupComboBox);
        setSimGroupData(simGroupComboBox);
      }

      @Override
      public void invokeExecution(final BeansPool beansPool, final TransactionManager transactionManager) throws Exception {
        gsmGroupData = beansPool.getConfigBean().listGSMGroups(MainApp.clientUID);
        simpleSimGroupsData = beansPool.getConfigBean().listSimpleSIMGroups(MainApp.clientUID);
      }

      @Override
      public String describeExecution() {
        return "request for list of gsm groups and sim groups";
      }
    });
  }

  public void setGSMGroupData(final ExtendedComboBox gsmGroupComboBox) {
    if (gsmGroupData != null && !gsmGroupComboBox.isFocusOwner()) {
      gsmGroupComboBox.setData(gsmGroupData, true);
    }
  }

  public void setSimGroupData(final ExtendedComboBox simGroupComboBox) {
    if (simpleSimGroupsData != null && !simGroupComboBox.isFocusOwner()) {
      simGroupComboBox.setData(simpleSimGroupsData, true);
    }
  }

  public void initializePermanentRefresher(final JButton buttonRefresh, final VoiceServerChannelsTable channelsTable, final VoiceServerChannelIndicators indicators, final String server) {
    addPermanentExecution(new RefresherExecution() {
      private volatile IServerData cachedServer;

      @Override
      public void updateGUIComponent() {
        setVoiceServerChannelAdaptersData(buttonRefresh, channelsTable, indicators);
      }

      @Override
      public void invokeExecution(final BeansPool beansPool, final TransactionManager transactionManager) throws Exception {
        voiceServerChannelAdaptersData = EMPTY_VS_CHANNELS_LIST;
        List<VoiceServerChannelAdapter> channels = new ArrayList<>();
        for (IServerData s : getServerList(beansPool)) {
          try {
            for (IVoiceServerChannelsInfo info : beansPool.getControlBean().listVoiceServerChannels(s)) {
              if (!info.getMobileGatewayChannelInfo().isLive() && !showUnliveChannels) {
                continue;
              }

              channels.add(new VoiceServerChannelAdapter(s, info));
            }
          } catch (Exception e) {
            e.printStackTrace();
          }
        }

        voiceServerChannelAdaptersData = channels.toArray(new VoiceServerChannelAdapter[channels.size()]);
      }

      private List<IServerData> getServerList(final BeansPool beansPool) throws NotPermittedException, StorageException {
        if (server == null) {
          return beansPool.getControlBean().listVoiceServers();
        } else {
          if (cachedServer == null || !server.equals(cachedServer.getName())) {
            for (IServerData s : beansPool.getControlBean().listVoiceServers()) {
              if (server.equals(s.getName())) {
                cachedServer = s;
                break;
              }
            }
          }
          return Collections.singletonList(cachedServer);
        }
      }

      @Override
      public String describeExecution() {
        return "list channels information";
      }
    });
  }

  public void setVoiceServerChannelAdaptersData(final JButton buttonRefresh, final VoiceServerChannelsTable channelsTable, final VoiceServerChannelIndicators indicators) {
    buttonRefresh.setEnabled(false);
    channelsTable.setData(voiceServerChannelAdaptersData);
    buttonRefresh.setEnabled(true);

    int ready = 0;
    int calling = 0;
    for (VoiceServerChannelAdapter adapter : voiceServerChannelAdaptersData) {
      if (adapter.isReady()) {
        ready++;
      }

      if (adapter.isCalling()) {
        calling++;
      }
    }

    indicators.setTotal(voiceServerChannelAdaptersData.length);
    indicators.setReady(ready);
    indicators.setCalling(calling);
  }

  public void fireEvent(final Component baseComponent, final String eventText, final List<VoiceServerChannelAdapter> selected) {
    addExecution(new RefresherExecution() {
      final List<String> errors = new ArrayList<>();

      @Override
      public void updateGUIComponent() {
        String msg;

        if (0 == errors.size()) {
          msg = "Event successfully sent";
          MessageUtils.showInfo(baseComponent, "Antrax Manager", msg);
        } else {
          msg = "Event sending error:\n\n";
          for (String error : errors) {
            msg += error + "\n";
          }
          MessageUtils.showError(baseComponent, "Antrax Manager", msg);
        }
      }

      @Override
      public void invokeExecution(final BeansPool beansPool, final TransactionManager transactionManager) throws Exception {
        String errorMessage;
        for (VoiceServerChannelAdapter adapter : selected) {
          ICCID simUID = adapter.getSimUID();
          if (simUID != null) {
            try {
              beansPool.getControlBean().fireEvent(adapter.getVoiceServer(), simUID, eventText);
            } catch (Exception e) {
              errorMessage = String.format("[%s:%s] %s",
                  adapter.getMobileGatewayChannelUID(),
                  adapter.getSimChannelUID(),
                  e.getMessage());

              errors.add(errorMessage);
            }
          }
        }
      }

      @Override
      public String describeExecution() {
        return "send USSD";
      }
    });
  }

  public void lock(final Component baseComponent, final boolean lock, final String lockReason, final List<VoiceServerChannelAdapter> selected) {
    addExecution(new RefresherExecution() {

      final StringBuilder errors = new StringBuilder();

      @Override
      public void updateGUIComponent() {
        baseComponent.setEnabled(true);
        if (errors.length() > 0) {
          MessageUtils.showError(baseComponent, "Antrax Manager", (lock ? "Lock" : "Unlock") + " gsm channels error:\n\n" + errors.toString());
        }
      }

      @Override
      public void invokeExecution(final BeansPool beansPool, final TransactionManager transactionManager) throws Exception {
        selected.forEach(a -> {
          ChannelUID gsmChannel = a.getMobileGatewayChannelUID();
          if (gsmChannel == null) {
            return;
          }

          if (!(a.isLive() && a.getChannelConfig().getChannelState() == ChannelConfig.ChannelState.ACTIVE)) {
            errors.append("[").append(gsmChannel).append("] ");
            errors.append("channel isn't live or has invalid licence.\n");
            return;
          }

          try {
            beansPool.getControlBean().lockGsmChannel(a.getVoiceServer(), gsmChannel, lock, lockReason);
          } catch (NotPermittedException | NoSuchFieldException | LockException e) {
            errors.append("[").append(gsmChannel).append(":").append(a.getSimChannelUID()).append("] ");
            errors.append(e.getMessage()).append("\n");
          }
        });
      }

      @Override
      public String describeExecution() {
        return lock ? "Lock gsm channels" : "Unlock gsm channels";
      }
    });
  }


  public void lockArfcn(final VoiceServerChannelsPanel baseComponent, final int lockArfcn, final List<VoiceServerChannelAdapter> selected) {
    addExecution(new RefresherExecution() {

      final StringBuilder errors = new StringBuilder();

      @Override
      public void updateGUIComponent() {
        baseComponent.setEnabled(true);
        if (errors.length() > 0) {
          MessageUtils.showError(baseComponent, "Antrax Manager", "Lock gsm channels to arfcn error:\n\n" + errors.toString());
        }
      }

      @Override
      public void invokeExecution(final BeansPool beansPool, final TransactionManager transactionManager) throws Exception {
        selected.forEach(a -> {
          ChannelUID gsmChannel = a.getMobileGatewayChannelUID();
          if (gsmChannel == null) {
            return;
          }

          try {
            beansPool.getControlBean().lockGsmChannelToArfcn(a.getVoiceServer(), gsmChannel, lockArfcn);
          } catch (NotPermittedException | NoSuchFieldException | LockArfcnException e) {
            errors.append("[").append(gsmChannel).append(":").append(a.getSimChannelUID()).append("] ");
            errors.append(e.getMessage()).append("\n");
          }
        });
      }

      @Override
      public String describeExecution() {
        return "Lock gsm channels to arfcn";
      }
    });
  }


  public void unLockArfcn(final VoiceServerChannelsPanel baseComponent, final List<VoiceServerChannelAdapter> selected) {
    addExecution(new RefresherExecution() {

      final StringBuilder errors = new StringBuilder();

      @Override
      public void updateGUIComponent() {
        baseComponent.setEnabled(true);
        if (errors.length() > 0) {
          MessageUtils.showError(baseComponent, "Antrax Manager", "Unlock gsm channels to default arfcn error:\n\n" + errors.toString());
        }
      }

      @Override
      public void invokeExecution(final BeansPool beansPool, final TransactionManager transactionManager) throws Exception {
        selected.forEach(a -> {
          ChannelUID gsmChannel = a.getMobileGatewayChannelUID();
          if (gsmChannel == null) {
            return;
          }

          try {
            beansPool.getControlBean().unLockGsmChannelToArfcn(a.getVoiceServer(), gsmChannel);
          } catch (NotPermittedException | NoSuchFieldException | LockArfcnException e) {
            errors.append("[").append(gsmChannel).append(":").append(a.getSimChannelUID()).append("] ");
            errors.append(e.getMessage()).append("\n");
          }
        });
      }

      @Override
      public String describeExecution() {
        return "Lock gsm channels to arfcn";
      }
    });
  }

  public void executeNetworkSurvey(final Component baseComponent, final List<VoiceServerChannelAdapter> selected) {
    addExecution(new RefresherExecution() {

      final StringBuilder errors = new StringBuilder();

      @Override
      public void updateGUIComponent() {
        baseComponent.setEnabled(true);
        if (errors.length() > 0) {
          MessageUtils.showError(baseComponent, "Antrax Manager", "Execute Network Survey gsm channels error:\n\n" + errors.toString());
        }
      }

      @Override
      public void invokeExecution(final BeansPool beansPool, final TransactionManager transactionManager) throws Exception {
        selected.forEach(a -> {
          ChannelUID gsmChannel = a.getMobileGatewayChannelUID();
          if (gsmChannel == null) {
            return;
          }

          if (!(a.isLive() && a.getChannelConfig().getChannelState() == ChannelConfig.ChannelState.ACTIVE)) {
            errors.append("[").append(gsmChannel).append("] ");
            errors.append("channel isn't live or has invalid licence.\n");
            return;
          }

          try {
            beansPool.getControlBean().executeNetworkSurvey(a.getVoiceServer(), gsmChannel);
          } catch (NotPermittedException | NoSuchFieldException | NetworkSurveyException e) {
            errors.append("[").append(gsmChannel).append(":").append(a.getSimChannelUID()).append("] ");
            errors.append(e.getMessage()).append("\n");
          }
        });
      }

      @Override
      public String describeExecution() {
        return "execute Network Survey";
      }
    });
  }

  public void sendUSSD(final Component baseComponent, final String ussdText, final List<VoiceServerChannelAdapter> selected, final boolean isGetResponse) {
    addExecution(new RefresherExecution() {
      final List<String> errors = new ArrayList<>();
      String response = "";

      @Override
      public void updateGUIComponent() {
        String msg;

        if (0 == errors.size()) {
          if (!isGetResponse) {
            msg = "USSD successfully sent";
            MessageUtils.showInfo(baseComponent, "Antrax Manager", msg);
            return;
          }

          if (!response.isEmpty()) {
            ((VoiceServerChannelsPanel) baseComponent).executeUSSD(response);
          }
        } else {
          msg = "USSD sending error:\n\n";
          for (String error : errors) {
            msg += error + "\n";
          }
          MessageUtils.showError(baseComponent, "Antrax Manager", msg);
          ussdDialogOpen.set(false);
        }
      }

      @Override
      public void invokeExecution(final BeansPool beansPool, final TransactionManager transactionManager) throws Exception {
        String errorMessage;
        for (VoiceServerChannelAdapter adapter : selected) {
          ICCID simUID = adapter.getSimUID();
          if (simUID != null) {
            try {
              if (!isGetResponse) {
                beansPool.getControlBean().sendUssd(adapter.getVoiceServer(), simUID, ussdText);
              } else {
                if (ussdDialogOpen.get()) {
                  response += adapter.getGSMChannel().getChannelUID().getCanonicalName() + ":\n" + beansPool.getControlBean()
                      .sendUSSDSessionCommand(adapter.getVoiceServer(), simUID,
                          ussdText) + "\n";
                } else {
                  response += adapter.getGSMChannel().getChannelUID().getCanonicalName() + ":\n" + beansPool.getControlBean()
                      .startUSSDSession(adapter.getVoiceServer(), simUID, ussdText) + "\n";
                }
              }
            } catch (Exception e) {
              errorMessage = String.format("[%s:%s] %s",
                  adapter.getMobileGatewayChannelUID(),
                  adapter.getSimChannelUID(),
                  e.getMessage());
              errors.add(errorMessage);
            }
          }
        }
        ussdDialogOpen.set(true);
      }

      @Override
      public String describeExecution() {
        return "send USSD";
      }
    });
  }

  public void endUSSDDialog(final List<VoiceServerChannelAdapter> selected) {
    addExecution(new RefresherExecution() {

      @Override
      public String describeExecution() {
        return "end USSD";
      }

      @Override
      public void invokeExecution(final BeansPool beansPool, final TransactionManager transactionManager) throws Exception {
        if (ussdDialogOpen.getAndSet(false)) {
          for (VoiceServerChannelAdapter adapter : selected) {
            ICCID simUID = adapter.getSimUID();
            if (simUID != null) {
              beansPool.getControlBean().endUSSDSession(adapter.getVoiceServer(), simUID);
            }
          }
        }
      }

      @Override
      public void updateGUIComponent() {
      }
    });
  }

  public void sendSMS(final Component baseComponent, final List<String> numbers, final String smsText, final List<VoiceServerChannelAdapter> selected) {
    addExecution(new RefresherExecution() {
      int totalMessagesCount;
      int successMessagesCount;

      final List<String> errors = new ArrayList<>();

      @Override
      public void updateGUIComponent() {
        String msg;

        if (0 == errors.size()) {
          msg = "Successfully sent " + totalMessagesCount;
          if (totalMessagesCount > 1) {
            msg += " messages";
          } else {
            msg += " message";
          }
          MessageUtils.showInfo(baseComponent, "Antrax Manager", msg);
        } else {
          if (successMessagesCount > 0) {
            msg = "Sent " + successMessagesCount + " of " + totalMessagesCount + " messages.\n\n";
          } else {
            msg = "No messages were sent.\n\n";
          }

          msg += "Sending error:\n";
          for (String error : errors) {
            msg += error + "\n";
          }
          MessageUtils.showError(baseComponent, "Antrax Manager", msg);
        }
      }

      @Override
      public void invokeExecution(final BeansPool beansPool, final TransactionManager transactionManager) throws Exception {
        String errorMessage;

        totalMessagesCount = selected.size() * numbers.size();
        successMessagesCount = 0;

        for (VoiceServerChannelAdapter adapter : selected) {
          ICCID simUID = adapter.getSimUID();
          if (simUID != null) {
            for (String phoneNumber : numbers) {
              try {
                beansPool.getControlBean().sendSMS(adapter.getVoiceServer(), simUID, phoneNumber, smsText);
                successMessagesCount++;
              } catch (Exception e) {
                errorMessage = String.format("[%s:%s -> %s] %s",
                    adapter.getMobileGatewayChannelUID(),
                    adapter.getSimChannelUID(),
                    phoneNumber,
                    e.getMessage());

                errors.add(errorMessage);
              }
            }
          }
        }
      }

      @Override
      public String describeExecution() {
        return "Send SMS";
      }
    });
  }

  public void sendDTMF(final Component baseComponent, final String dtmf, final List<VoiceServerChannelAdapter> selected) {
    addExecution(new RefresherExecution() {
      final List<String> errors = new ArrayList<>();

      @Override
      public void updateGUIComponent() {
        String msg;

        if (0 == errors.size()) {
          msg = "DTMF successfully sent";
          MessageUtils.showInfo(baseComponent, "Antrax Manager", msg);
        } else {
          msg = "DTMF sending error:\n\n";
          for (String error : errors) {
            msg += error + "\n";
          }
          MessageUtils.showError(baseComponent, "Antrax Manager", msg);
        }
      }

      @Override
      public void invokeExecution(final BeansPool beansPool, final TransactionManager transactionManager) throws Exception {
        String errorMessage;
        for (VoiceServerChannelAdapter adapter : selected) {
          ICCID simUID = adapter.getSimUID();

          if (simUID != null) {
            try {
              beansPool.getControlBean().sendDTMF(adapter.getVoiceServer(), simUID, dtmf);
            } catch (Exception e) {
              errorMessage = String.format("[%s:%s] %s",
                  adapter.getMobileGatewayChannelUID(),
                  adapter.getSimChannelUID(),
                  e.getMessage());

              errors.add(errorMessage);
            }
          }
        }
      }

      @Override
      public String describeExecution() {
        return "send DTMF";
      }
    });
  }

  public void setShowUnliveChannels(final boolean showUnliveChannels) {
    this.showUnliveChannels = showUnliveChannels;
  }

  public void enableSimUnit(final List<VoiceServerChannelAdapter> selectedElems, final boolean enable) {
    addExecution(new RefresherExecution() {

      @Override
      public void updateGUIComponent() {
      }

      @Override
      public void invokeExecution(final BeansPool beansPool, final TransactionManager transactionManager) throws Exception {
        List<ICCID> simForUpdate = selectedElems.stream().map(VoiceServerChannelAdapter::getSimUID).filter(Objects::nonNull).collect(Collectors.toList());
        if (simForUpdate.isEmpty()) {
          return;
        }
        beansPool.getActivationBean().enableSimUnit(MainApp.clientUID, simForUpdate.toArray(new ICCID[simForUpdate.size()]), enable);
      }

      @Override
      public String describeExecution() {
        return "enable simUid";
      }
    });
  }

}

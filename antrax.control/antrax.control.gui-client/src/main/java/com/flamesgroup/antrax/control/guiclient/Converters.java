/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient;

import com.flamesgroup.antrax.control.communication.ServerStatus;
import com.flamesgroup.antrax.control.swingwidgets.IconPool;
import com.flamesgroup.antrax.storage.state.CallChannelState;
import com.flamesgroup.antrax.storage.state.CallState;
import com.flamesgroup.antrax.storage.state.VoucherState;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import javax.swing.*;

public final class Converters {

  // Suppress default constructor, ensuring non-insatiability
  private Converters() {
    throw new AssertionError();
  }

  public static Icon convertServerStatus(final ServerStatus status) {
    if (status == null) {
      return null;
    }

    switch (status) {
      case STARTING:
        return IconPool.getShared("/img/serverstatus/starting.png");
      case STARTED:
        return IconPool.getShared("/img/serverstatus/started.png");
      case STOPPING:
        return IconPool.getShared("/img/serverstatus/stopping.png");
      case NO_CONNECTION:
        return IconPool.getShared("/img/serverstatus/noconnection.gif");
    }
    throw new UnsupportedOperationException("Status " + status + " is not supported");
  }

  public static Icon convertGsmViewStatus(final Date date) {
    if (date == null) {
      return IconPool.getShared("/img/notregistered.png");
    } else if (date.getTime() > (System.currentTimeMillis() - TimeUnit.DAYS.toMillis(30))) {
      return IconPool.getShared("/img/registered.png");
    } else {
      return IconPool.getShared("/img/registration.png");
    }
  }

  public static Icon convertEnability(final boolean enabled) {
    return (enabled)
        ? IconPool.getShared("/img/simstate/simstate_enable.png")
        : IconPool.getShared("/img/simstate/simstate_disable.png");
  }

  public static Icon convertLock(final boolean locked) {
    return (locked)
        ? IconPool.getShared("/img/simlockstate/simlockstate_l.gif")
        : IconPool.getShared("/img/simlockstate/simlockstate_u.gif");
  }

  public static Icon convertCallState(final CallState callState) {
    switch (callState.getState()) {
      case IDLE:
        return IconPool.getShared("/img/callstate/callstate_idle.gif");
      case DIALING:
        return IconPool.getShared("/img/callstate/callstate_dialing.gif");
      case ALERTING:
        return IconPool.getShared("/img/callstate/callstate_alerting.gif");
      case ACTIVE:
        return IconPool.getShared("/img/callstate/callstate_active.gif");
    }
    throw new IllegalStateException("Unknowns call state " + callState);
  }

  public static Icon convertCallChannelState(final CallChannelState state) {
    if (state == null) {
      return IconPool.getShared("/img/invisiblebox.gif");
    }

    switch (state.getState()) {
      case READY_TO_CALL:
        return IconPool.getShared("/img/channelstate/channelstate_ready_to_call.gif");
      case OUTGOING_CALL:
        return IconPool.getShared("/img/channelstate/channelstate_outgoing_call.png");
      case INCOMING_CALL:
        return IconPool.getShared("/img/channelstate/channelstate_incoming_call.png");
      case INCOMING_SCRIPT_CALL:
        return IconPool.getShared("/img/channelstate/channelstate_incoming_script_call.png");
      case IDLE_AFTER_REGISTRATION:
        return IconPool.getShared("/img/channelstate/channelstate_idle_after_registration.gif");
      case IDLE_BEFORE_UNREG:
        return IconPool.getShared("/img/channelstate/channelstate_idle_before_unreg.gif");
      case IDLE_BETWEEN_CALLS:
        return IconPool.getShared("/img/channelstate/channelstate_idle_between_calls.gif");
      case IDLE_BEFORE_SELF_CALL:
        return IconPool.getShared("/img/channelstate/channelstate_idle_before_self_call.gif");
      case IDLE_AFTER_SELF_CALL:
        return IconPool.getShared("/img/channelstate/channelstate_idle_after_self_call.gif");
      case RELEASING:
        return IconPool.getShared("/img/channelstate/channelstate_released.gif");
      case WAITING_SELF_CALL:
        return IconPool.getShared("/img/channelstate/channelstate_self_call.gif");
      case REGISTERING_IN_GSM:
        return IconPool.getShared("/img/channelstate/channelstate_registering_in_gsm.png");
      case STARTED_BUSINESS_ACTIVITY:
        return IconPool.getShared("/img/channelstate/channelstate_business_activity.gif");
      case NETWORK_SURVEY:
        return IconPool.getShared("/img/registration.png");
      case SETTING_IMEI:
        return IconPool.getShared("/img/barcode.png");
      case TURNING_ON_MODULE:
        return IconPool.getShared("/img/mobile_channel.png");
      case CELL_EQUALIZER:
        return IconPool.getShared("/img/cell_equalizer.png");
      default:
        return IconPool.getShared("/img/invisiblebox.gif");
    }
  }

  public static Icon asIcon(final VoucherState state) {
    if (state != null) {
      switch (state) {
        case BAD:
          return IconPool.getShared("/img/vaucherstate/code_bad.png");
        case NEW:
          return IconPool.getShared("/img/vaucherstate/code_new.png");
        case USED:
          return IconPool.getShared("/img/vaucherstate/code_used.png");
        case USING:
          return IconPool.getShared("/img/vaucherstate/code_using.png");
      }
    }
    return null;
  }

  public static String asText(final VoucherState state) {
    if (state != null) {
      switch (state) {
        case BAD:
          return "Bad";
        case NEW:
          return "New";
        case USED:
          return "Used";
        case USING:
          return "Using";
      }
    }
    return "";
  }

  public static String asText(final CallState.State callState) {
    if (callState != null) {
      switch (callState) {
        case IDLE:
          return "Idle";
        case DIALING:
          return "Dialing";
        case ALERTING:
          return "Alerting";
        case ACTIVE:
          return "Active";
      }
    }
    return "";
  }

  public static String asText(final CallChannelState.State callChannelState) {
    if (callChannelState != null) {
      switch (callChannelState) {
        case READY_TO_CALL:
          return "Ready to call";
        case OUTGOING_CALL:
          return "Outgoing call";
        case INCOMING_CALL:
          return "Incoming call";
        case INCOMING_SCRIPT_CALL:
          return "Incoming script call";
        case IDLE_AFTER_REGISTRATION:
          return "Idle after registration";
        case IDLE_BEFORE_UNREG:
          return "Idle before unerg";
        case IDLE_BETWEEN_CALLS:
          return "Idle between calls";
        case IDLE_BEFORE_SELF_CALL:
          return "Idle before self call";
        case IDLE_AFTER_SELF_CALL:
          return "Idle after self call";
        case RELEASING:
          return "Releasing";
        case WAITING_SELF_CALL:
          return "Waiting self call";
        case REGISTERING_IN_GSM:
          return "Registering in GSM";
        case STARTED_BUSINESS_ACTIVITY:
          return "Started business activity";
        case NETWORK_SURVEY:
          return "Network survey";
        case SETTING_IMEI:
          return "Setting IMEI";
        case TURNING_ON_MODULE:
          return "Turning on module";
        case CELL_EQUALIZER:
          return "Cell equalizer";
      }
    }
    return "";
  }
}

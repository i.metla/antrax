/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.panels.registry;

import com.flamesgroup.antrax.automation.registry.RegistryEntry;
import com.flamesgroup.antrax.control.swingwidgets.table.ColumnWriter;
import com.flamesgroup.antrax.control.swingwidgets.table.JUpdatableTable;
import com.flamesgroup.antrax.control.swingwidgets.table.TableBuilder;
import com.flamesgroup.antrax.control.swingwidgets.table.UpdateTableColumnModel;

import java.text.Format;
import java.text.SimpleDateFormat;

public class RegistryValuesTable extends JUpdatableTable<RegistryEntry, String> {

  private static final long serialVersionUID = -8621675776929229714L;

  public RegistryValuesTable() {
    super(new TableBuilder<RegistryEntry, String>() {

      private final Format formatter = new SimpleDateFormat("dd.MM.yy HH:mm:ss");

      @Override
      public void buildColumns(final UpdateTableColumnModel columns) {
        columns.addColumn("value", String.class);
        columns.addColumn("update time", String.class);
      }

      @Override
      public void buildRow(final RegistryEntry src, final ColumnWriter<RegistryEntry> dest) {
        dest.writeColumn(src.getValue());
        dest.writeColumn(calculateTime(src.getUpdatingTime().getTime()));
      }

      @Override
      public String getUniqueKey(final RegistryEntry src) {
        return src.getValue();
      }

      private String calculateTime(final long time) {
        if (time == 0) {
          return " - no - ";
        } else {
          return formatter.format(time);
        }
      }

    });
  }

}

/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.utils;

import com.cedarsoftware.util.GraphComparator;
import com.flamesgroup.antrax.control.authorization.ClientUID;
import com.flamesgroup.antrax.control.authorization.NotPermittedException;
import com.flamesgroup.antrax.control.communication.ControlBean;
import com.flamesgroup.antrax.control.communication.ISimServerStatus;
import com.flamesgroup.antrax.control.communication.IVoiceServerChannelsInfo;
import com.flamesgroup.antrax.control.communication.IVoiceServerStatus;
import com.flamesgroup.antrax.control.communication.MobileGatewayChannelInformation;
import com.flamesgroup.antrax.control.communication.SimViewData;
import com.flamesgroup.antrax.control.communication.SmsHistory;
import com.flamesgroup.antrax.control.communication.SmsStatistic;
import com.flamesgroup.antrax.control.communication.StorageException;
import com.flamesgroup.antrax.control.guiclient.MainApp;
import com.flamesgroup.antrax.distributor.ChannelConfig;
import com.flamesgroup.antrax.distributor.Server;
import com.flamesgroup.antrax.storage.commons.IServerData;
import com.flamesgroup.antrax.storage.commons.SessionParams;
import com.flamesgroup.antrax.storage.commons.impl.CDR;
import com.flamesgroup.antrax.storage.commons.impl.SIMEventRec;
import com.flamesgroup.antrax.storage.commons.impl.SimSearchingParams;
import com.flamesgroup.commons.ChannelUID;
import com.flamesgroup.commons.DTMFException;
import com.flamesgroup.commons.EventException;
import com.flamesgroup.commons.LockArfcnException;
import com.flamesgroup.commons.LockException;
import com.flamesgroup.commons.NetworkSurveyException;
import com.flamesgroup.commons.Pair;
import com.flamesgroup.commons.USSDException;
import com.flamesgroup.commons.VoiceServerCallStatistic;
import com.flamesgroup.unit.ICCID;
import com.flamesgroup.unit.sms.SMSException;

import java.util.Date;
import java.util.List;
import java.util.Map;

public class ControlBeanAccess {

  private final ControlBean controlBean;

  public ControlBeanAccess(final ControlBean controlBean) {
    this.controlBean = controlBean;
  }

  public List<IServerData> listVoiceServers() throws NotPermittedException, StorageException {
    return controlBean.listVoiceServers(MainApp.clientUID);
  }

  public List<IVoiceServerChannelsInfo> listVoiceServerChannels(final IServerData server) throws NotPermittedException, NoSuchFieldException {
    return controlBean.listVoiceServerChannels(MainApp.clientUID, server);
  }

  public List<IServerData> listSimServers() throws NotPermittedException, StorageException {
    return controlBean.listSimServers(MainApp.clientUID);
  }

  public List<SimViewData> getSimRuntimeData(final String serverName) throws NotPermittedException {
    return controlBean.getSimRuntimeData(MainApp.clientUID, serverName);
  }

  public void fireEvent(final IServerData server, final ICCID simUID, final String event) throws NotPermittedException, NoSuchFieldException, EventException {
    controlBean.fireEvent(MainApp.clientUID, server, simUID, event);
  }

  public void lockGsmChannel(final IServerData server, final ChannelUID gsmChannel, final boolean lock, final String lockReason) throws NotPermittedException, NoSuchFieldException, LockException {
    controlBean.lockGsmChannel(MainApp.clientUID, server, gsmChannel, lock, lockReason);
  }

  public void lockGsmChannelToArfcn(final IServerData server, final ChannelUID gsmChannel, final int arfn) throws NotPermittedException, NoSuchFieldException, LockArfcnException {
    controlBean.lockGsmChannelToArfcn(MainApp.clientUID, server, gsmChannel, arfn);
  }

  public void unLockGsmChannelToArfcn(final IServerData server, final ChannelUID gsmChannel) throws NotPermittedException, NoSuchFieldException, LockArfcnException {
    controlBean.unLockGsmChannelToArfcn(MainApp.clientUID, server, gsmChannel);
  }

  public void executeNetworkSurvey(final IServerData server, final ChannelUID gsmChannel) throws NotPermittedException, NoSuchFieldException, NetworkSurveyException {
    controlBean.executeNetworkSurvey(MainApp.clientUID, server, gsmChannel);
  }

  public void sendUssd(final IServerData server, final ICCID simUID, final String ussd) throws NotPermittedException, NoSuchFieldException, USSDException {
    controlBean.sendUssd(MainApp.clientUID, server, simUID, ussd);
  }

  public String startUSSDSession(final IServerData server, final ICCID simUID, final String ussd) throws NotPermittedException, NoSuchFieldException, USSDException {
    return controlBean.startUSSDSession(MainApp.clientUID, server, simUID, ussd);
  }

  public String sendUSSDSessionCommand(final IServerData server, final ICCID simUID, final String command) throws NotPermittedException, NoSuchFieldException, USSDException {
    return controlBean.sendUSSDSessionCommand(MainApp.clientUID, server, simUID, command);
  }

  public void endUSSDSession(final IServerData server, final ICCID simUID) throws NotPermittedException, NoSuchFieldException, USSDException {
    controlBean.endUSSDSession(MainApp.clientUID, server, simUID);
  }

  public void sendSMS(final IServerData server, final ICCID simUID, final String number, final String smsText)
      throws NotPermittedException, NoSuchFieldException, SMSException {
    controlBean.sendSMS(MainApp.clientUID, server, simUID, number, smsText);
  }

  public void sendDTMF(final IServerData server, final ICCID simUID, final String dtmf) throws NotPermittedException, NoSuchFieldException, DTMFException {
    controlBean.sendDTMF(MainApp.clientUID, server, simUID, dtmf);
  }

  public void resetStatistic(final IServerData server) throws NotPermittedException, NoSuchFieldException {
    controlBean.resetStatistic(MainApp.clientUID, server);
  }

  public CDR[] listCDRs(final Date fromDate, final Date toDate, final String gsmGroup, final String simGroup, final String callerNumber, final String calledNumber, final ICCID simUid,
      final int pageSize,
      final int offset)
      throws NotPermittedException {
    return controlBean.listCDRs(MainApp.clientUID, fromDate, toDate, gsmGroup, simGroup, callerNumber, calledNumber, simUid, pageSize, offset);
  }

  public List<SmsHistory> listSmsHistory(final Date fromDate, final Date toDate, final List<String> iccids, final String clientIp) throws NotPermittedException {
    return controlBean.listSmsHistory(MainApp.clientUID, fromDate, toDate, iccids, clientIp);
  }

  public List<VoiceServerCallStatistic> listVoiceServerCallStatistic(final Date fromDate, final Date toDate, final String prefix, final String server) throws NotPermittedException {
    return controlBean.listVoiceServerCallStatistic(MainApp.clientUID, fromDate, toDate, prefix, server);
  }

  public SIMEventRec[] listSIMEvents(final ICCID simUID, final Date fromDate, final Date toDate, final int limit) throws NotPermittedException {
    return controlBean.listSIMEvents(MainApp.clientUID, simUID, fromDate, toDate, limit);
  }

  public List<MobileGatewayChannelInformation> listVoiceServerChannelsStatistic(final IServerData server) throws NotPermittedException, NoSuchFieldException {
    return controlBean.listVoiceServerChannelsStatistic(MainApp.clientUID, server);
  }

  public List<SmsStatistic> listSmsStatistic() throws NotPermittedException, NoSuchFieldException {
    return controlBean.listSmsStatistic(MainApp.clientUID);
  }

  public ICCID[] findSIMList(final SimSearchingParams params) throws NotPermittedException, StorageException {
    return controlBean.findSIMList(MainApp.clientUID, params);
  }

  public Pair<Long, List<ChannelConfig>> getSimChannelConfig(final String serverName) throws NotPermittedException {
    return controlBean.getSimChannelConfig(MainApp.clientUID, serverName);
  }

  public List<GraphComparator.Delta> getSimRuntimeDataDiff(final String serverName) throws NotPermittedException {
    return controlBean.getSimRuntimeDataDiff(MainApp.clientUID, serverName);
  }

  public CDR getCDR(final Long id) throws NotPermittedException, StorageException {
    return controlBean.getCDR(MainApp.clientUID, id);
  }

  public Map<Server, IVoiceServerStatus> listVoiceServerStatus() throws NotPermittedException {
    return controlBean.listVoiceServerStatus(MainApp.clientUID);
  }

  public Map<Server, ISimServerStatus> listSimServerStatus() throws NotPermittedException {
    return controlBean.listSimServerStatus(MainApp.clientUID);
  }

  public int getSessionsCount() throws NotPermittedException {
    return controlBean.getSessionsCount(MainApp.clientUID);
  }

  public SessionParams[] getSessionInfos() throws NotPermittedException {
    return controlBean.getSessionInfos(MainApp.clientUID);
  }

  public boolean checkPermission(final ClientUID client, final String action) throws NotPermittedException {
    return controlBean.checkPermission(client, action);
  }

  public void checkSession() throws NotPermittedException {
    controlBean.checkSession(MainApp.clientUID);
  }

}

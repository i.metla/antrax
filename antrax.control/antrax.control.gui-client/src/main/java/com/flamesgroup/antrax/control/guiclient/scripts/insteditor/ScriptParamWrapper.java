/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.scripts.insteditor;

import com.flamesgroup.antrax.automation.meta.ScriptParameter;

public class ScriptParamWrapper {
  private final ScriptParameter param;
  private final int index;

  public ScriptParamWrapper(final ScriptParameter param, final int index) {
    assert param != null;
    this.param = param;
    this.index = index;
  }

  public ScriptParameter getParam() {
    return param;
  }

  public int getIndex() {
    return index;
  }

  public static ScriptParamWrapper[] wrap(final ScriptParameter... params) {
    ScriptParamWrapper[] retval = new ScriptParamWrapper[params.length];
    for (int i = 0; i < params.length; ++i) {
      retval[i] = new ScriptParamWrapper(params[i], i);
    }
    return retval;
  }
}

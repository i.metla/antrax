/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.panels.tablebuilders;

import com.flamesgroup.antrax.control.swingwidgets.table.ColumnWriter;
import com.flamesgroup.antrax.control.swingwidgets.table.TableBuilder;
import com.flamesgroup.antrax.control.swingwidgets.table.UpdateTableColumn;
import com.flamesgroup.antrax.control.swingwidgets.table.UpdateTableColumnModel;
import com.flamesgroup.antrax.storage.commons.IServerData;

import javax.swing.*;

public class ServerRowBuilder implements TableBuilder<IServerData, Long> {

  @Override
  public void buildRow(final IServerData server, final ColumnWriter<IServerData> dest) {
    dest.writeColumn(server.getName());
    if (server.getPublicAddress() != null) {
      dest.writeColumn(server.getPublicAddress().getHostAddress());
    } else {
      dest.writeColumn("");
    }
    dest.writeColumn(getPresentState(server.isVoiceServerEnabled()));
    dest.writeColumn(getPresentState(server.isSimServerEnabled()));
  }

  @Override
  public Long getUniqueKey(final IServerData server) {
    return server.getID();
  }

  private static String getPresentState(final boolean present) {
    return present ? "yes" : "";
  }

  @Override
  public void buildColumns(final UpdateTableColumnModel columns) {
    UpdateTableColumn columnName = columns.addColumn("Name", String.class);
    columns.addColumn("Public interface", String.class);
    columns.addColumn("Voice server", String.class);
    columns.addColumn("SIM server", String.class);

    columns.addColumnSortSequence(columnName, SortOrder.ASCENDING);
  }

}

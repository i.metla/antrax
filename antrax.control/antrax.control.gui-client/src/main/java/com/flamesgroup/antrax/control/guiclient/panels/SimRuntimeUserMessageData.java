/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.panels;

public class SimRuntimeUserMessageData {
  final String msg1;
  final String msg2;
  final String msg3;
  final long msgTimeout1;
  final long msgTimeout2;
  final long msgTimeout3;

  SimRuntimeUserMessageData(final String msg1, final long msgTimeout1, final String msg2, final long msgTimeout2, final String msg3, final long msgTimeout3) {
    this.msg1 = msg1;
    this.msg2 = msg2;
    this.msg3 = msg3;
    this.msgTimeout1 = msgTimeout1;
    this.msgTimeout2 = msgTimeout2;
    this.msgTimeout3 = msgTimeout3;
  }

  @Override
  public String toString() {
    return msg1;
  }

  public String getUserMsg2() {
    return this.msg2;
  }

  public String getUserMsg3() {
    return this.msg3;
  }

  public long getUserMsg1Timeout() {
    return this.msgTimeout1;
  }

  public long getUserMsg2Timeout() {
    return this.msgTimeout2;
  }

  public long getUserMsg3Timeout() {
    return this.msgTimeout3;
  }

}

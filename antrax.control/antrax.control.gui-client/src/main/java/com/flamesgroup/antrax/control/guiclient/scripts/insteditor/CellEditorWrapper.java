/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.scripts.insteditor;

import com.flamesgroup.antrax.automation.editors.PropertyEditor;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.EventObject;

import javax.swing.*;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.table.TableCellEditor;

public class CellEditorWrapper extends AbstractCellEditor implements TableCellEditor, CellEditorListener {

  private static final long serialVersionUID = -9162662306080382924L;

  private PropertyEditor<Object> propEditor = null;

  private final JButton button;
  private final CellEditorDialog dialog = new CellEditorDialog();

  public CellEditorWrapper() {
    button = new JButton("...");
    button.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent e) {
        if (dialog.showEditor(button, "", propEditor)) {
          fireEditingStopped();
        } else {
          fireEditingCanceled();
        }
      }
    });
    button.putClientProperty("JButton.buttonType", "recessed");
  }

  @SuppressWarnings("unchecked")
  public void wrap(final PropertyEditor<?> propEditor) {
    assert propEditor != null;
    if (this.propEditor != null) {
      propEditor.removeCellEditorListener(this);
    }
    this.propEditor = (PropertyEditor<Object>) propEditor;
    this.propEditor.addCellEditorListener(this);
  }

  @Override
  public Component getTableCellEditorComponent(final JTable table, final Object value, final boolean isSelected, final int row, final int column) {
    propEditor.setValue(value);

    Dimension editorSize = propEditor.getEditorComponent().getPreferredSize();
    //    if (editorSize.height - 10 > table.getRowHeight() || editorSize.width - 10 > table.getColumnModel().getColumn(column).getWidth()) {
    return button;
    //    } else {
    //      return propEditor.getEditorComponent();
    //    }
  }

  @Override
  public Object getCellEditorValue() {
    return propEditor.getValue();
  }

  @Override
  public boolean shouldSelectCell(final EventObject anEvent) {
    return propEditor.shouldSelectCell(anEvent);
  }

  @Override
  public boolean stopCellEditing() {
    boolean retval = propEditor.stopEditing();
    if (retval) {
      fireEditingStopped();
    } else {
      fireEditingCanceled();
    }
    return retval;
  }

  @Override
  public void editingCanceled(final ChangeEvent e) {
    if (dialog.isVisible()) {
      dialog.cancelEditing();
    } else {
      fireEditingCanceled();
    }
  }

  @Override
  public void editingStopped(final ChangeEvent e) {
    if (dialog.isVisible()) {
      dialog.stopEditing();
    } else {
      fireEditingStopped();
    }
  }
}

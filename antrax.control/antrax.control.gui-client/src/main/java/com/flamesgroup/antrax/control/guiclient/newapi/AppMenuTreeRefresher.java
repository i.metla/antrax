/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.newapi;

import com.flamesgroup.antrax.control.authorization.NotPermittedException;
import com.flamesgroup.antrax.control.communication.ISimServerStatus;
import com.flamesgroup.antrax.control.communication.IVoiceServerChannelsInfo;
import com.flamesgroup.antrax.control.communication.IVoiceServerShortInfo;
import com.flamesgroup.antrax.control.communication.IVoiceServerStatus;
import com.flamesgroup.antrax.control.communication.ServerStatus;
import com.flamesgroup.antrax.control.guiclient.Converters;
import com.flamesgroup.antrax.control.guiclient.MainApp;
import com.flamesgroup.antrax.control.guiclient.newapi.panels.GsmViewPanelProvider;
import com.flamesgroup.antrax.control.guiclient.newapi.panels.SimServersPanelProvider;
import com.flamesgroup.antrax.control.guiclient.newapi.panels.VoiceServersPanelProvider;
import com.flamesgroup.antrax.control.guiclient.utils.BeansPool;
import com.flamesgroup.antrax.control.guiclient.utils.Refresher;
import com.flamesgroup.antrax.distributor.ChannelConfig;
import com.flamesgroup.antrax.distributor.Server;
import com.flamesgroup.antrax.distributor.ServerType;
import com.flamesgroup.commons.Pair;
import com.flamesgroup.device.protocol.cudp.ExpireTime;

import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class AppMenuTreeRefresher extends Refresher {
  private static final String N_A = "N/A";

  private volatile String scriptsIsOk = N_A;
  private volatile String sessionsCount = N_A;
  private volatile String turnedOffVoiceServersCount = N_A;
  private volatile String turnedOffSimServersCount = N_A;
  private volatile Map<Server, IVoiceServerStatus> voiceServersStatuses = Collections.emptyMap();
  private volatile Map<Server, ISimServerStatus> simServersStatuses = Collections.emptyMap();
  private volatile Map<String, Pair<Long, List<ChannelConfig>>> simServersChannelsConfig = new ConcurrentHashMap<>();
  private volatile Map<String, Date> lastUpdateNetworkSurvey = Collections.emptyMap();

  private final AppMenuNode voiceServersNode;
  private final AppMenuNode gsmViewNode;
  private final AppMenuNode simServersNode;

  private final Badge sessionsBadge;
  private final Badge scriptsBadge;
  private final Badge voiceServersBadge;
  private final Badge simServersBadge;

  public AppMenuTreeRefresher(final AppMenuTree menuTree) {
    super("AppMenuTreeRefresher");
    this.voiceServersNode = menuTree.getMenuNode().findNode("VOICE SERVERS");
    this.gsmViewNode = menuTree.getMenuNode().findNode("GSM-view");
    this.simServersNode = menuTree.getMenuNode().findNode("SIM SERVERS");
    this.voiceServersBadge = getBadge(voiceServersNode);
    this.simServersBadge = getBadge(simServersNode);
    this.sessionsBadge = getBadge(menuTree, "SESSIONS");
    this.scriptsBadge = getBadge(menuTree, "CONFIGURATION/Scripts");
  }

  /**
   * Extracts badge from tree by path. Path is '/' separated node names. For example: "GATEWAYS/vs0"
   *
   * @param path
   * @return
   */
  private Badge getBadge(final AppMenuTree tree, final String path) {
    String[] splitedPath = path.split("/");
    AppMenuNode node = tree.getMenuNode();
    for (int i = 0; i < splitedPath.length; ++i) {
      node = node.findNode(splitedPath[i]);
      if (node == null) {
        break;
      }
    }
    return getBadge(node);
  }

  private Badge getBadge(final AppMenuNode node) {
    if (node == null) {
      return null;
    }
    return node.getBadge();
  }

  @Override
  public void handleDisabled(final BeansPool beansPool) {
  }

  @Override
  public void handleEnabled(final BeansPool beansPool) {
  }

  @Override
  protected void refreshInformation(final BeansPool beansPool) throws Exception {
    try {
      if (voiceServersNode != null) {
        voiceServersStatuses = beansPool.getControlBean().listVoiceServerStatus();
        turnedOffVoiceServersCount = badgeInteger(countVoiceServerStatus(voiceServersStatuses, ServerStatus.NO_CONNECTION));
      }
    } catch (NotPermittedException ignored) {
      voiceServersStatuses = Collections.emptyMap();
    }
    try {
      if (simServersNode != null) {
        simServersStatuses = beansPool.getControlBean().listSimServerStatus();
        for (Server server : simServersStatuses.keySet()) {
          simServersChannelsConfig.put(server.getName(), beansPool.getControlBean().getSimChannelConfig(server.getName()));
        }
        turnedOffSimServersCount = badgeInteger(countSimServerStatus(simServersStatuses, ServerStatus.NO_CONNECTION));
      }
    } catch (NotPermittedException ignored) {
      simServersStatuses = Collections.emptyMap();
      simServersChannelsConfig = new ConcurrentHashMap<>();
    }

    try {
      if (sessionsBadge != null) {
        sessionsCount = badgeInteger(beansPool.getControlBean().getSessionsCount());
      }
    } catch (NotPermittedException ignored) {
      sessionsCount = N_A;
    }
    try {
      if (scriptsBadge != null) {
        scriptsIsOk = beansPool.getConfigBean().isScriptsOk(MainApp.clientUID) ? null : "!";
      }
    } catch (NotPermittedException ignored) {
      scriptsIsOk = N_A;
    }

    if (gsmViewNode != null) {
      lastUpdateNetworkSurvey = beansPool.getGsmViewBean().getLastNetworkSurvey(MainApp.clientUID);
    }
  }

  private int countVoiceServerStatus(final Map<Server, IVoiceServerStatus> servers, final ServerStatus status) {
    int retval = 0;
    for (IVoiceServerStatus serverStatus : servers.values()) {
      if (serverStatus.getShortInfo().getStatus() == status) {
        retval++;
      }
    }
    return retval;
  }

  private int countSimServerStatus(final Map<Server, ISimServerStatus> servers, final ServerStatus status) {
    int retval = 0;
    for (ISimServerStatus serverStatus : servers.values()) {
      if (serverStatus.getShortInfo().getStatus() == status) {
        retval++;
      }
    }
    return retval;
  }

  @Override
  public void updateGuiElements() {
    if (voiceServersNode != null) {
      syncVoiceServersNode(voiceServersNode, voiceServersStatuses);
      updateVoiceServersBadges(voiceServersNode, voiceServersStatuses);
      voiceServersBadge.setNotice(turnedOffVoiceServersCount);
    }
    if (gsmViewNode != null) {
      syncGsmViewNode(gsmViewNode, voiceServersStatuses);
      updateGsmViewBadges(gsmViewNode, lastUpdateNetworkSurvey, voiceServersStatuses);
    }
    if (simServersNode != null) {
      syncSimServersNode(simServersNode, simServersStatuses);
      updateSimServersBadges(simServersNode, simServersStatuses, simServersChannelsConfig);
      simServersBadge.setNotice(turnedOffSimServersCount);
    }

    if (sessionsBadge != null) {
      sessionsBadge.setInfo(sessionsCount);
    }
    if (scriptsBadge != null) {
      scriptsBadge.setError(scriptsIsOk);
    }
  }

  private void updateSimServersBadges(final AppMenuNode node, final Map<Server, ISimServerStatus> servers, final Map<String, Pair<Long, List<ChannelConfig>>> simServersChannelsConfig) {
    if (servers.isEmpty()) {
      for (int i = 0; i < node.getChildCount(); ++i) {
        Badge badge = node.getChild(i).getBadge();
        if (badge != null) {
          badge.setInfo(N_A);
          badge.setNotice(N_A);
        }
      }
    } else {
      for (Map.Entry<Server, ISimServerStatus> entry : servers.entrySet()) {
        AppMenuNode ss = node.findNode(entry.getKey().getName());
        ss.setIcon(Converters.convertServerStatus(entry.getValue().getShortInfo().getStatus()));
        int count = 0;
        Pair<Long, List<ChannelConfig>> longListPair = simServersChannelsConfig.get(entry.getKey().getName());
        List<ChannelConfig> channelConfigs = longListPair.second();
        if (channelConfigs == null) {
          continue;
        }
        for (ChannelConfig channelConfig : channelConfigs) {
          if (channelConfig == null) {
            continue;
          }

          Calendar calendar = new GregorianCalendar();
          calendar.setTimeInMillis(longListPair.first());
          calendar.add(Calendar.DAY_OF_YEAR, 3);

          ExpireTime expireTime = channelConfig.getExpireTime();
          if (!channelConfig.getExpireTime().isUnlimited() && expireTime.getValue() <= calendar.getTime().getTime()) {
            count++;
          }
        }
        ss.getBadge().setWarning(badgeInteger(count));
        if (count > 0) {
          ss.getBadge().setWarningHelp("The license will expire in 3 days at " + count + " channels");
        } else {
          ss.getBadge().setWarningHelp(null);
        }
      }
    }
  }

  private void updateVoiceServersBadges(final AppMenuNode node, final Map<Server, IVoiceServerStatus> servers) {
    if (servers.isEmpty()) {
      for (int i = 0; i < node.getChildCount(); ++i) {
        Badge badge = node.getChild(i).getBadge();
        if (badge != null) {
          badge.setInfo(N_A);
          badge.setNotice(N_A);
        }
      }
    } else {
      for (Map.Entry<Server, IVoiceServerStatus> entry : servers.entrySet()) {
        AppMenuNode vs = node.findNode(entry.getKey().getName());
        IVoiceServerShortInfo shortInfo = entry.getValue().getShortInfo();
        List<IVoiceServerChannelsInfo> channelsInfo = entry.getValue().getChannelsInfo();
        int count = 0;
        for (IVoiceServerChannelsInfo voiceServerChannelsInfo : channelsInfo) {
          ChannelConfig channelConfig = voiceServerChannelsInfo.getMobileGatewayChannelInfo().getChannelConfig();
          if (channelConfig == null) {
            continue;
          }

          Calendar calendar = new GregorianCalendar();
          calendar.setTimeInMillis(voiceServerChannelsInfo.getServerTimeMillis());
          calendar.add(Calendar.DAY_OF_YEAR, 3);

          ExpireTime expireTime = channelConfig.getExpireTime();
          if (!channelConfig.getExpireTime().isUnlimited() && expireTime.getValue() <= calendar.getTime().getTime()) {
            count++;
          }
        }
        vs.setIcon(Converters.convertServerStatus(shortInfo.getStatus()));
        vs.getBadge().setInfo(badgeInteger(shortInfo.getOutgoingCalls()));
        vs.getBadge().setNotice(badgeInteger(shortInfo.getFreeGSMChannels()));
        vs.getBadge().setWarning(badgeInteger(count));
        if (count > 0) {
          vs.getBadge().setWarningHelp("The license will expire in 3 days at " + count + " channels");
        } else {
          vs.getBadge().setWarningHelp(null);
        }
      }
    }
  }

  private void updateGsmViewBadges(final AppMenuNode node, final Map<String, Date> lastUpdateNetworkSurvey, final Map<Server, IVoiceServerStatus> servers) {
    if (!servers.isEmpty()) {
      for (Server server : servers.keySet()) {
        AppMenuNode vs = node.findNode(server.getName());
        Date date = lastUpdateNetworkSurvey.get(server.getName());
        vs.setIcon(Converters.convertGsmViewStatus(date));
      }
    }
  }

  private String badgeInteger(final int value) {
    if (value == 0) {
      return null;
    }
    return String.valueOf(value);
  }

  private void syncSimServersNode(final AppMenuNode node, final Map<Server, ISimServerStatus> servers) {
    Set<Server> oldServers = collect(node, ServerType.SIM_SERVER);
    Set<Server> newServers = servers.keySet().stream().filter(e -> !oldServers.contains(e)).collect(Collectors.toSet());
    oldServers.removeAll(servers.keySet()); // Absolutely old: requires to be deleted

    // Removing old nodes
    for (Server server : oldServers) {
      node.removeNode(node.findNode(server.getName()));
    }
    // Adding new nodes
    for (Server server : newServers) {
      node.addNode(new AppMenuNode(server.getName(), null, createSimServerBadge(), new SimServersPanelProvider(server.getName())));
    }

    node.sortChildren();
  }

  private void syncVoiceServersNode(final AppMenuNode node, final Map<Server, IVoiceServerStatus> servers) {
    Set<Server> oldServers = collect(node, ServerType.VOICE_SERVER);
    Set<Server> newServers = servers.keySet().stream().filter(e -> !oldServers.contains(e)).collect(Collectors.toSet());
    oldServers.removeAll(servers.keySet()); // Absolutely old: requires to be deleted

    // Removing old nodes
    for (Server server : oldServers) {
      node.removeNode(node.findNode(server.getName()));
    }
    // Adding new nodes
    for (Server server : newServers) {
      node.addNode(new AppMenuNode(server.getName(), null, createVoiceServerBadge(), new VoiceServersPanelProvider(server.getName(),
          (VoiceServersPanelProvider) voiceServersNode.getPanel())));
    }

    node.sortChildren();
  }

  private void syncGsmViewNode(final AppMenuNode node, final Map<Server, IVoiceServerStatus> servers) {
    Set<Server> oldServers = collect(node, ServerType.VOICE_SERVER);
    Set<Server> newServers = servers.keySet().stream().filter(e -> !oldServers.contains(e)).collect(Collectors.toSet());
    oldServers.removeAll(servers.keySet()); // Absolutely old: requires to be deleted

    // Removing old nodes
    for (Server server : oldServers) {
      node.removeNode(node.findNode(server.getName()));
    }
    // Adding new nodes
    for (Server server : newServers) {
      node.addNode(new AppMenuNode(server.getName(), null, createGsmViewBadge(), new GsmViewPanelProvider(server.getName())));
    }

    node.sortChildren();
  }

  private Badge createVoiceServerBadge() {
    Badge retval = new Badge();
    retval.setInfoHelp("Current calls count");
    retval.setNoticeHelp("Free GSM channels");
    return retval;
  }

  private Badge createSimServerBadge() {
    return new Badge();
  }

  private Badge createGsmViewBadge() {
    return new Badge();
  }

  private Set<Server> collect(final AppMenuNode node, final ServerType serverType) {
    Set<Server> retval = new HashSet<>();
    for (int i = 0; i < node.getChildCount(); ++i) {
      retval.add(new Server(node.getChild(i).getText(), serverType));
    }
    return retval;
  }

}

package com.flamesgroup.antrax.control.swingwidgets.tree;

import javax.swing.*;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeWillExpandListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.ExpandVetoException;
import javax.swing.tree.MutableTreeNode;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

public class LazyLoadingTreeController implements TreeWillExpandListener {

  private final JTree tree;

  public LazyLoadingTreeController(final JTree tree) {
    this.tree = tree;
  }

  private TreeModel getTreeModel() {
    return tree.getModel();
  }

  @Override
  public void treeWillCollapse(final TreeExpansionEvent event)
      throws ExpandVetoException {
    // do nothing on collapse
  }

  /**
   * Invoked whenever a node in the tree is about to be expanded. If the Node
   * is a LazyLoadingTreeNode load it's children in a SwingWorker
   */
  @Override
  public void treeWillExpand(final TreeExpansionEvent event)
      throws ExpandVetoException {
    TreePath path = event.getPath();
    Object lastPathComponent = path.getLastPathComponent();
    if (lastPathComponent instanceof LazyLoadingTreeNode) {
      LazyLoadingTreeNode lazyNode = (LazyLoadingTreeNode) lastPathComponent;
      expandNode(lazyNode);
    }
  }

  /**
   * If the node is not already loaded
   *
   * @param node lazy loading tree node
   */
  public void expandNode(final LazyLoadingTreeNode node) {
    if (node.isChildrenLoaded()) {
      return;
    }

    node.setChildren(createLoadingNode());
    SwingWorker<MutableTreeNode[], ?> worker = createSwingWorker(node);
    worker.execute();
  }

  /**
   * Creates a new Loading node
   *
   * @return 'loading ...' node
   */
  protected MutableTreeNode createLoadingNode() {
    return new DefaultMutableTreeNode("Loading ...", false);
  }

  protected BackgroundWorker<MutableTreeNode[]> createNodeWorker(final LazyLoadingTreeNode node) {
    return new BackgroundWorker<MutableTreeNode[]>() {

      @Override
      public void done(final MutableTreeNode[] nodes) {
        node.setAllowsChildren(nodes != null && nodes.length > 0);
        node.setChildren(nodes);
      }

      @Override
      public MutableTreeNode[] doInBackground() {
        return node.loadChildren(getTreeModel());
      }

    };
  }

  /**
   * Create worker that will load the nodes
   */
  protected SwingWorker<MutableTreeNode[], ?> createSwingWorker(final LazyLoadingTreeNode node) {
    return new LazyLoadingWorker(createNodeWorker(node));
  }

}

package com.flamesgroup.antrax.control.swingwidgets.editor;

import java.text.NumberFormat;

import javax.swing.*;
import javax.swing.text.NumberFormatter;

public class JEditIntField extends JFormattedTextField {

  private static final long serialVersionUID = 3469291956168383253L;

  public JEditIntField(final int min, final int max) {
    super(createNumberFormatter(min, max));
    setValue(min);
  }

  private static NumberFormatter createNumberFormatter(final int min, final int max) {
    NumberFormat format = NumberFormat.getIntegerInstance();
    format.setGroupingUsed(false);
    NumberFormatter nf = new NumberFormatter(format);
    nf.setMinimum(min);
    nf.setMaximum(max);
    nf.setValueClass(Integer.class);
    return nf;
  }

  public int getIntValue() {
    try {
      int value = Integer.parseInt(getText());
      NumberFormatter nf = (NumberFormatter) getFormatterFactory().getFormatter(this);
      int max = (Integer) nf.getMaximum();
      int min = (Integer) nf.getMinimum();
      if (value > max) {
        value = max;
      }
      if (value < min) {
        value = min;
      }
      setValue(value);
    } catch (NumberFormatException ignored) {
    }
    Integer value = (Integer) getValue();
    return value.intValue();
  }

}

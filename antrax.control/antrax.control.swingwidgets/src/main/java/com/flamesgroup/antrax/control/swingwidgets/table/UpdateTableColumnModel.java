package com.flamesgroup.antrax.control.swingwidgets.table;

import javax.swing.*;

public interface UpdateTableColumnModel {

  UpdateTableColumn addColumn(String name, Class<?> type);

  void addColumnSortSequence(UpdateTableColumn column, SortOrder sortOrder);

  UpdateTableColumn getColumnAt(int columnIndex);

  int size();

  ColumnSortOrder[] getDefaultSortSequence();

  int columnIndexOf(UpdateTableColumn column);

}

package com.flamesgroup.antrax.control.swingwidgets.tree;

public interface BackgroundWorker<T> {

  void done(T result);

  T doInBackground();

}

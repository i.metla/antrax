package com.flamesgroup.antrax.control.swingwidgets.pagging;

import com.flamesgroup.antrax.control.swingwidgets.IconPool;
import com.flamesgroup.antrax.control.swingwidgets.dialog.WaitingDialog;
import com.flamesgroup.antrax.control.swingwidgets.editor.JReflectiveButton;
import com.flamesgroup.antrax.control.swingwidgets.field.IntegerField;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.swing.*;

public class JPaggingBar<Q> {

  private static final int FIRST_PAGE = 1;

  public interface PaggingHandler<Q> {
    boolean onPageChanged(Q queryData, int pageSize, int offset);
  }

  private final PaggingHandler<Q> handler;
  private Q queryData;
  private JLabel pageSizeLabel;
  private JPanel panel;
  private JButton buttonFirst;
  private JButton buttonPrevious;
  private JButton buttonNext;
  private JButton buttonLast;
  private JComboBox comboBoxPageSize;
  private IntegerField fieldCurrentPage;
  private int currentPage = FIRST_PAGE;
  private Integer maxOffset;

  public JPaggingBar(final PaggingHandler<Q> handler, final int defaultPageSize) {
    this.handler = handler;
    creteGUIElements(defaultPageSize);
    layoutGUIElements();
    setEditable(false);
    setMaxOffset(null);
  }

  public JComponent getComponent() {
    return panel;
  }

  private int getMaxPage() {
    if (maxOffset == null) {
      return Integer.MAX_VALUE;
    } else {
      int p = maxOffset / getPageSize();
      if (maxOffset % getPageSize() > 0) {
        p++;
      }
      return p;
    }
  }

  private void setMaxOffset(final Integer maxOffset) {
    this.maxOffset = maxOffset;
    getLastButton().setEnabled(maxOffset != null);
  }

  public void applyQuery(final Q queryData) {
    applyQuery(queryData, null);
  }

  public void applyQuery(final Q queryData, final Integer maxOffset) {
    this.queryData = queryData;
    setEditable(true);
    setMaxOffset(maxOffset);
    setCurrentPage(FIRST_PAGE, true);
  }

  private void creteGUIElements(final int defaultPageSize) {
    panel = new JPanel();
    pageSizeLabel = new JLabel("Page size");
    comboBoxPageSize = new JComboBox();
    comboBoxPageSize.setModel(new DefaultComboBoxModel(new Integer[] {10, 25, 50, 100, 150, 200, 250, 300, 500, 700, 1000}));
    comboBoxPageSize.setSelectedItem(defaultPageSize);

    fieldCurrentPage = new IntegerField(1, Integer.MAX_VALUE);
    fieldCurrentPage.setHorizontalAlignment(SwingConstants.CENTER);
    fieldCurrentPage.setContent(FIRST_PAGE);

    addCurrentPageListener();
  }

  private final KeyListener currentPageKeyListener = new KeyListener() {

    @Override
    public void keyPressed(final KeyEvent e) {
    }

    @Override
    public void keyReleased(final KeyEvent e) {
    }

    @Override
    public void keyTyped(final KeyEvent e) {
      if (e.getKeyChar() == '\n') {
        fireApplyCurrentPage();
      }
    }

  };

  private final FocusListener currentPageFieldFocusListener = new FocusListener() {

    @Override
    public void focusGained(final FocusEvent e) {

    }

    @Override
    public void focusLost(final FocusEvent e) {
      if (e.isTemporary()) {
        return;
      }
      fireApplyCurrentPage();
    }

  };

  private void fireApplyCurrentPage() {
    removeCurrentPageListener();
    setCurrentPage(fieldCurrentPage.getContent());
    addCurrentPageListener();
  }

  private void addCurrentPageListener() {
    fieldCurrentPage.addFocusListener(currentPageFieldFocusListener);
    fieldCurrentPage.addKeyListener(currentPageKeyListener);
  }

  private void removeCurrentPageListener() {
    fieldCurrentPage.removeFocusListener(currentPageFieldFocusListener);
    fieldCurrentPage.removeKeyListener(currentPageKeyListener);
  }

  private void setEditable(final boolean editable) {
    getLastButton().setVisible(false);

    getFirstButton().setEnabled(editable);
    getLastButton().setEnabled(editable);
    getPreviousButton().setEnabled(editable);
    getNextButton().setEnabled(editable);
    fieldCurrentPage.setEditable(editable);
  }

  private void layoutGUIElements() {
    GroupLayout layout = new GroupLayout(panel);
    panel.setLayout(layout);
    layout.setHorizontalGroup(layout.createParallelGroup().addGroup(
        layout.createSequentialGroup().addContainerGap().addComponent(pageSizeLabel).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(comboBoxPageSize, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(getFirstButton())
            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(getPreviousButton()).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(
            fieldCurrentPage, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(getNextButton())
            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(getLastButton())));
    layout.setVerticalGroup(layout.createParallelGroup().addGroup(
        layout.createSequentialGroup().addContainerGap().addGroup(
            layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(pageSizeLabel)
                .addComponent(comboBoxPageSize, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE).addComponent(getFirstButton()).addComponent(getPreviousButton())
                .addComponent(fieldCurrentPage, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE).addComponent(getNextButton()).addComponent(getLastButton()))));

  }

  private JButton createButton(final Icon icon, final String toolTipText) {
    return new JReflectiveButton.JReflectiveButtonBuilder().setIcon(icon).setToolTipText(toolTipText).build();
  }

  public int getPageSize() {
    return (Integer) comboBoxPageSize.getSelectedItem();
  }

  private int getCurrentPage() {
    return currentPage;
  }

  private void setCurrentPage(final int page) {
    setCurrentPage(page, false);
  }

  private void setCurrentPage(int page, final boolean force) {
    if (page <= 0) {
      page = 1;
    }

    if (!force && page == currentPage) {
      return;
    }

    currentPage = page;

    int pageSize = getPageSize();
    int offset = (page - 1) * getPageSize();

    boolean hasData = firePageChanged(pageSize, offset);

    if (currentPage > getMaxPage()) {
      currentPage = getMaxPage();
    }
    if (currentPage < FIRST_PAGE) {
      currentPage = FIRST_PAGE;
    }
    if (!hasData && page <= getMaxPage()) {
      setMaxOffset(offset);
    }

    fieldCurrentPage.setContent(currentPage);
    getNextButton().setEnabled(currentPage < getMaxPage());
  }

  public static Frame findParentWindow(final Component c) {
    if (c instanceof Frame) {
      return (Frame) c;
    }

    if (c.getParent() == null) {
      return null;
    }

    return findParentWindow(c.getParent());
  }

  private boolean firePageChanged(final int pageSize, final int offset) {
    final AtomicBoolean retval = new AtomicBoolean(false);

    WaitingDialog wd = new WaitingDialog(null, "Loading data . . .", "");
    wd.show(findParentWindow(getFirstButton()), new Runnable() {
      @Override
      public void run() {
        retval.set(handler.onPageChanged(queryData, pageSize, offset));
      }
    });

    return retval.get();
  }

  private JButton getFirstButton() {
    if (buttonFirst == null) {
      buttonFirst = createButton(IconPool.getShared("page-first.gif"), "First page");
      buttonFirst.addActionListener(new ActionListener() {

        @Override
        public void actionPerformed(final ActionEvent e) {
          setCurrentPage(FIRST_PAGE);
        }

      });
    }
    return buttonFirst;
  }

  private JButton getLastButton() {
    if (buttonLast == null) {
      buttonLast = createButton(IconPool.getShared("page-last.gif"), "Last page");
      buttonLast.addActionListener(new ActionListener() {

        @Override
        public void actionPerformed(final ActionEvent e) {
          if (maxOffset == null) {
            return;
          }

          int lastPage = maxOffset / getPageSize();
          if (maxOffset % getPageSize() > 0) {
            lastPage++;
          }
          setCurrentPage(lastPage);
        }

      });
    }
    return buttonLast;
  }

  private JButton getPreviousButton() {
    if (buttonPrevious == null) {
      buttonPrevious = createButton(IconPool.getShared("page-prev.gif"), "Previous page");
      buttonPrevious.addActionListener(new ActionListener() {

        @Override
        public void actionPerformed(final ActionEvent e) {
          if (getCurrentPage() == FIRST_PAGE) {
            return;
          }

          setCurrentPage(getCurrentPage() - 1);
        }

      });
    }
    return buttonPrevious;
  }

  private JButton getNextButton() {
    if (buttonNext == null) {
      buttonNext = createButton(IconPool.getShared("page-next.gif"), "Next page");
      buttonNext.addActionListener(new ActionListener() {

        @Override
        public void actionPerformed(final ActionEvent e) {
          if (getCurrentPage() == Integer.MAX_VALUE) {
            return;
          }
          setCurrentPage(getCurrentPage() + 1);
        }

      });
    }
    return buttonNext;
  }

  public static void main(final String[] args) {
    JFrame frame = new JFrame();
    frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

    JPaggingBar<Integer> pagging = new JPaggingBar<>(new PaggingHandler<Integer>() {

      @Override
      public boolean onPageChanged(final Integer queryData, final int pageSize, final int offset) {
        return true;
      }

    }, 100);

    frame.getContentPane().add(pagging.getComponent());
    frame.setSize(600, 300);
    frame.setLocationRelativeTo(null);
    frame.setVisible(true);

    pagging.applyQuery(1);

  }

}

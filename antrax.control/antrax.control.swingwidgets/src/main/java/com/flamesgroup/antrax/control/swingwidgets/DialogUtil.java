package com.flamesgroup.antrax.control.swingwidgets;

import java.awt.*;

public class DialogUtil {

  public static void center(final Window window, final Window owner) {
    Rectangle sr = window.getGraphicsConfiguration().getBounds();
    Rectangle or;
    if (owner != null && owner.isVisible()) {
      or = owner.getBounds();
    } else {
      or = sr;
    }
    Dimension d = window.getSize();

    Rectangle r = new Rectangle();
    r.setSize(d);

    int x = (int) (or.getX() + or.getWidth() / 2 - d.getWidth() / 2);
    int y = (int) (or.getY() + or.getHeight() / 2 - d.getHeight() / 2);

    if (x < 0) {
      x = 0;
    }
    if (y < 0) {
      y = 0;
    }
    if (x + r.getWidth() > sr.getMaxX()) {
      x = (int) (sr.getMaxX() - r.getWidth());
    }
    if (y + r.getHeight() > sr.getMaxY()) {
      y = (int) (sr.getMaxY() - r.getHeight());
    }

    r.setLocation(x, y);

    window.setBounds(r);
  }

  public static void center(final Window window) {
    center(window, null);
  }

  public static Frame findParentWindow(final Component c) {
    if (c == null) {
      return null;
    }

    if (c instanceof Frame) {
      return (Frame) c;
    }

    if (c.getParent() == null) {
      return null;
    }

    return findParentWindow(c.getParent());
  }
} 

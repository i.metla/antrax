package com.flamesgroup.antrax.control.swingwidgets;

import java.awt.*;

import javax.swing.*;

public class JLabeledComponent extends JPanel {

  private static final long serialVersionUID = 2946563246144486380L;
  private final JLabel lbl;
  private final Component cmp;

  public JLabeledComponent(final String label, final Component component) {
    super(new FlowLayout(FlowLayout.LEFT));
    lbl = new JLabel(label, JLabel.RIGHT);
    this.cmp = component;
    add(lbl);
    add(component);
    lbl.setLabelFor(component);
  }

  public static void fitComponentWidths(final JLabeledComponent... components) {
    int maxWidth = 0;
    for (JLabeledComponent c : components) {
      int width = c.cmp.getPreferredSize().width;
      if (width > maxWidth) {
        maxWidth = width;
      }
    }

    for (JLabeledComponent c : components) {
      c.cmp.setPreferredSize(new Dimension(maxWidth, c.cmp.getPreferredSize().height));
    }
  }

  public static void fitLabelWidths(final JLabeledComponent... components) {
    int maxWidth = 0;
    for (JLabeledComponent c : components) {
      int width = c.lbl.getPreferredSize().width;
      if (width > maxWidth) {
        maxWidth = width;
      }
    }

    for (JLabeledComponent c : components) {
      c.lbl.setPreferredSize(new Dimension(maxWidth, c.lbl.getPreferredSize().height));
    }
  }

}

package com.flamesgroup.antrax.control.swingwidgets.indicators;

import com.flamesgroup.antrax.control.swingwidgets.SwingHelper;

import java.awt.*;
import java.awt.geom.Rectangle2D;

import javax.swing.*;

public class OvalIndicator extends JLabel {

  private static final long serialVersionUID = 7939106658448102413L;

  public Color getColor() {
    return Color.GRAY;
  }

  public Color getTextColor() {
    return Color.WHITE;
  }

  @Override
  protected void paintComponent(final Graphics g) {
    Graphics2D gc = (Graphics2D) g.create();
    try {
      String text = getText();
      if (text == null || text.equals("")) {
        text = "";
      }

      Rectangle bounds = new Rectangle(0, 0, getWidth(), getHeight());
      gc.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

      int width;
      Icon icon = getIcon();
      if (icon != null) { // icon mode
        width = icon.getIconWidth();
        int x = bounds.x + bounds.width - width;
        int y = bounds.y;
        icon.paintIcon(this, gc, x, y);
      } else { // text mode
        // --------- Draw the badge background

        int h = bounds.height;
        // Get the bounds of the badge text in order to calculate the center
        Rectangle2D textBounds = gc.getFontMetrics().getStringBounds(text, gc);

        int defaultBadgeWidth = h * 3 / 4;
        int textWidth = (text.equals("") || (int) textBounds.getWidth() < defaultBadgeWidth)
            ? defaultBadgeWidth
            : (int) textBounds.getWidth();
        width = (2 * textWidth + h) / 2;

        gc.setColor(getColor());

        //gc.setComposite(AlphaComposite.SrcOver);

        gc.fillRoundRect(
            bounds.x + bounds.width - width,
            bounds.y,
            width,
            h,
            h, h);

        // --------- Draw badge text

        // Calculate the bottom left point to draw the text at
        int tx = bounds.x + bounds.width - width / 2 - (int) textBounds.getWidth() / 2;
        int ty = (bounds.height - (int) textBounds.getY()) / 2;

        // Set the color to use for the text - note this color is always
        // the same, though it won't always show because of the composite
        // set below
        gc.setColor(getTextColor());
        // if the badge is selected, punch out the text so that the
        //    underlying color shows through as the font color
        // else use use a standard alpha composite to simply draw on top of
        //    whatever is currently there
        //gc.setComposite(isSelected() ? AlphaComposite.DstOut : AlphaComposite.SrcOver);

        // draw the badge text.
        SwingHelper.drawString(gc, text, tx, ty);
      }

      setSize(width, getHeight());
    } finally {
      if (gc != null) {
        gc.dispose();
      }
    }
  }


}

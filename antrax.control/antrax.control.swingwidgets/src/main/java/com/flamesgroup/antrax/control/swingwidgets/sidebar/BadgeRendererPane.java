package com.flamesgroup.antrax.control.swingwidgets.sidebar;

import com.flamesgroup.antrax.control.swingwidgets.badge.JBadge;

import java.awt.*;
import java.io.IOException;
import java.io.ObjectOutputStream;

import javax.accessibility.Accessible;
import javax.accessibility.AccessibleContext;
import javax.accessibility.AccessibleRole;

public class BadgeRendererPane extends Container implements Accessible {

  private static final long serialVersionUID = 1072526334083362783L;

  /**
   * Construct a CellRendererPane object.
   */
  public BadgeRendererPane() {
    super();
    setLayout(null);
    setVisible(true);
  }

  /**
   * Overridden to avoid propagating a invalidate up the tree when the cell
   * renderer child is configured.
   */
  @Override
  public void invalidate() {
  }

  /**
   * Shouldn't be called.
   */
  @Override
  public void paint(final Graphics g) {
  }

  /**
   * Shouldn't be called.
   */
  @Override
  public void update(final Graphics g) {
  }

  /**
   * If the specified component is already a child of this then we don't
   * bother doing anything - stacking order doesn't matter for cell renderer
   * components (CellRendererPane doesn't paint anyway).<
   */
  @Override
  protected void addImpl(final Component c, final Object constraints, final int index) {
    if (c.getParent() != this) {
      super.addImpl(c, constraints, index);
    }
  }

  /**
   * Paint a cell renderer component c on graphics object g. Before the
   * component is drawn it's reparented to this (if that's necessary), it's
   * bounds are set to w,h and the graphics object is (effectively) translated
   * to x,y. If it's a JComponent, double buffering is temporarily turned off.
   * After the component is painted it's bounds are reset to -w, -h, 0, 0 so
   * that, if it's the last renderer component painted, it will not start
   * consuming input. The Container p is the component we're actually drawing
   * on, typically it's equal to this.getParent(). If shouldValidate is true
   * the component c will be validated before painted.
   */
  public int paintComponent(final JBadge badge, final Graphics2D gc, final Rectangle bounds, final boolean selected, final int marginY) {
    int x = bounds.x;
    int y = bounds.y + marginY;
    int w = bounds.width;
    int h = bounds.height - 2 * marginY;

    if (badge == null) {
      return 0;
    }

    if (badge.getParent() != this) {
      this.add(badge);
      badge.setFont(getFont());
    }

    //badge.setSelected(selected);
    badge.setBounds(x, y, w, h);

    boolean wasDoubleBuffered = false;
    if (badge.isDoubleBuffered()) {
      wasDoubleBuffered = true;
      badge.setDoubleBuffered(false);
    }

    Graphics cg = gc.create(x, y, w, h);
    try {
      badge.paint(cg);
    } finally {
      cg.dispose();
    }

    if (wasDoubleBuffered) {
      badge.setDoubleBuffered(true);
    }

    //badge.setBounds(-w, -h, 0, 0);

    return badge.getWidth();
  }

  private void writeObject(final ObjectOutputStream s) throws IOException {
    removeAll();
    s.defaultWriteObject();
  }

  //
  // Accessibility support
  //

  protected AccessibleContext accessibleContext = null;

  /**
   * Gets the AccessibleContext associated with this CellRendererPane. For
   * CellRendererPanes, the AccessibleContext takes the form of an
   * AccessibleCellRendererPane. A new AccessibleCellRendererPane instance is
   * created if necessary.
   *
   * @return an AccessibleCellRendererPane that serves as the
   * AccessibleContext of this CellRendererPane
   */
  @Override
  public AccessibleContext getAccessibleContext() {
    if (accessibleContext == null) {
      accessibleContext = new AccessibleCellRendererPane();
    }
    return accessibleContext;
  }

  /**
   * This class implements accessibility support for the
   * <code>CellRendererPane</code> class.
   */
  protected class AccessibleCellRendererPane extends AccessibleAWTContainer {
    private static final long serialVersionUID = -4283889279864557543L;

    // AccessibleContext methods
    //

    /**
     * Get the role of this object.
     *
     * @return an instance of AccessibleRole describing the role of the
     * object
     * @see AccessibleRole
     */
    @Override
    public AccessibleRole getAccessibleRole() {
      return AccessibleRole.PANEL;
    }
  }
}

package com.flamesgroup.antrax.control.swingwidgets.table;

public class TableExportException extends Exception {

  private static final long serialVersionUID = -5553953731504272482L;

  public TableExportException(final String message) {
    super(message);
  }

  public TableExportException(final String message, final Throwable cause) {
    super(message, cause);
  }

}

package com.flamesgroup.antrax.control.swingwidgets;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.*;

public class JBarButton extends JToggleButton {

  private static final long serialVersionUID = 1296322449469717392L;

  public JBarButton() {
    super();
    addListener();
  }

  public JBarButton(final String text) {
    super(text);
    addListener();
  }

  public JBarButton(final String text, final Icon icon) {
    super(text, icon);
    addListener();
  }

  public JBarButton(final Icon icon) {
    super(icon);
    addListener();
  }

  protected void addListener() {
    ItemListener itemListener = new ItemListener() {
      @Override
      public void itemStateChanged(final ItemEvent itemEvent) {
        int state = itemEvent.getStateChange();
        if (state == ItemEvent.SELECTED) {
          JBarButton.this.setSelected(false);
        }
      }
    };
    addItemListener(itemListener);
  }
}

package com.flamesgroup.antrax.control.swingwidgets.table;

import javax.swing.*;
import javax.swing.plaf.ComponentUI;

public class JMetalTable extends JTable {

  private static final long serialVersionUID = -1282083692068746454L;
  private transient JUpdatableTableProperties jUpdatableTableProperties;

  public JMetalTable() {
    super();
    setUI(new MetalTableUI());
    setShowHorizontalLines(true);
    setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
    //    getTableHeader().setDefaultRenderer(new ITunesTableHeaderRenderer());
    //    addHierarchyListener(new HierarchyListener() {
    //
    //      public void hierarchyChanged(HierarchyEvent e) {
    //        Component cmp = e.getChanged();
    //        if (cmp instanceof JScrollPane) {
    //          JScrollPane scp = (JScrollPane)cmp;
    //          scp.setCorner(ScrollPaneConstants.UPPER_TRAILING_CORNER,
    //              new ITunesTableUpperRightCorner());
    //        }
    //      }
    //
    //    });
  }

  public JUpdatableTableProperties getUpdatableTableProperties() {
    if (jUpdatableTableProperties == null) {
      jUpdatableTableProperties = new JUpdatableTableProperties(this);
    }
    return jUpdatableTableProperties;
  }

  @Override
  protected void setUI(final ComponentUI newUI) {
    if (newUI instanceof MetalTableUI) {
      super.setUI(newUI);
    }
  }

  @Override
  public boolean getScrollableTracksViewportHeight() {
    return getPreferredSize().height < getParent().getHeight();
  }

  @Override
  public boolean getScrollableTracksViewportWidth() {
    return getPreferredSize().width < getParent().getWidth();
  }

}

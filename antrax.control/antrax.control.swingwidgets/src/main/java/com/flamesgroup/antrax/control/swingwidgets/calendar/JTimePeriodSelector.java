package com.flamesgroup.antrax.control.swingwidgets.calendar;

import com.flamesgroup.antrax.control.swingwidgets.editor.JEditLabel;

import java.awt.*;
import java.lang.reflect.InvocationTargetException;
import java.util.Calendar;
import java.util.Date;

import javax.swing.*;

public class JTimePeriodSelector extends JToolBar {
  private static final long serialVersionUID = -3458207749295701928L;
  private JXDatePicker datePickerFrom;
  private JXDatePicker datePickerTo;
  private static final int prefWidthPicker = 150;
  private static final int prefHeightPicker = 22;

  public JTimePeriodSelector() {
    this("EEE dd-MM-yyyy HH:mm:ss");
  }

  public JTimePeriodSelector(final String dateFormat) {
    super();
    initUI(dateFormat);
  }

  private JXDatePicker createDateComponent(final String dateFormat, final Date date) {
    JXDatePicker datePicker = new JXDatePicker(date.getTime());
    datePicker.setFormats(dateFormat);
    datePicker.setMaximumSize(new Dimension(prefWidthPicker, prefHeightPicker));
    datePicker.setPreferredSize(new Dimension(prefWidthPicker, prefHeightPicker));
    return datePicker;
  }

  private JLabel createLabel(final String text) {
    return new JEditLabel(text);
  }

  private void initUI(final String dateFormat) {
    Calendar from = Calendar.getInstance();
    from.set(Calendar.HOUR_OF_DAY, 0);
    from.set(Calendar.MINUTE, 0);
    from.set(Calendar.SECOND, 0);
    from.set(Calendar.MILLISECOND, 0);

    Calendar to = Calendar.getInstance();
    to.set(Calendar.HOUR_OF_DAY, 23);
    to.set(Calendar.MINUTE, 59);
    to.set(Calendar.SECOND, 59);
    to.set(Calendar.MILLISECOND, 0);

    datePickerFrom = createDateComponent(dateFormat, from.getTime());
    datePickerTo = createDateComponent(dateFormat, to.getTime());

    Dimension separatorDim = new Dimension(3, 1);

    setFloatable(false);
    add(createLabel("From:"));
    addSeparator(separatorDim);
    add(datePickerFrom);
    addSeparator(separatorDim);
    add(createLabel("To:"));
    addSeparator(separatorDim);
    add(datePickerTo);
  }

  public Date getFromDate() {
    return datePickerFrom.getDate();
  }

  public Date getToDate() {
    return datePickerTo.getDate();
  }

  public void setFromDate(final Date date) {
    datePickerFrom.setDate(date);
  }

  public void setToDate(final Date date) {
    datePickerTo.setDate(date);
  }

  @Override
  public void setEnabled(final boolean enabled) {
    datePickerFrom.setEnabled(enabled);
    datePickerTo.setEnabled(enabled);
    super.setEnabled(enabled);
  }

  /**
   * @param args
   */
  public static void main(final String[] args) throws InterruptedException, InvocationTargetException {
    SwingUtilities.invokeAndWait(new Runnable() {
      @Override
      public void run() {
        try {
          UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
          JFrame mainFrame = new JFrame("BasicApp");
          mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
          final JTimePeriodSelector jCalendar = new JTimePeriodSelector();
          mainFrame.add(jCalendar, BorderLayout.CENTER);
          mainFrame.pack();
          mainFrame.setVisible(true);
          mainFrame.setLocationRelativeTo(null);
        } catch (Exception e) {
          e.printStackTrace();
        }

      }
    });
  }
}

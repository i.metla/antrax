/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.server.utils;

import com.flamesgroup.antrax.control.authorization.ClientUID;
import com.flamesgroup.antrax.control.communication.TransactionException;
import com.flamesgroup.antrax.control.properties.ControlPropUtils;
import com.flamesgroup.antrax.storage.dao.IConfigEditDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

public class EditTransactionManager {

  private static final Logger logger = LoggerFactory.getLogger(EditTransactionManager.class);

  private static final int FREE_TRANSACTION_ID = -1;

  private final Random random = new Random();

  private final AtomicLong lastUpdateTransaction = new AtomicLong();
  private final AtomicInteger transactionId = new AtomicInteger(FREE_TRANSACTION_ID);
  private final IConfigEditDAO configEditDAO;

  public EditTransactionManager(final IConfigEditDAO configEditDAO) {
    this.configEditDAO = configEditDAO;
    Timer timer = new Timer(true);
    timer.scheduleAtFixedRate(new RetainChecker(), 0, ControlPropUtils.getInstance().getControlServerProperties().getTransactionTimeout());
  }

  public synchronized int startEditTransaction(final ClientUID client) throws TransactionException {
    if (transactionId.get() != FREE_TRANSACTION_ID) {
      throw new TransactionException("Trying to start more than one transaction (only one transaction can be started at the same time)");
    }
    lastUpdateTransaction.set(System.currentTimeMillis());
    transactionId.set(random.nextInt());
    logger.debug("[{}] - start edit transaction for client: [{}], with transactionId: [{}]", this, client, transactionId);
    return transactionId.get();
  }

  public synchronized void commitEditTransaction(final ClientUID client, final int transactionId) throws TransactionException {
    if (!isValidTransaction(transactionId)) {
      throw new TransactionException("Can't commit transaction (transaction does not exist)");
    }
    this.transactionId.set(FREE_TRANSACTION_ID);
  }

  public synchronized void rollbackEditTransaction(final ClientUID client, final int transactionId) throws TransactionException {
    if (!isValidTransaction(transactionId)) {
      throw new TransactionException("Can't rollback transaction (transaction does not exist)");
    }
    this.transactionId.set(FREE_TRANSACTION_ID);
  }

  public synchronized void retainEditTransaction(final ClientUID client, final int transactionId) throws TransactionException {
    if (isValidTransaction(transactionId)) {
      lastUpdateTransaction.set(System.currentTimeMillis());
    } else {
      throw new TransactionException("Transaction with id: " + transactionId + " does not exist");
    }
  }

  public synchronized IConfigEditDAO getConfigEditDAO(final ClientUID client, final int transactionId) throws TransactionException {
    if (!isValidTransaction(transactionId)) {
      throw new TransactionException("Can't get configEditDAO for transaction (transaction does not exist)");
    }
    return configEditDAO;
  }

  public synchronized boolean isValidTransaction(final int transactionId) {
    return transactionId == this.transactionId.get();
  }

  private class RetainChecker extends TimerTask {

    @Override
    public void run() {
      if (transactionId.get() == FREE_TRANSACTION_ID) {
        return;
      }
      if (lastUpdateTransaction.get() + ControlPropUtils.getInstance().getControlServerProperties().getTransactionTimeout() < System.currentTimeMillis()) {
        transactionId.set(FREE_TRANSACTION_ID);
      }
    }

  }

}

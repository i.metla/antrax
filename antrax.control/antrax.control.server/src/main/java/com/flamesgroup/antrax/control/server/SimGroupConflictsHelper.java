/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.server;

import com.flamesgroup.antrax.automation.meta.ScriptInstance;
import com.flamesgroup.antrax.automation.meta.SimGroupScriptModificationConflict;
import com.flamesgroup.antrax.control.communication.SimGroupScriptModificationConflictImpl;
import com.flamesgroup.antrax.plugins.core.AntraxPluginsStore;
import com.flamesgroup.antrax.plugins.core.ScriptsHelper;
import com.flamesgroup.antrax.storage.commons.SIMGroup;
import com.flamesgroup.antrax.storage.commons.impl.SIMGroupImpl;
import com.flamesgroup.antrax.storage.commons.impl.ScriptCommons;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class SimGroupConflictsHelper {
  private final Logger logger = LoggerFactory.getLogger(SimGroupConflictsHelper.class);

  public SimGroupScriptModificationConflict[] createConflicts(final SIMGroup[] simGroups, final AntraxPluginsStore oldPS, final AntraxPluginsStore newPS) {
    List<SimGroupScriptModificationConflict> retval = new LinkedList<>();
    for (SIMGroup g : simGroups) {
      SimGroupScriptModificationConflict conflict = createConflict((SIMGroupImpl) g, oldPS, newPS);
      logger.debug("[{}] - adding new conflict: {}", this, conflict);
      retval.add(conflict);
    }
    return retval.toArray(new SimGroupScriptModificationConflict[retval.size()]);
  }

  private SimGroupScriptModificationConflict createConflict(final SIMGroupImpl oldGroup, final AntraxPluginsStore oldPS, final AntraxPluginsStore newPS) {
    SIMGroupImpl newGroup = oldGroup.clone();
    newGroup.setScriptCommons(migrateScriptCommons(oldGroup.listScriptCommons(), oldPS, newPS));

    Set<String> scriptCommons = collectCommons(newGroup.listScriptCommons());

    newGroup.setActionProviderScripts(migrateScripts(oldGroup.getActionProviderScripts(), oldPS, newPS, scriptCommons));
    newGroup.setActivityPeriodScript(ScriptsHelper.migrateScriptToNewPluginsStore(oldGroup.getActivityPeriodScript(), oldPS, newPS, scriptCommons));
    newGroup.setBusinessActivityScripts(migrateScripts(oldGroup.getBusinessActivityScripts(), oldPS, newPS, scriptCommons));
    newGroup.setFactorScript(ScriptsHelper.migrateScriptToNewPluginsStore(oldGroup.getFactorScript(), oldPS, newPS, scriptCommons));
    newGroup.setGWSelectorScript(ScriptsHelper.migrateScriptToNewPluginsStore(oldGroup.getGWSelectorScript(), oldPS, newPS, scriptCommons));
    newGroup.setIncomingCallManagementScript(ScriptsHelper.migrateScriptToNewPluginsStore(oldGroup.getIncomingCallManagementScript(), oldPS, newPS, scriptCommons));
    newGroup.setSessionScript(ScriptsHelper.migrateScriptToNewPluginsStore(oldGroup.getSessionScript(), oldPS, newPS, scriptCommons));
    newGroup.setVSFactorScript(ScriptsHelper.migrateScriptToNewPluginsStore(oldGroup.getVSFactorScript(), oldPS, newPS, scriptCommons));
    newGroup.setIMEIGeneratorScript(ScriptsHelper.migrateScriptToNewPluginsStore(oldGroup.getIMEIGeneratorScript(), oldPS, newPS, scriptCommons));
    newGroup.setSmsFilterScripts(migrateScripts(oldGroup.getSmsFilterScripts(), oldPS, newPS, scriptCommons));
    newGroup.setCallFilterScripts(migrateScripts(oldGroup.getCallFilterScripts(), oldPS, newPS, scriptCommons));

    return new SimGroupScriptModificationConflictImpl(oldGroup, newGroup);
  }

  private Set<String> collectCommons(final ScriptCommons[] commons) {
    Set<String> retval = new HashSet<>();
    for (ScriptCommons c : commons) {
      retval.add(c.getName());
    }
    return retval;
  }

  private ScriptCommons[] migrateScriptCommons(final ScriptCommons[] oldCommons, final AntraxPluginsStore oldPS, final AntraxPluginsStore newPS) {
    if (oldCommons == null) {
      return new ScriptCommons[0];
    }
    List<ScriptCommons> retval = new ArrayList<>();
    for (ScriptCommons oldCommon : oldCommons) {
      try {
        Class<?> oldClass = oldCommon.getValue(oldPS.getClassLoader()).getClass();
        Class<?> newClass = newPS.getClassByName(oldClass.getCanonicalName());
        if (ScriptsHelper.isClassEquals(oldClass, newClass)) {
          ScriptCommons newCommon = new ScriptCommons();
          newCommon.setName(oldCommon.getName());
          newCommon.setSerializedValue(oldCommon.getSerializedValue());
          retval.add(newCommon);
        }
      } catch (Exception ignored) {
      }
    }
    return retval.toArray(new ScriptCommons[retval.size()]);
  }

  private ScriptInstance[] migrateScripts(final ScriptInstance[] scripts, final AntraxPluginsStore oldPS, final AntraxPluginsStore newPS, final Set<String> scriptCommons) {
    if (scripts == null) {
      return new ScriptInstance[0];
    }
    ScriptInstance[] retval = new ScriptInstance[scripts.length];
    for (int i = 0; i < retval.length; ++i) {
      retval[i] = ScriptsHelper.migrateScriptToNewPluginsStore(scripts[i], oldPS, newPS, scriptCommons);
    }
    return retval;
  }
}

/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.server;

import com.flamesgroup.antrax.control.server.utils.DAOProvider;
import com.flamesgroup.antrax.storage.commons.Revisions;
import com.flamesgroup.antrax.plugins.core.AntraxPluginsStore;
import com.flamesgroup.utils.codebase.ScriptFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;

public class PluginsStoreCache {

  private final Logger logger = LoggerFactory.getLogger(PluginsStoreCache.class);
  private final DAOProvider daoProvider;

  private AntraxPluginsStore lastPluginsStore;

  private AntraxPluginsStore lastTransactionPluginsStore;
  private int lastTransactionId;

  public PluginsStoreCache(final DAOProvider daoProvider) {
    this.daoProvider = daoProvider;
  }

  public synchronized AntraxPluginsStore getPluginsStore(final int transactionId) throws Exception {
    if (transactionId < 0) {
      return getSharedPluginsStore();
    } else {
      return getTransactionPluginsStore(transactionId);
    }
  }

  private AntraxPluginsStore getTransactionPluginsStore(final int transactionId) throws Exception {

    if (lastTransactionId != transactionId) {
      lastTransactionPluginsStore = null;
    }
    lastTransactionId = transactionId;
    long currRev = Revisions.getInstance().getScriptFilesRevision();
    if (lastTransactionPluginsStore != null) {
      if (lastTransactionPluginsStore.getRevision() < currRev) {
        lastTransactionPluginsStore = readPluginsStore(currRev);
      }
      return lastTransactionPluginsStore;
    }
    if (lastPluginsStore != null && lastPluginsStore.getRevision() >= currRev) {
      return lastPluginsStore;
    }
    lastTransactionPluginsStore = readPluginsStore(currRev);
    return lastTransactionPluginsStore;
  }

  private AntraxPluginsStore getSharedPluginsStore() throws Exception {
    long currRev = Revisions.getInstance().getScriptFilesRevision();
    if (lastPluginsStore == null) {
      lastPluginsStore = readPluginsStore(currRev);
    } else {
      if (lastPluginsStore.getRevision() < currRev) {
        lastPluginsStore = readPluginsStore(currRev);

      }
    }
    return lastPluginsStore;
  }

  private AntraxPluginsStore readPluginsStore(final long revision) throws Exception {
    logger.debug("[{}] - asked to get plugins store", this);
    AntraxPluginsStore antraxPluginsStore = new AntraxPluginsStore();
    ScriptFile scriptFile = daoProvider.getConfigViewDAO().getLastScriptFile();
    if (scriptFile != null) {
      antraxPluginsStore.analyze(new ByteArrayInputStream(scriptFile.getData()), revision);
    }
    logger.debug("[{}] - returning plugins store", this);
    return antraxPluginsStore;
  }

}

/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.server.tool;

import static java.nio.file.FileVisitResult.CONTINUE;

import com.flamesgroup.antrax.control.server.config.ControlServerConfig;
import com.flamesgroup.antrax.storage.connection.DbConnectionPool;
import com.flamesgroup.antrax.storage.dao.conn.DBConnectionProps;
import com.flamesgroup.commons.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.tools.JavaFileObject;

public class JavaFileLoader {

  private static final Logger logger = LoggerFactory.getLogger(JavaFileLoader.class);

  public static void main(final String[] args) {
    System.out.println("Version of antrax and scripts must be 1.9.12");
    System.out.println("Scripts loading...");

    Map<String, String> javaFiles;
    try {
      javaFiles = scanJavaFilesDirectory(Paths.get(args[0]));
    } catch (IOException e) {
      System.err.println("Can't get java files");
      logger.error("[{}] - can't get java files", JavaFileLoader.class, e);
      return;
    }

    ControlServerConfig controlServerConfig = new ControlServerConfig();
    DBConnectionProps conProperties = controlServerConfig.getDbConnectionProps();
    DbConnectionPool dbConnectionPool = new DbConnectionPool(conProperties);

    try (Connection connection = dbConnectionPool.getConnection().getConnection();
        PreparedStatement deleteAllJavaFiles = connection.prepareStatement("delete from java_file");
        PreparedStatement javaFileStatement = connection.prepareStatement("insert into java_file (file_path, file_content) values (?, ?)")) {
      deleteAllJavaFiles.executeUpdate();
      for (Map.Entry<String, String> javafile : javaFiles.entrySet()) {
        javaFileStatement.setString(1, javafile.getKey());
        javaFileStatement.setString(2, javafile.getValue());
        javaFileStatement.addBatch();
      }
      javaFileStatement.executeBatch();
    } catch (SQLException e) {
      System.err.println("Can't get sql connection");
      logger.error("[{}] - can't get sql connection", JavaFileLoader.class, e);
    }
  }

  private static Map<String, String> scanJavaFilesDirectory(final Path dir) throws IOException {
    final Map<String, String> javaFiles = new HashMap<>();
    Files.walkFileTree(dir, new SimpleFileVisitor<Path>() {

      private final PathMatcher matcher = FileSystems.getDefault().getPathMatcher("glob:*.java");

      @Override
      public FileVisitResult visitFile(final Path file, final BasicFileAttributes attrs) throws IOException {
        if (matcher.matches(file.getFileName())) {
          Pair<String, String> javaFile = loadFromFS(dir.toFile(), file.toFile());
          javaFiles.put(javaFile.first(), javaFile.second());
        }
        return CONTINUE;
      }
    });
    return javaFiles;
  }

  public static Pair<String, String> loadFromFS(final File rootDirectory, final File file) throws IOException {
    String path = file.getAbsolutePath().replace(rootDirectory.getAbsolutePath(), "").replaceAll("[\\\\/:]", ".")
        .replaceAll(JavaFileObject.Kind.SOURCE.extension, "");
    if (path.startsWith(".")) {
      path = path.substring(1, path.length());
    }

    try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
      StringBuilder buf = new StringBuilder();
      while (reader.ready()) {
        buf.append(reader.readLine());
        buf.append("\r\n");
      }
      return new Pair<>(path, buf.toString());
    }
  }

}

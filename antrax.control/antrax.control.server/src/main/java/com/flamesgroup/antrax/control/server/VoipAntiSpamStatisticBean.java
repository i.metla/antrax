/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.server;

import com.flamesgroup.antrax.commons.VoipAntiSpamListNumbersStatus;
import com.flamesgroup.antrax.control.authorization.ClientUID;
import com.flamesgroup.antrax.control.authorization.NotPermittedException;
import com.flamesgroup.antrax.control.communication.IVoipAntiSpamStatisticBean;
import com.flamesgroup.antrax.control.communication.StorageException;
import com.flamesgroup.antrax.control.communication.TransactionException;
import com.flamesgroup.antrax.control.server.utils.DAOProvider;
import com.flamesgroup.antrax.storage.commons.impl.BlackListNumber;
import com.flamesgroup.antrax.storage.commons.impl.GrayListNumber;
import com.flamesgroup.antrax.storage.commons.impl.VoipAntiSpamPlotData;
import com.flamesgroup.antrax.storage.commons.impl.WhiteListNumber;
import com.flamesgroup.antrax.storage.exceptions.DataModificationException;
import com.flamesgroup.commons.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.swing.*;

public class VoipAntiSpamStatisticBean implements IVoipAntiSpamStatisticBean {

  private static final Logger logger = LoggerFactory.getLogger(VoipAntiSpamStatisticBean.class);

  private final DAOProvider daoProvider;

  public VoipAntiSpamStatisticBean(final DAOProvider daoProvider) {
    this.daoProvider = daoProvider;
  }

  @Override
  public boolean checkPermission(final ClientUID client, final String action) throws NotPermittedException {
    return true;
  }

  @Override
  public void insertWhiteListNumbers(final ClientUID clientuid, final Set<WhiteListNumber> whiteListNumbers) throws NotPermittedException, TransactionException, StorageException {
    try {
      daoProvider.getVoipAntiSpamDAO().insertVoipAntiSpamWhiteListNumbers(whiteListNumbers);

      Set<String> numbers = whiteListNumbers.stream().map(whiteListNumber -> whiteListNumber.getNumber()).collect(Collectors.toSet());
      daoProvider.getVoipAntiSpamDAO().deleteVoipAntiSpamGrayListNumbers(numbers);
      daoProvider.getVoipAntiSpamDAO().deleteVoipAntiSpamBlackListNumbers(numbers);
    } catch (DataModificationException e) {
      logger.warn("[{}] - Update whiteListNumbers error", this, e);
      throw new StorageException("Can't update whiteListNumbers", e);
    }
  }

  @Override
  public void importWhiteListNumbers(final ClientUID clientuid, final Set<String> numbers) throws NotPermittedException, TransactionException, StorageException {
    Set<WhiteListNumber> whiteListNumbersLocal = numbers.stream()
        .map(number -> new WhiteListNumber(number, VoipAntiSpamListNumbersStatus.MANUAL, 0, new Date()))
        .collect(Collectors.toSet());

    try {
      daoProvider.getVoipAntiSpamDAO().insertVoipAntiSpamWhiteListNumbers(whiteListNumbersLocal);

      daoProvider.getVoipAntiSpamDAO().deleteVoipAntiSpamGrayListNumbers(numbers);
      daoProvider.getVoipAntiSpamDAO().deleteVoipAntiSpamBlackListNumbers(numbers);
    } catch (DataModificationException e) {
      logger.warn("[{}] - Update whiteListNumbers error", this, e);
      throw new StorageException("Can't update whiteListNumbers", e);
    }
  }

  @Override
  public void deleteAllWhiteListNumbers(final ClientUID clientuid) throws NotPermittedException, TransactionException, StorageException {
    try {
      daoProvider.getVoipAntiSpamDAO().deleteAllVoipAntiSpamWhiteListNumbers();
    } catch (DataModificationException e) {
      logger.warn("[{}] - delete all whiteListNumbers error", this, e);
      throw new StorageException("Can't delete all whiteListNumbers", e);
    }
  }

  @Override
  public void deleteWhiteListNumbers(final ClientUID clientuid, final Set<String> whiteListNumbers) throws NotPermittedException, TransactionException, StorageException {
    try {
      daoProvider.getVoipAntiSpamDAO().deleteVoipAntiSpamWhiteListNumbers(whiteListNumbers);
    } catch (DataModificationException e) {
      logger.warn("[{}] - delete [{}] whiteListNumbers error", this, whiteListNumbers, e);
      throw new StorageException("Can't delete whiteListNumbers", e);
    }
  }

  @Override
  public List<WhiteListNumber> listWhiteListNumbers(final ClientUID clientuid, final String search, final Pair<String, SortOrder> sort, final int offset,
      final int limit) throws NotPermittedException {
    return daoProvider.getVoipAntiSpamDAO().listWhiteListNumbers(search, sort, offset, limit);
  }

  @Override
  public int getCountWhiteListNumbers(final ClientUID clientuid, final String search) throws NotPermittedException {
    return daoProvider.getVoipAntiSpamDAO().getCountOfWhiteListNumbers(search);
  }

  @Override
  public StringBuilder exportWhiteListNumbers(final ClientUID clientuid, final Set<String> columns) throws NotPermittedException {
    String DELIMITER = ",";
    List<WhiteListNumber> whiteListNumbers = daoProvider.getVoipAntiSpamDAO().listWhiteListNumbers(null, null, 0, 0);

    StringBuilder exportedDataAtCSV = new StringBuilder();
    for (String column : columns) {
      exportedDataAtCSV.append(column).append(DELIMITER);
    }
    exportedDataAtCSV.deleteCharAt(exportedDataAtCSV.length() - 1);
    exportedDataAtCSV.append("\n");

    for (WhiteListNumber whiteListNumber : whiteListNumbers) {
      if (columns.contains("numbers")) {
        exportedDataAtCSV.append(whiteListNumber.getNumber()).append(DELIMITER);
      }
      if (columns.contains("status")) {
        exportedDataAtCSV.append(whiteListNumber.getStatus()).append(DELIMITER);
      }
      if (columns.contains("add time")) {
        exportedDataAtCSV.append(whiteListNumber.getAddTime()).append(DELIMITER);
      }
      if (columns.contains("routing request count")) {
        exportedDataAtCSV.append(whiteListNumber.getRoutingRequestCount()).append(DELIMITER);
      }
      exportedDataAtCSV.deleteCharAt(exportedDataAtCSV.length() - 1);
      exportedDataAtCSV.append("\n");
    }

    return exportedDataAtCSV;
  }

  @Override
  public void deleteAllGrayListNumbers(final ClientUID clientuid) throws NotPermittedException, TransactionException, StorageException {
    try {
      daoProvider.getVoipAntiSpamDAO().deleteAllVoipAntiSpamGrayListNumbers();
    } catch (DataModificationException e) {
      logger.warn("[{}] - delete all grayListNumbers error", this, e);
      throw new StorageException("Can't delete all grayListNumbers", e);
    }
  }

  @Override
  public void deleteGrayListNumbers(final ClientUID clientuid, final Set<String> grayListNumbers) throws NotPermittedException, TransactionException, StorageException {
    try {
      daoProvider.getVoipAntiSpamDAO().deleteVoipAntiSpamGrayListNumbers(grayListNumbers);
    } catch (DataModificationException e) {
      logger.warn("[{}] - delete [{}] grayListNumbers error", this, grayListNumbers, e);
      throw new StorageException("Can't delete grayListNumbers", e);
    }
  }

  @Override
  public void moveToBlackListNumbers(final ClientUID clientuid, final Set<String> grayListNumbers) throws NotPermittedException, TransactionException, StorageException {
    insertBlackListNumbers(clientuid, grayListNumbers.stream().map(s -> new BlackListNumber(s, VoipAntiSpamListNumbersStatus.MANUAL, 0, "", new Date())).collect(Collectors.toSet()));
  }

  @Override
  public List<GrayListNumber> listGrayListNumbers(final ClientUID clientuid, final String search, final Pair<String, SortOrder> sort, final int offset, final int limit)
      throws NotPermittedException {
    return daoProvider.getVoipAntiSpamDAO().listGrayListNumbers(search, sort, offset, limit);
  }

  @Override
  public int getCountGrayListNumbers(final ClientUID clientuid, final String search) throws NotPermittedException {
    return daoProvider.getVoipAntiSpamDAO().getCountOfGrayListNumbers(search);
  }

  @Override
  public void insertBlackListNumbers(final ClientUID clientuid, final Set<BlackListNumber> blackListNumbers) throws NotPermittedException, TransactionException, StorageException {
    try {
      daoProvider.getVoipAntiSpamDAO().insertVoipAntiSpamBlackListNumbers(blackListNumbers);

      Set<String> numbers = blackListNumbers.stream().map(blackListNumber -> blackListNumber.getNumber()).collect(Collectors.toSet());
      daoProvider.getVoipAntiSpamDAO().deleteVoipAntiSpamGrayListNumbers(numbers);
      daoProvider.getVoipAntiSpamDAO().deleteVoipAntiSpamWhiteListNumbers(numbers);
    } catch (DataModificationException e) {
      logger.warn("[{}] - Update blackListNumbers error", this, e);
      throw new StorageException("Can't update blackListNumbers", e);
    }
  }

  @Override
  public void importBlackListNumbers(final ClientUID clientuid, final Set<String> numbers) throws NotPermittedException, TransactionException, StorageException {
    Set<BlackListNumber> blackListNumbers = numbers.stream()
        .map(number -> new BlackListNumber(number, VoipAntiSpamListNumbersStatus.MANUAL, 0, "", new Date()))
        .collect(Collectors.toSet());

    try {
      daoProvider.getVoipAntiSpamDAO().insertVoipAntiSpamBlackListNumbers(blackListNumbers);

      daoProvider.getVoipAntiSpamDAO().deleteVoipAntiSpamGrayListNumbers(numbers);
      daoProvider.getVoipAntiSpamDAO().deleteVoipAntiSpamWhiteListNumbers(numbers);
    } catch (DataModificationException e) {
      logger.warn("[{}] - Update blackListNumbers error", this, e);
      throw new StorageException("Can't update blackListNumbers", e);
    }
  }

  @Override
  public void deleteAllBlackListNumbers(final ClientUID clientuid) throws NotPermittedException, TransactionException, StorageException {
    try {
      daoProvider.getVoipAntiSpamDAO().deleteAllVoipAntiSpamBlackListNumbers();
    } catch (DataModificationException e) {
      logger.warn("[{}] - delete all blackListNumbers error", this, e);
      throw new StorageException("Can't delete all blackListNumbers", e);
    }
  }

  @Override
  public void deleteBlackListNumbers(final ClientUID clientuid, final Set<String> grayListNumbers) throws NotPermittedException, TransactionException, StorageException {
    try {
      daoProvider.getVoipAntiSpamDAO().deleteVoipAntiSpamBlackListNumbers(grayListNumbers);
    } catch (DataModificationException e) {
      logger.warn("[{}] - delete [{}] grayListNumbers error", this, grayListNumbers, e);
      throw new StorageException("Can't delete grayListNumbers", e);
    }
  }

  @Override
  public List<BlackListNumber> listBlackListNumbers(final ClientUID clientuid, final String search, final Pair<String, SortOrder> sort, final int offset,
      final int limit) throws NotPermittedException {
    return daoProvider.getVoipAntiSpamDAO().listBlackListNumbers(search, sort, offset, limit);
  }

  @Override
  public int getCountBlackListNumbers(final ClientUID clientuid, final String search) throws NotPermittedException {
    return daoProvider.getVoipAntiSpamDAO().getCountOfBlackListNumbers(search);
  }

  @Override
  public StringBuilder exportBlackListNumbers(final ClientUID clientuid, final Set<String> columns) throws NotPermittedException {
    String DELIMITER = ",";
    List<BlackListNumber> blackListNumbers = daoProvider.getVoipAntiSpamDAO().listBlackListNumbers(null, null, 0, 0);

    StringBuilder exportedDataAtCSV = new StringBuilder();
    for (String column : columns) {
      exportedDataAtCSV.append(column).append(DELIMITER);
    }
    exportedDataAtCSV.deleteCharAt(exportedDataAtCSV.length() - 1);
    exportedDataAtCSV.append("\n");

    for (BlackListNumber blackListNumber : blackListNumbers) {
      if (columns.contains("numbers")) {
        exportedDataAtCSV.append(blackListNumber.getNumber()).append(DELIMITER);
      }
      if (columns.contains("status")) {
        exportedDataAtCSV.append(blackListNumber.getStatus()).append(DELIMITER);
      }
      if (columns.contains("add time")) {
        exportedDataAtCSV.append(blackListNumber.getAddTime()).append(DELIMITER);
      }
      if (columns.contains("routing request count")) {
        exportedDataAtCSV.append(blackListNumber.getRoutingRequestCount()).append(DELIMITER);
      }
      exportedDataAtCSV.deleteCharAt(exportedDataAtCSV.length() - 1);
      exportedDataAtCSV.append("\n");
    }

    return exportedDataAtCSV;
  }

  @Override
  public List<VoipAntiSpamPlotData> getPlotDataForRoutingRequest(final ClientUID clientuid) throws NotPermittedException {
    return daoProvider.getVoipAntiSpamDAO().getPlotDataForRoutingRequest();
  }

  @Override
  public List<VoipAntiSpamPlotData> getPlotDataForBlockNumber(final ClientUID clientuid) throws NotPermittedException {
    return daoProvider.getVoipAntiSpamDAO().getPlotDataForBlockNumbers();
  }

}

/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.simserver;

import com.flamesgroup.antrax.storage.dao.ISimpleConfigEditDAO;
import com.flamesgroup.unit.ICCID;

public class SimActivityLogger implements ISimActivityLogger {

  private final ISimpleConfigEditDAO dao;

  public SimActivityLogger(final ISimpleConfigEditDAO simpleConfigEditDAO) {
    this.dao = simpleConfigEditDAO;
  }

  @Override
  public void logSimEnabled(final ICCID simUID) {
    dao.enableSIM(simUID, true);
  }

  @Override
  public void logSimDisabled(final ICCID simUID) {
    dao.enableSIM(simUID, false);
  }

  @Override
  public void logSimLocked(final ICCID simUID, final String reason) {
    dao.lockSIM(simUID, true, reason);
  }

  @Override
  public void logSimUnlocked(final ICCID simUID, final String reason) {
    dao.lockSIM(simUID, false, reason);
  }

  @Override
  public void logCallEnded(final ICCID simUID, final long callDuration) {
    dao.incrementSIMAllCallCount(simUID);
    if (callDuration > 0) {
      dao.incrementSIMSuccessfulCallCount(simUID);
      dao.increaseSIMCalledTime(simUID, callDuration);
    }
  }

  @Override
  public void logIncomingCallEnded(final ICCID simUID, final long callDuration) {
    dao.incrementSIMIncomingAllCallCount(simUID);
    if (callDuration > 0) {
      dao.incrementSIMIncomingSuccessfulCallCount(simUID);
      dao.increaseSIMIncomingCalledTime(simUID, callDuration);
    }
  }

  @Override
  public void logSentSMS(final ICCID simUID, final int totalSmsParts, final int successSendSmsParts) {
    dao.incrementSIMSMSCount(simUID, totalSmsParts, successSendSmsParts);
  }

  @Override
  public void logSMSStatus(final ICCID simUID) {
    dao.incrementSIMSMSStatuses(simUID, 1);
  }

  @Override
  public void logIncomingSMS(final ICCID simUID, final int parts) {
    dao.incrementSIMIncomingSMSCount(simUID, parts);
  }

  @Override
  public void logSimNote(final ICCID simUID, final String note) {
    dao.noteSIM(simUID, note);
  }

  @Override
  public void logAllowedInternet(final ICCID simUID, final boolean allowed) {
    dao.setAllowedInternet(simUID, allowed);
  }

  public void logTariffPlanEndDate(final ICCID uid, final long endDate) {
    dao.setTariffPlanEndDate(uid, endDate);
  }

}

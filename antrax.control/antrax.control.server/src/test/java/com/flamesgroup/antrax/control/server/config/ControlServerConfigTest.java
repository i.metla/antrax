/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.server.config;

import static org.junit.Assert.assertEquals;

import com.flamesgroup.utils.AntraxProperties;
import org.junit.BeforeClass;
import org.junit.Test;

public class ControlServerConfigTest {

  @BeforeClass
  public static void initSystemProperties() {
    System.setProperty("control.server.url", "rmi://192.158.5.7:8888");
    System.setProperty("server.name", "hello");
  }

  @Test
  public void correctPropertiesTest() throws Exception {
    assertEquals("192.158.5.7", AntraxProperties.CONTROL_SERVER_HOST_NAME);
    assertEquals(8888, AntraxProperties.CONTROL_SERVER_RMI_PORT);
    assertEquals("hello", AntraxProperties.SERVER_NAME);
  }

}

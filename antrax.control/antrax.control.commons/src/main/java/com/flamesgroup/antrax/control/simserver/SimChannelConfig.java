/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.simserver;

import com.flamesgroup.antrax.distributor.ChannelConfig;
import com.flamesgroup.antrax.storage.state.CHVState;
import com.flamesgroup.unit.ICCID;
import com.flamesgroup.unit.IMSI;
import com.flamesgroup.unit.PhoneNumber;

import java.io.Serializable;
import java.util.Objects;

public final class SimChannelConfig implements Serializable {

  private static final long serialVersionUID = -8559460983140759132L;

  private final ICCID iccid;
  private final CHVState chvState;
  private final ChannelConfig channelConfig;

  private final IMSI imsi;
  private final PhoneNumber phoneNumber;

  public SimChannelConfig(final ICCID iccid, final CHVState chvState, final ChannelConfig channelConfig) {
    this(iccid, chvState, channelConfig, null, null);
  }

  public SimChannelConfig(final ICCID iccid, final CHVState chvState, final ChannelConfig channelConfig, final IMSI imsi, final PhoneNumber phoneNumber) {
    Objects.requireNonNull(iccid, "iccid mustn't be null");
    Objects.requireNonNull(chvState, "chvState mustn't be null");
    Objects.requireNonNull(channelConfig, "channelConfig mustn't be null");

    this.iccid = iccid;
    this.chvState = chvState;
    this.channelConfig = channelConfig;
    this.imsi = imsi;
    this.phoneNumber = phoneNumber;
  }

  public ICCID getIccid() {
    return iccid;
  }

  public CHVState getChvState() {
    return chvState;
  }

  public ChannelConfig getChannelConfig() {
    return channelConfig;
  }

  public IMSI getImsi() {
    return imsi;
  }

  public PhoneNumber getPhoneNumber() {
    return phoneNumber;
  }

}

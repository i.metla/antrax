/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.communication;

import com.flamesgroup.utils.ServerSyncTimeProvider;
import com.flamesgroup.utils.TimeProvider;

import java.util.Calendar;

public class DayPeriod implements PeriodExpirationTrigger {

  private static final long serialVersionUID = 6156728649809978891L;
  private long expirationLimit = 0;
  private final TimeProvider timeProvider = new ServerSyncTimeProvider();

  @Override
  public void handleNewPeriod() {
    Calendar cal = Calendar.getInstance();
    cal.setTimeInMillis(timeProvider.currentTimeMillis());
    cal.set(Calendar.MINUTE, 0);
    cal.set(Calendar.SECOND, 0);
    cal.set(Calendar.MILLISECOND, 0);
    cal.set(Calendar.HOUR, 0);
    cal.add(Calendar.DAY_OF_YEAR, 1);
    expirationLimit = cal.getTimeInMillis();
  }

  @Override
  public boolean isPeriodExpired() {
    return System.currentTimeMillis() > expirationLimit;
  }

}

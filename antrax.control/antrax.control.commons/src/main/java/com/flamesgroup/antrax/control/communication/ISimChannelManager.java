/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.communication;

import com.flamesgroup.antrax.control.simserver.ISCReaderSubChannels;
import com.flamesgroup.antrax.control.simserver.PINRemoveFailedException;
import com.flamesgroup.commons.ChannelUID;
import com.flamesgroup.device.commonb.IndicationMode;
import com.flamesgroup.unit.CHV;

import java.rmi.RemoteException;

public interface ISimChannelManager extends ISCReaderSubChannels {

  void removeCHV(ChannelUID channel, CHV chv1) throws RemoteException, PINRemoveFailedException;

  void setIndicationMode(ChannelUID channel, IndicationMode indicationMode) throws RemoteException;

}

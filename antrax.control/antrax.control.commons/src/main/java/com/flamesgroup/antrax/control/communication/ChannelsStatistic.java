/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.communication;

import java.io.Serializable;

public class ChannelsStatistic implements Serializable {

  private static final long serialVersionUID = -582997949815182957L;

  private int talk;
  private int free;
  private int sleeping;
  private int notConnected;
  private int waitForSelfCalls;
  private int inBusinessActivity;

  public int getInBusinessActivity() {
    return inBusinessActivity;
  }

  public int getTotalChannelsCount() {
    return talk + waitForSelfCalls + sleeping + notConnected + inBusinessActivity + free;
  }

  public int getTalkingChannels() {
    return talk;
  }

  public int getWaitingSelfCallsChannels() {
    return waitForSelfCalls;
  }

  public int getSleepingChannels() {
    return sleeping;
  }

  public int getNotConnectedChannels() {
    return notConnected;
  }

  public int getBandwidth() {
    return talk + waitForSelfCalls + sleeping + free;
  }

  public int getFreeChannels() {
    return free;
  }

  public void incNotConnectedChannelsCount() {
    notConnected++;
  }

  public void decNotConnectedChannelsCount() {
    notConnected--;
  }

  public void decInBusinessActivity() {
    inBusinessActivity--;
  }

  public void decSleeping() {
    sleeping--;
  }

  public void decTalking() {
    talk--;
  }

  public void decWaitingSelfCall() {
    waitForSelfCalls--;
  }

  public void incSleeping() {
    sleeping++;
  }

  public void incInBusinessActivity() {
    inBusinessActivity++;
  }

  public void incTalking() {
    talk++;
  }

  public void incWaitingSelfCall() {
    waitForSelfCalls++;
  }

  public void incFree() {
    free++;
  }

  public void decFree() {
    free--;
  }

}

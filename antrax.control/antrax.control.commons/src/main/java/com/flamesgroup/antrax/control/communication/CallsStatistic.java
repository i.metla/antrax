/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.communication;

import java.io.Serializable;

public class CallsStatistic implements Serializable {

  private static final long serialVersionUID = -1218304861406004197L;

  private final CallsInfo successfulPerDay = new CallsInfo(new DayPeriod());
  private final CallsInfo successfulPerHour = new CallsInfo(new HourPeriod());
  private final CallsInfo successfulPerHistory = new CallsInfo(new UnlimitedPeriod());

  private final CallsInfo totalPerDay = new CallsInfo(new DayPeriod());
  private final CallsInfo totalPerHour = new CallsInfo(new HourPeriod());
  private final CallsInfo totalPerHistory = new CallsInfo(new UnlimitedPeriod());

  private final CallsInfo zeroPerDay = new CallsInfo(new DayPeriod());
  private final CallsInfo zeroPerHour = new CallsInfo(new HourPeriod());
  private final CallsInfo zeroPerHistory = new CallsInfo(new UnlimitedPeriod());

  public CallsInfo getSuccessfulPerDay() {
    return successfulPerDay;
  }

  public CallsInfo getSuccessfulPerHour() {
    return successfulPerHour;
  }

  public CallsInfo getSuccessfulPerHistory() {
    return successfulPerHistory;
  }

  public CallsInfo getTotalPerDay() {
    return totalPerDay;
  }

  public CallsInfo getTotalPerHour() {
    return totalPerHour;
  }

  public CallsInfo getTotalPerHistory() {
    return totalPerHistory;
  }

  public CallsInfo getZeroPerDay() {
    return zeroPerDay;
  }

  public CallsInfo getZeroPerHour() {
    return zeroPerHour;
  }

  public CallsInfo getZeroPerHistory() {
    return zeroPerHistory;
  }

  public void registerBadCall() {
    totalPerDay.incCount();
    totalPerHistory.incCount();
    totalPerHour.incCount();
    zeroPerDay.incCount();
    zeroPerHistory.incCount();
    zeroPerHour.incCount();
  }

  public void registerSuccessCall(final long duration) {
    totalPerDay.incCount();
    totalPerDay.incTotalDuration(duration);
    totalPerHistory.incCount();
    totalPerHistory.incTotalDuration(duration);
    totalPerHour.incCount();
    totalPerHour.incTotalDuration(duration);
    successfulPerDay.incCount();
    successfulPerDay.incTotalDuration(duration);
    successfulPerHistory.incCount();
    successfulPerHistory.incTotalDuration(duration);
    successfulPerHour.incCount();
    successfulPerHour.incTotalDuration(duration);
  }

}

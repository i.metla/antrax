/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.helper.editors.components;

import com.flamesgroup.antrax.automation.listeners.RegistryAccessListener;
import com.flamesgroup.antrax.automation.utils.registry.RegistryAccess;
import com.flamesgroup.antrax.helper.business.payment.UssdDialogOperator;
import com.flamesgroup.antrax.scripts.utils.UIComponentRegistryAccess;

import java.util.regex.Pattern;

import javax.swing.*;

public class UssdDialogOperatorEditorPanel extends JPanel implements RegistryAccessListener {

  private static final long serialVersionUID = 7257539861320139356L;

  private final JComboBox<String> operator = new JComboBox<>();

  public UssdDialogOperatorEditorPanel() {
    setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
    add(operator);
  }

  public UssdDialogOperator getUssdDialogOperator() {
    return new UssdDialogOperator((String) operator.getSelectedItem());
  }

  public void setUssdDialogOperator(final UssdDialogOperator ussdDialogOperator) {
    operator.setSelectedItem(ussdDialogOperator.getOperator());
  }

  @Override
  public void setRegistryAccess(final RegistryAccess registryLocal) {
    UIComponentRegistryAccess registry = new UIComponentRegistryAccess(registryLocal);
    String[] paths = registry.listPaths(Pattern.compile("(vauchers[.][^.]*)[.].*"));
    operator.setModel(new DefaultComboBoxModel<>(paths));
  }

}

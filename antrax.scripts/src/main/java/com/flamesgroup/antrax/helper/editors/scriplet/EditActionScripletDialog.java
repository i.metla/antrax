/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.helper.editors.scriplet;

import com.flamesgroup.antrax.automation.editors.PropertyEditor;
import com.flamesgroup.antrax.scripts.utils.scriplet.ScripletAtom;
import com.flamesgroup.antrax.scripts.utils.scriplet.ScripletFactory;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.*;

public class EditActionScripletDialog extends JDialog {

  private static final long serialVersionUID = 2978414949372723800L;

  private final JComboBox scriptsList;

  private PropertyEditor<?> propertyEditor;
  private final JPanel editorPanel = new JPanel();

  private final ScripletAtom child;
  private ScripletAtom currScript;

  public EditActionScripletDialog(final Window parentWindow, final ScripletAtom scriplet) {
    super(parentWindow);
    setContentPane(new JRootPane());
    this.child = scriplet;
    this.currScript = scriplet;
    setModalityType(ModalityType.DOCUMENT_MODAL);
    scriptsList = new JComboBox(ScripletFactory.listScripts());
    scriptsList.setSelectedItem(scriplet.getScript());
    JButton okButton = new JButton("OK");

    SpringLayout layout = new SpringLayout();
    getContentPane().setLayout(layout);

    getContentPane().add(scriptsList);
    getContentPane().add(editorPanel);
    getContentPane().add(okButton);

    layout.putConstraint(SpringLayout.NORTH, scriptsList, 5, SpringLayout.NORTH, getContentPane());
    layout.putConstraint(SpringLayout.NORTH, editorPanel, 5, SpringLayout.SOUTH, scriptsList);
    layout.putConstraint(SpringLayout.NORTH, okButton, 5, SpringLayout.SOUTH, editorPanel);

    layout.putConstraint(SpringLayout.HORIZONTAL_CENTER, scriptsList, 0, SpringLayout.HORIZONTAL_CENTER, getContentPane());
    layout.putConstraint(SpringLayout.HORIZONTAL_CENTER, editorPanel, 0, SpringLayout.HORIZONTAL_CENTER, getContentPane());
    layout.putConstraint(SpringLayout.HORIZONTAL_CENTER, okButton, 0, SpringLayout.HORIZONTAL_CENTER, getContentPane());

    propertyEditor = scriplet.getPropertyEditor();
    if (propertyEditor != null) {
      editorPanel.add(propertyEditor.getEditorComponent());
    }

    okButton.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent e) {
        if (propertyEditor != null) {
          currScript.setValue(propertyEditor.getValue());
        }
        scriplet.getParent().replaceScriplet(child, currScript);
        setVisible(false);
      }
    });

    scriptsList.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(final ActionEvent e) {
        editorPanel.removeAll();
        currScript = ScripletFactory.createScripletFor((String) scriptsList.getSelectedItem());
        propertyEditor = currScript.getPropertyEditor();
        if (propertyEditor != null) {
          editorPanel.add(propertyEditor.getEditorComponent());
        }
        pack();
      }
    });

    setPreferredSize(new Dimension(350, 130));
    setResizable(false);

    ActionListener listener = new ActionListener() {

      @Override
      public void actionPerformed(final ActionEvent e) {
        dispose();
      }
    };
    KeyStroke stroke = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0);
    ((JRootPane) getContentPane()).registerKeyboardAction(listener, stroke, JComponent.WHEN_IN_FOCUSED_WINDOW);

    pack();
  }

}

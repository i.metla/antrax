/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.helper.editors.components;

import com.flamesgroup.antrax.helper.editors.IntervalEditor;
import com.flamesgroup.antrax.scripts.utils.ActivityTimeoutByEventScriplet;

import java.awt.*;

import javax.swing.*;

public class ActivityTimeoutByEventScripletPanel extends JPanel {

  private static final long serialVersionUID = 6638845116074006344L;

  private final JTextField event;
  private final IntervalEditor timeout;

  public ActivityTimeoutByEventScripletPanel() {
    setOpaque(false);
    setLayout(new FlowLayout(FlowLayout.LEFT, 0, 0));

    event = new JTextField();
    event.setColumns(10);
    event.setToolTipText("activity timeout event");
    timeout = new IntervalEditor();
    ((JComponent) timeout.getEditorComponent()).setToolTipText("activity timeout period");

    add(event);
    add(new JLabel("; "));
    add(timeout.getEditorComponent());
  }

  private String getEvent() {
    String eventString = event.getText();
    if (eventString == null) {
      return "";
    }
    return eventString;
  }

  public ActivityTimeoutByEventScriplet getActivityTimeoutScriplet() {
    return new ActivityTimeoutByEventScriplet(getEvent(), timeout.getValue());
  }

  public void setActivityTimeoutScriplet(final ActivityTimeoutByEventScriplet activityTimeoutScriplet) {
    event.setText(activityTimeoutScriplet.getEvent());
    timeout.setValue(activityTimeoutScriplet.getTimeout());
  }

}

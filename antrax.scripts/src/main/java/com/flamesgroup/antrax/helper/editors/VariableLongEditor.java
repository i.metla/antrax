/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.helper.editors;

import com.flamesgroup.antrax.automation.editors.BasePropertyEditor;
import com.flamesgroup.antrax.scripts.utils.VariableLong;

import java.awt.*;

import javax.swing.*;

public class VariableLongEditor extends BasePropertyEditor<VariableLong> {

  private static class EditorComponent extends JPanel {
    private static final long serialVersionUID = 8381673742485638317L;

    private final JSpinner min = createLongSpinner();

    private final JSpinner max = createLongSpinner();

    public EditorComponent() {
      setOpaque(false);

      JLabel label = new JLabel("..");

      GroupLayout layout = new GroupLayout(this);
      setLayout(layout);
      layout.setHorizontalGroup(
      /**/layout.createParallelGroup()
      /*    */.addGroup(layout.createSequentialGroup()
      /*        */.addComponent(min, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
      /*        */.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
      /*        */.addComponent(label)
      /*        */.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
      /*        */.addComponent(max, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)));

      layout.setVerticalGroup(
      /* */layout.createParallelGroup()
      /*    */.addGroup(layout.createSequentialGroup()
      /*        */.addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
      /*            */.addComponent(min, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
      /*            */.addComponent(label)
      /*            */.addComponent(max, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))));
    }

    private static JSpinner createLongSpinner() {
      return new JSpinner(new SpinnerNumberModel(Long.valueOf(0), Long.valueOf(0), Long.valueOf(Long.MAX_VALUE), Long.valueOf(1)));
    }

    public VariableLong getValue() {
      return new VariableLong((Long) min.getValue(), (Long) max.getValue());
    }

    public void setValue(final VariableLong vl) {
      if (vl == null) {
        min.setValue(0L);
        max.setValue(0L);
      } else {
        min.setValue(vl.getMin());
        max.setValue(vl.getMax());
      }
    }
  }

  private final EditorComponent editorComponent = new EditorComponent();

  @Override
  public Component getEditorComponent() {
    return editorComponent;
  }

  @Override
  public Class<? extends VariableLong> getType() {
    return VariableLong.class;
  }

  @Override
  public VariableLong getValue() {
    return editorComponent.getValue();
  }

  @Override
  public void setValue(final VariableLong value) {
    editorComponent.setValue(value);
  }

}

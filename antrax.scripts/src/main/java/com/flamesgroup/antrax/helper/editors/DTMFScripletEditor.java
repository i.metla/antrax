/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.helper.editors;

import com.flamesgroup.antrax.automation.editors.BasePropertyEditor;
import com.flamesgroup.antrax.scripts.business.dtmf.DTMFScriplet;
import com.flamesgroup.antrax.scripts.utils.TimePeriod;
import com.flamesgroup.unit.PhoneNumber;

import java.awt.*;
import java.text.ParseException;

import javax.swing.*;
import javax.swing.text.DefaultFormatter;
import javax.swing.text.DefaultFormatterFactory;

public class DTMFScripletEditor extends BasePropertyEditor<DTMFScriplet> {

  private final JFormattedTextField editorComponent;

  public DTMFScripletEditor() {
    editorComponent = new JFormattedTextField();
    DefaultFormatterFactory factory = new DefaultFormatterFactory(new DTMFFormatter());
    editorComponent.setFormatterFactory(factory);
  }

  @Override
  public Component getEditorComponent() {
    return editorComponent;
  }

  @Override
  public Class<? extends DTMFScriplet> getType() {
    return DTMFScriplet.class;
  }

  @Override
  public DTMFScriplet getValue() {
    try {
      return scripletFromString(editorComponent.getText());
    } catch (ParseException e) {
      return null;
    }
  }

  @Override
  public void setValue(final DTMFScriplet value) {
    editorComponent.setText(value != null ? value.toString() : "");
  }

  private DTMFScriplet scripletFromString(final String text) throws ParseException {
    String[] parts = text.split(";");

    PhoneNumber phoneNumber = null;
    try {
      phoneNumber = new PhoneNumber(parts[0]);
    } catch (Exception e) {
      throw new ParseException(String.format("Illegal Phone Number [%s]", parts[0]), 0);
    }
    DTMFScriplet scriplet = new DTMFScriplet(phoneNumber);
    for (int i = 1; i < parts.length; i++) {
      String[] values = parts[i].split(":");
      try {
        if (values.length == 2) {
          scriplet.addTimeout(new TimePeriod(Long.parseLong(values[0])));
          scriplet.addDial(values[1].charAt(0));
        } else if (values.length == 1 && i == parts.length - 1) {
          scriplet.addTimeout(new TimePeriod(Long.parseLong(values[0])));
        } else {
          throw new ParseException("Illegal Script", i);
        }
      } catch (ParseException e) {
        throw e;
      } catch (Exception e) {
        throw new ParseException("Illegal Script", i);
      }
    }
    return scriplet;
  }

  private class DTMFFormatter extends DefaultFormatter {

    private static final long serialVersionUID = 6059478660499406623L;

    public DTMFFormatter() {
      setOverwriteMode(false);
    }

    @Override
    public String valueToString(final Object value) throws ParseException {
      return super.valueToString(value);
    }

    @Override
    public Object stringToValue(final String text) throws ParseException {
      try {
        scripletFromString(text);
        return text;
      } catch (Exception e) {
        return "Invalid input";
      }
    }

  }
}

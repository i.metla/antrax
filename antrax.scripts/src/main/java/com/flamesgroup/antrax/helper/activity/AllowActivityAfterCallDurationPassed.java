/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.helper.activity;

import com.flamesgroup.antrax.automation.annotations.StateField;
import com.flamesgroup.antrax.automation.predictions.AlwaysFalsePrediction;
import com.flamesgroup.antrax.automation.predictions.AlwaysTruePrediction;
import com.flamesgroup.antrax.automation.predictions.Prediction;
import com.flamesgroup.antrax.automation.statefulscripts.ScriptSaver;
import com.flamesgroup.antrax.helper.activity.api.BaseSimpleActivityScript;
import com.flamesgroup.antrax.predictions.CallDurationPrediction;
import com.flamesgroup.antrax.scripts.utils.TimeInterval;
import com.flamesgroup.antrax.storage.commons.impl.SimData;

/**
 * Session will be denied until sim card has enough call duration. This script
 * should be used in pair with others, combined with or, because card will not
 * have chance to call enough duration.
 */
public class AllowActivityAfterCallDurationPassed extends BaseSimpleActivityScript {

  private static final long serialVersionUID = -8291854907692515656L;

  @StateField
  private volatile long limit;

  private volatile SimData simData;

  private transient TimeInterval limitValue;

  private final ScriptSaver saver = new ScriptSaver();

  public void setCallDurationLimit(final TimeInterval limit) {
    this.limitValue = limit;
    calculateLimit();
  }

  private void calculateLimit() {
    limit = limitValue.random();
    saver.save();
  }

  @Override
  public void setSimData(final SimData simData) {
    this.simData = simData;
    saver.save();
  }

  @Override
  public boolean isActivityAllowed() {
    return simData.getCallDuration() >= limit;
  }

  @Override
  public Prediction predictEnd() {
    if (!isActivityAllowed()) {
      return new AlwaysTruePrediction();
    }
    return new AlwaysFalsePrediction();
  }

  @Override
  public Prediction predictStart() {
    if (isActivityAllowed()) {
      return new AlwaysTruePrediction();
    }
    return new CallDurationPrediction(limit - simData.getCallDuration());
  }

  @Override
  public String toString() {
    return String.format("%s(%s)", getClass().getSimpleName(), limitValue);
  }

  @Override
  public ScriptSaver getScriptSaver() {
    return saver;
  }

}

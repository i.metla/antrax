/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.helper.business.payment;

import com.flamesgroup.antrax.automation.businesscripts.RegisteredInGSMChannel;
import com.flamesgroup.antrax.automation.businesscripts.USSDSession;
import com.flamesgroup.antrax.automation.utils.registry.RegistryAccess;
import com.flamesgroup.antrax.scripts.utils.TimeInterval;
import com.flamesgroup.antrax.scripts.utils.TimePeriod;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

public class USSDPaymentHelper implements PaymentHelper {

  private static final long serialVersionUID = -2298750113528845660L;

  private static final Logger logger = LoggerFactory.getLogger(USSDPaymentHelper.class);

  private final String ussd;
  private final String[] responceCheckRegexes;
  private final String operator;

  public USSDPaymentHelper(final String operator, final String ussd, final String... responceCheckerRegexes) {
    this.operator = operator;
    this.ussd = ussd;
    this.responceCheckRegexes = responceCheckerRegexes;
  }

  @Override
  public void pay(final RegisteredInGSMChannel channel, final RegistryAccess registry, final int moneyAmount, final boolean skipIntermStates, final boolean confirmPayment) throws Exception {
    logger.debug("[{}] - going to pay a {} money", this, moneyAmount);
    new VaucherHelper(operator, registry, moneyAmount, skipIntermStates) {
      @Override
      protected boolean executePayment(final String vaucher) throws Exception {
        for (int i = 0; i < 2; ++i) {
          try {
            return USSDPaymentHelper.this.executePayment(vaucher, channel, confirmPayment);
          } catch (Exception e) {
            if (i == 1) {
              throw e;
            }
            logger.info("[{}] - while paying", this, e);
          }
          Thread.sleep(new TimeInterval(TimePeriod.inSeconds(50), TimePeriod.inSeconds(70)).random());
        }
        throw new Exception("PaymentFailed"); // Never rich this code
      }
    }.pay();
  }

  private boolean executePayment(final String voucher, final RegisteredInGSMChannel channel, final boolean confirmPayment) throws Exception {
    String request = ussd.replaceFirst("[$]", voucher);
    logger.debug("[{}] - sending such ussd: {}", this, request);
    String response;
    if (confirmPayment) {
      USSDSession ussdSession = channel.startUSSDSession(request);
      try {
        response = ussdSession.readResponse();
        if (checkUSSD(response)) {
          ussdSession.sendCommand(voucher);
          logger.debug("[{}] - got response: {}", this, ussdSession.readResponse());
          return true;
        }
      } finally {
        ussdSession.endSession();
      }
    } else {
      response = channel.sendUSSD(request);
      if (checkUSSD(response)) {
        return true;
      }
    }

    logger.debug("[{}] - response is BAD", this);
    throw new Exception("Bad payment response: " + response);
  }

  public String getUSSD() {
    return ussd;
  }

  public String getOperator() {
    return operator;
  }

  public String[] getResponceCheckerRegexes() {
    return responceCheckRegexes;
  }

  @Override
  public String toString() {
    return String.format("%s at %s checked with %s", ussd, operator, Arrays.toString(responceCheckRegexes));
  }

  private boolean checkUSSD(final String response) {
    logger.debug("[{}] - got such response: {}", this, response);
    for (String checkRegex : responceCheckRegexes) {
      if (response.matches(checkRegex)) {
        logger.debug("[{}] - response is ok", this);
        return true;
      }
    }
    return false;
  }

}

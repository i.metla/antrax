/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.helper.editors.components;

import com.flamesgroup.antrax.scripts.utils.TimePeriod;
import com.flamesgroup.antrax.scripts.utils.TimeUtils;

import java.awt.*;
import java.beans.PropertyChangeListener;
import java.text.ParseException;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.MaskFormatter;

public class TimePeriodEditor extends JSpinner {

  private static final long serialVersionUID = -2204527854893003957L;

  /* if you increasing hours upper limit, please, add time step for spinner */

  private static final long steps[] = {360000000, 36000000, 3600000, 0, 600000, 60000, 0, 10000, 1000, 0, 100, 10, 1};

  private static class Editor extends DefaultEditor implements PropertyChangeListener {

    private static final long serialVersionUID = 8181022433218286904L;

    public Editor(final JSpinner spinner) throws ParseException {
      super(spinner);

      MaskFormatter formatter = new MaskFormatter(calculateUpperLimitMask() + ":##:##");
      DefaultFormatterFactory factory = new DefaultFormatterFactory(formatter);
      JFormattedTextField ftf = getTextField();
      ftf.setEditable(true);
      ftf.setFormatterFactory(factory);
    }

    private String calculateUpperLimitMask() {
      int limit = TimeUtils.getHoursUpperLimit();
      String upperLimitMask = String.valueOf(limit).replaceAll("[0-9]", "#");
      return "#" + upperLimitMask;

    }

    public int getCaretPosition() {
      return getTextField().getCaretPosition();
    }

    @Override
    public void stateChanged(final ChangeEvent e) {
      int pos = getTextField().getCaretPosition();
      super.stateChanged(e);
      getTextField().setCaretPosition(pos);
    }
  }

  public TimePeriodEditor() {
    setModel(new AbstractSpinnerModel() {
      private static final long serialVersionUID = -6530164233668785899L;
      private TimePeriod period = new TimePeriod(0);

      @Override
      public void setValue(final Object value) {
        if (value instanceof String) {
          period = TimePeriod.parseTimePeriod((String) value);
        } else if (value instanceof TimePeriod) {
          period = (TimePeriod) value;
        } else {
          throw new IllegalArgumentException("Supported only TimePeriod and String in TimePeriod format. But passed " + value);
        }

        if (period.toString().length() > 9) {
          period = TimePeriod.parseTimePeriod("000:00:00");
        }


        fireStateChanged();
      }

      @Override
      public Object getValue() {
        return period;
      }

      @Override
      public Object getPreviousValue() {
        if (period.getPeriod() == 0 || period.getPeriod() - determineStepSize() < 0) {
          return period;
        }
        return new TimePeriod(period.getPeriod() - determineStepSize());
      }

      @Override
      public Object getNextValue() {
        TimePeriod retval = new TimePeriod(period.getPeriod() + determineStepSize());
        //if (retval.toString().length() > 14) {
        if (retval.toString().length() > 9) {
          return period;
        }
        return retval;
      }
    });
  }

  @Override
  protected JComponent createEditor(final SpinnerModel model) {
    try {
      return new Editor(this);
    } catch (ParseException e) {
      return super.createEditor(model);
    }
  }

  private long determineStepSize() {
    return steps[((Editor) getEditor()).getCaretPosition()];
  }

  public static void main(final String[] args) {
    TimePeriodEditor editor1 = new TimePeriodEditor();
    TimePeriodEditor editor2 = new TimePeriodEditor();

    editor1.setValue("000:00:00");
    editor2.setValue("000:00:00");

    String str = editor1.getValue().toString();

    final JFrame frame = new JFrame();
    frame.getContentPane().add(editor1, BorderLayout.LINE_START);
    frame.getContentPane().add(editor2, BorderLayout.LINE_END);

    frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    frame.pack();

    SwingUtilities.invokeLater(new Runnable() {
      @Override
      public void run() {
        frame.setVisible(true);
      }
    });
  }

}

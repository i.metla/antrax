/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.helper.activity.api;

import com.flamesgroup.antrax.automation.annotations.StateField;
import com.flamesgroup.antrax.automation.predictions.AlwaysFalsePrediction;
import com.flamesgroup.antrax.automation.predictions.AlwaysTruePrediction;
import com.flamesgroup.antrax.automation.predictions.Prediction;
import com.flamesgroup.antrax.automation.statefulscripts.ScriptSaver;
import com.flamesgroup.antrax.scripts.utils.VariableLong;
import com.flamesgroup.antrax.storage.commons.impl.SimData;

/**
 * Base class for creating activity limitations of some characteristic value
 * growth. Value growth is limited during activity. Value is limited with random
 * number which is recalculated each activity start. Random value calculates
 * using min and max.
 */
public abstract class BaseLimitCharacteristicPerActivity extends BaseSimpleActivityScript {
  /**
   * Extracts characteristic, which is used for limit. For example: <code>
   * return simData.getTotalCount()
   * </code>
   *
   * @param simData
   * @return
   */
  protected abstract long getCharacteristicValue(SimData simData);

  protected abstract Prediction getCharacteristicPrediction(long value);

  private static final long serialVersionUID = 5691378575249844376L;

  private volatile long limit;

  private volatile SimData simData;
  @StateField
  private volatile long initialValue;

  private VariableLong valueLimit;

  private final ScriptSaver saver = new ScriptSaver();

  protected void setLimitValue(final VariableLong limit) {
    this.valueLimit = limit;
    calculateLimit();
  }

  private void calculateLimit() {
    limit = valueLimit.random();
    saver.save();
  }

  @Override
  public void setSimData(final SimData simData) {
    if (simData != null) {
      initialValue = getCharacteristicValue(simData);
    }
    this.simData = simData;
    saver.save();
  }

  @Override
  public boolean isActivityAllowed() {
    return getCharacteristicValue(simData) - initialValue < limit;
  }

  @Override
  public void handlePeriodStart() {
    calculateLimit();
    saver.save();
  }

  @Override
  public void handlePeriodEnd() {
    initialValue = getCharacteristicValue(simData);
    saver.save();
  }

  @Override
  public String toString() {
    return String.format("%s(%d)", getClass().getSimpleName(), limit);
  }

  @Override
  public Prediction predictEnd() {
    if (!isActivityAllowed()) {
      return new AlwaysTruePrediction();
    }
    return getCharacteristicPrediction(limit - (getCharacteristicValue(simData) - initialValue));
  }

  @Override
  public Prediction predictStart() {
    if (isActivityAllowed()) {
      return new AlwaysTruePrediction();
    }
    return new AlwaysFalsePrediction();
  }

  @Override
  public ScriptSaver getScriptSaver() {
    return saver;
  }

}

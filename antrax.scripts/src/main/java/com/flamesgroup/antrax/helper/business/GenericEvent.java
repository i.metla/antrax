/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.helper.business;

import com.flamesgroup.antrax.automation.businesscripts.RegisteredInGSMChannel;

import java.io.Serializable;

public abstract class GenericEvent implements Serializable {
  private static final long serialVersionUID = -8194563745699882223L;

  public enum EventType {
    CHECKED,
    UNCHECKED
  }

  private final String event;

  public abstract boolean isSuccessResponce(String event, Serializable... args);

  public abstract boolean isFailureResponce(String event, Serializable... args);

  public abstract void fireEvent(RegisteredInGSMChannel channel);

  public abstract void respondFailure(RegisteredInGSMChannel channel, String error);

  public abstract void respondSuccess(RegisteredInGSMChannel channel);

  public abstract String getSuccessResponce();

  public abstract String getErrorResponce();

  public GenericEvent(final String event) {
    this.event = event;
  }

  public String getEvent() {
    return this.event;
  }

  @Override
  public String toString() {
    return getEvent();
  }

  private static class CheckedGenericEvent extends GenericEvent {
    private static final long serialVersionUID = 3595682378059244751L;
    private final String successResponce;
    private final String errorResponce;

    public CheckedGenericEvent(final String event, final String successResponce, final String errorResponce) {
      super(event);
      this.successResponce = successResponce;
      this.errorResponce = errorResponce;
    }

    @Override
    public void fireEvent(final RegisteredInGSMChannel channel) {
      channel.fireGenericEvent(getEvent(), EventType.CHECKED, successResponce, errorResponce);
    }

    @Override
    public boolean isFailureResponce(final String event, final Serializable... args) {
      return errorResponce.equals(event);
    }

    @Override
    public boolean isSuccessResponce(final String event, final Serializable... args) {
      return successResponce.equals(event);
    }

    @Override
    public String getSuccessResponce() {
      return successResponce;
    }

    @Override
    public String getErrorResponce() {
      return errorResponce;
    }

    @Override
    public void respondFailure(final RegisteredInGSMChannel channel, final String error) {
      channel.fireGenericEvent(errorResponce, error);
    }

    @Override
    public void respondSuccess(final RegisteredInGSMChannel channel) {
      channel.fireGenericEvent(successResponce);
    }
  }

  private static class UncheckedGenericEvent extends GenericEvent {

    private static final long serialVersionUID = -1945523850419055287L;

    public UncheckedGenericEvent(final String event) {
      super(event);
    }

    @Override
    public boolean isFailureResponce(final String event, final Serializable... args) {
      return false;
    }

    @Override
    public boolean isSuccessResponce(final String event, final Serializable... args) {
      return false;
    }

    @Override
    public void respondFailure(final RegisteredInGSMChannel channel, final String error) {
    }

    @Override
    public void respondSuccess(final RegisteredInGSMChannel channel) {
    }

    @Override
    public String getSuccessResponce() {
      return null;
    }

    @Override
    public String getErrorResponce() {
      return null;
    }

    @Override
    public void fireEvent(final RegisteredInGSMChannel channel) {
      channel.fireGenericEvent(getEvent(), EventType.UNCHECKED);
    }

  }

  public static GenericEvent checkedEvent(final String event, final String successResponce, final String failureResponce) {
    return new CheckedGenericEvent(event, successResponce, failureResponce);
  }

  public static GenericEvent uncheckedEvent(final String event) {
    return new UncheckedGenericEvent(event);
  }

  public static GenericEvent wrapEvent(final String event, final Serializable... args) {
    if (args.length > 0 && EventType.CHECKED.equals(args[0])) {
      if (args.length >= 3 && args[1] instanceof String && args[2] instanceof String) {
        return new CheckedGenericEvent(event, (String) args[1], (String) args[2]);
      }
    }
    return new UncheckedGenericEvent(event);
  }

  public String getFailReason(final String event, final Serializable[] args) {
    return (String) args[0];
  }

  @Override
  public boolean equals(final Object obj) {
    if (obj instanceof GenericEvent) {
      return getEvent().equals(((GenericEvent) obj).getEvent());
    }
    return false;
  }
}

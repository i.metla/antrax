/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.helper.activity;

import com.flamesgroup.antrax.automation.annotations.StateField;
import com.flamesgroup.antrax.automation.predictions.Prediction;
import com.flamesgroup.antrax.helper.activity.api.BaseLimitCharacteristicPerDay;
import com.flamesgroup.antrax.predictions.TimePeriodPrediction;
import com.flamesgroup.antrax.scripts.utils.TimeInterval;
import com.flamesgroup.antrax.scripts.utils.VariableLong;
import com.flamesgroup.antrax.storage.commons.impl.SimData;

public class LimitActivityTimePerDay extends BaseLimitCharacteristicPerDay {

  private static final long serialVersionUID = 4928502651925353813L;

  @StateField
  private long prevActivationTime;
  private long periodStarted;

  /**
   *
   */
  public void setActivityTimeDurationLimit(final TimeInterval activityTimeDurationLimit) {
    setLimitValue(new VariableLong(activityTimeDurationLimit.getMin().getPeriod(), activityTimeDurationLimit.getMax().getPeriod()));
  }

  @Override
  protected Prediction getCharacteristicPrediction(final long value) {
    return new TimePeriodPrediction(value);
  }

  @Override
  protected long getCharacteristicValue(final SimData simData) {
    if (periodStarted == 0) {
      periodStarted = System.currentTimeMillis();
    }
    return System.currentTimeMillis() - this.periodStarted;
  }

  @Override
  public void handlePeriodStart() {
    calculateLimit();
    periodStarted = System.currentTimeMillis();
  }

  @Override
  protected void calculateLimit() {
    limit = limitValue.random();
    limit = limit - this.prevActivationTime;
    saver.save();
  }

  @Override
  public void handlePeriodEnd() {
    if (isActivityAllowed()) {
      this.prevActivationTime = System.currentTimeMillis() - this.periodStarted;
    } else {
      prevActivationTime = 0;
    }
    saver.save();
  }

}

/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.helper.editors.scriplet;

import com.flamesgroup.antrax.helper.editors.ActivityScripletEditor;
import com.flamesgroup.antrax.scripts.utils.scriplet.BaseScriplet;
import com.flamesgroup.antrax.scripts.utils.scriplet.ScripletAtom;
import com.flamesgroup.antrax.scripts.utils.scriplet.ScripletBlock;
import com.flamesgroup.antrax.scripts.utils.scriplet.ScripletFactory;
import com.flamesgroup.antrax.scripts.utils.scriplet.ScripletOperation;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class EndBlockComponent extends JPanel {

  private static final long serialVersionUID = 924751120734288449L;

  private final JLabel lblOperation = ActivityScripletEditor.createLabel(")", false);
  private final JButton btnAdd = new JButton(ActivityScripletEditor.createIcon(ActivityScripletEditor.ADD_ICON));
  private JToolBar toolbar;
  private final BaseScriplet scriplet;
  private final ActivityScripletEditor editor;
  private final JPopupMenu popupMenu = new JPopupMenu();

  public EndBlockComponent(final BaseScriplet parentScriplet, final ActivityScripletEditor editor) {
    super(new BorderLayout());
    this.scriplet = parentScriplet;
    this.editor = editor;
    setOpaque(true);
    add(createToolbar(), BorderLayout.PAGE_START);
    add(lblOperation, BorderLayout.CENTER);
    initiazePopupMenu();
    initializeListeners();
  }

  private void initiazePopupMenu() {
    ActionListener listener = new ActionListener() {

      @Override
      public void actionPerformed(final ActionEvent e) {
        ScripletAtom newScriplet = ScripletFactory.createScripletFor(e.getActionCommand());
        if (scriplet.getParent() == null) {
          ScripletBlock block = new ScripletBlock(scriplet);
          block.addSibling(ScripletOperation.AND, newScriplet);
          editor.setValue(block);
        } else {
          (scriplet.getParent()).insertSibling(scriplet, ScripletOperation.AND, newScriplet);
          editor.refresh();
        }
      }
    };

    ButtonGroup group = new ButtonGroup();
    for (String s : ScripletFactory.listScripts()) {
      JRadioButtonMenuItem mi = new JRadioButtonMenuItem(s);
      group.add(mi);
      popupMenu.add(mi);
      mi.addActionListener(listener);
    }
  }

  private JToolBar createToolbar() {
    toolbar = new JToolBar();
    toolbar.setFloatable(false);
    toolbar.setRollover(true);

    toolbar.add(btnAdd);
    return toolbar;
  }

  private void initializeListeners() {
    btnAdd.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent e) {
        popupMenu.show(btnAdd, 0, 0);
      }
    });
  }

}

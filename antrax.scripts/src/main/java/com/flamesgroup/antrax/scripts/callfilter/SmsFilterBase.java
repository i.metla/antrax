/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.callfilter;

import com.flamesgroup.antrax.automation.annotations.ScriptParam;
import com.flamesgroup.antrax.automation.annotations.StateField;
import com.flamesgroup.antrax.automation.listeners.SMSListener;
import com.flamesgroup.antrax.automation.statefulscripts.ScriptSaver;
import com.flamesgroup.antrax.automation.statefulscripts.StatefullScript;
import com.flamesgroup.antrax.scripts.utils.VariableLong;
import org.slf4j.Logger;

import java.time.LocalDate;
import java.time.ZoneId;

public class SmsFilterBase extends FilterBase implements StatefullScript, SMSListener {

  private static final long serialVersionUID = 3542689119253033519L;

  @StateField
  protected volatile long limit;
  @StateField
  protected volatile long countSendSms;
  @StateField
  protected volatile LocalDate prevDay;

  protected VariableLong limitPerDay = new VariableLong(40, 50);
  private ScriptSaver saver = new ScriptSaver();

  public SmsFilterBase(final Logger logger) {
    super(logger);
  }

  @ScriptParam(name = "limit per day", doc = "limit of sms per day")
  public void setLimitPerDay(final VariableLong limitPerDay) {
    this.limitPerDay = limitPerDay;
  }

  public VariableLong getLimitPerDay() {
    return limitPerDay;
  }

  public boolean isAcceptsSmsParts(final int parts) {
    logger.info("[{}] - parts: [{}]", this, parts);
    if (prevDay == null || LocalDate.now(ZoneId.systemDefault()).isAfter(prevDay)) {
      logger.info("[{}] - in", this);
      prevDay = LocalDate.now(ZoneId.systemDefault());
      limit = limitPerDay.random();
      countSendSms = 0;
      saver.save();
    }
    logger.info("[{}] - countSendSms: [{}], limit: [{}]", this, countSendSms, limit);
    return countSendSms + parts <= limit;
  }

  @Override
  public ScriptSaver getScriptSaver() {
    return saver;
  }

  @Override
  public void handleIncomingSMS(final String phoneNumber, final String text) {
  }

  @Override
  public void handleSentSMS(final String phoneNumber, final String text, final int parts) {
    countSendSms += parts;
    saver.save();
  }

  @Override
  public void handleFailSentSMS(final int parts) {
  }

}

/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.imeigeneration;

import com.flamesgroup.antrax.automation.annotations.ScriptParam;
import com.flamesgroup.antrax.automation.annotations.StateField;
import com.flamesgroup.antrax.automation.listeners.ActivityListener;
import com.flamesgroup.antrax.automation.listeners.SimDataListener;
import com.flamesgroup.antrax.automation.scripts.IMEIGeneratorScript;
import com.flamesgroup.antrax.automation.statefulscripts.ScriptSaver;
import com.flamesgroup.antrax.automation.statefulscripts.StatefullScript;
import com.flamesgroup.antrax.scripts.utils.CalendarPeriod;
import com.flamesgroup.antrax.storage.commons.GSMGroup;
import com.flamesgroup.antrax.storage.commons.impl.SimData;

public abstract class IMEIGenerator implements IMEIGeneratorScript, StatefullScript, ActivityListener, SimDataListener {

  private static final long serialVersionUID = -1186393075314696817L;

  private IMEIGeneratorRule imeiGeneratorRule;
  private final ScriptSaver saver = new ScriptSaver();
  private SimData simData;

  @StateField
  private long lastPeriod;
  @StateField
  private CalendarPeriod lastPeriodState;
  @StateField
  private int registrations;

  @ScriptParam(name = "imei generator rule", doc = "generate new imei for specific option")
  public void setIMEIGeneratorRule(final IMEIGeneratorRule imeiGeneratorRule) {
    this.imeiGeneratorRule = imeiGeneratorRule;
  }

  public IMEIGeneratorRule getIMEIGeneratorRule() {
    return new IMEIGeneratorRule();
  }

  @Override
  public void handleActivityStarted(final GSMGroup gsmGroup) {
    registrations++;
    saver.save();
  }

  @Override
  public void handleActivityEnded() {
  }

  @Override
  public ScriptSaver getScriptSaver() {
    return saver;
  }

  @Override
  public boolean shouldGenerateIMEI() {
    switch (imeiGeneratorRule.getType()) {
      case NONE:
        return false;
      case PERIOD:
        CalendarPeriod period = imeiGeneratorRule.getPeriod();
        long currentPeriod = period.getCurrentPeriod();
        boolean shouldGenerateIMEI = currentPeriod > lastPeriod && lastPeriod != 0 && period == lastPeriodState;
        if (currentPeriod > lastPeriod || period != lastPeriodState) {
          lastPeriod = currentPeriod;
          lastPeriodState = period;
          saver.save();
        }
        return shouldGenerateIMEI;
      case REGISTRATION_COUNT:
        int registrationCount = imeiGeneratorRule.getRegistrationCount();
        return (registrations % registrationCount == registrationCount - 1);
      default:
        return false;
    }
  }

  @Override
  public void setSimData(final SimData simData) {
    this.simData = simData;
  }

  public SimData getSimData() {
    return simData;
  }

}

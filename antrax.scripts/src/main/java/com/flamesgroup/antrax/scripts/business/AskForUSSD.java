/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.business;

import com.flamesgroup.antrax.automation.actionscripts.Action;
import com.flamesgroup.antrax.automation.annotations.Script;
import com.flamesgroup.antrax.automation.annotations.ScriptParam;
import com.flamesgroup.antrax.automation.businesscripts.RegisteredInGSMChannel;
import com.flamesgroup.antrax.automation.listeners.GenericEventListener;
import com.flamesgroup.antrax.automation.scripts.BusinessActivityScript;
import com.flamesgroup.antrax.helper.business.GenericEvent;
import com.flamesgroup.antrax.scripts.utils.TimePeriod;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

@Script(name = "ask for ussd", doc = "ask other sim card to send USSD to this one (requires PhoneNumber to be set)")
public class AskForUSSD implements BusinessActivityScript, GenericEventListener {

  private static final Logger logger = LoggerFactory.getLogger(AskForUSSD.class);

  private String actionName = "ussd";

  private String event = "askForUSSD";

  private String eventOnSuccess = "";

  private String eventOnFailure = "";

  private volatile GenericEvent incomingEvent = null;

  @ScriptParam(name = "action", doc = "name of action, which can provide send ussd")
  public void setActionName(final String action) {
    this.actionName = action;
  }

  public String getActionName() {
    return actionName;
  }

  @ScriptParam(name = "event", doc = "name of event")
  public void setEvent(final String event) {
    this.event = event;
  }

  public String getEvent() {
    return event;
  }

  @ScriptParam(name = "even on success", doc = "this event will be generated if sending ussd was succesfull")
  public void setEventOnSuccess(final String eventOnSuccess) {
    this.eventOnSuccess = eventOnSuccess;
  }

  public String getEventOnSuccess() {
    return eventOnSuccess;
  }

  @ScriptParam(name = "event on failure", doc = "this event will be generated if sending ussd failed")
  public void setEventOnFailure(final String eventOnFailure) {
    this.eventOnFailure = eventOnFailure;
  }

  public String getEventOnFailure() {
    return eventOnFailure;
  }

  @Override
  public String describeBusinessActivity() {
    return "ask for ussd";
  }

  @Override
  public void invokeBusinessActivity(final RegisteredInGSMChannel channel) throws Exception {
    if (channel.getSimData().getPhoneNumber() == null) {
      logger.debug("[{}] - there is no phone number, locking card", this);
      channel.lock("no PhoneNumber but using \"ask for ussd\" script");
    }

    try {
      String phoneNumber = channel.getSimData().getPhoneNumber().getValue();
      Long simGroupID = channel.getSimData().getSimGroup().getID();
      Action action = new Action(actionName, phoneNumber, simGroupID);
      logger.debug("[{}] - {} execute action {}", this, channel.getSimData().getPhoneNumber().toString(), action.toString());
      channel.executeAction(action, TimePeriod.inMinutes(5));
      incomingEvent.respondSuccess(channel);

      if (eventOnSuccess != null && !eventOnSuccess.isEmpty()) {
        channel.fireGenericEvent(eventOnSuccess);
      }
    } catch (Exception e) {
      logger.warn("[{}] - ask for ussd failed", this, e);
      incomingEvent.respondFailure(channel, e.getMessage());
      if (eventOnFailure != null && !eventOnFailure.isEmpty()) {
        channel.fireGenericEvent(eventOnFailure);
      }
    } finally {
      incomingEvent = null;
    }
  }

  @Override
  public boolean shouldStartBusinessActivity() {
    return incomingEvent != null;
  }

  @Override
  public void handleGenericEvent(final String event, final Serializable... args) {
    if (this.event.equals(event)) {
      this.incomingEvent = GenericEvent.wrapEvent(event, args);
    }
  }

}

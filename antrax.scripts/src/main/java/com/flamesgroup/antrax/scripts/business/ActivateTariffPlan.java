/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.business;

import com.flamesgroup.antrax.automation.annotations.Script;
import com.flamesgroup.antrax.automation.annotations.ScriptParam;
import com.flamesgroup.antrax.automation.businesscripts.RegisteredInGSMChannel;
import com.flamesgroup.antrax.automation.listeners.GenericEventListener;
import com.flamesgroup.antrax.automation.scripts.BusinessActivityScript;
import com.flamesgroup.antrax.helper.business.GenericEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.ZoneId;

@Script(name = "activate tariff plan", doc = "set the end date of the tariff plan")
public class ActivateTariffPlan implements BusinessActivityScript, GenericEventListener {

  private static final Logger logger = LoggerFactory.getLogger(ActivateTariffPlan.class);

  private String event = "activate_tariff_plan";
  private int tariffPlanDays;
  private String eventOnTrue = "activate_tariff_plan_true";
  private String eventOnFalse = "activate_tariff_plan_false";

  private volatile GenericEvent caughtEvent;

  @Override
  public void invokeBusinessActivity(final RegisteredInGSMChannel channel) throws Exception {
    long tariffPlanEndDate = LocalDate.now().atStartOfDay().plusDays(tariffPlanDays).atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
    channel.setTariffPlanEndDate(tariffPlanEndDate);
    caughtEvent = null;
  }

  @Override
  public boolean shouldStartBusinessActivity() {
    return caughtEvent != null;
  }

  @Override
  public String describeBusinessActivity() {
    return "activate tariff plan";
  }

  @Override
  public void handleGenericEvent(final String event, final Serializable... args) {
    if (this.event.equals(event)) {
      logger.debug("[{}] - caught event: {}", this, event);
      caughtEvent = GenericEvent.wrapEvent(event, args);
    }
  }

  @ScriptParam(name = "event", doc = "event to activate tariff plan")
  public void setEvent(final String event) {
    this.event = event;
  }

  public String getEvent() {
    return event;
  }

  @ScriptParam(name = "tariff plan days", doc = "days of tariff plan")
  public void setTariffPlanDays(final int tariffPlanDays) {
    this.tariffPlanDays = tariffPlanDays;
  }

  public int getTariffPlanDays() {
    return tariffPlanDays;
  }

  @ScriptParam(name = "event on true", doc = "this event will be generated when activation was successful")
  public void setEventOnTrue(final String eventOnTrue) {
    this.eventOnTrue = eventOnTrue;
  }

  public String getEventOnTrue() {
    return eventOnTrue;
  }

  @ScriptParam(name = "event on false", doc = "this event will be generated when activation was failed")
  public void setEventOnFalse(final String eventOnFalse) {
    this.eventOnFalse = eventOnFalse;
  }

  public String getEventOnFalse() {
    return eventOnFalse;
  }

}

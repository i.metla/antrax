/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.business;

import com.flamesgroup.antrax.automation.annotations.Script;
import com.flamesgroup.antrax.automation.annotations.ScriptParam;
import com.flamesgroup.antrax.automation.businesscripts.RegisteredInGSMChannel;
import com.flamesgroup.antrax.automation.listeners.GenericEventListener;
import com.flamesgroup.antrax.automation.scripts.BusinessActivityScript;
import com.flamesgroup.antrax.helper.business.GenericEvent;

import java.io.Serializable;

@Script(name = "reset IMEI on event", doc = "reset IMEI if event occurs")
public class ResetIMEIOnEvent implements BusinessActivityScript, GenericEventListener {

  private String event = "resetIMEI";
  private String eventOnFail = "resetIMEIFailed";
  private String eventOnSuccess = "resetIMEISuccess";

  private volatile GenericEvent eventCaught;

  @ScriptParam(name = "event", doc = "event to reset IMEI")
  public void setEvent(final String event) {
    this.event = event;
  }

  public String getEvent() {
    return this.event;
  }

  public String getEventOnFail() {
    return eventOnFail;
  }

  @ScriptParam(name = "event on fail", doc = "this event will be generated when reset IMEI failed")
  public void setEventOnFail(final String eventOnFail) {
    this.eventOnFail = eventOnFail;
  }

  public String getEventOnSuccess() {
    return eventOnSuccess;
  }

  @ScriptParam(name = "event on success", doc = "this event will be generated when reset IMEI success")
  public void setEventOnSuccess(final String eventOnSuccess) {
    this.eventOnSuccess = eventOnSuccess;
  }

  @Override
  public void handleGenericEvent(final String event, final Serializable... args) {
    if (this.event.equals(event)) {
      eventCaught = GenericEvent.wrapEvent(event, args);
    }
  }

  @Override
  public String describeBusinessActivity() {
    return "reset IMEI if event occurs";
  }

  @Override
  public void invokeBusinessActivity(final RegisteredInGSMChannel channel) throws Exception {
    try {
      channel.resetIMEI();
      eventCaught.respondSuccess(channel);
      channel.fireGenericEvent(eventOnSuccess);
    } catch (final Exception e) {
      eventCaught.respondFailure(channel, e.getMessage());
      channel.fireGenericEvent(eventOnFail);
    } finally {
      eventCaught = null;
    }
  }

  @Override
  public boolean shouldStartBusinessActivity() {
    return eventCaught != null;
  }

}

/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.business;

import static com.flamesgroup.antrax.scripts.utils.GeneratePeriodHelper.generatePeriod;

import com.flamesgroup.antrax.automation.annotations.Script;
import com.flamesgroup.antrax.automation.annotations.ScriptParam;
import com.flamesgroup.antrax.automation.annotations.StateField;
import com.flamesgroup.antrax.automation.businesscripts.RegisteredInGSMChannel;
import com.flamesgroup.antrax.automation.listeners.ActivityListener;
import com.flamesgroup.antrax.automation.scripts.BusinessActivityScript;
import com.flamesgroup.antrax.automation.statefulscripts.ScriptSaver;
import com.flamesgroup.antrax.automation.statefulscripts.StatefullScript;
import com.flamesgroup.antrax.helper.business.GenericEvent;
import com.flamesgroup.antrax.scripts.utils.Chance;
import com.flamesgroup.antrax.scripts.utils.TimePeriod;
import com.flamesgroup.antrax.scripts.utils.TimeUtils;
import com.flamesgroup.antrax.scripts.utils.VariableLong;
import com.flamesgroup.antrax.storage.commons.GSMGroup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Calendar;

@Script(name = "generate event randomly by time", doc = "generates events randomly; specified number of events generates during specified time interval")
public class GenerateEventByTime implements BusinessActivityScript, StatefullScript, ActivityListener {

  private static final long serialVersionUID = -6641117047100254928L;

  private static final Logger logger = LoggerFactory.getLogger(GenerateEventByTime.class);

  @StateField
  private volatile int events;
  @StateField
  private long eventLimit = 12;
  @StateField
  private long nextPeriod;

  private GenericEvent randomEvent = GenericEvent.uncheckedEvent("randomEvent");

  private VariableLong limit = new VariableLong(10, 15);
  private TimePeriod startTime = new TimePeriod(TimePeriod.inHours(12));
  private TimePeriod endTime = new TimePeriod(TimePeriod.inHours(14));
  private Chance eventChance = new Chance(60);

  private final ScriptSaver saver = new ScriptSaver();

  @ScriptParam(name = "events limit", doc = "limit generated events count interval")
  public void setEventsLimit(final VariableLong limit) {
    this.limit = limit;
    eventLimit = limit.random();
  }

  public VariableLong getEventsLimit() {
    return this.limit;
  }

  @ScriptParam(name = "random event", doc = "this event generates randomly")
  public void setRandomEvent(final String event) {
    this.randomEvent = GenericEvent.uncheckedEvent(event);
  }

  public String getRandomEvent() {
    return randomEvent.getEvent();
  }

  @ScriptParam(name = "start time", doc = "start time of period in which events will be generated")
  public void setStartTime(final TimePeriod startTime) {
    this.startTime = startTime;
  }

  public TimePeriod getStartTime() {
    return startTime;
  }

  @ScriptParam(name = "end time", doc = "end time of period in which events will be generated")
  public void setEndTime(final TimePeriod endTime) {
    this.endTime = endTime;
  }

  public TimePeriod getEndTime() {
    return endTime;
  }

  @ScriptParam(name = "event chance", doc = "chance of event generation in percent")
  public void setEventChance(final Chance eventChance) {
    this.eventChance = eventChance;
  }

  public Chance getEventChance() {
    return eventChance;
  }

  @Override
  public String describeBusinessActivity() {
    return "generates event randomly by time";
  }

  @Override
  public void invokeBusinessActivity(final RegisteredInGSMChannel channel) throws Exception {
    randomEvent.fireEvent(channel);
    events++;
    nextPeriod = generatePeriod(startTime.getPeriod(), endTime.getPeriod(), getTimeOfDay(), eventLimit, events);
    logger.debug("[{}] - next period: {}", this, TimeUtils.writeTime(nextPeriod));
    saver.save();
  }

  @Override
  public boolean shouldStartBusinessActivity() {
    return shouldGenerateEvent();
  }

  private boolean shouldGenerateEvent() {
    long currentTime = getTimeOfDay();
    if (currentTime > startTime.getPeriod() && currentTime < endTime.getPeriod()) {
      if (currentTime < nextPeriod || events >= eventLimit) {
        return false;
      } else if (nextPeriod <= 0) {
        nextPeriod = generatePeriod(startTime.getPeriod(), endTime.getPeriod(), currentTime, eventLimit, events);
        logger.debug("[{}] - next period: {}", this, TimeUtils.writeTime(nextPeriod));
        return false;
      }

      boolean chance = eventChance.countChance();
      if (!chance) { // if chance false wait next period to try generate event
        events++;
        nextPeriod = generatePeriod(startTime.getPeriod(), endTime.getPeriod(), currentTime, eventLimit, events);
        logger.debug("[{}] - next period: {}", this, TimeUtils.writeTime(nextPeriod));
        saver.save();
      }
      return chance;
    } else {
      if (events != 0) {
        logger.debug("[{}] - {} Reseting eventsInPeriod", this, this);
        eventLimit = limit.random();
        events = 0;
        nextPeriod = 0;
        saver.save();
      }
      return false;
    }
  }

  @Override
  public void handleActivityStarted(final GSMGroup gsmGroup) {
    long currentTime = getTimeOfDay();
    if (currentTime > startTime.getPeriod() && currentTime < endTime.getPeriod() && currentTime >= nextPeriod) {
      nextPeriod = generatePeriod(startTime.getPeriod(), endTime.getPeriod(), currentTime, eventLimit, events);
      logger.debug("[{}] - current time: {} exceed next period time, so next period: {}", this, TimeUtils.writeTime(currentTime), TimeUtils.writeTime(nextPeriod));
      saver.save();
    }
  }

  @Override
  public void handleActivityEnded() {
  }

  long getTimeOfDay() {
    Calendar cal = Calendar.getInstance();
    cal.set(Calendar.DAY_OF_YEAR, 1);
    cal.clear(Calendar.MONTH);
    cal.clear(Calendar.YEAR);
    cal.set(Calendar.ZONE_OFFSET, 0);
    return cal.getTimeInMillis();
  }

  @Override
  public ScriptSaver getScriptSaver() {
    return saver;
  }

}

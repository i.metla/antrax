/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.imeigeneration;

import com.flamesgroup.antrax.automation.annotations.Script;
import com.flamesgroup.antrax.automation.annotations.ScriptParam;
import com.flamesgroup.commons.IMEI;
import com.flamesgroup.commons.IMEIException;
import com.flamesgroup.commons.IMEIPattern;

@Script(name = "Random IMEI", doc = "generates IMEI randomly according to pattern")
public class RandomIMEIGenerator extends IMEIGenerator {

  private static final long serialVersionUID = 1789435462715273581L;

  private IMEIPattern imeiPattern;

  @ScriptParam(name = "pattern", doc = "pattern for IMEI generation")
  public void setPattern(final IMEIPattern imeiPattern) {
    this.imeiPattern = imeiPattern;
  }

  public IMEIPattern getPattern() {
    return new IMEIPattern("35XXXXXXXXXXXX");
  }

  @Override
  public IMEI generateIMEI() throws IMEIException {
    return IMEI.generate(imeiPattern, getSimData().getPhoneNumber());
  }

}

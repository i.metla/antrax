/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.action;

import com.flamesgroup.antrax.automation.actionscripts.Action;
import com.flamesgroup.antrax.automation.annotations.Script;
import com.flamesgroup.antrax.automation.annotations.ScriptParam;
import com.flamesgroup.antrax.automation.businesscripts.RegisteredInGSMChannel;
import com.flamesgroup.antrax.automation.listeners.GenericEventListener;
import com.flamesgroup.antrax.automation.listeners.RegistryAccessListener;
import com.flamesgroup.antrax.automation.scripts.ActionProviderScript;
import com.flamesgroup.antrax.automation.statefulscripts.ScriptSaver;
import com.flamesgroup.antrax.automation.utils.registry.RegistryAccess;
import com.flamesgroup.antrax.helper.business.GenericEvent;
import com.flamesgroup.antrax.helper.business.checkbalance.CheckBalanceHelper;
import com.flamesgroup.antrax.helper.business.checkbalance.USSDCheckBalanceHelper;
import com.flamesgroup.antrax.helper.business.transfer.MoneyTransferHelper;
import com.flamesgroup.antrax.helper.business.transfer.USSDMoneyTransferHelper;
import com.flamesgroup.antrax.helper.business.transfer.USSDResponseAnswer;
import com.flamesgroup.antrax.helper.business.transfer.USSDSessionMoneyTransferHelper;
import com.flamesgroup.antrax.helper.business.transfer.WrongResponseException;
import com.flamesgroup.antrax.scripts.utils.TimeInterval;
import com.flamesgroup.antrax.scripts.utils.TimePeriod;
import com.flamesgroup.unit.PhoneNumber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

@Script(name = "Money transfer action", doc = "transfer money from this SIM card to another")
public class MoneyTransferAction implements ActionProviderScript, GenericEventListener, RegistryAccessListener {

  private static final Logger logger = LoggerFactory.getLogger(MoneyTransferAction.class);

  private String providedAction = "money_transfer";
  private int amount = 5;
  private double remainsAmount = 5;
  private CheckBalanceHelper checkBalanceHelper;

  private boolean useUSSDSession;
  private String ussdRequest;
  private final List<USSDResponseAnswer> ussdResponseAnswerList = new LinkedList<>();

  private String allowCheckEvent = "allow_check";
  private GenericEvent checkingFailedEvent = GenericEvent.uncheckedEvent("checing fail");
  private GenericEvent actionFailedEvent = GenericEvent.uncheckedEvent("action fail");
  private GenericEvent actionImpossibleEvent = GenericEvent.uncheckedEvent("action impossible");

  private volatile GenericEvent caughtEvent = GenericEvent.uncheckedEvent("event");

  private double lastBalanceValue;

  private final ScriptSaver saver = new ScriptSaver();
  private transient RegistryAccess registry;

  @ScriptParam(name = "use USSD session", doc = "use USSD session instead of separate USSD")
  public void setUseUSSDSession(final boolean useUSSDSession) {
    this.useUSSDSession = useUSSDSession;
  }

  public boolean getUseUSSDSession() {
    return useUSSDSession;
  }

  @ScriptParam(name = "provided action", doc = "determines name of action, which provided by this script ")
  public void setProvidedAction(final String providedAction) {
    this.providedAction = providedAction;
  }

  @Override
  public String getProvidedAction() {
    return providedAction;
  }

  @ScriptParam(name = "amount", doc = "amount of money to transfer")
  public void setAmount(final int amount) {
    this.amount = amount;
  }

  public int getAmount() {
    return amount;
  }

  @ScriptParam(name = "USSD request", doc = "place $ for amount and @ for number. Example: *150*@*$#")
  public void setUSSDRequest(final String ussdRequest) {
    this.ussdRequest = ussdRequest;
  }

  public String getUSSDRequest() {
    return "*150*@*$#";
  }

  @ScriptParam(name = "response patterns list", doc = "regular expressins for parsing response and generating answers")
  public void addUSSDResponseAnswerList(final USSDResponseAnswer ussdResponseAnswer) {
    ussdResponseAnswerList.add(ussdResponseAnswer);
  }

  public USSDResponseAnswer getUSSDResponseAnswerList() {
    return new USSDResponseAnswer("(.*)", "$1", true);
  }

  @ScriptParam(name = "remains amount", doc = "minimum amount of money which must remains after transfer. Put 0 or negative value here to discard balance checking")
  public void setRemainsAmount(final double amount) {
    this.remainsAmount = amount;
  }

  public double getRemainsAmount() {
    return remainsAmount;
  }

  @ScriptParam(name = "balance check data", doc = "balance checking data")
  public void setCheckBalanceHelper(final CheckBalanceHelper checkBalanceHelper) {
    this.checkBalanceHelper = checkBalanceHelper;
  }

  public CheckBalanceHelper getCheckBalanceHelper() {
    return new USSDCheckBalanceHelper("*111#", ".*bonus (\\d+\\.\\d{2})hrn\\. Dzvinki po 0.*");
  }

  @ScriptParam(name = "allow check event", doc = "event to allow balance checking")
  public void setAllowCheckEvent(final String event) {
    this.allowCheckEvent = event;
  }

  public String getAllowCheckEvent() {
    return allowCheckEvent;
  }

  @ScriptParam(name = "checking failed event", doc = "generates on checking action execution fail")
  public void setCheckingFailedEvent(final String event) {
    this.checkingFailedEvent = GenericEvent.uncheckedEvent(event);
  }

  public String getCheckingFailedEvent() {
    return this.checkingFailedEvent.getEvent();
  }

  @ScriptParam(name = "action failed event", doc = "generates on action execution fail")
  public void setActionFailedEvent(final String event) {
    this.actionFailedEvent = GenericEvent.uncheckedEvent(event);
  }

  public String getActionFailedEvent() {
    return this.actionFailedEvent.getEvent();
  }

  @ScriptParam(name = "action impossible event", doc = "generates when action can't be executed")
  public void setActionImpossibleEvent(final String event) {
    this.actionImpossibleEvent = GenericEvent.uncheckedEvent(event);
  }

  public String getActionImpossibleEvent() {
    return this.actionImpossibleEvent.getEvent();
  }

  @Override
  public boolean canExecuteAction(final Action action, final RegisteredInGSMChannel channel) throws Exception {
    if (!action.getAction().equals(providedAction) || caughtEvent == null) {
      return false;
    } else {
      if (remainsAmount > 0) {
        boolean retVal;
        try {
          lastBalanceValue = checkBalanceHelper.getBalanceValue(channel, registry);
          retVal = lastBalanceValue - amount >= remainsAmount;
        } catch (Exception e) {
          logger.info("[{}] - while checking execution action {}", this, action, e);
          checkingFailedEvent.fireEvent(channel);
          throw e;
        }
        if (!retVal) {
          caughtEvent = null;
          actionImpossibleEvent.fireEvent(channel);
          saver.save();
        }
        return retVal;
      } else {
        return true;
      }
    }
  }

  @Override
  public void executeAction(final Action action, final RegisteredInGSMChannel channel) throws Exception {
    MoneyTransferHelper moneyTransferHelper;
    if (useUSSDSession) {
      moneyTransferHelper = new USSDSessionMoneyTransferHelper(ussdRequest, ussdResponseAnswerList);
    } else {
      moneyTransferHelper = new USSDMoneyTransferHelper(ussdRequest, ussdResponseAnswerList);
    }
    try {
      moneyTransferHelper.transferMoney(channel, (PhoneNumber) action.getArgs()[0], amount);
    } catch (WrongResponseException e) {
      logger.info("[{}] - while executing action {}", this, action, e);
      actionFailedEvent.fireEvent(channel);
      throw e;
    } catch (Exception e) {
      logger.info("[{}] - while executing action {}", this, action, e);
      logger.info("[{}] - checking action success", this);
      try {
        if (checkActionSuccess(channel)) {
          return;
        }
      } catch (Exception e1) {
        logger.info("[{}] - while checking action success", this, e1);
      }
      actionFailedEvent.fireEvent(channel);
      throw e;
    }
  }

  private boolean checkActionSuccess(final RegisteredInGSMChannel channel) throws Exception {
    Thread.sleep(new TimeInterval(TimePeriod.inSeconds(50), TimePeriod.inSeconds(70)).random());
    return lastBalanceValue > checkBalanceHelper.getBalanceValue(channel, registry);
  }

  @Override
  public void handleGenericEvent(final String event, final Serializable... args) {
    if (this.allowCheckEvent.equals(event)) {
      caughtEvent = GenericEvent.wrapEvent(event, args);
      saver.save();
    }
  }

  @Override
  public void setRegistryAccess(final RegistryAccess registry) {
    this.registry = registry;
  }

}

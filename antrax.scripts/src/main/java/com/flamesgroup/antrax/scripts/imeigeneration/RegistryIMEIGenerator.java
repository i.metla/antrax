/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.imeigeneration;

import com.flamesgroup.antrax.automation.annotations.Script;
import com.flamesgroup.antrax.automation.annotations.ScriptParam;
import com.flamesgroup.antrax.automation.listeners.RegistryAccessListener;
import com.flamesgroup.antrax.automation.registry.RegistryEntry;
import com.flamesgroup.antrax.automation.utils.registry.RegistryAccess;
import com.flamesgroup.antrax.automation.utils.registry.UnsyncRegistryEntryException;
import com.flamesgroup.antrax.storage.exceptions.DataModificationException;
import com.flamesgroup.commons.IMEI;
import com.flamesgroup.commons.IMEIException;

@Script(name = "Registry IMEI", doc = "get IMEI from registry")
public class RegistryIMEIGenerator extends IMEIGenerator implements RegistryAccessListener {

  private static final long serialVersionUID = -6492971190971847122L;

  private transient RegistryAccess registry;
  private String path = "fullImei.list";

  @ScriptParam(name = "path", doc = "path to IMEI")
  public void setPath(final String path) {
    this.path = path;
  }

  public String getPath() {
    return path;
  }

  @Override
  public IMEI generateIMEI() throws IMEIException {
    RegistryEntry[] registryEntries = registry.listEntries(path, 1);
    if (registryEntries.length == 0) {
      throw new IMEIException(String.format("IMEI isn't present in registry path: %s", path));
    }

    String imei = registryEntries[0].getValue();
    String badPath = path + ".bad";
    for (byte b : imei.getBytes()) {
      if (!Character.isDigit(b)) {
        try {
          registry.move(registryEntries[0], badPath);
        } catch (UnsyncRegistryEntryException | DataModificationException ex) {
          throw new IMEIException(String.format("Can't move IMEI: %s to path: %s", imei, badPath), ex);
        }
        throw new IMEIException(String.format("IMEI: %s must contain only digit chars", imei));
      }
    }

    IMEI generatedIMEI;
    try {
      generatedIMEI = IMEI.valueOf(imei);
    } catch (IMEIException | IllegalArgumentException e) {
      try {
        registry.move(registryEntries[0], badPath);
      } catch (UnsyncRegistryEntryException | DataModificationException ex) {
        ex.addSuppressed(e);
        throw new IMEIException(String.format("Can't move IMEI: %s to path: %s", imei, badPath), ex);
      }
      throw e;
    }
    String usedPath = path + ".used";
    try {
      registry.move(registryEntries[0], usedPath);
    } catch (UnsyncRegistryEntryException | DataModificationException e) {
      throw new IMEIException(String.format("Can't move IMEI: %s to path: %s", imei, usedPath), e);
    }
    return generatedIMEI;
  }

  @Override
  public void setRegistryAccess(final RegistryAccess registry) {
    this.registry = registry;
  }

}

/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.business;

import com.flamesgroup.antrax.automation.annotations.Script;
import com.flamesgroup.antrax.automation.annotations.ScriptParam;
import com.flamesgroup.antrax.automation.businesscripts.RegisteredInGSMChannel;
import com.flamesgroup.antrax.automation.listeners.GenericEventListener;
import com.flamesgroup.antrax.automation.listeners.RegistryAccessListener;
import com.flamesgroup.antrax.automation.registry.RegistryEntry;
import com.flamesgroup.antrax.automation.scripts.BusinessActivityScript;
import com.flamesgroup.antrax.automation.utils.registry.RegistryAccess;
import com.flamesgroup.antrax.helper.business.GenericEvent;
import com.flamesgroup.antrax.helper.business.PhoneNumberGenerator;
import com.flamesgroup.antrax.helper.business.sms.SMSBox;
import com.flamesgroup.antrax.scripts.utils.TimeInterval;
import com.flamesgroup.antrax.scripts.utils.VariableLong;
import com.flamesgroup.unit.PhoneNumber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.Random;

@Script(name = "SMS", doc = "waits until event occured and then sends SMS to the generated phone number")
public class SMS implements BusinessActivityScript, GenericEventListener, RegistryAccessListener {

  private static final Logger logger = LoggerFactory.getLogger(SMS.class);

  private PhoneNumberGenerator phoneNumberGenerator = new PhoneNumberGenerator("(8050).......");
  private int attemptsCount = 2;
  private TimeInterval attemptsInterval = new TimeInterval(10000, 15000);
  private String event = "send_sms";
  private boolean emptyText = false;
  private boolean lockOnFail = false;

  private String eventOnSuccess = "";
  private String eventOnFailure = "";

  private VariableLong smsCount = new VariableLong(1, 1);
  private TimeInterval smsInterval = new TimeInterval(15000, 20000);
  private boolean skipErrors = false;

  private String registryPath = "sms.numbers";

  private volatile GenericEvent caughtEvent;
  private transient RegistryAccess registryAccess;

  @ScriptParam(name = "number pattern", doc = "phone number pattern. Pattern consists of numbers, asterisk (any number) and set of number put in []. For example 8(068|063|099)******** will generate one of the 8068, 8063 or 8099 and random number")
  public void setPhoneNumberGenerator(final PhoneNumberGenerator phoneNumberGenerator) {
    this.phoneNumberGenerator = phoneNumberGenerator;
  }

  public PhoneNumberGenerator getPhoneNumberGenerator() {
    return phoneNumberGenerator;
  }

  @ScriptParam(name = "attempts count", doc = "how many attempts should be made on SMS sending failure")
  public void setAttemptsCount(final int attemptsCount) {
    this.attemptsCount = attemptsCount;
  }

  @ScriptParam(name = "interval between attempts", doc = "Time interval between attempts sending SMS in case of error")
  public void setAttemptsInterval(final TimeInterval attemptsInterval) {
    this.attemptsInterval = attemptsInterval;
  }

  public TimeInterval getAttemptsInterval() {
    return attemptsInterval;
  }

  public int getAttemptsCount() {
    return attemptsCount;
  }

  @ScriptParam(name = "event", doc = "event to send SMS")
  public void setEvent(final String event) {
    this.event = event;
  }

  public String getEvent() {
    return event;
  }

  @ScriptParam(name = "lock on fail", doc = "lock card if send sms failed")
  public void setLockOnFail(final boolean lockOnFail) {
    this.lockOnFail = lockOnFail;
  }

  public boolean getLockOnFail() {
    return lockOnFail;
  }

  @ScriptParam(name = "SMS count", doc = "Number of SMS sending in one script execution")
  public void setSMSCount(final VariableLong smsCount) {
    this.smsCount = smsCount;
  }

  public VariableLong getSMSCount() {
    return smsCount;
  }

  @ScriptParam(name = "interval between SMS", doc = "Time interval between SMSes sending")
  public void setSMSInterval(final TimeInterval smsInterval) {
    this.smsInterval = smsInterval;
  }

  public TimeInterval getSMSInterval() {
    return smsInterval;
  }

  @ScriptParam(name = "empty text", doc = "SMS will be send with empty text instead of random")
  public void setEmptyText(final boolean emptyText) {
    this.emptyText = emptyText;
  }

  public boolean getEmptyText() {
    return emptyText;
  }

  @ScriptParam(name = "skip errors", doc = "if set, error on SMS sending will be ignored, otherwise, failed event will generate on first error")
  public void setSkipErrors(final boolean skipErrors) {
    this.skipErrors = skipErrors;
  }

  public boolean getSkipErrors() {
    return skipErrors;
  }

  @ScriptParam(name = "path in registry", doc = "path to the registry key, which contains numbers for call. Number take randomly from registry. If registry empty, will use number pattern")
  public void setRegistryPath(final String path) {
    this.registryPath = path;
  }

  public String getRegistryPath() {
    return registryPath;
  }

  @ScriptParam(name = "even on success", doc = "this event will be generated on successful sending sms")
  public void setEventOnSuccess(final String eventOnSuccess) {
    this.eventOnSuccess = eventOnSuccess;
  }

  public String getEventOnSuccess() {
    return eventOnSuccess;
  }

  @ScriptParam(name = "event on failure", doc = "this event will be generated on failure send sms")
  public void setEventOnFailure(final String eventOnFailure) {
    this.eventOnFailure = eventOnFailure;
  }

  public String getEventOnFailure() {
    return eventOnFailure;
  }

  @Override
  public void setRegistryAccess(final RegistryAccess registry) {
    this.registryAccess = registry;
  }

  @Override
  public String describeBusinessActivity() {
    return "sending SMS";
  }

  @Override
  public void invokeBusinessActivity(final RegisteredInGSMChannel channel) throws Exception {
    try {
      for (int i = 0; i < smsCount.random(); i++) {
        try {
          sendSMS(channel);
        } catch (Exception e) {
          if (!skipErrors) {
            caughtEvent.respondFailure(channel, e.getMessage());
            throw e;
          }
        }
        Thread.sleep(smsInterval.random());
      }

      caughtEvent.respondSuccess(channel);
      if (eventOnSuccess != null && !eventOnSuccess.isEmpty()) {
        channel.fireGenericEvent(eventOnSuccess);
      }
    } catch (Exception e) {
      if (lockOnFail) {
        channel.lock("SMS sending failed: " + e.getMessage());
      }

      logger.warn("[{}] - SMS sending failed", this, e);
      if (eventOnFailure != null && !eventOnFailure.isEmpty()) {
        channel.fireGenericEvent(eventOnFailure);
      }
    } finally {
      caughtEvent = null;
    }
  }

  private void sendSMS(final RegisteredInGSMChannel channel) throws Exception {
    int attemptCount = 0;
    while (true) {
      try {
        String smsText = emptyText ? "" : SMSBox.randomSMS();
        PhoneNumber number = generatePhoneNumber();
        logger.debug("[{}] - {} is trying to send SMS [{}] to {}", this, channel, smsText, number);

        channel.sendSMS(number, smsText);
        break;
      } catch (Exception e) {
        attemptCount++;
        logger.info("[{}] - while sending SMS", this, e);
        if (attemptCount >= attemptsCount) {
          throw e;
        }
      }
      Thread.sleep(attemptsInterval.random());
    }
  }

  @Override
  public boolean shouldStartBusinessActivity() {
    return caughtEvent != null;
  }

  @Override
  public void handleGenericEvent(final String event, final Serializable... args) {
    if (this.event.equals(event)) {
      caughtEvent = GenericEvent.wrapEvent(event, args);
    }
  }

  private PhoneNumber generatePhoneNumber() {
    RegistryEntry[] entries = registryAccess.listEntries(registryPath, Integer.MAX_VALUE);
    if (entries != null && entries.length != 0) {
      int index = new Random(System.currentTimeMillis()).nextInt(entries.length);
      String value = entries[index].getValue();
      logger.debug("[{}] - use number [{}] from registry", this, value);
      return new PhoneNumberGenerator(value).generate();
    } else {
      logger.debug("[{}] - use phoneNumberGenerator [{}]", this, phoneNumberGenerator);
      return phoneNumberGenerator.generate();
    }
  }

}

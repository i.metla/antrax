/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.action;

import com.flamesgroup.antrax.automation.actionscripts.Action;
import com.flamesgroup.antrax.automation.annotations.Script;
import com.flamesgroup.antrax.automation.annotations.ScriptParam;
import com.flamesgroup.antrax.automation.businesscripts.RegisteredInGSMChannel;
import com.flamesgroup.antrax.automation.listeners.RegistryAccessListener;
import com.flamesgroup.antrax.automation.scripts.ActionProviderScript;
import com.flamesgroup.antrax.automation.utils.registry.RegistryAccess;
import com.flamesgroup.antrax.helper.business.GenericEvent;
import com.flamesgroup.antrax.helper.business.checkbalance.CheckBalanceHelper;
import com.flamesgroup.antrax.helper.business.checkbalance.USSDCheckBalanceHelper;
import com.flamesgroup.antrax.helper.business.transfer.MoneyTransferHelper;
import com.flamesgroup.antrax.helper.business.transfer.USSDMoneyTransferHelper;
import com.flamesgroup.antrax.helper.business.transfer.USSDResponseAnswer;
import com.flamesgroup.unit.PhoneNumber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

@Script(name = "TF Transfer action", doc = "transfers money")
public class TFTransferAction implements ActionProviderScript, RegistryAccessListener {

  private final Logger logger = LoggerFactory.getLogger(TFTransferAction.class);

  public String providesAction = "TFTransferAction";
  private GenericEvent actionFailedEvent = GenericEvent.uncheckedEvent("TFTransferFailed");

  private CheckBalanceHelper checkBalanceHelper;
  private transient RegistryAccess registry;

  private int amount = 5;
  private double remainsAmount = 5;
  private boolean transferAll = false;

  private String ussdRequest;
  private final List<USSDResponseAnswer> ussdResponseAnswerList = new LinkedList<>();

  @ScriptParam(name = "action name", doc = "action name")
  public void setAction(final String action) {
    this.providesAction = action;
  }

  public String getAction() {
    return providesAction;
  }

  @ScriptParam(name = "amount", doc = "amount of money to transfer")
  public void setAmount(final int amount) {
    this.amount = amount;
  }

  public int getAmount() {
    return amount;
  }

  @ScriptParam(name = "USSD request", doc = "place $ for amount and @ for number. Example: *150*@*$#")
  public void setUSSDRequest(final String ussdRequest) {
    this.ussdRequest = ussdRequest;
  }

  public String getUSSDRequest() {
    return "*124*$*@#";
  }

  @ScriptParam(name = "response patterns list", doc = "regular expressions for parsing response and generating answers")
  public void addUSSDResponseAnswerList(final USSDResponseAnswer ussdResponseAnswer) {
    ussdResponseAnswerList.add(ussdResponseAnswer);
  }

  public USSDResponseAnswer getUSSDResponseAnswerList() {
    return new USSDResponseAnswer("(.*)", "$1", true);
  }

  @ScriptParam(name = "transfer all", doc = "transfer all except remain amount")
  public void setTransferAll(final boolean transferAll) {
    this.transferAll = transferAll;
  }

  public boolean getTransferAll() {
    return transferAll;
  }

  @ScriptParam(name = "remains amount", doc = "minimum amount of money which must remains after transfer")
  public void setRemainsAmount(final double amount) {
    this.remainsAmount = amount;
  }

  public double getRemainsAmount() {
    return remainsAmount;
  }

  @ScriptParam(name = "balance check data", doc = "balance checking data. Balance checking may take up to 4 minutes")
  public void setCheckBalanceHelper(final CheckBalanceHelper checkBalanceHelper) {
    this.checkBalanceHelper = checkBalanceHelper;
  }

  public CheckBalanceHelper getCheckBalanceHelper() {
    return new USSDCheckBalanceHelper("*111#", ".*rahunku: (\\d+\\.\\d{2}) ?grn.*");
  }

  @ScriptParam(name = "action failed event", doc = "generates on action execution fail")
  public void setActionFailedEvent(final String event) {
    this.actionFailedEvent = GenericEvent.uncheckedEvent(event);
  }

  public String getActionFailedEvent() {
    return this.actionFailedEvent.getEvent();
  }

  PhoneNumber extractSrcPhoneNumber(final Action action) {
    Serializable[] args = action.getArgs();
    if (args.length != 2) {
      throw new IllegalArgumentException("Not enough arguments of action");
    }
    String phoneNumber = args[0].toString();
    return new PhoneNumber(phoneNumber);
  }

  PhoneNumber extractDstPhoneNumber(final Action action) {
    Serializable[] args = action.getArgs();
    if (args.length != 2) {
      throw new IllegalArgumentException("Not enough arguments of action");
    }
    String phoneNumber = args[1].toString();
    return new PhoneNumber(phoneNumber);
  }


  @Override
  public boolean canExecuteAction(final Action action, final RegisteredInGSMChannel channel) throws Exception {
    if (!action.getAction().equals(providesAction)) {
      return false;
    }

    PhoneNumber srcNumber = extractSrcPhoneNumber(action);
    if (channel.getSimData().getPhoneNumber().equals(srcNumber)) {
      logger.debug("Found source card: " + srcNumber);
      return true;
    }
    return false;
  }

  @Override
  public void executeAction(final Action action, final RegisteredInGSMChannel channel) throws Exception {
    PhoneNumber srcNumber = extractSrcPhoneNumber(action);
    PhoneNumber dstNumber = extractDstPhoneNumber(action);

    logger.debug("[{}] - checking balance value...", this);
    double currentAmount = checkBalanceHelper.getBalanceValue(channel, registry);
    int transferAmount;

    if (currentAmount > remainsAmount) {
      if (transferAll || amount > (int) Math.round(currentAmount - remainsAmount)) {
        transferAmount = (int) Math.round(currentAmount - remainsAmount); //transfer all money except reminder
      } else {
        transferAmount = amount;
      }
    } else {
      transferAmount = 0;
    }

    logger.debug("[{}] - {} transfer money ({}): -> {}", this, srcNumber.getValue(), dstNumber.getValue(), transferAmount);

    if (0 == transferAmount) {
      logger.info("[{}] - while executing action {}. Amount to transfer is 0", this, action);
      actionFailedEvent.fireEvent(channel);
    } else {
      MoneyTransferHelper moneyTransferHelper;
      moneyTransferHelper = new USSDMoneyTransferHelper(ussdRequest, ussdResponseAnswerList);
      try {
        moneyTransferHelper.transferMoney(channel, dstNumber, transferAmount);
      } catch (Exception e) {
        logger.info("[{}] - while executing action {}", this, action, e);
        //here we can check were money transferred for real (see MoneyTransferAction)
        actionFailedEvent.fireEvent(channel);
        throw e;
      }
    }
  }

  @Override
  public void setRegistryAccess(final RegistryAccess registry) {
    this.registry = registry;
  }

  @Override
  public String getProvidedAction() {
    return providesAction;
  }

}

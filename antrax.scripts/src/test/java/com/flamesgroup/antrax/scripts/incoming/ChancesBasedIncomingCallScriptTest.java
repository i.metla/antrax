/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.incoming;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import com.flamesgroup.antrax.scripts.utils.Chance;
import com.flamesgroup.antrax.scripts.utils.TimeInterval;
import com.flamesgroup.antrax.scripts.utils.TimePeriod;
import com.flamesgroup.unit.PhoneNumber;
import org.junit.Test;

public class ChancesBasedIncomingCallScriptTest {

  @Test
  public void testBaseBusinessActivityScript() throws InterruptedException {
    ChancesBasedIncomingCallScript script = mkScript(100, 50, 100, 20);
    assertFalse(script.shouldAnswer());
    assertFalse(script.shouldDrop());

    script.handleIncomingCall(new PhoneNumber("111"));
    Thread.sleep(50);
    assertTrue(script.shouldAnswer());
    assertFalse(script.shouldDrop());

    script.handleIncomingCallAnswered();
    assertTrue(script.shouldAnswer());
    assertFalse(script.shouldDrop());
    Thread.sleep(300);
    assertTrue(script.shouldAnswer());
    assertTrue(script.shouldDrop());
    script.handleIncomingCallDropped(50);
    assertFalse(script.shouldAnswer());
    assertFalse(script.shouldDrop());

    script = mkScript(0, 50, 100, 20);
    script.handleIncomingCall(new PhoneNumber("111"));
    assertFalse(script.shouldAnswer());
  }

  private ChancesBasedIncomingCallScript mkScript(final int incomingCallAcceptChance, final long incomingCallDurationMin, final long incomingCallDurationMax, final long idle) {
    ChancesBasedIncomingCallScript retval = new ChancesBasedIncomingCallScript();
    retval.setAcceptChance(new Chance(incomingCallAcceptChance));
    retval.setActive(new TimeInterval(new TimePeriod(incomingCallDurationMin), new TimePeriod(incomingCallDurationMax)));
    retval.setIdle(new TimeInterval(idle, idle));
    return retval;
  }

}

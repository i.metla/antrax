/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.gateway;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import com.flamesgroup.antrax.storage.commons.IServerData;
import com.flamesgroup.antrax.storage.commons.impl.ServerData;
import org.junit.Test;

import java.net.UnknownHostException;

public class AnyExceptPreviousGatewayScriptTest {

  @Test
  public void testAnyExceptPreviousGatewayScript() throws UnknownHostException {
    IServerData gateway1 = createGateway(1, "gateway_1", "192.168.1.1");
    IServerData gateway2 = createGateway(2, "gateway_2", "192.168.1.2");
    AnyExceptPreviousGatewayScript gwScript = new AnyExceptPreviousGatewayScript();
    assertTrue(gwScript.isGatewayApplies(gateway1));
    assertTrue(gwScript.isGatewayApplies(gateway2));
    assertEquals("any", gwScript.predictNextGateway().toLocalizedString());
    gwScript.handleSessionStarted(gateway1);
    assertFalse(gwScript.isGatewayApplies(gateway1));
    assertTrue(gwScript.isGatewayApplies(gateway2));
    assertEquals("any except gateway_1", gwScript.predictNextGateway().toLocalizedString());
    gwScript.handleSessionStarted(gateway2);
    assertTrue(gwScript.isGatewayApplies(gateway1));
    assertFalse(gwScript.isGatewayApplies(gateway2));
    assertEquals("any except gateway_2", gwScript.predictNextGateway().toLocalizedString());
  }

  @Test
  public void testAnyExceptPreviousWithEmptyDepth() throws UnknownHostException {
    IServerData gateway1 = createGateway(1, "gateway_1", "192.168.1.1");
    IServerData gateway2 = createGateway(2, "gateway_2", "192.168.1.2");
    AnyExceptPreviousGatewayScript gwScript = new AnyExceptPreviousGatewayScript();
    gwScript.setDepth(0);
    assertTrue(gwScript.isGatewayApplies(gateway1));
    assertTrue(gwScript.isGatewayApplies(gateway2));
    assertEquals("any", gwScript.predictNextGateway().toLocalizedString());
    gwScript.handleSessionStarted(gateway1);
    assertTrue(gwScript.isGatewayApplies(gateway1));
    assertTrue(gwScript.isGatewayApplies(gateway2));
    assertEquals("any", gwScript.predictNextGateway().toLocalizedString());
    gwScript.handleSessionStarted(gateway2);
    assertTrue(gwScript.isGatewayApplies(gateway1));
    assertTrue(gwScript.isGatewayApplies(gateway2));
    assertEquals("any", gwScript.predictNextGateway().toLocalizedString());
  }

  @Test
  public void testAnyExceptPreviousWithNegativeDepth() throws UnknownHostException {
    IServerData gateway1 = createGateway(1, "gateway_1", "192.168.1.1");
    IServerData gateway2 = createGateway(2, "gateway_2", "192.168.1.2");
    AnyExceptPreviousGatewayScript gwScript = new AnyExceptPreviousGatewayScript();
    gwScript.setDepth(-1);
    assertTrue(gwScript.isGatewayApplies(gateway1));
    assertTrue(gwScript.isGatewayApplies(gateway2));
    assertEquals("any", gwScript.predictNextGateway().toLocalizedString());
    gwScript.handleSessionStarted(gateway1);
    assertTrue(gwScript.isGatewayApplies(gateway1));
    assertTrue(gwScript.isGatewayApplies(gateway2));
    assertEquals("any", gwScript.predictNextGateway().toLocalizedString());
    gwScript.handleSessionStarted(gateway2);
    assertTrue(gwScript.isGatewayApplies(gateway1));
    assertTrue(gwScript.isGatewayApplies(gateway2));
    assertEquals("any", gwScript.predictNextGateway().toLocalizedString());
  }

  @Test
  public void testAnyExceptPreviousWithChangebleDepth() throws UnknownHostException {
    IServerData gateway1 = createGateway(1, "gateway_1", "192.168.1.1");
    IServerData gateway2 = createGateway(2, "gateway_2", "192.168.1.2");
    IServerData gateway3 = createGateway(3, "gateway_3", "192.168.1.3");
    AnyExceptPreviousGatewayScript gwScript = new AnyExceptPreviousGatewayScript();
    gwScript.setDepth(0);
    assertTrue(gwScript.isGatewayApplies(gateway1));
    assertTrue(gwScript.isGatewayApplies(gateway2));
    assertTrue(gwScript.isGatewayApplies(gateway3));
    assertEquals("any", gwScript.predictNextGateway().toLocalizedString());
    gwScript.handleSessionStarted(gateway1);
    assertTrue(gwScript.isGatewayApplies(gateway1));
    assertTrue(gwScript.isGatewayApplies(gateway2));
    assertTrue(gwScript.isGatewayApplies(gateway2));
    assertEquals("any", gwScript.predictNextGateway().toLocalizedString());

    gwScript.setDepth(1);
    gwScript.handleSessionStarted(gateway2);
    assertTrue(gwScript.isGatewayApplies(gateway1));
    assertFalse(gwScript.isGatewayApplies(gateway2));
    assertTrue(gwScript.isGatewayApplies(gateway3));
    assertEquals("any except gateway_2", gwScript.predictNextGateway().toLocalizedString());

    gwScript.setDepth(3);
    gwScript.handleSessionStarted(gateway1);
    assertFalse(gwScript.isGatewayApplies(gateway1));
    assertFalse(gwScript.isGatewayApplies(gateway2));
    assertTrue(gwScript.isGatewayApplies(gateway3));
    assertEquals("any except gateway_2 and gateway_1", gwScript.predictNextGateway().toLocalizedString());

    gwScript.handleSessionStarted(gateway3);
    gwScript.setDepth(2);
    assertFalse(gwScript.isGatewayApplies(gateway1));
    assertTrue(gwScript.isGatewayApplies(gateway2));
    assertFalse(gwScript.isGatewayApplies(gateway3));
    assertEquals("any except gateway_1 and gateway_3", gwScript.predictNextGateway().toLocalizedString());

    gwScript.setDepth(0);
    assertTrue(gwScript.isGatewayApplies(gateway1));
    assertTrue(gwScript.isGatewayApplies(gateway2));
    assertTrue(gwScript.isGatewayApplies(gateway3));
    gwScript.handleSessionStarted(gateway1);
    gwScript.handleSessionStarted(gateway2);
    gwScript.handleSessionStarted(gateway3);
    gwScript.setDepth(2);
    assertTrue(gwScript.isGatewayApplies(gateway1));
    assertTrue(gwScript.isGatewayApplies(gateway2));
    assertTrue(gwScript.isGatewayApplies(gateway3));
    assertEquals("any", gwScript.predictNextGateway().toLocalizedString());

  }

  private IServerData createGateway(final long id, final String name, final String address) throws UnknownHostException {
    IServerData gateway1 = new ServerData(id);
    gateway1.setName(name);
    return gateway1;
  }

}

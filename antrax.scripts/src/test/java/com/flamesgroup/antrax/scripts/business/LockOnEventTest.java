/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.business;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import com.flamesgroup.antrax.helper.business.GenericEvent;
import com.flamesgroup.device.gsmb.atengine.http.HTTPResponse;
import org.junit.Test;

import java.io.Serializable;
import java.util.concurrent.atomic.AtomicReference;

public class LockOnEventTest {

  @Test
  public void testLockEvent() throws Exception {
    LockOnEvent script = new LockOnEvent();
    script.setEvent("lockMe");

    script.handleGenericEvent("lockMe", GenericEvent.EventType.CHECKED, "success", "failure");
    assertTrue(script.shouldStartBusinessActivity());

    final AtomicReference<Boolean> locked = new AtomicReference<>(Boolean.FALSE);
    script.invokeBusinessActivity(new RegisteredInGSMChannelAdapter() {

      @Override
      public void lock(final String reason) {
        locked.set(Boolean.TRUE);
      }

      @Override
      public HTTPResponse sendHTTPRequest(final String apn, final String url, final String caCertificate) throws Exception {
        return null;
      }

      @Override
      public void fireGenericEvent(final String event, final Serializable... args) {
        assertEquals("success", event);
      }

    });
    assertTrue(locked.get());
  }
}

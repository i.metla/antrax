/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.storage.commons.impl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectStreamClass;

public class ScriptCommons extends Domain {

  private static final long serialVersionUID = -8258930413914817696L;

  private String name;
  private transient Object value;
  private byte[] valueStream;

  public String getName() {
    return name;
  }

  public ScriptCommons setName(final String name) {
    this.name = name;
    return this;
  }

  public Object getValue(final ClassLoader classLoader) {
    if (value != null) {
      return value;
    }
    if (valueStream == null) {
      return null;
    }
    try {
      ObjectInputStream stream = new ObjectInputStream(new ByteArrayInputStream(valueStream)) {
        @Override
        protected Class<?> resolveClass(final ObjectStreamClass desc) throws IOException, ClassNotFoundException {
          return Class.forName(desc.getName(), true, classLoader);
        }
      };

      //Class<?> clazz = Class.forName(ObjectInputStream.class.getCanonicalName(), true, classLoader);
      //ObjectInputStream stream = (ObjectInputStream) clazz.getConstructor(InputStream.class).newInstance(new ByteArrayInputStream(valueStream));
      Object retval = stream.readObject();
      stream.close();
      return value = retval;
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  public ScriptCommons setSerializedValue(final byte[] val) {
    this.valueStream = val;
    this.value = null;
    return this;
  }

  public byte[] getSerializedValue() {
    return valueStream;
  }

  public ScriptCommons setValue(final Object value) {
    ByteArrayOutputStream out = new ByteArrayOutputStream();
    try {
      new ObjectOutputStream(out).writeObject(value);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
    this.valueStream = out.toByteArray();
    this.value = value;
    return this;
  }

  @Override
  public String toString() {
    return getName();
  }

}

/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.storage.commons.impl;

import com.flamesgroup.antrax.automation.meta.ScriptInstance;
import com.flamesgroup.antrax.commons.Timeout;
import com.flamesgroup.antrax.commons.impl.InvariableTimeout;
import com.flamesgroup.antrax.storage.commons.SIMGroup;

public class SimNoGroup implements SIMGroup {

  private static final long serialVersionUID = 7481617761003823991L;

  @Override
  public long getID() {
    return 0;
  }

  @Override
  public long getRevision() {
    return -1;
  }

  @Override
  public void setRevision(final long revision) {
  }

  @Override
  public String getName() {
    return "NO GROUP";
  }

  @Override
  public String getOperatorSelection() {
    return "";
  }

  @Override
  public Timeout getIdleAfterRegistration() {
    return new InvariableTimeout(0, 0);
  }

  @Override
  public Timeout getIdleBeforeUnregistration() {
    return new InvariableTimeout(0, 0);
  }

  @Override
  public Timeout getIdleAfterSuccessfullCall() {
    return new InvariableTimeout(0, 0);
  }

  @Override
  public Timeout getIdleAfterZeroCall() {
    return new InvariableTimeout(0, 0);
  }

  @Override
  public int getSelfDialFactor() {
    return 0;
  }

  @Override
  public int getSelfDialChance() {
    return 0;
  }

  @Override
  public Timeout getIdleBeforeSelfCall() {
    return new InvariableTimeout(0, 0);
  }

  @Override
  public Timeout getIdleAfterSelfCallTimeout() {
    return new InvariableTimeout(0, 0);
  }

  @Override
  public ScriptInstance getActivityPeriodScript() {
    return null;
  }

  @Override
  public ScriptInstance[] getBusinessActivityScripts() {
    return new ScriptInstance[0];
  }

  @Override
  public ScriptInstance[] getActionProviderScripts() {
    return new ScriptInstance[0];
  }

  @Override
  public ScriptInstance[] getCallFilterScripts() {
    return new ScriptInstance[0];
  }

  @Override
  public ScriptInstance[] getSmsFilterScripts() {
    return null;
  }

  @Override
  public ScriptInstance getSessionScript() {
    return null;
  }

  @Override
  public ScriptInstance getGWSelectorScript() {
    return null;
  }

  @Override
  public ScriptInstance getFactorScript() {
    return null;
  }

  @Override
  public ScriptInstance getIncomingCallManagementScript() {
    return null;
  }

  @Override
  public ScriptInstance getVSFactorScript() {
    return null;
  }

  @Override
  public ScriptInstance getIMEIGeneratorScript() {
    return null;
  }

  @Override
  public boolean isSyntheticRinging() {
    return false;
  }

  @Override
  public SIMGroup setName(final String name) {
    return this;
  }

  @Override
  public SIMGroup setOperatorSelection(final String operatorSelection) {
    return this;
  }

  @Override
  public String getDescription() {
    return "";
  }

  @Override
  public boolean canBeAppointed() {
    return true;
  }

  @Override
  public SIMGroup setIdleAfterRegistration(final Timeout idleAfterRegistration) {
    return this;
  }

  @Override
  public SIMGroup setCanBeAppointed(final boolean appointed) {
    return this;
  }

  @Override
  public SIMGroup setSyntheticRinging(final boolean enabled) {
    return this;
  }

  @Override
  public SIMGroup setBusinessActivityScripts(final ScriptInstance[] businessActivityScripts) {
    return this;
  }

  @Override
  public SIMGroup setFactorScript(final ScriptInstance factorScript) {
    return this;
  }

  @Override
  public SIMGroup setGWSelectorScript(final ScriptInstance gwSelectorScript) {
    return this;
  }

  @Override
  public SIMGroup setSessionScript(final ScriptInstance sessionScript) {
    return this;
  }

  @Override
  public SIMGroup setActivityPeriodScript(final ScriptInstance activityPeriodScript) {
    return this;
  }

  @Override
  public SIMGroup setActionProviderScripts(final ScriptInstance[] actionProviderScripts) {
    return this;
  }

  @Override
  public SIMGroup setIncomingCallManagementScript(final ScriptInstance incomingCallManagementScript) {
    return this;
  }

  @Override
  public SIMGroup setVSFactorScript(final ScriptInstance vsFactorScript) {
    return this;
  }

  @Override
  public ScriptCommons[] listScriptCommons() {
    return new ScriptCommons[0];
  }

  @Override
  public SIMGroup setScriptCommons(final ScriptCommons[] scriptCommons) {
    return this;
  }

  @Override
  public SIMGroup setFASDetection(final boolean fasDetection) {
    return this;
  }

  @Override
  public boolean isFASDetection() {
    return false;
  }

  @Override
  public SIMGroup setSmsDeliveryReport(final boolean smsDeliveryReport) {
    return null;
  }

  @Override
  public boolean isSmsDeliveryReport() {
    return false;
  }

  @Override
  public boolean isSuppressIncomingCallSignal() {
    return false;
  }

  @Override
  public SIMGroup setSuppressIncomingCallSignal(final boolean extendedAT) {
    return this;
  }

  @Override
  public SIMGroup setCallFilterScripts(final ScriptInstance[] scriptInstances) {
    return this;
  }

  @Override
  public SIMGroup setSmsFilterScripts(final ScriptInstance[] scriptInstance) {
    return this;
  }

  @Override
  public SIMGroup setIMEIGeneratorScript(final ScriptInstance scriptInstance) {
    return this;
  }

  @Override
  public int getRegistrationPeriod() {
    return 0;
  }

  @Override
  public SIMGroup setRegistrationPeriod(final int registrationPeriod) {
    return this;
  }

  @Override
  public int getRegistrationCount() {
    return 0;
  }

  @Override
  public SIMGroup setRegistrationCount(final int registrationCount) {
    return this;
  }

  @Override
  public SIMGroup setIdleAfterSuccessfullCall(final Timeout idleAfterSuccessfullCall) {
    return this;
  }

  @Override
  public SIMGroup setIdleAfterZeroCall(final Timeout idleAfterZeroCall) {
    return this;
  }

  @Override
  public SIMGroup setIdleBeforeUnregistration(final Timeout idleBeforeUnregistration) {
    return this;
  }

  @Override
  public SIMGroup setIdleBeforeSelfCall(final Timeout idleBeforeSelfCall) {
    return this;
  }

  @Override
  public SIMGroup setIdleAfterSelfCallTimeout(final Timeout idleAfterSelfCallTimeout) {
    return this;
  }

  @Override
  public SIMGroup setSelfDialFactor(final int selfDialFactor) {
    return this;
  }

  @Override
  public SIMGroup setSelfDialChance(final int selfDialChance) {
    return this;
  }

  @Override
  public SIMGroup setDescription(final String description) {
    return this;
  }

  @Override
  public boolean equals(final Object object) {
    return this == object || object instanceof SimNoGroup;
  }

  @Override
  public int hashCode() {
    return (int) getID();
  }

}

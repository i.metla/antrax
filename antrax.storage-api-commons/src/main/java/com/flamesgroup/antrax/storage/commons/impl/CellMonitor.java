/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.storage.commons.impl;

import com.flamesgroup.commons.ChannelUID;

import java.util.Date;
import java.util.Objects;

public class CellMonitor extends Domain {

  private static final long serialVersionUID = -1353884526633949265L;

  private ChannelUID gsmChannel;
  private Short cell;
  private Integer bsic;
  private Integer lac;
  private Integer cellId;
  private Integer arfcn;
  private Integer rxLev;
  private Integer c1;
  private Integer c2;

  private Integer ta;
  private Short rxQual;
  private String plmn;

  private Date addTime;

  public CellMonitor() {
  }

  public CellMonitor(final long id) {
    this.id = id;
  }

  public ChannelUID getGsmChannel() {
    return gsmChannel;
  }

  public CellMonitor setGsmChannel(final ChannelUID gsmChannel) {
    this.gsmChannel = gsmChannel;
    return this;
  }

  public Short getCell() {
    return cell;
  }

  public CellMonitor setCell(final Short cell) {
    this.cell = cell;
    return this;
  }

  public Integer getBsic() {
    return bsic;
  }

  public CellMonitor setBsic(final Integer bsic) {
    this.bsic = bsic;
    return this;
  }

  public Integer getLac() {
    return lac;
  }

  public CellMonitor setLac(final Integer lac) {
    this.lac = lac;
    return this;
  }

  public Integer getCellId() {
    return cellId;
  }

  public CellMonitor setCellId(final Integer cellId) {
    this.cellId = cellId;
    return this;
  }

  public Integer getArfcn() {
    return arfcn;
  }

  public CellMonitor setArfcn(final Integer arfcn) {
    this.arfcn = arfcn;
    return this;
  }

  public Integer getRxLev() {
    return rxLev;
  }

  public CellMonitor setRxLev(final Integer rxLev) {
    this.rxLev = rxLev;
    return this;
  }

  public Integer getC1() {
    return c1;
  }

  public CellMonitor setC1(final Integer c1) {
    this.c1 = c1;
    return this;
  }

  public Integer getC2() {
    return c2;
  }

  public CellMonitor setC2(final Integer c2) {
    this.c2 = c2;
    return this;
  }

  public Integer getTa() {
    return ta;
  }

  public CellMonitor setTa(final Integer ta) {
    this.ta = ta;
    return this;
  }

  public Short getRxQual() {
    return rxQual;
  }

  public CellMonitor setRxQual(final Short rxQual) {
    this.rxQual = rxQual;
    return this;
  }

  public String getPlmn() {
    return plmn;
  }

  public CellMonitor setPlmn(final String plmn) {
    this.plmn = plmn;
    return this;
  }

  public Date getAddTime() {
    return addTime;
  }

  public CellMonitor setAddTime(final Date addTime) {
    this.addTime = addTime;
    return this;
  }

  @Override
  public boolean equals(final Object object) {
    if (this == object) {
      return true;
    }
    if (!(object instanceof CellMonitor)) {
      return false;
    }
    if (!super.equals(object)) {
      return false;
    }
    CellMonitor that = (CellMonitor) object;

    return Objects.equals(gsmChannel, that.gsmChannel)
        && Objects.equals(cell, that.cell)
        && Objects.equals(bsic, that.bsic)
        && Objects.equals(lac, that.lac)
        && Objects.equals(cellId, that.cellId)
        && Objects.equals(arfcn, that.arfcn)
        && Objects.equals(rxLev, that.rxLev)
        && Objects.equals(c1, that.c1)
        && Objects.equals(c2, that.c2)
        && Objects.equals(ta, that.ta)
        && Objects.equals(rxQual, that.rxQual)
        && Objects.equals(plmn, that.plmn)
        && Objects.equals(addTime, that.addTime);
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + Objects.hashCode(gsmChannel);
    result = prime * result + Objects.hashCode(cell);
    result = prime * result + Objects.hashCode(bsic);
    result = prime * result + Objects.hashCode(lac);
    result = prime * result + Objects.hashCode(cellId);
    result = prime * result + Objects.hashCode(arfcn);
    result = prime * result + Objects.hashCode(rxLev);
    result = prime * result + Objects.hashCode(c1);
    result = prime * result + Objects.hashCode(c2);
    result = prime * result + Objects.hashCode(ta);
    result = prime * result + Objects.hashCode(rxQual);
    result = prime * result + Objects.hashCode(plmn);
    result = prime * result + Objects.hashCode(addTime);
    return result;
  }

}

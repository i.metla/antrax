/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.automation.meta;

import com.flamesgroup.antrax.storage.commons.impl.Domain;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class ScriptDefinitionImpl extends Domain implements ScriptDefinition {

  private static final long serialVersionUID = -1830745921023713211L;

  private final JavaFileName fileName;
  private final String name;
  private final ScriptType type;
  private final ScriptParameterDefinition[] params;

  private final String description;

  public ScriptDefinitionImpl(final JavaFileName code, final String name, final String description, final ScriptType type, final ScriptParameterDefinition... params) {
    checkArgument(code, "code");
    checkArgument(name, "name");
    checkArgument(type, "type");
    this.fileName = code;
    this.name = name;
    this.type = type;
    this.description = description;
    if (params != null) {
      this.params = params;
      for (ScriptParameterDefinition scriptParameterDefinition : params) {
        if (scriptParameterDefinition != null) {
          scriptParameterDefinition.setScriptDefinition(this);
        }
      }
    } else {
      this.params = new ScriptParameterDefinition[0];
    }
  }

  private void checkArgument(final Object o, final String name) {
    if (o == null) {
      throw new IllegalArgumentException("Can't create script definition: " + name + " can't be null");
    }
  }

  @Override
  public JavaFileName getCode() {
    return fileName;
  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public ScriptParameterDefinition[] getParameters() {
    return params;
  }

  @Override
  public ScriptType getType() {
    return type;
  }

  @Override
  public String getDescription() {
    return description;
  }

  @Override
  public ScriptInstance createInstance() {
    return new ScriptInstanceImpl(this);
  }

  @Override
  public String toString() {
    return String.format("%s %s(%s)", getType(), getName(), Arrays.toString(getParameters()));
  }

  @Override
  public boolean differs(final ScriptDefinition that) {
    Map<String, ScriptParameterDefinition> aa = collect(this.getParameters());
    Map<String, ScriptParameterDefinition> bb = collect(that.getParameters());

    if (!aa.keySet().equals(bb.keySet())) {
      return true;
    }

    for (String key : aa.keySet()) {
      ScriptParameterDefinition pa = aa.get(key);
      ScriptParameterDefinition pb = bb.get(key);

      if (!(pa.getType().equals(pb.getType()) && pa.isArray() == pb.isArray())) {
        return true;
      }
    }

    return false;
  }

  private Map<String, ScriptParameterDefinition> collect(final ScriptParameterDefinition[] definitions) {
    HashMap<String, ScriptParameterDefinition> retval = new HashMap<>();
    for (ScriptParameterDefinition def : definitions) {
      retval.put(def.getName(), def);
    }
    return retval;
  }

  @Override
  public int compareTo(final ScriptDefinition that) {
    return this.getName().compareToIgnoreCase(that.getName());
  }

}

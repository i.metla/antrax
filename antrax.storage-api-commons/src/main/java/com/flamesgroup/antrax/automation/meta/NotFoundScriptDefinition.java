/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.automation.meta;

import java.io.Serializable;
import java.util.Objects;

public class NotFoundScriptDefinition implements ScriptDefinition, Serializable {

  private static final long serialVersionUID = -1233330138676805090L;

  private final ScriptDefinition scriptDefinition;

  public NotFoundScriptDefinition(final ScriptDefinition scriptDefinition) {
    Objects.requireNonNull(scriptDefinition, "scriptDefinition mustn't be null");
    this.scriptDefinition = scriptDefinition;
  }

  @Override
  public long getID() {
    return scriptDefinition.getID();
  }

  @Override
  public String getName() {
    return scriptDefinition.getName();
  }

  @Override
  public String getDescription() {
    return scriptDefinition.getDescription();
  }

  @Override
  public ScriptType getType() {
    return scriptDefinition.getType();
  }

  @Override
  public JavaFileName getCode() {
    return scriptDefinition.getCode();
  }

  @Override
  public ScriptParameterDefinition[] getParameters() {
    return scriptDefinition.getParameters();
  }

  @Override
  public ScriptInstance createInstance() {
    return new ScriptInstanceImpl(this);
  }

  @Override
  public boolean differs(final ScriptDefinition other) {
    return scriptDefinition.differs(other);
  }

  @Override
  public int compareTo(final ScriptDefinition o) {
    return scriptDefinition.compareTo(o);
  }

}

/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.automation.meta;

import com.flamesgroup.antrax.storage.commons.impl.Domain;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectStreamClass;

public class ScriptParameterImpl extends Domain implements ScriptParameter {

  private static final long serialVersionUID = -3697047812973449129L;

  private final ScriptParameterDefinition definition;
  private byte[] valueStream;
  private transient Object value;
  private boolean hasValue = false;
  private final ScriptInstance scriptInstance;

  public ScriptParameterImpl(final ScriptParameterDefinition definition, final ScriptInstance scriptInstance, final byte[] defaultValue) {
    checkArgument(definition, "definition");
    checkArgument(scriptInstance, "scriptInstance");
    this.definition = definition;
    this.scriptInstance = scriptInstance;
    this.valueStream = defaultValue;
  }

  private void checkArgument(final Object o, final String name) {
    if (o == null) {
      throw new IllegalArgumentException("Can't create script parameter: " + name + " can't be null");
    }
  }

  @Override
  public ScriptParameterDefinition getDefinition() {
    return definition;
  }

  @Override
  public byte[] getSerializedValue() {
    return valueStream;
  }

  @Override
  public boolean hasValue() {
    return hasValue;
  }

  @Override
  public String toString() {
    return definition.getName();
  }

  @Override
  public ScriptInstance getInstance() {
    return scriptInstance;
  }

  @Override
  public Object getValue(final ClassLoader classLoader) {
    if (value != null) {
      return value;
    }
    if (valueStream == null) {
      return null;
    }
    try {
      ObjectInputStream stream = new ObjectInputStream(new ByteArrayInputStream(valueStream)) {
        @Override
        protected Class<?> resolveClass(final ObjectStreamClass desc) throws IOException, ClassNotFoundException {
          return Class.forName(desc.getName(), true, classLoader);
        }
      };

      //Class<?> clazz = Class.forName(ObjectInputStream.class.getCanonicalName(), true, classLoader);
      //ObjectInputStream stream = (ObjectInputStream) clazz.getConstructor(InputStream.class).newInstance(new ByteArrayInputStream(valueStream));
      Object retval = stream.readObject();
      stream.close();
      return value = retval;
    } catch (Exception e) {
      LoggerFactory.getLogger(ScriptParameterImpl.class).warn("failed to deserialized parameter value", e);
    }
    return null;
  }

  @Override
  public void setSerializedValue(final byte[] val) {
    this.valueStream = val;
    this.hasValue = true;
    this.value = null;
  }

  @Override
  public void setValue(final Object val, final ClassLoader classLoader) {
    getDefinition().checkAssign(val, classLoader);
    ByteArrayOutputStream out = new ByteArrayOutputStream();
    try {
      new ObjectOutputStream(out).writeObject(val);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
    this.valueStream = out.toByteArray();
    this.value = val;
    this.hasValue = true;
  }

}

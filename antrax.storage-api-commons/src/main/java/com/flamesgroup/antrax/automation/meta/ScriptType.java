/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.automation.meta;

import com.flamesgroup.antrax.storage.commons.SIMGroup;

public enum ScriptType {

  BUSINESS_ACTIVITY,
  ACTION_PROVIDER,
  FACTOR,
  ACTIVITY_PERIOD,
  GATEWAY_SELECTOR,
  INCOMING_CALL_MANAGEMENT,
  CALL_FILTER,
  SMS_FILTER,
  IMEI_GENERATOR;

  public static ScriptType strToSimState(final String state) {
    return (state == null) ? null : ScriptType.valueOf(state);
  }

  public static ScriptField getCorrespondScriptField(final ScriptInstance scriptInstance, final SIMGroup simGroup) {
    ScriptType scriptType = scriptInstance.getDefinition().getType();
    switch (scriptType) {
      case ACTION_PROVIDER:
        return ScriptField.VS_ACTION_PROVIDER;

      case BUSINESS_ACTIVITY:
        return ScriptField.VS_BUSINESS;

      case GATEWAY_SELECTOR:
        return ScriptField.SS_GW_SELECTOR;

      case INCOMING_CALL_MANAGEMENT:
        return ScriptField.VS_INCOMING_CALL_MANAGER;

      case ACTIVITY_PERIOD:
        if (scriptInstance.equals(simGroup.getActivityPeriodScript())) {
          return ScriptField.SS_ACTIVITY;
        } else if (scriptInstance.equals(simGroup.getSessionScript())) {
          return ScriptField.SS_SESSION;
        }
        break;

      case FACTOR:
        if (scriptInstance.equals(simGroup.getVSFactorScript())) {
          return ScriptField.VS_FACTOR;
        } else if (scriptInstance.equals(simGroup.getFactorScript())) {
          return ScriptField.SS_FACTOR;
        }
        break;
      case IMEI_GENERATOR:
        return ScriptField.SS_IMEI_GENERATOR;
    }

    String msg = "Correspond script field not found for script - type: " + String.valueOf(scriptType) + ", instance: " + String.valueOf(scriptInstance);
    throw new IllegalArgumentException(msg);
  }

}

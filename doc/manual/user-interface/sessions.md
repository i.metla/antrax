## SESSIONS ##

The history of user's connections to Antrax system is displayed in SESSIONS section.

![session](sessions.assets/session.png)

### Description of column headings ###

| Heading | Description|
| -------- | -------- |
| IP | IP address connected to session|
| User | user's name |
| Group | user's group |
| Active | current activity |
| Start Time | session start time |
| Last Accessed | start time of the last activity |
| End Time | session end time |

 


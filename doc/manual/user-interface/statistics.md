## STATISTICS ##

The STATISTICS section contains the information about Voice servers and VoIP AntiSpam activities.

### Voice Servers ###

![1.13_statistic](statistics.assets/statistic.png)

Below you will find description of columns.

| Heading | Description|
| -------- | -------- |
| Voice Servers | name of Voice server GSM channel belongs to |
| Mobile chan. | number of GSM channel |
| ASR | A percentage ratio of answered calls and overall quantity of attempst to make a call |
| ACD | avarage duration of calls |
| Success calls | quantity of successful calls|
| Total calls | overall quantity of calls |
| Calls dur. | overall duration of conversations |
| Success SMS | count of successful SMS |
| Total SMS | overall quantity of SMS |
| SMS ASR | the ratio of successful SMS to the total number of sending attempts|
| Inc. Total SMS | overall quantity of incoming SMS |

### VoIP AntiSpam ###

#### VoIP AntiSpam activity graph ####

![antispam_table](statistics.assets/antispam_table.png)

 **Routing request** graph displays the statistics of incoming calls quantity for a period of 24 hours.
 **Block number** graph displays the statistics of number blocks for 24 hours. 

####  White list ####

Numbers of white list do not go through filtration.

![antispam_white_list](statistics.assets/antispam_white_list.png)

Only those numbers enter the trust-based number table which call duration is more than assigned (1 minute by default) for particular time interval (10 minutes by default) with a specified quantity (2 calls by default).That is, if 2 calls with duration of more than 1 minute were conducted for the time period of 10 minutes, that particular number goes directly to white list. There is a possibility to add numbers in white list manually. Filtration parameters are set in file, which you can find in project resources.

| Button names | Button functions |
| -------- | -------- |
| Clear | clear up the number table  | 
| Add| add numbers to white list |
| Bulk del | mass removal of numbers. Numbers for removal are inserted in dialogue window. Any non-numerical symbol is used as a delimiter.  |
| Selected Del | removal of selected numbers in the table  |
| Import from file | imports the data from the file. Any non-numerical symbol is used as a delimiter. |
| Export To file | records the data from the table to file with extension .csv |

| Name of fields | Field description |
| -------- | -------- |
| number | number in the white list |
| status | **manual** -  number is added manually<br>**auto** - number is added automatically, after analysis |
| add time | time of number adding |
| routing request count | quantity of requests for routing after the number entered the white list |

#### Grey list ####

Numbers of grey list go through active filtration.
Numbers enter the grey list based on filtration algorithms of routing requests.
If the calls with a small duration have exceeded the maximal quantity of permitted ones for particalar time period, this particular number goes to grey list.
After the blocking time expiry, a number is no more regarded as blocked, but remains under further observation. Filtration parameters are set on the Configuration - VoIP AntiSpam tab.

![antispam_grey_list](statistics.assets/antispam_grey_list.png)

| Name of button | Button function |
| -------- | -------- |
| Clear | clear the number table | 
| Bulk del | mass removal of numbers. Numbers for removal are inserted in dialogue window. Any non-numerical symbol is used as a delimiter. |
| Selected Del | removal of selected numbers in the table  |filtartion
| Selected to black list | import of numbers to the black list |

| Name of the fields | Field description |
| -------- | -------- |
| number | number which is in the grey list |
| status| number status in the list |
| block time | the start time of the last blocking |
| block count | overall quantity of blockings  |
| block time left | tome of unblocking |
| routing request count | quantity of routing requests after number entered the grey list |

#### Black list ###

Numbers of black list are blocked.
Numbers enter the black list based on filtration algorithm of routing requests.
If the quantity of blocks in the grey list based on filtration algorithm of call duration exceeded the maximally available value, the specified number goes from grey list directly to black list .
Despite algorithms there is a possibility to add numbers to the given table manually. Numbers from this list are always regarded as blocked. Filtration parameters are set in Configuration - VoIP AntiSpam tab.

![anitspam_black_list](statistics.assets/anitspam_black_list.png)

| Name of buttons | Button functions |
| -------- | -------- |
| Clear | clear the number table | 
| Add| add the numbers to black list |
| Bulk del | mass removal of numbers. Numbers for removal are inserted in dialogue window. Any non-numerical symbol is used as a delimiter. |
| Selected Del | removal of selected numbers in the table  |
| Import from file | imports the data from file.Any non-numerical symbol is used as a delimiter. |
| Export To file | records data from the table to file with extension .csv |

| Name of fields | Field description |
| -------- | -------- |
| number | number in the white list |
| status | **manual** -  number is added manually<br>**auto** - number is added automatically, after analysis |
| add time | time of number adding |
| routing request count | quantity of requests for routing after the number entered the black list |

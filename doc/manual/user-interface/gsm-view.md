## GSM-view ##

Monitoring of GSM-covering state and possibility of running LAC, CELL, BSIC, ARFCN.

**The realized functionality allows:**

  * To perform the review of GSM network - Network survey
  * To perform the definition Imsi-catcher.
  * To assign the quartet (LAC, CELL, BSIC, ARFCN) where Cell equalizer can be registered.

**LAC, CELL, BSIC, ARFCN are shaped on the basis of network overview(Network survey).**

### The algorithm of channel distribution among cells ###

There is a possibility to choose the algorithm of channel distribution among cells: Basic and Random.

**Description of buttons and filters:**

| Name | Description |
| -------- | -------- |
| Selected serving | the function for marking/marker removal in Serving column |
| Selected trusted | the function gor marking/marker removal in Trusted column |
| **Clear** | full clean-up of the table with quartets|
| **Export to excel** | export of the table to the excel file|
| **Reset statistic** | statistics reset on quartet|
| ![nr_vs_filter](gsm-view.assets/nr_vs_filter.jpg) | searching space |


####  Basic ####

**"Basic"** - The standard algorithm, the cells marked as "serving" will have the module registered on it, the cells which are not marked as "serving" won’t go through registration.

![gsm-view_basic](gsm-view.assets/gsm-view_basic.png)

####  Random ####

**"Random"** - The algorithm of random distribution.

1. After module registration in GSM network the basic set-up of operation with cells is carried out (The cells which will have the module (serving) registered on it are chosen.
2. The "Random" algorithm starts to operate after the module received full information about GSM network(1-2 minutes).
3. Checks if it is able to perform the migration ( if the time installed in the settings has passed since the last migration).
4. The algorithm receives 7 cells that appear to be the closest ones, after that it discards the cells which have CellId <= 0, lac <= 0.
5. Checks whether the processing of the previous migration is carried out,  if "no" it keeps processing, if "yes" it completes the previous migration.
6. Performs the generating of the next launch of algorithm. It is a random number from the interval specified in GUI("Period of regenerated cell equalizer").
7. Defines the current level (RxLev) of registered cell. Adds "10" to the current level - it will be the required level of signal to manually appreciate the cell.
8. The cell is chosen to appreciate. It is a random cell from 7 already chosen from the system.
9. If the cell is chosen which has a module registered on it the algorithm waits for the next selection of cell, if not, it keeps operating.
10. If it is the first algorithm passage the appreciation of the signal level only is carried out,otherwise,depreciation of the cell from the previous algorithm is carried out additionally.
11. Appreciation and depreciation of signal levels is carried out gradually on the random number from 1 to 10 within the waiting time between interactions ("Waiting time between iterations"), appreciation is carried out until the level of the chosen cell becomes 10 points as high as the current one, depreciation is carried out until the cell takes its physical signal level.
12. After reaching the signal levels the algorithm is in sleep mode till the time set up in the paragraph 6.

![gsm-view_random](gsm-view.assets/gsm-view_random.png)

#### Statistic ####

The mechanism of random migration among cells:
1. After module registration in GSM network the basic set-up of operation with cells is carried out (The cells which will have the module (serving) registered on it are chosen.
2. The "Random" algorithm starts to operate after the module received full information about GSM network(1-2 minutes).
3. Checks if it is able to perform the migration ( if the time installed in the settings has passed since the last migration).
4. The algorithm receives 7 cells that appear to be the closest ones, after that it discards the cells which have CellId <= 0, lac <= 0.
5. Checks whether the processing of the previous migration is carried out,  if "no" it keeps processing, if "yes" it completes the previous migration.
6. Executes the algorithm for generating the next run after one of the limits: Successful call count - in this interval, a random number of successful calls will be selected; USSD calculate c - in this interval, a random number of USSD messages will be selected; Outgoing number of SMS-messages - in this interval a random number of SMS-messages will be sent.
7. Defines the current level (RxLev) of registered cell. Adds "10" to the current level - it will be the required level of signal to manually appreciate the cell.
8. The cell is chosen to appreciate. It is a random cell from 7 already chosen from the system.
9. If the cell is chosen which has a module registered on it the algorithm waits for the next selection of cell, if not, it keeps operating.
10. If it is the first algorithm passage the appreciation of the signal level only is carried out,otherwise,depreciation of the cell from the previous algorithm is carried out additionally.
11. Appreciation and depreciation of signal levels is carried out gradually on the random number from 1 to 10 within the waiting time between interactions ("Waiting time between iterations"), appreciation is carried out until the level of the chosen cell becomes 10 points as high as the current one, depreciation is carried out until the cell takes its physical signal level.
12. After reaching the signal levels the algorithm is in sleep mode till the time set up in the paragraph 6.

![gsm-view_staticsic](gsm-view.assets/gsm-view_staticsic.png)

### The description of columns in the table ###

| Name | Description|
| -------- | -------- |
| Serving | means the permission for being registered in the specified quartet, established in false be default|
| Trusted | means the trusted quartet, established in false by default. If the definition in false and module sees it while performing MONI, Imsi-catcher rules will immediately operate|
| LAC | code of local zone. Local zone is a set of BS, which are served by one BSC — base station controller.|
| Cell | cell identifier|
| BSIC | code used in GSM for unique identification of base station.|
| ARFCN | the absolute channel number represents a couple of frequencies, used by cell connection operators for data reception and transmission.|
| Last RxLev | The last real signal level received from network overview|
| Operator | means the operator's name which this quartet belongs to|
| Number occurrences | the number of quartet occurrences in GSM network|
| Successful Calls Count| the amount of successful calls for the quartet|
| Totall Calls Count| overall amount of calls for the quartet|
| Acerage PDD| the average value of PDD |
| Calls Duration| the overall value of call duration for the quartet |
| ASR | ASR value for the quartet |
| ACD | ACD value for the quartet |
| Outgoing Sms Count | overall amount of outgoing SMS for the quartet |
| Incoming Sms Count | overall amount of incoming SMS for the quartet |
| Ussd Count | overall amount of USSD for the quartet |
| First appearance | the time of the first quartet occurrence|
| Last appearance | the time of the last quartet occurrence|
| Notes | The record spaces for the quartet |

**Icons' colour in GSM-view:**

**Green** - if the latest network overview was performed no later than a month ago

**Yellow** - if the latest overview was performed more than a month ago

**Red** - if the network overview has never been performed.

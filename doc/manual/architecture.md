## System architecture ##

The picture below shows ANTRAX software-based hardware system. GSM and SIM modules are hardware-based modules. All of the rest components are software-based.

![ANTRAX architecture](architecture.assets/antrax_architecture.png)

**Control Server** is a main software-based system module. The main task of this module is initialization of the other system modules, further interaction with them, and also service of remote GUI Client modules. Therefore, Control Server must be launched before other modules. 

**GUI** (Graphical User Interface) is a user's application for remote monitoring and system control. Several **GUI** can be connected to the system . You can find the detailed description of the specified module in the section **User interface**.

**SIM** and **GSM** are hardware-based modules which service SIM cards (SIM channels) and GSM channels. These modules interact with software-based modules of the system by means of stacking UDP network protocols based on Ethernet technology. The main parameter for SIM and GSM modules configuration is module’s IP address as well as Master IP. Master IP for GSM module is the address of the Voice server which turns to be the master of this module. The module will accept network packets only from its master.
Master IP for SIM module stands for the SIM server address.

**Voice Server** accepts calls directly from VoIP and terminates them into GSM. Voice server interacts with GSM channels the master of which is turns to be. One of the tasks of the Voice Server is to connect GSM channels with SIM channels, thus creating the voice channel. By means of voice channels calls are transmitted from VoIP into GSM network along with other activities as SMS sending, balance top-up, services order through IVR operator’s menu etc. The system may have several Voice servers.

GSM channels are combined into GSM groups while SIM channels - into SIM groups.

**GSM group** - is a complecx of SIM channels, which have common features . Each GSM group can contain GSM channels, which can be found on different  GSM modules and served by different Voice servers. Each GSM group has a unique name. The only feature of GSM group is connection with one or more SIM groups.

**SIM group** - is a complex of SIM channels, characterized by common features. Similarly to GSM groups, one SIM group can contain SIM cards, located on different SIM modules. Each  SIM group has a unique name. Unlike GSM group, SIM group is characterized by variety of features and settings, which establish the behavioral pattern of SIM channels. You can find detailed information about SIM group settings in chapter **SIM group setting**.

**The connection between GSM group and SIM group** means that SIM cards within one SIM group can be registered only on those GSM channels which fall into one GSM group. Each GSM group can be connected with several SIM groups and vice versa - one SIM group can be connected with several GSM groups.

**SIM Server** manages SIM channels. Unlike Voice Server, there can only be one SIM Server in the system. Consequently it appears to be the master for all SIM modules in the system. To create a voice channel Voice server requests a SIM channel from the SIM Server. The choice of an appropriate SIM channel is described in details in the chapter ‘SIM channel selection to connect with GSM channel’.

**DB** (Database) - is a complex of data, arranged in accordance with conceptual structure, which describes characteristic points of this data and its relationship, and at the same time provides a support for one or more fields of implementation (based on PostgreSQL).

**Softswitch** - a central device in a telecommunications network which connects telephone calls from one phone line to another, across a telecommunication network or the public Internet, entirely by means of software running on a general-purpose computer system (based on Yate).

### Flow of call ###

![flow_of_call](architecture.assets/flow_of_call.jpg)

**Stages of call passing:**

1. The call comes on Yate (SoftSwitch)
2. Using messaging protocol sends a message to Yate call.route in which Control Server specify the route for the call
3. Control Server answers call.routewhich indicates the route for the call. At this stage the call filtering by Antispam
4. Yate (SoftSwitch) orwards the call to the Voice Server uses the IAX protocol
5. Completed call

### SIM channel selection for connection with GSM channel ###

One of the tasks for Voice server is a formation of voice channels. Voice channel is created by means of SIM and GSM channels' connection following the registration in GSM network. Created voice channels are ready to process calls and perform other activities (send SMS, top up the balance, etc). Voice channels are arranged into the list according to the Voice Server Factor Script.

Formation of voice channels lasts continuously and doesn't depend on presence or absence of calls. Task of Voice server is a connection between all free GSM channels and SIM channels. Voice server requests SIM card from SIM server continuously.

Destruction of voice channel occurs according to the SIM groups setting (SIM-card is located in this SIM-group and active in current voice channel). Voice server also requests  SIM-server information about time for voice channel destruction.

First channel from the list of arranged voice channels will be active on new call. In case of missing free voice channels, Voice server will reject call.

![sim_getting](architecture.assets/sim_getting.jpg)

On picture  you can see process of SIM channels and GSM channels' selection for link creation. This Voice server contains several GSM channels ( Agsm and Bgsm groups). GSM groups are connected with SIM groups in such way:  Agsm <>  Asim, Bgsm <> Bsim.

![gsm_group](architecture.assets/gsm_group.png)

Lets consider the process of GSM channels (Agsm group) connection with suitable SIM channels. Agsm group connects with Asim group, thus SIM channel must belong to the Asim group. As you can see on the picture, Voice server hasn't got any SIM channels with Asim group. That's why Voice server requests SIM channels from SIM server. SIM server chooses all GSM channels from Asim group and arranges them by SIM Server Factor Script. First SIM channel will be taken from this arranged list. Session Period Script checks if selected SIM channel can go to the Voice server at the moment. In case of positive result it is checked if selected SIM channel can go to the current Voice server (Gateway Selector Script). SIM channel is transferred into Voice server if both conditions are met. In case of failure of at least one condition (Session Period Script or Gateway Selector Script), next SIM channel will be chosen from the arranged list and the same checking will take place.

Then system checks if SIM channel can register in GSM network at the moment (Activity Period Script). In case of positive result GSM and SIM channels connect with each other and create new voice channel, which is placed into the list of voice channels on Voice server. If received SIM channel can't register in GSM network at the moment, then Voice server requests SIM server for one SIM channel one more time.

On picture below you can see block-scheme for choosing algorithm of SIM channels.

![block_diagramm](architecture.assets/block_diagramm.jpg)

It is worth mentioning, that SIM channels stay on Voice server while all conditions of Session Period Script are being performed. Unless conditions are met, voice channel is destroyed, and SIM channel returns back to SIM server.

 


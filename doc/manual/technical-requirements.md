## Technical requirements ##
### Internet ###
Required connection for steady activity: 50kbps for one GSM channel
### Power supply ###
Equipment power consumption 250w for sub rack with 15 GSM-boards and 24w for BOX device, power supply network 100-240В.
### Protocols and codecs ###
Protocols being used: SIP, IAX2.
Codecs being used: G729.
### Server's ###

### Advisable PC requirements for CS(CS, YATE, DB) for 15 voice/sms channels ###

* CPU: Intel/AMD 4 core CPU by ~3Ghz 64х;
* RAM: 4 Gb;
* HDD: 500 Gb;
* LAN boards: 1;
* OS: CentOS 6.7 x86_64, Debian 8 x64.

### Advisable PC requirements for CS(CS, YATE, DB) for 32 voice/sms channels ###

* CPU: Intel/AMD 8 core CPU by ~3Ghz 64х;
* RAM: 8 Gb;
* HDD: 1 Tb;
* LAN boards: 1;
* OS: CentOS 6.7 x86_64, Debian 8 x64.

### Advisable PC requirements for VS with connected sub rack with 15 GSM-boards ###

* CPU: Intel/AMD 4 core CPU by ~3Ghz 64х;
* RAM: 4 Gb;
* HDD: 120 Gb;
* LAN boards: 2(One - for statiс IP, another - for sub rack connection);
* OS: CentOS 6.7 x86_64, Debian 8 x64.

### Advisable PC requirements for SS with connected sub rack with 15 SIM-boards ###

* CPU: Intel/AMD 4 core CPU by ~3Ghz 64х;
* RAM: 4 Gb;
* HDD: 120 Gb;
* LAN boards: 2(One - for statiс IP, another - for Box connection);
* OS: CentOS 6.7 x86_64, Debian 8 x64.

## SIM box(SB60, SB120) ##
**SIM box** is a hardware component of Antrax system.
**SIM box** performs the role of switchboard component.
**SIM box** is manufactured in two types: 
  * **SB60 - SIM box** operates with **60 SIM cards**;

![sb60_intro_3](sim-box.assets/sb60_intro_3.jpg)

![sb60_intro_4](sim-box.assets/sb60_intro_4.jpg)

  * **SB120 - SIM box** operates with **120 SIM cards**.

![sb60_intro_3](sim-box.assets/sb120_intro_3.jpg)

![sb60_intro_4](sim-box.assets/sb120_intro_4.jpg)

### SB60 ###
**SB60 basic characteristics:**
Independent management of **60 SIM cards** ISO7816 standard:
  * Supply voltage support of cards 1,8V and 3,3V;
  * Possibility of SIM cards hot swap.
**Characteristics of control unit:**
__CPU__: Allwinner A20 dual core Cortex-A7 processor, each core with the frequency of 1GHz;
__RAM__: 1GB DDR3;
__ROM__: 8GB ssd;
__OS__: Debian 8.X armel7.
**Overall dimension SB60 in millimeters: 305х175х52**

![size_sb60](sim-box.assets/size_sb60.jpg)

**Weight SB60 box (net):**
SIMBOX60 - 1625g
power supply with wires - 460g

### SB120 ###
**SB120 basic characteristics:**
Independent management **120 SIM cards** ISO7816 standard:
  * Supply voltage support of cards 1,8V and 3,3V;
  * Possibility of SIM cards hot swap.
**Characteristics of control unit:**
__CPU__: Allwinner A20 dual core Cortex-A7 processor, each core with the frequency of 1GHz;
__RAM__: 1GB DDR3;
__ROM__: 8GB ssd;
__OS__: Debian 8.X armel7.
**Overall dimension SB120 in millimetres: 305х175х52**

![size_sb120](sim-box.assets/size_sb120.jpg)

**Weight SB120 box (net):**
SIMBOX120 - 2000g
power supply with wires - 460g

### SIM box indication ###
SIM box indication: **SB60** and **SB120**.

| State of SIM channel | Green LED | Red LED |
| -------- | -------- | -------- |
| card is missing | is not lit | is not lit |
| card is initialized | rapid blinking| is not lit |
| card is ready to operate | slow blinking| is not lit |
| card is used| solid glow | is not lit |
| card is de-initialized | is not lit| is not lit |
| card is blocked| is not lit | solid glow |
| software error occurred during the exchange with card | is not lit | rapid blinking |
| hardware error occurred during the exchange with card | is not lit | rapid blinking |

## IMEI Generator Script ##
There is a possibility to assign each SIM card with particular IMEI. To automatize this task IMEI Generator Script, which particular SIM group is assigned with is used.

There are such kinds of scripts:
* Random IMEI
* IMEI based on TAC and FAC
* Registry IMEI

### 1. Random IMEI ###

Generating IMEI randomly based on the pattern.

![random_imei](imei_gen_script.assets/random_imei.png)

| Parameter | Description |
| -------- | -------- |
| IMEI generetor rule | Via this option has possibility to set rule when IMEI will be generating next time |
| Pattern | Pattern set-up for IMEI random generating. Can use digit or symbol 'X'. X - means in which position will generate a random value from 0 to 9 |

### 2. IMEI generation based on TAC and FAC lists ###

Generating IMEI using TAC, FAC from registry and SNR generating randomly according to pattern. After get first value TACFAC from registry, script move this value to end of this list

About TAC, FAC, SNR can read at [WIKI](https://en.wikipedia.org/wiki/International_Mobile_Equipment_Identity#Structure_of_the_IMEI_and_IMEISV_.28IMEI_software_version.29)

![imei_based_on_tac_fac.png](imei_gen_script.assets/imei_based_on_tac_fac.png)

| Parameter | Description |
| -------- | -------- |
| path | path to parameter, which contains a list of TACFAC values in registry |
| snrPattern | uniquely identifying a unit of this model, generate it randomly. Can use digit or symbol 'X'. X - means in which position will generate a random value from 0 to 9 |
| IMEI generetor rule | Via this option has possibility to set rule when IMEI will be generating next time |


### 3. Registry IMEI ###

Set IMEI from the registry to SIM card. Script get first value from the list, then check it on contains only digits. 
If this value has non-numeric characters, then this IMEI move to list with ".bad" suffix. 
If has only digits then it moves to path with the suffix ".used" and set IMEI to sim card.

![imei_based_on_tac_fac.png](imei_gen_script.assets/imei_registry_script.png)

| Parameter | Description |
| -------- | -------- |
| path | path to parameter, which contains a list of IMEI values in registry |
| IMEI generetor rule | Via this option has possibility to set rule when IMEI will be generating next time |


#### Base parameters for this scripts ####

##### IMEI generator rule #####

There are several rules to change IMEI on SIM card.

![imei_generator_rule_select](imei_gen_script.assets/imei_generator_rule_select.png)

| Parameter | Description |
| -------- | -------- |
| none | not to change IMEI for SIM card (generate  IMEI once for SIM card) |
| period | period of IMEI change on SIM card |
| registration count | quantity of registrations to change IMEI on SIM card |

![reg_count](imei_gen_script.assets/reg_count.png)

There is a possibility to change IMEI on SIM card according to period.

![period_generation](imei_gen_script.assets/period_generation.png)

| Parameter | Description |
| -------- | -------- |
| HOUR | IMEI generating every hour |
| DAY | IMEI generating every day |
| WEEK | IMEI generating every week |
| MONTH | IMEI generating every month |

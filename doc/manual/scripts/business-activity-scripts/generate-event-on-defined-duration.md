## generate event on defined duration ##

Given script is designed for any event performance when particular active call duration is reached by the card.

![define_duration](generate-event-on-defined-duration.assets/define_duration.png)

### Parameters ###

| name| description|
| -------- | -------- |
|  **event** | event, which must be launched when the card reached the particular active call duration  |
|  **duration bounds** | active call duration limits |
|  **use outgoing calls** | given checkmark is used to calculate the duration of outgoing calls from the card |
|  **use incoming calls** | given checkmark is used to calculate the duration of incoming calls to the card |

**If both checkmarks are switched on, the calculation will be carried out for both incoming and outgoing duration.**

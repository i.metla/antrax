## generate event after timeout passing ##

Given script is designed for the cyclic performance of any action after a definite period.

![g_e_after_timeout_passing](generate-event-after-timeout-passing.assets/g_e_after_timeout_passing.png)

### Parameters ###

| name| description|
| -------- | -------- |
|  **lock on failure  ** | block the card on unsuccessful script performance |
|  **send event** | event, which is performed after **timeout** is over |
|  **timeout** | time after which  **send event** is performed  |



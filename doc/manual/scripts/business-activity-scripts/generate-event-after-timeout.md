## generate event after timeout ##

Given script is designed to perform any action after a definite period.

![g_e_after_timeout](generate-event-after-timeout.assets/g_e_after_timeout.png)

### Parameters ###

| name| description|
| -------- | -------- |
|  **lock on failure  ** | block the card on unsuccessful script performance |
|  **send event** | event is generated after **delayTimeout** is over |
|  **receive event** | event, which activates the given script  |
|  **greedy script** | "freeze" the card for  **delayTimeout** period|
|  **delayTimeout** | time after which **send event** is performed |


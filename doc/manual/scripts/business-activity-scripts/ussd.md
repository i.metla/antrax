## USSD ##

It sends USSD request, processes USSD response.

**USSD** (Unstructured Supplementary Service Data) — is a standard service in GSM networks, which enables to arrange interactive cooperation with service applications in frames of short message sending.

USSD service has a lot in common with SMS, both of them are used for data transmission in frames of short messages. However USSD is basically designed for  message exchange between the subscriber and other services, or, in a nutshell -  answerphone service of balance, whereas SMS basically serves for short message exchange between  subscribers.

USSD, in contrast to  SMS, has no intermediate database and does not ensure repeated message delivery, that makes message exchange immediate. USSD is a session-oriented technology, the whole dialogue of subscriber and application is carried out in frames of one session. The system on IVR(Interactive Voice Response) basis stands for analogue of USSD-service, in spite of which USSD lacks voice connections and uses just a signal channel. Data exchange in USSD-session is more efficient and demonstrative.

The basic direction of USSD-service utilization is an opportunity to get additional information from apps and manage these apps.

![ussd](ussd.assets/ussd.png)

### Parameters ###

| name | description|
| -------- | -------- |
|  USSD response pattern | regular expression to check USSD response |
|  event | event essential to activate the script |
|  attempts count | quantity of attempts in case of failure |
|  USSD request | number of request |
|  event on fail | event on failure |
|  event on true | event on successful performance |

 


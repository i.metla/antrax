## generate event on incoming call ##

Given script is designed for any action performance on every incoming call.

![g_e_on_incoming_call](generate-event-on-incoming-call.assets/g_e_on_incoming_call.png)

### Parameters ###

| name| description|
| -------- | -------- |
|  **event** | event which requires launching on incoming call |
|  **duration bounds** | active call duration limits performed by the card |
|  **require answer** | generates **event** after successful call, which duration corresponds to **duration bounds** |

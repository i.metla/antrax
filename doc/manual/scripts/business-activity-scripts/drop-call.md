## drop call ##

Given script drops the inbound/outbound call on event 

![drop_call](drop-call.assets/drop_call.png)

### Parameters ###

| name| description|
| -------- | -------- |
|  event | initial event essential to activate the script |


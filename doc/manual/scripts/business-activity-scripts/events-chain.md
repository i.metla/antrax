## events chain ##

Chain of events. Designed for sequential generation of one or several events.

![eventschain](events-chain.assets/eventschain.jpg)

### Parameters ###

| name| description|
| -------- | -------- |
|  event | event which will be generated |
|  initial event | event essential for script activation |
|  wait event timeout | maximal time of script fulfillment |

When you press ![changechain](events-chain.assets/changechain.jpg) the menu window pops up, you can add new event by pressing "Add":

![eventschainadd](events-chain.assets/eventschainadd.jpg)

Swap the events -  "Down", "Up" or delete -  "Remove"

![eventschaindown](events-chain.assets/eventschaindown.jpg)

Generated events will be performed in sequence "from top to bottom".

 


## activity timeout generator ##

The script gives a possibility to generate the chain of events when the card breaks off its activity and generates a particular event when the particular time period expires. If event generating is interrupted by a call, it can be completed or event will be generated after it is completed.

Given script performs in tandem with activity timeout by event script from Session/Activity Period Script group.

![activity_timeout_generator](activity-timeout-generator.assets/activity_timeout_generator.png)

### Parameters ###

| name| description|
| -------- | -------- |
|  event | event to request timer generating |
|  event after timeout activity passed | event which is generated after timeout |
|  event to generate activity timeout | event which generates timer |
|  drop call | drop the call or wait till the call is over |

### activity timeout by event ###

The script refers to Session/Activity Period Script group, gives a possibility to specify the wait timer.

![activity-timeout-period-script](activity-timeout-generator.assets/activity-timeout-period-script.png)

### Parameters ###

| name| description|
| -------- | -------- |
|  activity_timeout | event which generates timer |

 


## events on activation ##

Given script enables to generate several events sequentially on activation of SIM card

![eventsonactivation](events-on-activation.assets/eventsonactivation.jpg)

| name| description|
| -------- | -------- |
|  event | generated event (can be several) |
|  wait event timeout | time required for performance of events |
|  lock on failure | block the card in case of failure |


## generate event after sms ##

Given script is designed to perform any action after definite amount of outcoming SMS.

![g_e_after_sms](generate-event-after-sms.assets/g_e_after_sms.png)

### Parameters ###

| name| description|
| -------- | -------- |
|  **event** | event, that must be performed |
|  **sms limit** | the amount of outcoming calls from the period, after which the  **event** will be performed |


## check SMS ##

This script enables to process the incoming SMS and generate the events on assigned samples. There is a possibility to create several samples for inbound SMS processing.

![check-sms](check-sms.assets/check-sms.png)

### Parameters ###

| name| description|
| -------- | -------- |
|  start first | on selection of this option the event only for the first SMS concurrence with the sample will be generated |
|  pattern for SMS/event for generate | regular expression to analyze the incoming SMS and generate the assigned event |

### pattern for SMS/event for generate set-up ###

![pattern](check-sms.assets/pattern.png)

### Parameters ###

| name| description|
| -------- | -------- |
|  Pattern | regular expression to check the incoming SMS |
|  Event | event which must be performed on concurrence of incoming SMS with the sample |

 


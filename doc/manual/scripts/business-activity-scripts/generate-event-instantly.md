## generate event instantly ##

Given script is designed for any action when the activity starts and repeats it particular times.

![g_e_instantly](generate-event-instantly.assets/g_e_instantly.png)

### Parameters ###

| name| description|
| -------- | -------- |
|  **event  ** | event, which must be performed |
|  **event limit** | amount of events, which will be generated for the activity period |
|  **lock on finish** | block the card on successful script performance  |
|  **lock on failed** | block the card on successful script performance |
|  **timeout** | time to fulfill the **event** |


## check balance ##

The task of given script is to check the balance and generate the events in accordance with the state of balance.

![check_balance](check-balance.assets/check_balance.png)

### Parameters ###

| name| description|
| -------- | -------- |
|  event | event which is essential for script activation |
|  balance check data | data essential for script to send USSD and analyze the reply|
|  minimum amount of money | needed amount of money on balance |
|  event on less | event, which will be generated, if money on balance is less than index in minimum amount of money field |
|  event on more | event, which will be generated, if money on balance exceed or equals the index in minimum amount of money field |
|  fail on less | block the card on failure |
|  alternative ussd responses | alternative expression to check USSD response |
|  parse alternative ussd responses | if an alternative expression of USSD response needs checking|

### bill check data set-up ###

![bill_check_data](check-balance.assets/bill_check_data.png)

In this case, ussd field should be filled with balance check number, the second space is filled with regular expression, assigned for withdrawal of the remaining amount of money from USSD response.

The example of more universal expression:

```
(?s).*{part of expression before amount } (-?(\d+\.\d+)|(\d+)) {part of expression after amount}.*
```

Given expression takes negative or zero balance into account and the next line symbol  - in the reply.


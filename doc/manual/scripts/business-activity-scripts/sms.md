## SMS ##

Sends SMS onto generated number.

**SMS** (Short Message Service) - technology, which allows to perform receiving and sending of short messages. Usually, this messages are delivered in several seconds.

You can send messages onto  offline or switched off phone. As soon as addressee becomes available in the network, he will receive the message. You can send a message to an addressee, who is talking at that moment.

![sms](sms.assets/sms.png)

### Parameters ###

| name| description|
| -------- | -------- |
|  attempts account | quantity of attempts in the case of unsuccessful performance |
|  event | event to activate the script |
|  skip errors | if set up, then all errors on sending SMS will be ignored |
|  interval between attempts | interval of time between attempts in the case of error |
|  number pattern | regular expression, which defines  numbers where sms was sent |
|  lock on fail | need of locking card in case of unsuccessful performance |
|  empty text | to send empty sms |
|  sms count | amount of sms to send |
|  interval between sms | interval of time between successfully sent sms |



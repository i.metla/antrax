## generate event on asr or acd limit ##

Given script is designed to perform any action when ASR and/or ACD stated parameters are obtained.

![g_e_on_asr_or_acd](generate-event-on-asr-or-acd-limit.assets/g_e_on_asr_or_acd.png)

### Parameters ###

| name| description|
| -------- | -------- |
|  **end time  ** | due time of ASR and/or ACD analysis |
|  **period** | time period which ASR and/or ACD are analyzed for |
|  **event** | evet generated when ASR and/or ACD limits are reached  |
|  **begin time** | time when ASR and/or ACD analysis begin |
|  **acd** |ACD limit, which **event** is generated on |
|  **asr** |ASR limit, which **event**  is generated on|
|  **skip zero calls** | zero calls are skipped for ASR and /or ACD analysis |
|  **min calls** | minimal amount of calls for ASR and/or ACD analysis |


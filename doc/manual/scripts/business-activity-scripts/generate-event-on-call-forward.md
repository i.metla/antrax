## generate event on call forward ##

Given script is designed for any action on detection of call forwarding.

![g_e_on_call_forward](generate-event-on-call-forward.assets/g_e_on_call_forward.png)

### Parameters ###

| name| description|
| -------- | -------- |
|  **event  ** | event, which requires launching when call forwarding is detected |


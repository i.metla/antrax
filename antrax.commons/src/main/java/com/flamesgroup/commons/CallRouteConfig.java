/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.commons;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

public class CallRouteConfig implements Serializable {

  private static final long serialVersionUID = -7162963949680863893L;

  private final String calledRegex;
  private final int priority;
  private final List<Direction> directions;

  public CallRouteConfig(final String calledRegex, final int priority, final List<Direction> directions) {
    Objects.requireNonNull(calledRegex, "calledRegex mustn't be null");
    Objects.requireNonNull(directions, "directions mustn't be null");

    this.calledRegex = calledRegex;
    this.priority = priority;
    this.directions = directions;
  }

  public String getCalledRegex() {
    return calledRegex;
  }

  public int getPriority() {
    return priority;
  }

  public List<Direction> getDirections() {
    return directions;
  }

  @Override
  public boolean equals(final Object object) {
    if (this == object) {
      return true;
    }
    if (!(object instanceof CallRouteConfig)) {
      return false;
    }
    CallRouteConfig that = (CallRouteConfig) object;

    return priority == that.priority
        && calledRegex.equals(that.calledRegex)
        && directions.equals(that.directions);
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = calledRegex.hashCode();
    result = prime * result + priority;
    result = prime * result + directions.hashCode();
    return result;
  }

}

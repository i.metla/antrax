/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.commons;

import java.util.regex.Pattern;

public class SNRPattern extends BasePattern {

  private static final long serialVersionUID = 2748253018066067198L;

  private static final String REGEX_TEMPLATE = "((\\d)|(X)|(\\(N\\d+[+-]\\d\\))){6,42}";

  private static final Pattern regexPattern = Pattern.compile(REGEX_TEMPLATE);


  public SNRPattern(final String pattern) {
    super(pattern);
  }

  @Override
  protected Pattern getRegexPattern() {
    return regexPattern;
  }

}

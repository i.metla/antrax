/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.rmi;

import java.rmi.server.RMIClientSocketFactory;
import java.util.Objects;

public final class RemoteAccessor<T> {

  private final String serviceUrl;
  private final Class<T> serviceInterface;

  private final boolean lookupStubOnStartup;
  private final boolean cacheStub;
  private final boolean refreshStubOnConnectFailure;

  private final RMIClientSocketFactory registryClientSocketFactory;

  public static class Builder<T> {

    private final String serviceUrl;
    private final Class<T> serviceInterface;

    private boolean lookupStubOnStartup = false;
    private boolean cacheStub = true;
    private boolean refreshStubOnConnectFailure = true;

    private RMIClientSocketFactory registryClientSocketFactory;

    public Builder(final String serviceUrl, final Class<T> serviceInterface) {
      Objects.requireNonNull(serviceUrl, "'serviceUrl' is required");
      Objects.requireNonNull(serviceInterface, "'serviceInterface' is required");
      if (!serviceInterface.isInterface()) {
        throw new IllegalArgumentException("serviceInterface [" + serviceInterface + "] must be an interface");
      }

      this.serviceUrl = serviceUrl;
      this.serviceInterface = serviceInterface;
    }

    public Builder(final String host, final int port, final Class<T> serviceInterface) {
      this(host, port, serviceInterface, serviceInterface.getSimpleName());
    }

    public Builder(final String host, final int port, final Class<T> serviceInterface, final String serviceName) {
      this(String.format("rmi://%s:%d/%s", host, port, serviceName), serviceInterface);
    }

    public Builder<T> setLookupStubOnStartup(final boolean lookupStubOnStartup) {
      this.lookupStubOnStartup = lookupStubOnStartup;
      return this;
    }

    public Builder<T> setCacheStub(final boolean cacheStub) {
      this.cacheStub = cacheStub;
      return this;
    }

    public Builder<T> setRefreshStubOnConnectFailure(final boolean refreshStubOnConnectFailure) {
      this.refreshStubOnConnectFailure = refreshStubOnConnectFailure;
      return this;
    }

    public Builder<T> setRegistryClientSocketFactory(final RMIClientSocketFactory registryClientSocketFactory) {
      this.registryClientSocketFactory = registryClientSocketFactory;
      return this;
    }

    public RemoteAccessor<T> build() {
      return new RemoteAccessor<>(this);
    }

  }

  private RemoteAccessor(final Builder<T> builder) {
    this.serviceUrl = builder.serviceUrl;
    this.serviceInterface = builder.serviceInterface;
    this.lookupStubOnStartup = builder.lookupStubOnStartup;
    this.cacheStub = builder.cacheStub;
    this.refreshStubOnConnectFailure = builder.refreshStubOnConnectFailure;
    this.registryClientSocketFactory = builder.registryClientSocketFactory;
  }

  public String getServiceUrl() {
    return serviceUrl;
  }

  public Class<T> getServiceInterface() {
    return serviceInterface;
  }

  public boolean isLookupStubOnStartup() {
    return lookupStubOnStartup;
  }

  public boolean isCacheStub() {
    return cacheStub;
  }

  public boolean isRefreshStubOnConnectFailure() {
    return refreshStubOnConnectFailure;
  }

  public RMIClientSocketFactory getRegistryClientSocketFactory() {
    return registryClientSocketFactory;
  }

}

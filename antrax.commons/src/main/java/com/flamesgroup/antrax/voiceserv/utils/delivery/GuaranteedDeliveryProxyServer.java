/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.voiceserv.utils.delivery;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * Proxy Server which guarantee delivery when RMI call failed because of network
 * problems
 * <p>
 * This server should be started first
 */
public class GuaranteedDeliveryProxyServer {

  private final LinkedList<ExecutionCommand> queue = new LinkedList<>();
  private DeliveryThread thread;
  private final String name;
  private final List<Class<? extends Throwable>> ioExceptions;
  private volatile boolean started = false;

  public GuaranteedDeliveryProxyServer(final String name, final Set<Class<? extends Throwable>> ioExceptions) {
    this.name = name;
    this.ioExceptions = new ArrayList<>(ioExceptions);
  }

  public GuaranteedDeliveryProxyServer(final String name, final Class<? extends Throwable> exception) {
    this(name, wrap(exception));
  }

  private static Set<Class<? extends Throwable>> wrap(final Class<? extends Throwable> exception) {
    HashSet<Class<? extends Throwable>> retval = new HashSet<>();
    retval.add(exception);
    return retval;
  }

  public synchronized void start() {
    if (isStarted()) {
      throw new IllegalStateException("delivery server is already started");
    }
    started = true;
    thread = new DeliveryThread(name, queue, ioExceptions);
    thread.start();
  }

  public synchronized void terminate() {
    if (!isStarted()) {
      throw new IllegalStateException("DeliveryServer is not started");
    }
    started = false;
    thread.interrupt();
  }

  public boolean isStarted() {
    return this.started;
  }

  public <T> T proxy(final T delegate, final Class<T> clazz, final DeliveryConfig deliveryConfig) {
    return clazz.cast(Proxy.newProxyInstance(clazz.getClassLoader(), new Class<?>[] {clazz}, new ThisInvocationHandler(delegate, deliveryConfig)));
  }

  public class ThisInvocationHandler implements InvocationHandler {

    private final Object delegate;
    private final DeliveryConfig deliveryConfig;

    public ThisInvocationHandler(final Object delegate, final DeliveryConfig deliveryConfig) {
      this.delegate = delegate;
      this.deliveryConfig = deliveryConfig;
    }

    @Override
    public Object invoke(final Object proxy, final Method method, final Object[] args) throws Throwable {
      if (!isStarted()) {
        throw new IllegalStateException("DeliveryServer is not started");
      }
      if (!deliveryConfig.isGuaranteed(method) || queue.size() > 10000) {
        try {
          return method.invoke(delegate, args);
        } catch (Exception e) {
          if (deliveryConfig.isExceptionless(method)) {
            return deliveryConfig.defaultValue(method);
          } else {
            if (e instanceof InvocationTargetException) {
              throw ((InvocationTargetException) e).getCause();
            }
            throw e;
          }
        }
      } else {
        ExecutionHandlerImpl handler = new ExecutionHandlerImpl();
        synchronized (queue) {
          queue.add(new ExecutionCommand(delegate, method, args, handler, deliveryConfig));
          queue.notifyAll();
        }
        if (deliveryConfig.isAsync(method)) {
          return deliveryConfig.defaultValue(method);
        } else {
          return handler.getResult();
        }
      }
    }
  }
}

/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;

public class PropUtils implements IReloadListener {

  private final Logger logger = LoggerFactory.getLogger(getClass());

  private static volatile PropUtils instance;

  protected static final String COMMONS_PROPERTY = "commons.properties";

  protected final Map<String, ServerProperties> properties;

  protected PropUtils() {
    properties = getProperties();
    reload();
  }

  public CommonsProperties getCommonsProperties() {
    return (CommonsProperties) properties.get(COMMONS_PROPERTY);
  }

  public static PropUtils getInstance() {
    if (instance == null) {
      synchronized (PropUtils.class) {
        if (instance == null) {
          instance = new PropUtils();
        }
      }
    }
    return instance;
  }

  protected Map<String, ServerProperties> getProperties() {
    Map<String, ServerProperties> localProperties = new HashMap<>();
    localProperties.put(COMMONS_PROPERTY, new CommonsProperties());
    return localProperties;
  }

  private Properties getPropertiesFromFile(final String propertyFile) {
    Objects.requireNonNull(propertyFile, "propertyFile mustn't be null");
    InputStream propertyResourceStream = getClass().getClassLoader().getResourceAsStream(propertyFile);

    try {
      Properties props = new Properties();
      props.load(propertyResourceStream);
      return props;
    } catch (IOException e) {
      throw new RuntimeException("Failed to load properties", e);
    }
  }

  @Override
  public String getName() {
    return COMMONS_PROPERTY;
  }

  @Override
  public boolean reload() {
    for (Map.Entry<String, ServerProperties> prop : properties.entrySet()) {
      logger.debug("[{}] - try to reload properties [{}]", this, prop.getKey());
      prop.getValue().load(getPropertiesFromFile(prop.getKey()));
      logger.debug("[{}] - reloaded properties [{}]", this, prop.getKey());
    }
    return true;
  }

}
